		<!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<?php $i=0; foreach($sliders as $slide){?>
			<li data-target="#myCarousel" data-slide-to="<?=$i?>" class="<?php if($i == 1){ echo 'active';} ?>"></li>
			<?php $i++; } ?>
		</ol>
		<div class="carousel-inner" role="listbox">
			<?php $i=0; foreach($sliders as $slide){?>
			<div class="item <?php if($i == 1){ echo 'active';} ?>">
				<img src="<?=base_url();?><?=$slide->image_path?>" class="img-responsive"/>
			</div>
			<?php $i++; } ?>
		</div>
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
		<!-- The Modal -->
    </div> 
	<!-- //banner -->	
	
	<!-- New-arrivals -->
	<div class="banner-bootom-w3-agileits">
		<div class="w3_agile_latest_arrivals">
			<div class="container">
				<h3 class="wthree_text_info"><hr /><span>New Arrivals</span></h3>
				<p style="text-align:center; padding:10px 30px;margin: -20px 0 25px 0;">
					To give you a great headstart, we have listed below trending and latest products that you can consider buying.
				</p>
				<div class="row">
					<div class="col-md-12">
						<div class="carousel slide multi-item-carousel" id="theCarousel">
							<div class="carousel-inner">
								<div class="item active">
									<?php $count=0; foreach($new_arrivals_cat2 as $new){$count++;?>
										<div class="col-md-3 product-men single">
											<div class="men-pro-item simpleCart_shelfItem">
												<div class="men-thumb-item">
													<?php
														$images =  explode(',', $new->prod_image_urls);
														$counter3 = 0;
														foreach ($images as $img) {
															if($counter3 == 1){
																
																echo '<img src="'.IMAGEBASEPATH.$new->prod_image_url.'" alt="" class="';
																echo 'pro-image-front" />';
															}else{
																echo '<img src="'.IMAGEBASEPATH.$img.'" alt="" class="';
																echo 'pro-image-back" />';
															}
															$counter3++;
															if($counter3 == 2){break;}
														}
													?>	
													<!--
													<div class="men-cart-pro">
														<div class="inner-men-cart-pro">
															<a href="<?=site_url();?>/website_home/product_details/<?=$new->fk_pcat_id?>/<?=$new->fk_pchild_id?>/<?=$new->prod_id?>" class="link-product-add-cart">Quick View</a>
														</div>
													</div>	
													-->													
												</div>
												<div class="item-info-product ">
													<h4 style="height: 22px;overflow: hidden;"><a href="#single.html"><?=$new->prod_name;?></a></h4>
													<div class="info-product-price">
														<span class="item_price"><i class="fa fa-inr"></i> <?=$new->prod_price;?></span>
														<del><i class="fa fa-inr"></i> <?=$new->prod_mrp;?></del>
													</div>
													<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
														<input type="button" onclick="window.location = '<?=site_url();?>/website_home/product_details/<?=$new->fk_pcat_id?>/<?=$new->fk_pchild_id?>/<?=$new->prod_id?>/';" value="View" class="button" />
													</div>
													<hr style="margin-top: -32px;" />
												</div>
											</div>
										</div>
										<?php if($count == 4){?>
												<div class="clearfix"> </div>
											</div>
											<div class="item">
										<?php }?>
									<?php }?>
								</div>
								<!--  Example item end -->
							</div>
							<a style="width:35px;margin-left: -25px;" class="left carousel-control" href="#theCarousel" data-slide="prev"><i style="color:black" class="glyphicon glyphicon-chevron-left"></i></a>
							<a style="width:35px;margin-right: -25px;" class="right carousel-control" href="#theCarousel" data-slide="next"><i style="color:black" class="glyphicon glyphicon-chevron-right"></i></a>
						</div>
					</div>
				</div>
				<!--//slider_owl-->
			</div>
		</div>
	</div>
	<!-- //New-arrivals -->
		
	<div class="banner-bootom-w3-agileits">
		<div class="container">
			<!---728x90--->
			<h3 class="wthree_text_info"><hr /><span>Browse Our Categories</span></h3>
			<br /> 
			<div class="col-md-6 bb-grids bb-left-agileits-w3layouts" onmouseout="graycolor('one');" onmouseover="colored('one');" >
				<a href="<?=site_url();?>/website_home/product_list/<?=$parent_cats[0]['parent_cat_id']?>">
				   <div class="bb-left-agileits-w3layouts-inner grid">
						<figure class="effect-roxy">
							<img src="<?=IMAGEBASEPATH.$parent_cats[0]['parent_cat_img_url']?>" id="one" class="img-responsive">
							<figcaption>
								<h3><?=$parent_cats[0]['parent_cat_name']?></h3>
								<p>Upto 55%</p>
							</figcaption>			
						</figure>
					</div>
				</a>
			</div>
			<div class="col-md-6 bb-grids bb-middle-agileits-w3layouts" onmouseout="graycolor('two');" onmouseover="colored('two');" >
			   <div class="bb-middle-agileits-w3layouts grid">
				   <figure class="effect-roxy">
						<img src="<?=base_url();?>website_assets/images/bottom3.jpg" id="two" class="img-responsive">
						<figcaption>
							<h3><?php if(isset($parent_cats[1]['parent_cat_name'])){ echo $parent_cats[1]['parent_cat_name'];}?></h3>
							<p>Upto 55%</p>
						</figcaption>			
					</figure>
				</div>
				<div class="bb-middle-agileits-w3layouts forth grid" onmouseout="graycolor('three');" onmouseover="colored('three');" >
					<figure class="effect-roxy">
						<img src="<?=base_url();?>website_assets/images/bottom4.jpg" id="three" class="img-responsive">
						<figcaption>
							<h3><?php if(isset($parent_cats[2]['parent_cat_name'])){ echo $parent_cats[2]['parent_cat_name'];}?></h3>
							<p>Upto 65%</p>
						</figcaption>		
					</figure>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- schedule-bottom -->
	
	<div class="schedule-bottom">
		<div class="col-md-5 agileinfo_schedule_bottom_left">
			<img src="<?=base_url();?>/website_assets/images/mid.jpg" alt=" " class="img-responsive" />
		</div>
		<div class="col-md-7 agileits_schedule_bottom_right">
			<div class="w3ls_schedule_bottom_right_grid">
				<h3>Save up to <span>50%</span> in this week</h3>
				<p>Suspendisse varius turpis efficitur erat laoreet dapibus. 
					Mauris sollicitudin scelerisque commodo.Nunc dapibus mauris sed metus finibus posuere.</p>
				<div class="col-md-4 w3l_schedule_bottom_right_grid1">
					<i class="fa fa-user-o" aria-hidden="true"></i>
					<h4>Customers</h4>
					<h5 class="counter">653</h5>
				</div>
				<div class="col-md-4 w3l_schedule_bottom_right_grid1">
					<i class="fa fa-calendar-o" aria-hidden="true"></i>
					<h4>Events</h4>
					<h5 class="counter">823</h5>
				</div>
				<div class="col-md-4 w3l_schedule_bottom_right_grid1">
					<i class="fa fa-shield" aria-hidden="true"></i>
					<h4>Awards</h4>
					<h5 class="counter">45</h5>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
	<!-- //schedule-bottom -->
	
	<!-- New-arrivals -->
	<div class="banner-bootom-w3-agileits">
		<div class="w3_agile_latest_arrivals">
			<div class="container">
				<div class="row"> 
					<h3 class="wthree_text_info"><hr /><span>Deals Of The Day</span></h3>
					<div class="banner_bottom_agile_info_inner_w3ls">
						<div class="carousel slide multi-item-carousel" id="theCarousel2">
							<div class="carousel-inner">
								<div class="item active">
									<?php $i=0; foreach($deals_of_the_day as $deal){$i++; ?>
										<div class="col-md-3 grid customgrids">
											<a href="<?=site_url();?>/website_home/product_details/<?=$deal->pcat_id?>/<?=$deal->ccat_id?>/<?=$deal->prod_id?>">
												<img src="<?=IMAGEBASEPATH.$deal->deal_image_url?>" style="width:100%;" class="img-responsive">
											</a>
										</div>
										<?php if($i == 3){ $i==0;?>
												<div class="clearfix"> </div>
											</div>
											<div class="item">
										<?php }?>
									<?php }?>
								</div>
								<!--  Example item end -->
							</div>
							<a style="width:35px;margin-left: -25px;" class="left carousel-control" href="#theCarousel2" data-slide="prev"><i style="color:black" class="glyphicon glyphicon-chevron-left"></i></a>
							<a style="width:35px;margin-right: -25px;" class="right carousel-control" href="#theCarousel2" data-slide="next"><i style="color:black" class="glyphicon glyphicon-chevron-right"></i></a>
						</div>
						<div class="clearfix"></div>
						<br />
						<hr />
					</div>
					<style>
						.customgrids{width:33.3%}
						@media(max-width:768px){.customgrids{width:100%}}
					</style>
					<!--//slider_owl-->
				</div>
			</div>
		</div>
	</div>
	<!-- //New-arrivals -->
	
	<div class="coupons">
		<div class="coupons-grids text-center">
			<div class="w3layouts_mail_grid">
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<!--<i class="fa fa-truck" aria-hidden="true"></i>-->
						<img src="<?=base_url();?>/website_assets/images/icons/Delivery.png" style="width:100%; padding:12px;" />
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>ON TIME DELIVERY</h3>
					</div>
				</div>
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<!--<i class="fa fa-gift" aria-hidden="true"></i>-->
						<img src="<?=base_url();?>/website_assets/images/icons/Quality.png" style="width:100%; padding:12px;" />
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>Quality</h3>
					</div>
				</div>
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<!--<i class="fa fa-shopping-bag" aria-hidden="true"></i>-->
						<img src="<?=base_url();?>/website_assets/images/icons/Return-exchange.png" style="width:100%; padding:12px;" />
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>Return and exchnage</h3>
					</div>
				</div>
					<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<img src="<?=base_url();?>/website_assets/images/icons/Support.png" style="width:100%; padding:12px;" />
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>Support</h3>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
</div>