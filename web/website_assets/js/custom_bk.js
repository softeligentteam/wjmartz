var validation_const = 'true';
// For grayscale to colored image effect.
function colored(id){
	$('#'+id).css('filter','none');
	$('#'+id).css('-webkit-filter','grayscale(0%)');
}
function graycolor(id){
	$('#'+id).css('filter','100%');
	$('#'+id).css('-webkit-filter','grayscale(100%)');
}

//User registration form validation.
function validateRegForm(){
	var inputData = {
		'name' : $('#reg_name').val(),
		'mobile' : $('#reg_mobile').val(),
		'email' : $('#reg_email').val(),
		'password' : $('#reg_password').val()
	};
	if(inputData['name'].match(/^[a-zA-Z ]+$/) == null){
		validation_const = 'false';
		$('#err_reg_name').text('Please enter proper name. Only alphabets are allowed.');
	}
	if(inputData['email'].match(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i) == null){
		validation_const = 'false';
		$('#err_reg_email').text('Please enter a proper email id.');
	}
	if(inputData['mobile'].match(/^[789]\d{9}$/) == null){
		validation_const = 'false';
		$('#err_reg_mobile').text('Please enter a proper mobile number.');
	}
	if(inputData['password'] == null || inputData['password'] == '' ){
		validation_const = 'false';
		$('#err_reg_password').text('Password field can not be blank.');
	}	
	return inputData;
}
function validateMobileNo(number){
	var targetUrl = siteURL+'user_registration/check_for_mobile_number_is_unique';
	$.ajax ({
		url : targetUrl, 
		type: 'POST', 
		data: {'number' : number}, 
		cache: false,
		success: function(resp)
		{ 
			if(resp != '0'){
				validation_const = 'false';
			}
		} 
	}); 
	return validation_const;
}

//Ajax calls for new user registration.
$('#throwOTP').click(function(){
	$('.err_').text(' ');
	var data = validateRegForm();
	validateMobileNo(data['mobile']);
	if(validation_const == true){
		var target_url = siteURL+'user_registration/set_reg_user_temp_session_data_and_send_otp';
		$.ajax ({
			url : target_url, 
			type: 'POST', 
			data: data, 
			cache: false, 
			beforeSend: function()
			{
				$('#throwOTP').hide();
			},
			success: function(resp)
			{ 
				alert(validation_const)
				if(resp != 'true'){
					alert('Something went wrong, please try once again by refreshing this page');
				}else{
					if(validation_const == 'true'){ 
						alert();
					}else{
						$('#reg_otp_verification').slideDown();
						$('#reg_details').slideUp();
					}
				}
			} 
		}); 
		/*return false; */
	}
});

$('#change_number').click(function(){
	$('#throwOTP').show();
	$('#reg_details').slideDown();
	$('#reg_otp_verification').slideUp();
});
function updateLabel()
{
	var send_otp_on_mobile_no = $('#reg_mobile').val();
	$('#regMobileNo').text(send_otp_on_mobile_no);
}