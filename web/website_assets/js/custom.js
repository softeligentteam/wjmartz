var passstatus=true;
// For grayscale to colored image effect.
function colored(id){
	$('#'+id).css('filter','none');
	$('#'+id).css('-webkit-filter','grayscale(0%)');
}
function graycolor(id){
	$('#'+id).css('filter','100%');
	$('#'+id).css('-webkit-filter','grayscale(100%)');
}

//User Login validation.
function validateLogin(){
	var LoginValidated = true;
	var loginDetails = {
		'mobile' : $('#mobNo').val(),
		'password' : $('#pass').val()
	}; 

	if(loginDetails['mobile'].match(/^[789]\d{9}$/) === null){
		LoginValidated = false;
		$('#err_mobNo').text('Please enter a proper mobile number.');
	}
	else{		
		$('#err_mobNo').text(' ');
		$.ajax ({
			async: false,
			url : siteURL+'LoginController/check_mobile_for_login', 
			type: 'POST', 
			data: {'mobile' : loginDetails['mobile']}, 
			cache: false,
			success: function(resp)
			{
				resp = $.trim(resp);
				if(resp == '0'){
					$('#err_mobNo').text('This mobile no. is not registered.');
					LoginValidated =false; 
				}else{
					LoginValidated =true; 
					$('#err_mobNo').text(' ');
					
				    //alert(loginDetails['mobile']);
					//alert(loginDetails['password']);
					
					$.ajax ({
						async: false,
						url : siteURL+'LoginController/check_password_for_login ', 
						type: 'POST', 
						data: {'mobile' : loginDetails['mobile'],'password' : loginDetails['password']}, 
						cache: false,
						success: function(resp)
						{
						    resp = $.trim(resp);
							if(resp == '0'){
								$('#err_pass').text('Password mismatched.');
								LoginValidated = false;
							}
							else{
								$('#err_pass').text('');
								LoginValidated = true;
							}
						},
						error: function(){
							LoginValidated =false;
							alert('Something went wrong, please try again.');
							$('#err_mobNo').text(' ');
						}
					});
				}
			},
			error: function(){
			    LoginValidated =false;
				alert('Something went wrong, please try again.');
				$('#err_mobNo').text(' ');
			}
			
		});
	}
	return LoginValidated;
}

//User registration form validation.
function validateRegForm(){
	var inputData = {
		'fname' : $('#reg_fname').val(),
		'lname' : $('#reg_lname').val(),
		'mobile' : $('#reg_mobile').val(),
		'email' : $('#reg_email').val(),
		'password' : $('#reg_password').val(),
		'validation' : true
	};
	if(inputData['fname'].match(/^[a-zA-Z ]+$/) === null){
		inputData['validation'] = false;
		$('#err_reg_fname').text('Please enter proper first name. Only alphabets are allowed.');
	}
	if(inputData['lname'].match(/^[a-zA-Z ]+$/) === null){
		inputData['validation'] = false;
		$('#err_reg_lname').text('Please enter proper last name. Only alphabets are allowed.');
	}
	if(inputData['mobile'].match(/^[789]\d{9}$/) === null){
		inputData['validation'] = false;
		$('#err_reg_mobile').text('Please enter a proper mobile number.');
	}
	if(inputData['email'].match(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i) === null){
		inputData['validation'] = false;
		$('#err_reg_email').text('Please enter a proper email id.');
	}
	if(inputData['password'] === null || inputData['password'] === '' ){
		inputData['validation'] = false;
		$('#err_reg_password').text('Password field can not be blank.');
	}
	if(passstatus === false){
		$('#err_reg_password').text("*Password Length : Min:6; Max:15 characters.");
		inputData['validation'] = false;
	}	
	return inputData;
}

//Ajax calls for new user registration.
$('#throwOTP').click(function(){

	$('.err_').text(' ');
	var data = validateRegForm();
	var targetUrl = siteURL+'user_registration/check_for_mobile_number_is_unique';
	$.ajax ({
		url : targetUrl, 
		type: 'POST', 
		data: {'number' : $('#reg_mobile').val()}, 
		cache: false,
		success: function(resp)
		{
			resp = $.trim(resp);
		
			if(resp != '0'){
				data['validation'] = false;
				$('#err_reg_mobile').text('This mobile number is already in use, please try another.');
			}
			
			var urlTarget = siteURL+'user_registration/check_for_email_is_unique';
			$.ajax ({
				url : urlTarget, 
				type: 'POST', 
				data: {'email' : $('#reg_email').val()}, 
				cache: false,
				success: function(resp)
				{ 
					resp = $.trim(resp);
					if(resp != '0'){
						data['validation'] = false;
						$('#err_reg_email').text('This email id is already in use, please try another.');
					}else{
						if(data['validation'] === true){
							var target_url = siteURL+'user_registration/set_reg_user_temp_session_data_and_send_otp';
							$.ajax ({
								url : target_url, 
								type: 'POST', 
								data: data, 
								cache: false, 
								beforeSend: function()
								{
									$('#throwOTP').hide();
								},
								success: function(resp)
								{
									resp = $.trim(resp);
									if(resp != 'true'){
										alert('Something went wrong, please try once again by refreshing this page');
									}else{
										// $('#reg_otp_verification').slideDown();
										// $('#reg_details').slideUp();
										var ut = siteURL+'user_registration/set_otp_in_temp_session';
										$.ajax ({
											url : ut, 
											type: 'POST', 
											data: {'mobile' : $('#reg_mobile').val()}, 
											cache: false,
											success: function(resp)
											{ 
												resp = $.trim(resp);
												if(resp != 'true'){
													alert('Something went wrong, please try again!');
												}else{
													$('#reg_otp_verification').slideDown();
													$('#reg_details').slideUp();
												}
											} 
										});
									}
								}
							});
						}
					}
				} 
			});		
		}
	});
});

function changeRegDetails(){
	$('#throwOTP').show();
	$('#reg_details').slideDown();
	$('#reg_otp_verification').slideUp();
	$('#reg_address').slideUp();	
	$('#regOtp').val(' ');	
}

function updateLabel()
{
	var send_otp_on_mobile_no = $('#reg_mobile').val();
	$('#regMobileNo').text(send_otp_on_mobile_no);
}

function verifyOtp()
{
	var otp = $('#regOtp').val();
	$.ajax ({
		url : siteURL+'user_registration/verify_temp_session_otp', 
		type: 'POST',
		data: {'otp' : otp}, 
		cache: false,
		success: function(resp)
		{
			resp = $.trim(resp);
			if(resp != 'true'){
				alert('OTP Does not match, please try again');
			}else{
				$('#reg_otp_verification').slideUp();
				$('#reg_address').slideDown();
			}
		}
	});
}

/*Changes made by PJ on 13/04/2018*/
/* function pincodeEntered()
{
    var pincode = $('#regPincode').val();
    $.ajax ({
        url : siteURL+'user_registration/get_address_details_using_pincode', 
        type: 'POST',
        data: {'pincode' : pincode}, 
        cache: false,
        success: function(resp)
        {
        	resp = $.trim(resp);
			if(resp == 'false' || resp == 'null'){
                $('#err_pincode').html('Can not find pincode '+pincode+', please try again.');
                $('#regPincodeId').val('');
                $('#regState').val('');
                $('#regStateId').val('');
                $('#regCity').val('');
                $('#regCityId').val('');
            }else{
                $('#err_pincode').html('');
                resp = JSON.parse(resp);
                $('#regPincodeId').val(resp.pincode_id);
                $('#regState').val(resp.state);
                $('#regStateId').val(resp.state_id);
                $('#regCity').val(resp.city);
                $('#regCityId').val(resp.city_id);
                $('#regArea').html('');
                  $('#regArea').append($("<option/>", {
                      value: '',
                      text: 'Select Area'
                  }));
                $.each(resp.areas, function(index, area){
                  $('#regArea').append($('<option/>', {
                      value: area.area_id,
                      text: area.area_name
                  }));
               });
            }
        }
    });
} */

/* Update User Profile */
function validateUpdateProfileForm(id){
	var valu = $('#'+id).val();
	var ret = true;
	// alert(valu);
	if(id == 'cname' && valu.match(/^[a-zA-Z ]+$/) === null){
		ret = false;
		$('#errCname').html('Please enter proper name. Only alphabets are allowed.');
	}
	if(id == 'email' && valu.match(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i) === null){
		ret = false;
		$('#errCemail').html('Please enter a proper email id.');
	}
	if(id == 'cnumber'){
		// alert($('#cotp').val());
		$.ajax ({
			async: false,
			url : siteURL+'user_registration/verify_temp_session_otp', 
			type: 'POST', 
			data: {'otp' : $('#cotp').val()},
			cache: false,
			success: function(resp)
			{
				resp = $.trim(resp);
				if(resp){
					if(resp != 'true'){
						ret = false;
						$('#errCmobile').html('OTP does not match, please enter correct OTP sent on your new mobile no :'+$('#cnumber').val());
						// alert('asdasdasdsd');
					}
				}
			}
		});
		return ret;	
	}
	return ret;
}

$('#close_mobile').click(function(){
	$('#checkOtp').hide();
	$('#cotp').hide();
	$('#updtNum').hide();
	var disp_mobile = $('#disp_mobile').text();
	$('#cnumber').val(disp_mobile);
	$('#cnumber').show();
	$('#check').prop('disabled',false);
	$('#check').show();
});

function verify_number(valu)
{
	//var valu = $('#cnumber').val();
	if(valu.match(/^[789]\d{9}$/) === null){
		$('#errCmobile').html('Please enter a proper mobile number.');
		$('#errCmobilem').html('Please enter a proper mobile number.');
	}else{
		$.ajax ({
			async: false,
			url : siteURL+'user_registration/check_for_mobile_number_is_unique', 
			type: 'POST', 
			data: {'number' : valu}, 
			cache: false,
			beforeSend: function(){
				$('#errCmobile').html(' ');
				$('#check').attr('disabled',true);
			},
			success: function(resp)
			{ 
				resp = $.trim(resp);
				if(resp != '0'){
					$('#errCmobile').html('This mobile no. is already in use, please try another.');
					$('#errCmobilem').html('This mobile no. is already in use, please try another.');
					$('#check').attr('disabled',false);
				}else{
					$.ajax ({
						async: false,
						url : siteURL+'user_registration/set_otp_in_temp_session', 
						type: 'POST', 
						data: {'mobile' : valu},
						cache: false,
						beforeSend: function(){
						},
						success: function(resp)
						{
							resp = $.trim(resp);
							if(resp == 'true'){
								$('#errCmobile').html(' ');
								$('#check').hide();
								$('#updtNum').show();
								$('#cnumber').hide();
								$('#cotp').prop('placeholder','Enter otp sent to the new number : '+valu);
								$('#cotp').slideDown();
								
								$('#errCmobilem').html(' ');
								$('#checkm').hide();
								$('#updtNumm').show();
								$('#cnumberm').hide();
								$('#cotpm').prop('placeholder','Enter otp sent to the new number : '+valu);
								$('#cotpm').slideDown();
							}else{
								alert('Sorry, Please try again');
							}
						}
					}); 
				}
			}
		});
	}
}

function update_email(valu)
{
	//var valu = $('#cemail').val();
	if(valu.match(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i) === null){
		$('#errCemail').html('Please enter a proper email id.');
	}else{
		$.ajax ({
			async : false,
			url : siteURL+'user_registration/check_for_email_is_unique',
			type: 'POST',
			data: {'email' : valu}, 
			cache: false,
			beforeSend: function(){
				$('#checke').attr('disabled',true);
				$('#errCemail').html(' ');
			},
			success: function(resp)
			{ 
				resp = $.trim(resp);
				if(resp != '0'){
					$('#errCemail').html('This email id is already in use, please try another.');
					$('#checke').attr('disabled',false);
				}else{
					$('#errCemail').html(' ');
					$('#checke').hide();
					$('#updtEmail').show();
				}
			}
		});
	}
}

/****** change password validation written by Pooja Start ******/
function validate_oldpassword()
{
    if($('#oldpassword').val() == '' || $('#oldpassword').val() == null ){
        $('#Error-msg-oldpassword').text("*Password can not be blank.");
        return false;
    }else if($('#oldpassword').val().length < 6 || $('#oldpassword').val().length > 15){
        $('#Error-msg-oldpassword').text("*Password Length : Min:6; Max:15 characters.");
        return false;
    }
    else{
        $('#Error-msg-oldpassword').text("");
        return true;
    }
    
}

function validate_newpassword()
{
    if($('#password').val() == '' || $('#password').val() == null ){
        $('#Error-msg-newpassword').text("*Password can not be blank.");
        return false;
    }else if($('#password').val().length < 6 || $('#password').val().length > 15){
        $('#Error-msg-newpassword').text("*Password Length : Min:6; Max:15 characters.");
        return false;
    }else if($('#password').val()==$('#oldpassword').val()){
        $('#Error-msg-newpassword').text("*Old password and new password musth be different");
        return false;
    }else{
        $('#Error-msg-newpassword').text("");
        return true;
    }
    
}

function validate_change_password()
{
    if ($('#password').val()!=$('#conpass').val()) {
        $('#Error-msg-conpassword').text("*Re-entered password mismatch");
        return false;
    }else{
        $('#Error-msg-conpassword').text("");
        return true;
    }    
}

function onSubmitUpdate(){
	debugger;
    if(validate_oldpassword()&&validate_newpassword()&&validate_change_password()){
        return true;
    }else{return false;}
}

/****** change password validation written by Pooja End ******/

/****** Update Address ******/
function updd(ele){
	$('.edit_').hide();
	$('.c').hide();
	$('.e').show();
	$('.disp').show();
	$('#addAdrBtn').text('Add Address');
	$('#addNewAddLbl').hide();
	$('#addNewAddSec').hide();
	$('#addAdrBtnm').text('Add Address');
	$('#addNewAddLblm').hide();
	$('#addNewAddSecm').hide();
	
	$('#disp_'+ele).hide();
	$('#edit_'+ele).hide();
	$('#close_'+ele).show();
	$('#input_'+ele).slideDown();
}
function updu(ele){
	$('#disp_'+ele).show();
	$('#edit_'+ele).show();
	$('#close_'+ele).hide();
	$('#input_'+ele).hide();

	$('#cnumberm').show();
	$('#check').show();
	
	$('#cnumberm').show();
	$('#checkm').show();
	
	$('#cotpm').prop('placeholder','OTP did not sent, try refreshing page.');
	$('#cotpm').hide();
	$('#updtNumm').hide();
		
	$('#cotp').prop('placeholder','OTP did not sent, try refreshing page.');
	$('#cotp').hide();
	$('#updtNum').hide();
	
	location.reload(true);
}

function togAddAdr(){
	if($('#addAdrBtn').text() == 'X'){
		$('#addAdrBtn').text('Add Address');
		$('#addNewAddLbl').hide();
		$('#addNewAddSec').hide();
		$('#addAdrBtnm').text('Add New Address');
		$('#addNewAddSecm').hide();
		
		$('#addAdrBtnm').text('Add Address');
		$('#addNewAddLblm').hide();
		$('#addNewAddSecm').hide();
	}else{
		$('#addAdrBtn').text('X');			
		$('.edit_').hide();
		$('.c').hide();
		$('.e').show();
		$('.disp').show();
		$('#addNewAddLbl').show();
		$('#addNewAddSec').show();
		$('#addNewAddSecm').show();
	}
}

function getPincodeToUpdate(i){
	var pincode = $('#pin'+i).val();;
	// alert(pincode);
    $.ajax ({
        url : siteURL+'user_registration/get_address_details_using_pincode', 
        type: 'POST',
        data: {'pincode' : pincode}, 
        cache: false,
        success: function(resp)
        {
        	resp = $.trim(resp);
        	if(resp == 'false' || resp == 'null'){
	                $('#err_pin'+i).html('Can not find pincode '+pincode+', please try again.');
	                $('#chpinId'+i).val('');
	                $('#chState'+i).val('');
	                $('#chCity'+i).val(''); 
	                $('#chArea'+i).html('');
            	}else{
	                $('#err_pincode').html('');
	                resp = JSON.parse(resp); 
	                $('#chpinId'+i).val(resp.pincode_id);
	                $('#chState'+i).val(resp.state); 
	                $('#chCity'+i).val(resp.city); 
	                $('#chArea'+i).html('');
			$('#chArea'+i).append($("<option/>", {
				value: '',
				text: 'Select Area'
			}));
                	$.each(resp.areas, function(index, area){
		                $('#chArea'+i).append($('<option/>', {
		                      value: area.area_name,
		                      text: area.area_name
		                }));
              		});
	            }
        	}
   	 });
}


$(function(){
	$( "#reg_password" ).keyup(function() {
		$('#err_reg_password').html("");
	});
});
	
function validate_pass()
{
	
    if($('#reg_password').val() == '' || $('#reg_password').val() == null ){
        $('#err_reg_password').text("*Password can not be blank.");
		passstatus=false;
        return passstatus;
    }
    
    else if($('#reg_password').val().length < 6 || $('#reg_password').val().length > 15){
        $('#err_reg_password').text("*Password Length : Min:6; Max:15 characters.");
		passstatus=false;
        return passstatus;
    }else{
        $('#err_reg_password').text("");
        $('#err_reg_password').text("");
       passstatus=true;
       return passstatus;
    }
    
}

function validate_change_password_fp()
{
    if ($('#password').val()!=$('#conpass').val()) {
		 $('#Error-msg-conpassword').css("color", "Red");
        $('#Error-msg-conpassword').text("*Re-entered password mismatch");
		FpStatus=false;
        return FpStatus;
    }else{
        $('#Error-msg-conpassword').text("");
		FpStatus=true;
        return FpStatus;
    }    
}

function validateForgotPassword()
{
	if(FpStatus)
	{
		return true;
	}else{
		return false;
	}	
}
