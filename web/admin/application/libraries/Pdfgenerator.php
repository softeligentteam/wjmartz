<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdfgenerator {

	public function __construct() {
			
			require_once APPPATH.'third_party/dompdf/dompdf_config.inc.php';
			
			$pdf = new DOMPDF();
			
			$CI =& get_instance();
			$CI->dompdf = $pdf;
	}
	
  public function generate($html, $filename='', $stream=TRUE, $paper = 'A4', $orientation = "portrait")
  {
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->set_paper($paper, $orientation);
    $dompdf->render();
    if ($stream) {
        $dompdf->stream($filename.".pdf", array("Attachment" => 0));
    } else {
        return $dompdf->output();
    }
  }
}