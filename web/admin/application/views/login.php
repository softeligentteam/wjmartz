<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
	
</head>
<body>
    <div class="container">    
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<img src="<?=base_url();?>assets/img/2.png" style="width:100%;" />
            <div class="panel panel-info" >
				<div class="panel-heading">
					<div class="panel-title">Sign In</div>
					<div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>
				</div>     

				<div style="padding-top:30px" class="panel-body" > 
					<div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
					<?php 
						if(NULL!==$this->session->flashdata('message')){
							echo "<div class='".$this->session->flashdata('css_class')."'>".$this->session->flashdata('message')."</div>";
						}
                     ?>
				 <?=form_open('login/authenticate')  ?>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							 <input type="email" name="email" class="form-control" value="<?=set_value('email', '')?>" required="required" placeholder="Email Id">
						</div>
						<div style="margin-bottom: 25px" class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							 <input type="password" class="form-control" name="password" required="required" placeholder="Password">
						</div>
						
						<hr />
						
						<div style="margin-top:10px" class="form-group">
							<!-- Button --> 
							<div class="col-sm-12 controls">
								 <input type="submit" class="register" name="login-submit" value="Login">
							</div>
						</div>
						<hr />  
					 <?=form_close();?>   
				</div>                     
			</div>  
        </div>
    </div>
</body>
</html>