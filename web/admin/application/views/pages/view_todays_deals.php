
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12"> 
				<div class="col-md-4"> 
					<h5 style="padding-bottom:0;">Deals Of The Day</h5>
				</div>
				<div class="col-md-8">  
						<a onclick="$('#addTodayDeals').slideDown();" <?php if (count($deals_of_the_day_data) == '20'){ ?> disabled <?php   } ?> class="btn btn-info pull-right" >Add Todays Deals</a> 
				</div>
			</div>
		</div>
		<hr style="margin-top:0;" /> 
		
		<div class="row">
			<div  id="addTodayDeals" style="display:none;">
				<div class="clearfix"></div>
				<hr />
				<?=form_open_multipart('products/add_deals_of_the_day');?>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							<img id="dealImagePreview" src="" style="width:100%;" alt="" />
						</div>
						<div class="panel-footer back-footer-blue">
						<input type='file' name="deal_img" id="deal_img" onchange="previewImage(deal_img,'dealImagePreview');" class="" style="width:100%" required /> 
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="col-md-12">
						<div class="col-md-3">
							<div class="form-group">
								<label>Select Parent Category</label>
								<span class="pull-right">:</span>
							</div>
						</div>
						<div class="col-md-9">
							<div class="form-group">
								<?=form_dropdown("parent_cats", $parent_cats_list, set_value('parent_cats', $current_parent_cat_id), "tabindex='5' id='parent_cats' required class='form-control'")?>		
								<p class="help-block" id="selectParentCatError" style="color:red; height:20px;"></p>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-3">
							<div class="form-group">
								<label>Select Child Category</label>
								<span class="pull-right">:</span>
							</div>
						</div>
						<div class="col-md-9">
							<div class="form-group">
								<!--<?=form_dropdown("child_cats", $child_cats_list, set_value('child_cats', $current_child_cat_id), "tabindex='5' id='child_cats' required class='form-control'")?>-->
								<?=form_dropdown("child_cats", "Select Child Category", set_value('child_cats', ''), "tabindex='5' id='child_cats'  required class='form-control'")?>
								<p class="help-block" id="selectMainCatError" style="color:red; height:20px;"></p>
							</div>
						</div>
					</div>
					<div class="col-md-12"> 
						<div class="col-md-3">
							<div class="form-group">
								<label>Select Products</label>
								<span class="pull-right">:</span>
							</div>
						</div>
						<?php $product_list = null;?>
						<div class="col-md-9">
							<div class="form-group">
							
								<!--<input type="text" class="form-control" id = "search_product" placeholder="Type Product Name to search.">-->
								<?=form_dropdown("product_list", "Select Product", set_value('product_list', ''), "tabindex='5' id='product_list'  required class='form-control'")?>
								
								<p class="help-block" id="selectMainCatError" style="color:red; height:20px;"></p>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-3">
							<div class="form-group">
								<label>Enter The Deal Price</label>
								<span class="pull-right">:</span>
							</div>
						</div>
						<div class="col-md-9">
							<div class="form-group">
								<input type = "text" pattern="[0-9]+(\.[0-9]{0,2})?%?" title="This must be a number with up to 2 decimal places and/or %" class="form-control" name="deal_price" id = "deal_price" required />
							</div> 
						</div>
					<div>
					<div class="col-md-12">
						<input type="submit" value="Save" class="btn btn-success" />
						<a onclick="location.reload();" class="btn btn-warning">cancel</a>
					</div>
				</div>
				<div class="clearfix"></div>
				<?=form_close();?>
				<!-- /. ROW  --> 
			</div>
			
		</div>
				<div class="col-md-12">
					<span class="text-info">Note:- Max Size:300KB | Width:300px | Height:300px </span>
				</div>
				<div class="clearfix"></div>
				<hr />
			</div>
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th class="text-center">Procduct&nbsp;Image</th>
							<th class="text-center">Procduct&nbsp;Name</th>
							<th class="text-center">Deal&nbsp;Price</th>
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>

						<?php $i=0; foreach($deals_of_the_day_data as $deals_data){ $i++;?>
						<tr>
							<td><?=$i?></td>
							<td class="text-center"><img src="<?=IMAGEACCESSPATH.$deals_data->deal_image_url?>" style="height:70px;" /></td>
							<td><?=$deals_data->prod_name?></td>
							<td><?=$deals_data->deal_price?></td>
							<td class="text-center"><a onclick="$('#deletedeals<?=$deals_data->deal_id?>').slideDown();" class="btn btn-danger">Delete</a></td>
						</tr>
						<tr>
							<td colspan="5" style="text-align:center; display:none;" id="deletedeals<?=$deals_data->deal_id?>">
								<br />
								<h4>Are you sure you want to delete Deals of the day : <?=$deals_data->prod_name?> ?</h4>
								<br /><br />
								<a href="<?=site_url();?>products/delete_deals/<?=$deals_data->deal_id?>/<?=$deals_data->deal_product_id?>" class="btn btn-danger">Yes !</a>
								<a onclick="location.reload();" class="btn btn-info">cancel</a>
								<br /><br />
							</td>
						</tr>
						<?php }?>
					</tbody>
				</table>
				<?php echo $this->pagination->create_links();?>
			</div>
		</div>
		<!-- /. ROW  -->
	</div>
	<!-- /. PAGE INNER  -->
</div>
<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<!--<script src="<?=base_url();?>assets/js/add/product_autopopulate_list.js"></script>-->
<script>
var site_url = '<?=site_url();?>';
 var baseUrl = '<?=base_url();?>';
 var jsonProductListData = <?php echo json_encode($product_list); ?>;
 
$(document).ready(function () {
    $('#parent_cats').change(function () {
		
        var pid = $('#parent_cats').val();
        if (pid !== '') {
            $.ajax({
				// async:false;
                url: '<?=site_url();?>Child_categories/get_child_cat_list',
                dataType: 'json',
                data: { 'parent_cats': pid },
                type: "get",
                success: function (data) {
				//alert(data);
			$('#child_cats').html('');
                    $('#child_cats').append($('<option/>', {
                        value: '',
                        text: "Select Child Category"
                    })).attr('selected', true);
                    $.each(data, function (Index, Child) {
                        $('#child_cats').append($('<option/>', {
                            value: Child.child_cat_id,
                            text: Child.child_cat_name
                        }));
                    });
                },
                error: function (error) {
                    alert('error' +JSON.stringify(error));
                }
            });
        }
        else {
            reset_subcategory();
        }
    });
});

function reset_subcategory() {

    $('#child_cats').html('');
    $('#child_cats').append($("<option/>", {
        value: '',
        text: 'Select Child Category'
    }));

}

$(document).ready(function () {
    $('#child_cats').change(function () {
		
        var cid = $('#child_cats').val();
		var pid = $('#parent_cats').val();
        if (cid !== '') {
            $.ajax({
                url: '<?=site_url();?>products/get_product_list',
                dataType: 'json',
                data: { 'child_cats': cid,
						'parent_cats': pid, },
                type: "get",
                success: function (data) {
					//alert(data);
					//var jsonProductListData = data;
					//autpop(jsonProductListData);
					$('#product_list').html('');
					$('#product_list').append($('<option/>', {
						value: '',
						text: "Select Product"
					})).attr('selected', true);
					$.each(data, function (Index, product) {
						$('#product_list').append($('<option/>', {
							value: product.prod_id,
							text: product.prod_name
						}));
					});
                },
                error: function (error) {
                    alert('error' +JSON.stringify(error));
                }
            });
        }
        else {
            reset_subcategory();
        }
    });
});

function reset_subcategory() {

    $('#product_list').html('');
    $('#product_list').append($("<option/>", {
        value: '',
        text: 'Select Product'
    }));

}

	
</script>