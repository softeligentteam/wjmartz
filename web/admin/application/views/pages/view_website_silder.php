<div id="page-wrapper">
	<div id="page-inner" style="padding:0;">
		<div class="row">
			<div class="col-md-12">
				<div class="agile-grids agile-Updating-grids member_info no_border">
					<div class="col-md-12">  
						<div class="table-heading">
							<h5 style="padding-bottom:0;">View And Edit Slider Images</h5>
						</div>					
					</div>					
					<div class="clearfix"></div>
					<hr style="margin-top:0;"/>
					<div class="col-md-12">  
						<div class="col-md-12">  
							<span class="text-info">Note:- Max Size:300KB | Width:1680px | Height:650px </span><br /><br />
						</div>					
					</div>
					<div class="clearfix"></div> 
				<div class="row">  
				<?php 
				$counter=0;
				foreach($website_silder as $silder){
					$counter++;
					?>
					<div class="insert_success" id="activeSlider<?=$silder->slider_img_id?>" style="display:none;">
						<div class="row">
							<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
								<div class="row">
									<div class="col-sm-12 text-center"> 
										<h4>Activete Slider Image</h4>
										<center>
										<img src="<?=IMAGEACCESSPATH.$silder->slider_image?>" class='img-responsive' style='width:150px;' />
										</center>
										<p>
											are you sure you want to activate this slider image ?
										</p>
										<br />
									</div>
									<div class="col-sm-12 text-center">
									
										<a title="Delete"class="btn btn-success" href="<?php echo site_url('Slider/activeSliderWebsite/').$silder->slider_img_id; ?>" class="btn btn-danger">Active</a>
										<a onclick="$('#activeSlider<?=$silder->slider_img_id?>').slideUp();" id="myBtn" class="btn btn-primary">No</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="insert_success" id="inactiveSlider<?=$silder->slider_img_id?>" style="display:none;">
						<div class="row">
							<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
								<div class="row">
									<div class="col-sm-12 text-center"> 
										<h4>Inactivate Slider Image</h4>
										<center>
										<img src="<?=IMAGEACCESSPATH.$silder->slider_image?>" class='img-responsive' style='width:150px;' />
										</center>
										<p>
											are you sure you want to Inactivate this slider image ?
										</p>
										<br />
									</div>
									<div class="col-sm-12 text-center">
										
										<a title="Delete"class="btn btn-danger" href="<?php echo site_url('Slider/inactiveSliderWebsite/') . $silder->slider_img_id; ?>" class="btn btn-danger">Inactive</a>
										
										<a onclick="$('#inactiveSlider<?=$silder->slider_img_id?>').slideUp();" id="" class="btn btn-primary">Not Sure</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4"> 
						<?php 
						//echo IMAGEACCESSPATH.$silder->slider_image;
							if(!empty($silder->slider_image)) {
								echo "<img src='".IMAGEACCESSPATH.$silder->slider_image."' class='img-responsive' style='margin-bottom:20px; box-shadow: 2px 2px 3px grey;' />";
							}
							else{
								echo "<img src='".IMAGEACCESSPATH."static/uploads/silder/no-image-available.png' class='img-responsive' style='margin-bottom:20px; box-shadow: 2px 2px 3px grey;' />";
							}
						?>  
						<button onclick="$('#editSliderimg<?=$silder->slider_img_id?>').slideDown();" class="btn btn-default w3ls-button" style="margin:10px; width:40%; float:left;">Edit Slider</button> 
						<?php
							if($silder->status==ACTIVE)
							{
								?><a title="Delete"class="btn btn-danger"  style="margin:10px; width:40%; float:right;" href="#" class="btn btn-danger" role="button" onClick="$('#inactiveSlider<?=$silder->slider_img_id?>').slideDown();"><i class="fa fa-trash"></i>Inactive</a>
							<?php }
							else if($silder->status==INACTIVE)
							{
								?><a title="Delete"class="btn btn-success"  style="margin:10px; width:40%; float:right;" href="#" class="btn btn-danger" role="button" onClick="$('#activeSlider<?=$silder->slider_img_id?>').slideDown();"></i>Active</a>
						<?php } ?>
					</div>
					<div id="editSliderimg<?=$silder->slider_img_id?>" style="display:none;">
						<div class="col-md-4">
							<img id="img<?=$silder->slider_img_id?>" src="<?=IMAGEACCESSPATH.$silder->slider_image?>" style=" box-shadow: 2px 2px 3px grey; margin-bottom:20px; width:100%;" alt="UPLOADED IMAGE" class="img-responsive" />
							<div class="clearfix"></div>
							<!--<form action="<?=site_url();?>SliderImg/update_app_slider_img" enctype="multipart/form-data" method="post">-->
							<?=form_open_multipart('Slider/update_website_slider_img')?>
								<input type="hidden" name="slider_img_id" value="<?=$silder->slider_img_id?>" />
								<input type="file" name="slider_img" onchange="previewImage(this,'img<?=$silder->slider_img_id?>');" style="margin:10px; width:62%; float:left;" />
								<input type="submit" class="btn btn-default w3ls-button" style="margin:10px; width:44%; float:left;" value="Save Slider" />
								<a onclick="$('#editSliderimg<?=$silder->slider_img_id?>').hide();" class="btn btn-info w3ls-button" style="margin:7px; width:44%; float:right;">Cancel</a>
							<?=form_close()?>
						</div>
						<div class="clearfix"></div><hr /> 
					</div>
					
				<?php
				if($counter == 3){ 
					$counter = 0;
					echo '<div class="clearfix"></div> <hr />';
				}
				} ?>
				<!--?php if($i==3){ echo'<div class="clearfix"></div>'; } } ?-->
		</div> 
						
						<!--?php if($i==3){ echo'<div class="clearfix"></div>'; } } ?-->
						<div class="clearfix"></div>
						<hr /> 

				</div> 
			</div>
		</div>
	</div>
</div>	
<style>
	.no_border{
		border:none;
	}
	#addWebSliderImg, #addDrAppSliderImg, #addUserAppSliderImg{
		display:none;
	}
</style> 

 
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
<script>
	//For slider image form opening.
	function addWebSliderImg(){
		$('#addWebSliderImg').slideToggle();
	}
	function addDrAppSliderImg(){
		$('#addDrAppSliderImg').slideToggle();
	}
	function addUserAppSliderImg(){
		$('#addUserAppSliderImg').slideToggle();
	}   
	
	//For image preview before uploading.
	function readURL(input, output) { 
		if (input.files && input.files[0]) {
			var reader = new FileReader(); 

			reader.onload = function (e) {
				$('#'+output).attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	} 
	function previewImage(id, otpt) {   
		readURL(id, otpt);
	}
</script>	


<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="$('.insert_success').slideUp();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
