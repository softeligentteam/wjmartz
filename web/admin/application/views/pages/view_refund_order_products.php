<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12"> 
				<div class="transaction_det"> 
					<table class="table table-striped table-bordered table-hover">
						<tr>
							<td colspan="4" class="text-center tableheading">
								Single Return Exchange Order Details
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-center tableheading">
								<!--For Necessory Spacing-->
							</td>
						</tr>
						<tr>
							<td>Return Exchange ID :</td>
							<td><?= $order->refund_id ?> </td>
							<td>Order ID. :</td>
							<td><?= $order->order_id ?> </td>
						</tr>
						<tr>
							<td>Customer ID :</td>
							<td><?=$order->cust_id?> </td>
							<td>Customer Name :</td>
							<td><?=$order->first_name." ".$order->last_name?> </td>
						</tr>
						<tr>
							<td>Contact No :</td>
							<td><?= $order->mobile ?> </td>
							<td>Email ID :</td>
							<td><?= $order->email ?> </td>
						</tr>
						<tr>
							<td colspan="4" class="text-center tableheading">
								Products Details
							</td>
						</tr>
						<?php  if(count($refund_order_product)>0){
							$total_amount = 0;
							foreach ($refund_order_product as $product) { ?>
						<tr> 
							<td rowspan="6" class="text-center">
								<a href="<?=IMAGEACCESSPATH.$product->prod_image_url?>" target="_blank">
									<img src="<?=IMAGEACCESSPATH.$product->prod_image_url?>" style="width:100px;" /></img>
								</a>
							</td>
						</tr> 
						<tr>
							<th>Product Name</th> 
							<td colspan="2"><?=$product->prod_name?></td>
						</tr> 
						<tr>
							<th>Size</th> 
							<td colspan="2"><?=$product->size?></td>
						</tr>
						<tr>
							<th>Price</th> 
							<td colspan="2"><?=$product->price?></td> 
						</tr>
						<tr>
							<th>Quantity</th> 
							<td colspan="2"><?=$product->quantity?></td>
						</tr>
						<tr>
							<th>Amount</th>
							<td colspan="2">Rs. <?=$product->amount?></td>
						</tr> 
						<?php $total_amount = $total_amount+$product->amount;} }?>
						<tr>
							<th>Total Amount</th>
							<td colspan="3">Rs. <?=$total_amount?></td>
						</tr> 
						
					</table>
					<div class="clearfix"></div> 
					<?=anchor('order/view_refund_order', 'Back', "class='btn btn-lg btn-warning'");?> 
				</div>
			</div>
		</div>
	</div> 
</div>
<style>
.tableheading{
	font-size: 20px !important;
    font-weight: 600 !important;
    padding: 10px 0 !important;
    color: #31969e !important;
    border-top: 1px solid white !important;
    border-left: 1px solid white !important;
    border-right: 1px solid white !important;
    background-color: white !important;
}
.up-img {
	width: 100%;
	height: 170px;
	float: right;
}

.form-title h3 {text-align:center; color:white;}
.ttl {font-weight:600;}
.transaction_det div{margin:7px 0;}
</style>