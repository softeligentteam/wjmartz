<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h5 style="padding-bottom:0;">
					Pincode Wise Price List
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Enter New Pincode</a>
				</h5>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr style="margin-top:0;" />
		<div class="row">
			<div id="addNewSubCat" style="display:none;">
				<?=form_open('pincode/add_pincodewise_delivery_charges');?>
					<div class="col-md-4">
						<div class="form-group">
							<label>Enter New Pincode</label>
							<input type ="number" class="form-control" name="pincode" required />
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Enter Respective Service Charge</label>
							<input type ="number" class="form-control" name="charge" required />
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-12">
						<input type="submit" value="Save" class="btn btn-success" />
						<a onclick="location.reload();" class="btn btn-warning">cancel</a>
					</div>
					<div class="clearfix"></div>
					<hr />
				<?=form_close();?>
			</div>
			<div class="col-md-12 text-right">
				<div id="updateNewpincode" style="display:none;">
					<?=form_open('pincode/edit_pincode');?>
						<div class="col-md-3">
							<div class="form-group">
								<label>Pincode ID</label>
								<input class="form-control" id="pin_id" name="pin_id" readonly required />
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label> Pincode</label>
								<input type ="number" class="form-control" id="pin" name="pin" value="" required />
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label> Respective Service Charge</label>
								<input type ="number" class="form-control" id="del_charge" name="del_charge"  required />
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
						</div>
						<div class="col-md-12">
							<input type="submit" value="Save" class="btn btn-success" />
							<a onclick="location.reload();" class="btn btn-warning">cancel</a>
						</div>
						<div class="clearfix"></div>
						<hr />
					<?=form_close();?>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Pincode</th>
							<th>Delivery&nbsp;Charges</th>
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						<?php  
                           foreach ($pincode_list as $pincode)  
                          {  
                        ?>	
						<tr>
							<td><?=$index?></td>
							<td><?=$pincode->pincode?></td>
							<td><?=$pincode->delivery_charges?></td>
							<td class="text-center"><a onclick="edit_this('<?=$pincode->pincode_id?>','<?=$pincode->pincode?>','<?=$pincode->delivery_charges?>');" id="myBtn" class="btn btn-primary">Edit</a></td>
							<!--td class="text-center"><a onclick="delete_this('< ?=$pincode->pincode_id?>', ' < ?=$pincode->pincode?>');" class="btn btn-danger">Delete</a></td-->
						</tr>
					   <?php $index++;
						 
						  } ?>	
					</tbody>
				</table> 
				<?php echo $this->pagination->create_links();?>
			</div>
		</div>
		<!-- /. ROW  -->
	</div>
	<!-- /. PAGE INNER  -->
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/pincode.js"></script>
	
	