<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12"> 
				<div class="transaction_det"> 
					<table class="table table-striped table-bordered table-hover">
						<tr>
							<td colspan="4" class="text-center tableheading">
								Single Delivered Order Details
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-center tableheading">
								<!--For Necessory Spacing-->
							</td>
						</tr>
						<tr>
							<td>Order ID. :</td>
							<td><?= $order->order_id ?> </td>
							<td>Customer Name :</td>
							<td><?=$order->first_name." ".$order->last_name?> </td>
						</tr>
						<tr>
							<td>Contact No :</td>
							<td><?= $order->mobile ?> </td>
							<td>Email ID :</td>
							<td><?= $order->email ?> </td>
						</tr>
						<tr>
							<td>Customer Address :</td>
							<td colspan="3"><?=$order->address." ".$order->area_name." ".$order->city."-".$order->pincode?> </td>
						</tr>
						<tr>
							<td colspan="4" class="text-center tableheading">
								Transaction Details
							</td>
						</tr>
						<tr>
							<td>Transaction ID : </td> 
							<td> <?=$order->transaction_id?></td>
							<td>Transaction Status : </td> 
							<td><?=$order->payment_status?></td>
						</tr>
						<tr>
							<td>Transaction Date : </td>
							<td><?=$order->payment_datetime?></td>
							<td>Promo Code :</td>
							<td>
							<?php if(($order->promocode_applied)!=0){
								$promocode= $order->promocode;
							}else
							{	$promocode= 'Promo Code Not Applied';
								
							}?>
							<?=$promocode?></td>
						
						</tr>
						<tr>
							<?php 
								if(count($order_product)>0)
								{
									$total_gst_amount=0;
										foreach ($order_product as $gst) 
										{
											$total_gst_amount= $total_gst_amount+$gst->sgst_amount+$gst->cgst_amount;
										}
								}
								else{$total_gst_amount=0;}
								$Product_amt = $order->prod_amt-$total_gst_amount;
							?> 
							<td>Product Price:</td> 
							<td> <i class="fa fa-inr"></i> <?=$Product_amt?></td>
							<td>GST Amount: </td>
							<td><i class="fa fa-inr"></i> <?=$total_gst_amount?></td>
						</tr>
						<tr>
							<td>Total Amount :</td> 
							<td <?php if( $order->promocode_applied == 0){ echo'colspan="3"'; }?>> 
								<i class="fa fa-inr"></i> <?=$order->prod_amt?>
							</td>
							<?php if( $order->promocode_applied != 0){?>
								<td>Discount :</td>
								<td> <i class="fa fa-inr"></i> <?=$order->prod_amt*($order->discount/100)?></td>
							<?php }?>
						</tr>
						<tr>
							<td>Delivery Charge:</td> 
							<td> <i class="fa fa-inr"></i> <?=$order->delivery_charges?></td>
							<th>Payable Amount :</th> 
							<th><i class="fa fa-inr"></i><?=$order->discount_amount?></th>
						</tr>
						<tr>
							<td colspan="4" class="text-center tableheading">
								Products Details
							</td>
						</tr>
						<?php  if(count($order_product)>0){foreach ($order_product as $product) { ?>
						<tr> 
							<td rowspan="6" class="text-center">
								<a href="<?=IMAGEACCESSPATH.$product->prod_image_url?>" target="_blank">
									<img src="<?=IMAGEACCESSPATH.$product->prod_image_url?>" style="width:100px;" /></img>
								</a>
							</td>
						</tr> 
						<tr>
							<th>Product Name</th> 
							<td colspan="2"><?=$product->prod_name?></td>
						</tr> 
						<tr>
							<th>Size</th> 
							<td colspan="2"><?=$product->size?></td>
						</tr>
						<tr>
							<th>Price</th> 
							<td colspan="2"><?=$product->price?></td> 
						</tr>
						<tr>
							<th>Quantity</th> 
							<td colspan="2"><?=$product->quantity?></td>
						</tr>
						<tr>
							<th>Amount</th>
							<td colspan="2"><?=$product->amount?></td>
						</tr> 
						<?php } } ?> 
					</table>
					<div class="clearfix"></div>  
					<?=anchor('order/view_delivered_order', 'Back', "class='btn btn-lg btn-warning'");?>  
				</div>
			</div>
		</div>
	</div>
</div>
<style>
.tableheading{
	font-size: 20px !important;
    font-weight: 600 !important;
    padding: 10px 0 !important;
    color: #31969e !important;
    border-top: 1px solid white !important;
    border-left: 1px solid white !important;
    border-right: 1px solid white !important;
    background-color: white !important;
}
.up-img {
	width: 100%;
	height: 170px;
	float: right;
}

.form-title h3 {text-align:center; color:white;}
.ttl {font-weight:600;}
.transaction_det div{margin:7px 0;}
</style>