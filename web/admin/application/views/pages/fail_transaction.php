<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12"> 
				<div class="col-md-4"> 
					<h5 style="padding-bottom:0;">Failure Transaction Details</h3>
				</div>
				<div class="col-md-8">  
					<?=form_open('payment/search_fail_transaction_details');?>
						<div class="col-md-8">  
							<div class="form-group"> 
								<input type="text" class="form-control" id = "payment_details" name = "payment_details" placeholder="Enter Name or Date(YYYY/MM/DD) for search" /> 		
							</div>  
							<button type="submit" class="btn btn-info" style="position: absolute; right: 0; top: 0;">
								<i class="fa fa-search"></i>
							</button>
						</div>  
					<?=form_close();?>
					<div class="col-md-4 py-0">
						 <form method="post" action="<?=site_url(); ?>/excel_export/export_payment_failed_transaction">
							<input type="submit" name="export" class="btn btn-info pull-right" value="Generate Excel Sheet" />
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr style="margin-top:0;" />
		<div class="row">
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Ref&nbsp;Order&nbsp;ID</th> 
							<th>Transaction&nbsp;ID</th> 
							<th>Customer&nbsp;Name</th> 
							<th>Tansaction&nbsp;Date</th> 
							<th>Transaction&nbsp;Status</th>
							<th>Failure&nbsp;Reason</th> 
						</tr>
					</thead>
					<tbody>
						
						<?php  if(count($fail_data)>0){
						$i=1;
                           foreach ($fail_data as $fail)  
                          {  
                        ?>	
						<tr>
							<td><?=$i?></td>
							<td><?=$fail->ref_order_id?></td>
							<td><?=$fail->transaction_id?></td>
							<td><?=$fail->first_name .' '.$fail->last_name?></td>
							<td><?=$fail->payment_datetime?></td>
							<td><?=$fail->payment_status?></td>
							<td><?=$fail->trans_reason?></td>
						</tr>
					   <?php $i++;
						 
							}  
						}else { ?>
							<tr class="warning no-result">
								<td colspan="7"><i class="fa fa-warning"/> No records found...</td>
							</tr>
						<?php } ?> 	
					</tbody>
				</table>
				<?php echo $this->pagination->create_links();?>
				<!--div class="col-sm-12" style="padding-left:0;"> 
				  <ul class="pagination">
					<li><a href="#">&lt;</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">&gt;</a></li>
				  </ul> 
				</div-->
			</div>
		</div>
		<!-- /. ROW  -->
	</div>
	<!-- /. PAGE INNER  -->
</div>
	