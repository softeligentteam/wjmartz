<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h5 style="padding-bottom:0;margin-top:0;">
					Admin Dashboard
				</h2>
			</div>
		</div>
		<hr style="margin-top:0;" />
		<div class="row">
			<div class="col-md-3 col-sm-3 col-xs-6"> 
				<div class="panel panel-primary text-center no-boder bg-color-blue">
					<div class="panel-body">
						<i class="fa fa-user fa-2x"></i>
						<h3 style="margin:0;"><?=$customer_count?></h3>
					</div>
					<div class="panel-footer back-footer-blue">
						No. of Customers
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6"> 
				<div class="alert alert-info text-center">
					<i class="fa fa-shopping-cart fa-2x"></i>
					<h3 style="margin:0;"><?=$order_paced?></h3>
					<br />Pending Orders
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6"> 
				<div class="panel panel-primary text-center no-boder bg-color-blue">
					<div class="panel-body">
						<i class="fa fa-archive fa-2x"></i>
						<h3 style="margin:0;"><?=$order_delivered?></h3>
					</div>
					<div class="panel-footer back-footer-blue">
						Delivered Orders 
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-6"> 
				<div class="alert alert-info text-center">
					<i class="fa fa-ban fa-2x"></i>
					<h3 style="margin:0;"><?=$order_cancelled?></h3>
					<br />Cancelled Orders
				</div>
			</div>
		</div>
		<!-- /. ROW  ---->
		<div class="row">
			<div class="col-md-12"> 
				<h5>Recent Orders	<a href="<?=site_url();?>order/view_all" class="btn btn-info pull-right">View All</a></h5>
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Order&nbsp;ID</th> 
							<th>Customer&nbsp;Name</th> 
							<th>Contact</th> 
							<th>Email</th> 
							<th>Address</th> 
							<th>Order&nbsp;Date</th> 
							<th>Order&nbsp;Amount</th> 
						</tr>
					</thead>
					<tbody>
						
						
						<?php  if(count($order_list)>0){
						$i=1;
                           foreach ($order_list as $order)  
                          {  
                        ?>	
						<tr>
							<td><?=$i?></td>
							<td><?=$order->order_id?></td>
							<td><?=$order->first_name .' '.$order->last_name?></td>
							<td><?=$order->mobile?></td>
							<td><?=$order->email?></td>
							<td><?=$order->address .' '.$order->area_name .' '.$order->city .' '.$order->state .'-'.$order->pincode?></td>
							<td><?=$order->order_date?></td>
							<td><?=$order->discount_amount?></td>
						</tr>
					   <?php $i++;
					}  
				}
				else
				{
				?>
					<tr class="warning no-result">
						<td colspan="8">No records found...</td>
					</tr>
				<?php
				}
				?>
					</tbody>
				</table>
			</div>
			<div class="col-md-12"> 
				<h5>Registered Users	<a href="<?=site_url();?>customer/view_all/" class="btn btn-info pull-right">View All</a></h5>
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Sr. No</th>
							<th>Customer&nbsp;Name</th> 
							<th>Contact</th> 
							<th>Email</th> 
						</tr>
					</thead>
					<tbody>
						<?php  
						if(count($customer_list)>0){
							$i=1;
							foreach ($customer_list as $customer)  
							{  
                        ?>	
								<tr>
									<td><?=$i?></td>
									<td><?=$customer->first_name .' '.$customer->last_name?></td>
									<td><?=$customer->mobile?></td>
									<td><?=$customer->email?></td>
								</tr>
					   <?php 
							$i++;
							}  
						}
						else
						{
						?>
							<tr class="warning no-result">
								<td colspan="4">No records found...</td>
							</tr>
						<?php
						}
						?>	
					</tbody>
				</table>
			</div>
			
	</div>
	<!-- /. PAGE INNER  -->
	</div>