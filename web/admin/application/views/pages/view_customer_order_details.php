<div id="page-wrapper">
	<div id="page-inner"> 
		<div class="row">
			<div class="col-md-12">
				<table class="table table-striped table-bordered table-hover">
					<tr>
							<td colspan="4" class="text-center tableheading">
								Products Details
							</td>
						</tr>
					<tbody>
						<?php 
						 if(count($order_product_details)>0)
						 {
							foreach($order_product_details as $product)
							{
						
						?>
						<!--php loop-->
						<tr> 
							<td rowspan="7" class="text-center">
								<a href="<?=IMAGEACCESSPATH.$product->prod_image_url?>" target="_blank">
									<img src="<?=IMAGEACCESSPATH.$product->prod_image_url?>" style="width:100px;" /></img>
								</a>
							</td>
						</tr> 
						<tr>
							<th>Product Name</th> 
							<td><?=$product->prod_name?></td>
						</tr> 
						<tr>
							<th>Size</th> 
							<td><?=$product->size?></td>
						</tr>
						<tr>
							<th>Price</th> 
							<td><?=$product->price?></td> 
						</tr>
						<tr>
							<th>Quantity</th> 
							<td><?=$product->quantity?></td>
						</tr>
						<tr>
							<th>Amount</th>
							<td><?=$product->amount?></td>
						</tr> 
						<tr>
							<th>GST</th>
							<td><?=$product->sgst_amount+$product->cgst_amount?></td>
						</tr> 
						<?php
							}
						 }
						 else { ?>
							<tr class="warning no-result">
								<td colspan="7"><i class="fa fa-warning"/> No records found...</td>
							</tr>
						<?php } ?> 	
						<!--php loop-->
					</tbody>
				</table>
				<div class="clearfix"></div> 
					</div> 	
					<hr>
					<div class="col-md-9">
						<?=anchor('customer/view_customer_details/'.$cust_id_uri, 'Back', "class='btn btn-lg btn-warning'");?> 
				</div>
				</div> 	
			</div>
		</div>
	</div>
	<style>
		.tableheading {
			font-size: 20px !important;
			font-weight: 600 !important;
			padding: 10px 0 !important;
			color: #31969e !important;
			border-top: 1px solid white !important;
			border-left: 1px solid white !important;
			border-right: 1px solid white !important;
			background-color: white !important;
		}
	</style>