<div id="page-wrapper">
	<div id="page-inner" style="padding:0;">
		<div class="row">
			<div class="col-md-12">
				<div class="agile-grids agile-Updating-grids member_info no_border">
					<div class="col-md-12">  
						<div class="table-heading">
							<h2 style="">Slider Images</h2><hr />
						</div>					
					</div>					
					<div class="col-md-12">  
						<div class="col-md-12">  
							<span class="text-info">Note:- Max Size:300KB | Width:900px | Height:700px </span><br /><br />
						</div>					
					</div>
					<div class="clearfix"></div> 
						<?php foreach($user_silder as $user){?>
							<div class="col-md-4"> 
								<div class="col-md-12" style="min-height:275px;"> 
									<?php 
										if(!empty($user->image_path)) {
											echo "<img src='".IMAGEACCESSPATH.$user->image_path."' class='img-responsive' style='margin-bottom:20px; box-shadow: 2px 2px 3px grey;' />";
										}
										else{
											echo "<img src='".IMAGEACCESSPATH."static/uploads/silder/no-image-available.png' class='img-responsive' style='margin-bottom:20px; box-shadow: 2px 2px 3px grey;' />";
										}
									?>  
								</div>
								<div class="col-md-12"> 
									<button onclick="$('#editSliderimg<?=$user->slider_id?>').slideDown();" class="btn btn-info w3ls-button" style="margin:10px; width:44%; float:left;">Edit Slider</button><div class="clearfix"></div><hr />
								</div>
							</div>
											
							<div id="editSliderimg<?=$user->slider_id?>" style="display:none;">
								<div class="col-md-4">
									<div class="col-md-12">
										<img id="here<?=$user->slider_id?>" src="<?=IMAGEACCESSPATH.$user->image_path?>" style=" box-shadow: 2px 2px 3px grey; margin-bottom:20px;" alt="UPLOADED IMAGE" class="img-responsive" />
										<div class="clearfix"></div>
										<form action="<?=site_url();?>slider/update_slider" enctype="multipart/form-data" method="post">
											<input type="hidden" name="sid" value="<?=$user->slider_id?>">
											<input type='file' id="up<?=$user->slider_id?>" name="slider_img" onchange="previewImage(up<?=$user->slider_id?> ,'here<?=$user->slider_id?>');" style="margin:10px; width:62%; float:left;" />
											<button type="submit" name="submit" value="user" class="btn btn-success w3ls-button" style="margin:10px; width:42%; float:left;">Save Slider</button>
											<a onclick="$('#editSliderimg<?=$user->slider_id?>').hide();" class="btn btn-warning w3ls-button" style="margin:10px; width:42%; float:right;">Cancel</a>
										</form>
									</div>
								</div>
								<div class="clearfix"></div> 
							</div>
											
						<?php } ?>
						
						<!--?php if($i==3){ echo'<div class="clearfix"></div>'; } } ?-->
						<div class="clearfix"></div>
						<hr /> 

				</div> 
			</div>
		</div>
	</div>
</div>	
<style>
	.no_border{
		border:none;
	}
	#addWebSliderImg, #addDrAppSliderImg, #addUserAppSliderImg{
		display:none;
	}
</style> 

 
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
<script>
	//For slider image form opening.
	function addWebSliderImg(){
		$('#addWebSliderImg').slideToggle();
	}
	function addDrAppSliderImg(){
		$('#addDrAppSliderImg').slideToggle();
	}
	function addUserAppSliderImg(){
		$('#addUserAppSliderImg').slideToggle();
	}   
	
	//For image preview before uploading.
	function readURL(input, output) { 
		if (input.files && input.files[0]) {
			var reader = new FileReader(); 

			reader.onload = function (e) {
				$('#'+output).attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	} 
	function previewImage(id, otpt) {   
		readURL(id, otpt);
	}
</script>	


<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="$('.insert_success').slideUp();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
