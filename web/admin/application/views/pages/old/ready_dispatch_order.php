<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>Ready To Dispatch Order Details</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Order ID</th> 
							<th>Customer Name</th> 
							<th>Address</th> 
							<th colspan="4"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						
						<?php  
                           foreach ($ready_order_list as $order)  
                          {  
                        ?>	
						<tr>
							<td><?=$index?></td>
							<td><?=$order->order_id?></td>
							<td><?=$order->first_name .' '.$order->last_name?></td>
							<td><?=$order->address .'-'.$order->pincode?></td>
							<td class="text-center">
								<form action="<?=site_url()?>order/delivered_order" method="POST">
									<button type="submit" class="btn btn-success" name="done" value="<?=$order->order_id?>">Done</button>
								</form>
							</td>
							<td class="text-center">
								<a onclick="orderdetails('order<?=$order->order_id?>');" class="btn btn-info"> View Details </a>
							</td>
							<td class="text-center">
								<form action="<?=site_url()?>order/get_in_process" method="POST">
									<button type="submit" class="btn btn-warning" name="in_process" value="<?=$order->order_id?>">In Process</button>
								</form>
							</td>
							<td class="text-center">
								<form action="<?=site_url()?>order/cancellation_order" method="POST">
									<button type="submit" class="btn btn-danger" name="cancel" value="<?=$order->order_id?>">Cancel Order</button>
								</form>
							</td>
						</tr>
						<tr class="hidden" id="order<?=$order->order_id?>">
							<td colspan="8">
								<br /> 
								<div class="col-sm-6 inpad20">
									<div class="col-sm-6 bld">Customer Name : </div>
									<div class="col-sm-6"><?=$order->first_name .' '.$order->last_name?></div>
									<div class="col-sm-6 bld">Contact No. :</div>
									<div class="col-sm-6"><?=$order->mobile?></div>
									<div class="col-sm-6 bld">Email :</div>
									<div class="col-sm-6"><?=$order->email?></div>
									<div class="col-sm-6 bld">Address :</div>
									<div class="col-sm-6"><?=$order->address?></div> 
								</div>
								<div class="col-sm-6 inpad20"> 
									<div class="col-sm-6 bld"> Order Id :</div>
									<div class="col-sm-6"><?=$order->order_id?></div>
									<div class="col-sm-6 bld">Order Date :</div>
									<div class="col-sm-6"><?=$order->order_date?></div>
									<div class="col-sm-6 bld">Ammount :</div>
									<div class="col-sm-6"><?=$order->amount?></div>
								</div>
								<div class="col-sm-12">
									<a onclick="over('order<?=$order->order_id?>')" class="btn btn-primary">Cancel</a>
									<!--a href="<?=site_url();?>order/view_order_products/<?=$order->order_id?>" class="btn btn-info">View Full Order</a-->
								</div>
							</td>
						</tr>
					   <?php $index++;
						 
						 } ?>	
					</tbody>
				</table>
				<?php echo $this->pagination->create_links();?>
				<!--div class="col-sm-12" style="padding-left:0;"> 
				  <ul class="pagination">
					<li><a href="#">&lt;</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">&gt;</a></li>
				  </ul> 
				</div-->
			</div>
		</div>
		<!-- /. ROW  -->
	</div>
	<!-- /. PAGE INNER  -->
</div>
<style>
	.inpad20 div{margin-bottom:15px;}
	.bld {font-weight:600;}
</style>
<script>
	function orderdetails(order){
		$('#'+order).removeClass('hidden');
	}
	function over(id){
		$('#'+id).addClass('hidden');
	}
</script>