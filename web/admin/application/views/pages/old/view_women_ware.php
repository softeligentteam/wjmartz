
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2> View Product List </h2>
			</div> 		
			<div class="col-md-12">
				<table class="table table-hover table-bordered results">
					<thead>
						<tr>
							<th>Product No.</th>
							<th>Product Image</th>
							<th>Product Name</th>
							<th>Product Price</th>
							<th>Wholesale Price</th>
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody> 
						<?php 
							if(count($product_list)!==0)
							{
								foreach($product_list as $record)
								{
						?> 
							<tr>
								<th>
									<?=$record->prod_id ?>
								</th>
								<td>
									<img src="<?=IMAGEACCESSPATH.$record->image_url?>" style="height:50px;width:50px">
								</td>
								<td>
									<?=$record->prod_name ?>
								</td>
								<td>
									<?=$record->prod_price ?>
								</td>
								<td>
									<?=$record->pwholesale_price ?>
								</td>
								<td>
									<?php echo anchor('products/update_product_women_ware/'.$record->prod_id, 'Update', "class='btn btn-sm btn-primary'");?>
								</td>
								<td>
									<a onclick="$('#delete<?=$record->prod_id?>').show();" class="btn btn-danger">Delete</a>
								</td> 
							</tr>
							<tr style="display:none; text-align:center;" colspan="6" id="delete<?=$record->prod_id?>" >
								<td colspan="7">
									<br />
									<h4>Are you sure you want to delete  <?=$record->prod_name?></h4>
									<br /><br />
									<a class="btn btn-danger" href="<?=site_url();?>products/delete_women_ware/<?=$record->prod_id?>">Yes ! </a>
									<a class="btn btn-info" onclick="$('#delete<?=$record->prod_id?>').hide();">cancel</a>
									<br /><br />
								</td>
							</tr> 
						<?php }  } else { ?>
							<tr class="warning no-result">
								<td colspan="7"><i class="fa fa-warning"/> No records found...</td>
							</tr>
						<?php } ?> 
					</tbody>
				</table>
				<div>
					<ul class="pagination">
					<?php foreach ($links as $link) {
						echo "<li>". $link."</li>";
					} ?>
					<ul>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="$('.insert_success').slideUp();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>



<script src="<?=base_url();?>assets/js/add/product.js"></script>
	
			