<script src="<?=base_url();?>assets/js/add/support_service.js"></script>
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Support Services
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Add New Support Service</a>
				</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div id="addNewSubCat" style="display:none;">
			<?=form_open('supportservices/add_support_services');?>
				<div class="col-md-12">
					<div class="form-group">
						<label>Enter New Support Service Title</label>
						<input class="form-control" name="service_name">
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div> 
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Enter Help Text</label>
						<textarea class="form-control" rows="7" name="service_desc"></textarea>
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div> 
				</div>
				<div class="col-md-12">
					<input type="submit" value="Save" class="btn btn-success" />
					<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">cancel</a>
				</div>
				<div class="clearfix"></div>
				<hr />
			<?=form_close();?>
			</div>
			<div class="col-md-12 text-right"> 
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Support Service</th>
							<th>Help Text</th>
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						<?php  
                           foreach ($services_list as $services)  
                          {  
                        ?>	
						<tr>
							<td><?=$index?></td>
							<td><?=$services->support_service?></td>
							<td><?=$services->service_description?></td>
							<td class="text-center"><a onclick='edit_this("<?=$services->service_id?>","<?=$services->support_service?>","<?=$services->service_description?>");' id="myBtn" class="btn btn-primary">Edit</a></td>
							<td class="text-center"><a onclick="delete_this('<?=$services->service_id?>', '<?=$services->support_service?>');" class="btn btn-danger">Delete</a></td>
						</tr>
					   <?php $index++;
						 
						  } ?>	
					</tbody>
				</table> 
				<?php echo $this->pagination->create_links();?>
			</div>
		</div>
		
		<div id="updateNewService" style="display:none;">
			<?=form_open('supportservices/edit_service');?>
				<div class="col-md-12">
					<div class="form-group">
						<label>Support Service ID</label>
						<input class="form-control" id="ser_id" name="ser_id">
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div> 
					<div class="form-group">
						<label>Support Service Title</label>
						<input class="form-control" id="name" name="name">
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div> 
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Help Text</label>
						<textarea class="form-control" rows="7" id="desc" name="desc"></textarea>
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div> 
				</div>
				<div class="col-md-12">
					<input type="submit" value="Save" class="btn btn-success" />
					<a onclick="$('#updateNewService').slideUp();" class="btn btn-warning">cancel</a>
				</div>
				<div class="clearfix"></div>
				<hr />
			<?=form_close();?>
			</div>
		<!-- /. ROW  -->
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>

<div class="insert_success" id="confirm_delete" style="display:none">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 id="dlt_msg" class="text-center">Are you sure you want to delete</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a href="" id="deleteMAinCatHref" id="" class="btn btn-primary">Yes</a>
					<a onclick="$('#confirm_delete').hide()" id="" class="btn btn-primary">No</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
	
	