<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>Add New Product<a href="<?=site_url();?>products/view_products/" class="btn btn-info pull-right">View All Products</a></h2>
			</div>
		</div>
		<hr />
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-3">
				<h5>Upload Product Image :</h5>
				<div class="panel panel-primary text-center no-boder bg-color-blue">
					<div class="panel-body" id="previewImg" style="min-height:200px; padding:0; text-align:center;">
						<img id="productImagePreview" src="" style="width:100%;" alt="" />
					</div>
					<div class="panel-footer back-footer-blue">
						<input type='file' id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%" /> 
					</div>
				</div> 
			</div>
			<div class="col-md-9">
				<h5>Product Details :</h5> 
				<div class="col-md-12">
					<div class="form-group">
						<label>Enter New Product Name</label>
						<input class="form-control">
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div> 
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Select Parnt Category</label>
						<select class="form-control">
							<option>Select Parent Category...</option>
							<option>Demo</option>
							<option>Demo</option>
							<option>Demo</option>
						</select>
						<p class="help-block" id="selectMainCatError" style="color:red; height:20px;"></p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Select Brand</label>
						<select class="form-control">
							<option>Select Brand...</option>
							<option>Demo</option>
							<option>Demo</option>
							<option>Demo</option>
						</select>
						<p class="help-block" id="selectMainCatError" style="color:red; height:20px;"></p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Enter Price</label>
						<input class="form-control">
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div> 
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Enter Quantity</label>
						<input class="form-control">
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div> 
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label>Enter Product Description</label>
					<textarea class="form-control" rows="7"></textarea>
					<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
				</div> 
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label><input type="checkbox" class="" /> &nbsp;&nbsp;Check To Make This Product Available For Subscription</label><br />
					<label><input type="checkbox" class="" /> &nbsp;&nbsp;Check If Product Is Pune Special</label> 
				</div> 
			</div> 
			<div class="col-md-12">
				<a href="#" class="btn btn-success">Save</a>
				<a onclick="$('#addNewProduct').slideUp();" class="btn btn-warning">cancel</a>
			</div>

		</div>
		<hr>
		<!-- /. ROW  --> 
	</div>
	<!-- /. PAGE INNER  -->
</div>
	