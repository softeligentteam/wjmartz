<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2> Add Product </h2>
				<hr/ >
			</div> 
			<div class="col-md-12"> 
				<?=form_open_multipart('products/add_women_wares_submit', "class='form-horizontal' onsubmit='return validate();'"); ?>
					<div class="col-md-3">
						<label for="brand_name"  >Brand Name: </label>
						<?=form_dropdown('brand_id', $brand_list, set_value('brand_id'), "id='brand_id' class='form-control' ='' autofocus")?>
						<?=form_error('brand_id'); ?><label class="error" id="brand_id_error"></label>
					</div>
					
					<div class="col-md-3"> 
						<label for="child_cat"  >Parent Category: </label> 
						<?=form_dropdown('ppcat_id', $pp_catlist, set_value('ppcat_id'), "id='ppcat_id' class='form-control' =''")?>
					</div>
					
					<div class="col-md-3"> 
						<label for="child_cat"  >Child Category: </label> 
						<?=form_dropdown('pccat_id', $pc_catlist, set_value('pccat_id'), "id='pccat_id' class='form-control' =''")?>
						<?=form_error('pccat_id'); ?><label class="error" id="pccat_id_error"></label>
					</div>
					
					<div class="col-md-3"> 
						<label for="fabric_type"  >Fabric Type: </label> 
						<?=form_dropdown('ftype_id', $ftype_list, set_value('ftype_id'), array('id'=>'ftype_id', 'class'=>'form-control'))?>
						<?=form_error('ftype_id'); ?><label class="error" id="ftype_id_error"></label>
					</div>
					<div class="clearfix"></div>
					
					<div class="col-md-12"><br />
						<label for="product_name"  >Product Name</label> 
						<?=form_input(array('name'=>'pname', 'id'=>'pname', 'value'=>set_value('pname'), 'class'=>'form-control', 'maxlength'=>'50', ''=>'', 'placeholder'=>'Enter Product Name'))?>
						<?=form_error('pname'); ?><label class="error" id="pname_error"></label>
					</div>
					<style>
						.myTxt{
							position:absolute;
							bottom:0;
							right:0;
							margin-right:25%;
							width:75px;
							display:none;
						}
					</style>
					<script>
						function viewInput(id){
							$('#sizeQty'+id).slideToggle();
						}
					</script>
					<div class="col-md-12"><br />
						<label for="product_size">Product Sizes : Quantity</label> <br />
						<hr style="margin:4px 0;"/>
						<?php foreach($size_list as $object){ ?>
							<div class='col-md-3' style='padding:0'>
								
								<?=form_checkbox(array('name'=>'psize[]', 'value'=>$object->size_id, 'onchange'=>'viewInput(this.id);', 'id'=>$object->size_id, 'checked'=>set_checkbox('psize[]', $object->size_id, 'false')));?>

								<?= form_label($object->size)?>
								
								<?=form_input(array('name'=>'sizeQty[]', 'id'=>'sizeQty'.$object->size_id, 'class'=>'myTxt', 'placeholder'=>'Quantity'))?>
								
							</div>
							<?php } ?>
						<div class="clearfix"></div>
						<?=form_error('psize'); ?><label class="error" id="psize_error"></label>
						<hr style="margin:4px 0;"/>
					</div>
					
					<div class="col-md-12"><br />
						<label for="product_color">Product Color: </label> <br />
						<hr style="margin:4px 0;"/>
						<?php 
							foreach($color_list as $object){
								echo "<div class='col-md-3' style='padding:0'>".form_radio(array('name'=>'pcolor', 'value'=>$object->color_id, 'checked'=>set_radio('pcolor', $object->color_id, false)));
								echo form_label($object->color_name).'</div>';
							}
						?>
						<div class="clearfix"></div>
						<?=form_error('pcolor'); ?><label class="error" id="pcolor_error"></label>
						<hr style="margin:4px 0;"/>
					</div>
					
					<div class="col-md-3"><br />
						<label for="product_mrp"  >Product MRP</label> 
						<?=form_input(array('name'=>'pmrp', 'id'=>'pmrp', 'value'=>set_value('pmrp'), 'class'=>'form-control', 'type'=>'number','placeholder'=>'Enter Product MRP'))?>
						<?=form_error('pmrp'); ?><label class="error" id="pmrp_error"></label>
					</div>
					
					<div class="col-md-3"><br />
						<label for="product_price"  >Product Price</label>
						<?=form_input(array('name'=>'pprice', 'id'=>'pprice', 'value'=>set_value('pprice'), 'class'=>'form-control', 'type'=>'number',  'placeholder'=>'Enter Product Price'))?>
						<?=form_error('pprice'); ?><label class="error" id="pprice_error"></label>
					</div>
					
					<div class="col-md-3"><br />
						<label for="pwholesale_price"  >Wholesale Price</label> 
						<?=form_input(array('name'=>'pwprice', 'id'=>'pwprice', 'value'=>set_value('pwprice'), 'class'=>'form-control', 'type'=>'number', 'placeholder'=>'Enter Product Wholesale Price'))?>
						<?=form_error('pwprice'); ?><label class="error" id="pwprice_error"></label>
					</div>
					
					<!--<div class="col-md-3"><br />
						<label for="product_name"  >Product Stock Quantity</label> 
						<?=form_input(array('name'=>'pqty', 'id'=>'paty', 'value'=>set_value('pqty'), 'class'=>'form-control',  ''=>'', 'placeholder'=>'Enter Product Quantity Available'))?>
						<?=form_error('pname'); ?><label class="error" id="paty_error"></label>
					</div>-->
					<div class="clearfix"></div>
					
					<div class="col-md-12"><br />
						<label for="pfitdetails"  >Fit Details</label> 
						<?=form_textarea(array('name'=>'pfitdetails', 'id'=>'pfitdetails', 'value'=>set_value('pfitdetails'), 'class'=>'form-control', 'cols'=>'10', 'rows'=>'5', 'maxlength'=>'2000', 'placeholder'=>'Enter Product Fit Details'))?>
						<?=form_error('pfitdetails'); ?><label class="error" id="pfitdetails_error"></label>
						
					</div>
					
					<div class="col-md-12"><br />
						<label for="pfabricdetails"  >Fabric Details</label>
						<?=form_textarea(array('name'=>'pfabricdetails', 'id'=>'pfabricdetails', 'value'=>set_value('pfabricdetails'), 'class'=>'form-control', 'cols'=>'10', 'rows'=>'5', 'maxlength'=>'2000', 'placeholder'=>'Enter Product Fabric Details'))?>
						<?=form_error('pfabricdetails'); ?><label class="error" id="pfabricdetails_error"></label>
						
					</div>
					
					<div class="col-md-12"><br />
						<label for="pdescription"  >Description</label> 
						<?=form_textarea(array('name'=>'pdesc', 'id'=>'pdesc', 'value'=>set_value('pdesc'), 'class'=>'form-control', 'cols'=>'10', 'rows'=>'5', 'maxlength'=>'2000', 'placeholder'=>'Enter Product Description'))?>
						<?=form_error('pdesc'); ?><label class="error" id="pdesc_error"></label>
						
					</div>
					
					<div class="col-md-12"><br />
						<label for="prod_img"  >Product Images</label><br /><hr style="margin:4px 0;"/><br />
						<?=form_upload(array('name'=>'prod_img1', 'id'=>'prod_img1'))?>
						<?=form_error('prod_img1');?>					
						<?=form_upload(array('name'=>'prod_img2', 'id'=>'prod_img2'))?>
						<?=form_error('prod_img2');?>
						<?=form_upload(array('name'=>'prod_img3', 'id'=>'prod_img3'))?>
						<?=form_error('prod_img3');?>
						<?=form_upload(array('name'=>'prod_img4', 'id'=>'prod_img4'))?>
						<?=form_error('prod_img4');?> 
						<label class="error" id="prod_img_error"></label>
						<span class="text-info"><br />Note:- Max Size:300KB | Width:400px | Height:550px </span>
					</div>
					<div class="col-md-12"><br />
						<?=form_submit('product-submit', 'Add', 'class="btn btn-lg btn-primary"')?>
						<?=form_reset('product-reset', 'Reset', 'class="btn btn-lg btn-warning"')?>
					</div>
				<?=form_close(); ?>  
			</div>
		</div>
	</div>
</div>




<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="$('.insert_success').slideUp();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>














<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
<script> 
	//For image preview before uploading.
	function readURL(input, output) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#'+output).attr('src', e.target.result);
			}

			reader.readAsDataURL(input.files[0]);
		}
	} 
	function previewImage(id, otpt) {
		readURL(id, otpt);
	}
</script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$('#ppcat_id').change(function(){
			var ppcat = $('#ppcat_id option:selected').val();
			if(ppcat!==''){
			$.ajax({
				url: '<?=site_url('product/product_child_cat')?>',
				dataType: 'json', 
				data: {'ppcat': ppcat},
				type: "get",
				success: function(data){
					$('#pccat_id').html('');
					$('#pccat_id').append($("<option/>", {
							value: '',
							text: 'Select Product Child Category'
					}));
					
					$.each(data, function(index, pccat_list){
					   $('#pccat_id').append($("<option/>", {
							value: pccat_list.pchild_cat_id,
							text: pccat_list.pchild_category
						}));
					});
				}
			});
			}
			else{
				$('#pccat_id').html('');
					$('#pccat_id').append($("<option/>", {
						value: '',
						text: 'Select Product Child Category'
				}));
			}
		});
	}); 
</script>

<script>
var validated = true;
$('input').change(function() {
	var id = $(this).attr('id');
	$('#'+id+'_error').html('');
});
$('textarea').change(function() {
	var id = $(this).attr('id');
	$('#'+id+'_error').html('');
});
$('select').change(function() {
	var id = $(this).attr('id');
	$('#'+id+'_error').html('');
});
$('input[type=checkbox]').change(function(){
	$('#psize_error').html('');
});
$('input[type=radio]').change(function(){
	$('#pcolor_error').html('');
});
$('input[type=file]').change(function(){
	$('#prod_img_error').html('');
});
function validate(){
	$('input').each(function() {
		if($(this).attr('id') == 'pname'){
			var id = $(this).attr('id');
			var val = $(this).val();
			if(val == ''){
				$('#'+id+'_error').html('This field cannot be blank.');
				validated = false; 
			}
			else{$('#'+id+'_error').html('');}
		}
		else if($(this).attr('id') == 'pmrp'){
			var id = $(this).attr('id');
			var val = $(this).val();
			if(val == ''){
				$('#'+id+'_error').html('This field cannot be blank.');
				validated = false;
			}
			else if(val%1 != 0){$('#'+id+'_error').html('Input a valid ammount without decimals or other characters');}
			else{$('#'+id+'_error').html('');}
		}
		
		else if($(this).attr('id') == 'pprice'){
			var id = $(this).attr('id');
			var val = $(this).val();
			if(val == ''){
				$('#'+id+'_error').html('This field cannot be blank.');
				validated = false;
			}
			else if(val%1 != 0){$('#'+id+'_error').html('Input a valid ammount without decimals or other characters');}
			else{$('#'+id+'_error').html('');}
		}
		
		else if($(this).attr('id') == 'pwprice'){
			var id = $(this).attr('id');
			var val = $(this).val();
			if(val == ''){
				$('#'+id+'_error').html('This field cannot be blank.');
				validated = false;
			}
			else if(val%1 != 0){$('#'+id+'_error').html('Input a valid ammount without decimals or other characters');}
			else{$('#'+id+'_error').html('');}
		}
		
		else if($(this).attr('id') == 'paty'){
			var id = $(this).attr('id');
			var val = $(this).val();
			if(val == ''){
				$('#'+id+'_error').html('This field cannot be blank.');
				validated = false;
			}
			else if(val%1 != 0){$('#'+id+'_error').html('Input a valid ammount without decimals or other characters');}
			else{$('#'+id+'_error').html('');}
		}
		
	});
	
	
	$('textarea').each(function() {
		var id = $(this).attr('id');
		var val = $(this).val();
		if(val == ''){
			$('#'+id+'_error').html('This field cannot be blank.');
			validated = false; 
		}
		else{$('#'+id+'_error').html('');}
	}); 
	
	
	var psizes = 0;
	$('input[type=checkbox]').each(function () {
		if($(this).prop('checked'))
		{
			psizes = psizes+1;
		}
	}); 
	if(psizes == 0){validated = false; $('#psize_error').html('You must select atleast one product size');}
	else{$('#psize_error').html('');}
	
	
	var pcolor = false;
	$('input[type=radio]').each(function () {
		if($(this).prop('checked'))
		{
			pcolor = true;
		}
	});
	if(pcolor == false){validated = false; $('#pcolor_error').html('You must select one product color');}
	else{$('#pcolor_error').html('');}
	
	
	$('select').each(function () {
		var id = $(this).attr('id');
		var val = $(this).val();
		if(val == ''){
			$('#'+id+'_error').html('You must select a value here.');
			validated = false; 
		}
		else{$('#'+id+'_error').html('');}
	});
	
	var prod_img = 4;
	$('input[type=file]').each(function () { 
		var val = $(this).val();
		if(val == ''){
			prod_img = prod_img - 1
		}
	});
	if(prod_img < 3){validated = false; $('#prod_img_error').html('You must upload atlease 3 images');}
	else{$('#prod_img_error').html('');}
	
	
	return validated;
}
</script>
<style>
.error{
	position: absolute;
    color: red;
    font-size: 12px;
	background: white;
}
</style>