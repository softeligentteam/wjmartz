<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>Customer Details</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Customer ID</th> 
							<th>Customer Name</th> 
							<th>Contact</th> 
							<th>Email</th> 
							<th>Address</th> 
							<th>BirthDate</th> 
						</tr>
					</thead>
					<tbody>
						
						<?php  
                           foreach ($customer_list as $customer)  
                          {  
                        ?>	
						<tr>
							<td><?=$index?></td>
							<td><?=$customer->cust_id?></td>
							<td><?=$customer->first_name .' '.$customer->last_name?></td>
							<td><?=$customer->mobile?></td>
							<td><?=$customer->email?></td>
							<td><?=$customer->address ." ".$customer->area_name ." ".$customer->city ."-". $customer->pincode?></td>
							<td><?=$customer->dob?></td>
						</tr>
					   <?php $index++;
						 
						  } ?>	
					</tbody>
				</table>
				<?php echo $this->pagination->create_links();?>
				<!--div class="col-sm-12" style="padding-left:0;"> 
				  <ul class="pagination">
					<li><a href="#">&lt;</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">&gt;</a></li>
				  </ul> 
				</div-->
			</div>
		</div>
		<!-- /. ROW  -->
	</div>
	<!-- /. PAGE INNER  -->
</div>
	