﻿<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Child Categories
				</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div class="col-md-12"> 
				<div class="col-md-6"> 
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-left" >Add New Child Category</a>
				</div>
				<?=form_open('child_categories/view_all');?>
					<div class="col-md-4">  
						<div class="form-group"> 
							<?=form_dropdown("parent_cat", $parent_cat_list, set_value('parent_cat', $current_parent_cat_id), "tabindex='5' id='parent_cat'  required class='form-control'")?>		
						</div>  
					</div>
					<div class="col-md-2"> 
						<input type="submit" class="btn btn-info" style="width:100%" value="Filter" />
					</div>
				<?=form_close();?>
			</div>
		</div>
		
		<div class="row">
			<div  id="addNewSubCat" style="display:none;">
				<div class="clearfix"></div>
				<hr />
				<?=form_open_multipart('child_categories/add_child_category');?>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							<img id="productImagePreview" src="" style="width:100%;" alt="" />
						</div>
						<div class="panel-footer back-footer-blue">
						<input type='file' name="child_cat_img" id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%" required /> 
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="col-md-12">
						<div class="form-group">
							<label>Select Parent Category</label>
							<?=form_dropdown("parent_cats", $parent_cat_list, set_value('parent_cats', ''), "tabindex='5' id='parent_cats'  required class='form-control'")?>		
							<p class="help-block" id="selectMainCatError" style="color:red; height:20px;"></p>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter New Child Category Name</label>
							<input class="form-control" name="child_category" required />
							<p class="help-block" id="enterSubCatNameError" style="color:red; "></p>
							<label>&lt; Upload Child-Category Image</label>
						</div> 
					</div>
					<div class="col-md-12">
						<input type="submit" value="Save" class="btn btn-success" />
						<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">cancel</a>
					</div>
				</div>
				<div class="clearfix"></div><hr />

				<?=form_close();?>
				<!-- /. ROW  --> 
			</div>
		</div>
		
		<div class="col-md-12">
			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th class="text-center">#</th>
						<th class="text-center">Child Category Image</th>
						<th class="text-center">Child Category Name</th>
						<th colspan="2"><center>Action</center></th>
					</tr>
				</thead>
				<tbody>
					<?php $i=0; foreach($child_cats as $child_cat){ $i++;?>
					<tr>
						<td><?=$child_cat->child_cat_id?></td>
						<td class="text-center"><img src="<?=IMAGEACCESSPATH.$child_cat->child_cat_img_url?>" style="height:70px;" /></td>
						<td><?=$child_cat->child_cat_name?></td>
						<td class="text-center"><a onclick="$('#edit<?=$child_cat->child_cat_id?>').slideDown();" class="btn btn-warning">Edit</a></td>
						<td class="text-center"><a onclick="$('#delete<?=$child_cat->child_cat_id?>').slideDown();" class="btn btn-danger">Delete</a></td>
					</tr>
					<tr>
						<td colspan="5" style="text-align:center; display:none;" id="delete<?=$child_cat->child_cat_id?>">
							<br />
							<h4>Are you sure you want to delete Child Category : <?=$child_cat->child_cat_name?> ?</h4>
							<br /><br />
							<a href="<?=site_url();?>child_categories/delete_child_category/<?=$child_cat->child_cat_id?>" class="btn btn-danger">Yes !</a>
							<a onclick="$('#delete<?=$child_cat->child_cat_id?>').slideUp();" class="btn btn-info">cancel</a>
							<br /><br />
						</td>
					</tr>
					<tr>
						<td colspan="5" style="display:none;" id="edit<?=$child_cat->child_cat_id?>">
							<?=form_open_multipart('child_categories/update_child_category');?>
							<div class="col-md-3 col-sm-3 col-xs-6">
								<div class="panel panel-primary text-center no-boder bg-color-blue">
									<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
										<img id="productImagePreview<?=$child_cat->child_cat_id?>" src="<?=IMAGEACCESSPATH.$child_cat->child_cat_img_url?>" style="width:100%;" alt="" />
									</div>
									<div class="panel-footer back-footer-blue">
									<input type='file' name="child_cat_img" id="productImg<?=$child_cat->child_cat_id?>" onchange="previewImage(productImg<?=$child_cat->child_cat_id?>,'productImagePreview<?=$child_cat->child_cat_id?>');" class="" style="width:100%" /> 
									</div>
								</div>
							</div>
							<div class="col-md-9">
								<div class="col-md-12">
									<div class="form-group">
										<label>Change Parent Category</label>
										<?=form_dropdown("edit_parent_cat", $parent_cat_list, set_value('parent_cats', $child_cat->parent_cat_id), "tabindex='5' id='parent_cats'  required class='form-control'")?>		
										<p class="help-block" id="selectMainCatError" style="color:red; height:20px;"></p>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Change Child Category Name</label>
										<input class="form-control" name="edit_child_category_name" value="<?=$child_cat->child_cat_name?>" required />
										<p class="help-block" id="enterSubCatNameError" style="color:red; "></p>
										<label>&lt; Upload Child-Category Image</label>
									</div> 
								</div>
								<div class="col-md-12">
									<input type="text" hidden value="<?=$child_cat->child_cat_id?>"  class="" name="child_cat_id" />
									<input type="text" hidden value="<?=$child_cat->child_cat_img_url?>"  class="" name="child_cat_img_url" />
									<input type="submit" value="Save" class="btn btn-success" />
									<a onclick="$('#edit<?=$child_cat->child_cat_id?>').slideUp();" class="btn btn-warning">cancel</a>
								</div>
							</div>
							<?=form_close();?>
						</td>
					</tr>
					<?php }?>
				</tbody>
			</table>
			<?php echo $this->pagination->create_links();?>
		</div>
		
			
		<!-- /. ROW  -->
		
		
		
		
		
		
	</div>
	<!-- /. PAGE INNER  -->
</div>
<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/child_category.js"></script>
	