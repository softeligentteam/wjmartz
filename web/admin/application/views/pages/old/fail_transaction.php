<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>Failure Transaction Details</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Ref Order ID</th> 
							<th>Transaction ID</th> 
							<th>Customer Name</th> 
							<th>Tansaction Date</th> 
							<th>Transaction Status</th>
							<th>Failure Reason</th> 
						</tr>
					</thead>
					<tbody>
						
						<?php  if(count($fail_data)>0){
						$i=1;
                           foreach ($fail_data as $fail)  
                          {  
                        ?>	
						<tr>
							<td><?=$i?></td>
							<td><?=$fail->ref_order_id?></td>
							<td><?=$fail->transaction_id?></td>
							<td><?=$fail->first_name .' '.$fail->last_name?></td>
							<td><?=$fail->payment_datetime?></td>
							<td><?=$fail->payment_status?></td>
							<td><?=$fail->trans_reason?></td>
						</tr>
					   <?php $i++;
						 
						}  } ?>	
					</tbody>
				</table>
				<?php echo $this->pagination->create_links();?>
				<!--div class="col-sm-12" style="padding-left:0;"> 
				  <ul class="pagination">
					<li><a href="#">&lt;</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">&gt;</a></li>
				  </ul> 
				</div-->
			</div>
		</div>
		<!-- /. ROW  -->
	</div>
	<!-- /. PAGE INNER  -->
</div>
	