<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12"> 
				<div class="col-md-4"> 
					<h5 style="padding-bottom:0;">Permant Promocode</h5>
				</div>
				<div class="col-md-8">  
					<?=form_open('promocode/search_permant_promocode_details');?>
						<div class="col-md-8">  
							<div class="form-group"> 
								<input type="text" class="form-control" id = "promocode" name = "promocode" placeholder="Enter Promocode Name for search" /> 		
							</div>  
							<button type="submit" class="btn btn-info" style="position: absolute; right: 0; top: 0;">
								<i class="fa fa-search"></i>
							</button>
						</div>   
					<?=form_close();?>
					<div class="col-md-4 py-0">  
						<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Add New Promocode</a>
					</div>
				</div>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr style="margin-top:0;" />
		<div class="row">
			<?=form_open_multipart('promocode/add_subcat_promocode', 'onsubmit="return on_form_submit();"');?>
				<div id="addNewSubCat" style="display:none;">
					<div class="col-md-12">
						<span class="text-info">Note:- Max Size:300KB | Width:700px | Height:400px </span>
						<br /><br />
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="panel panel-primary text-center no-boder bg-color-blue">
							<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
								<img id="PermantPrmoImagePreview" src="" style="width:100%;" alt="" />
							</div>
							<div class="panel-footer back-footer-blue">
							<input type='file' name="subcat_promo_img" id="permant_promo_img" onchange="previewImage(permant_promo_img,'PermantPrmoImagePreview');" class="" style="width:100%" required /> 
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="col-md-12">
							<div class="form-group">
								<label>Enter Child category</label>
									<?=form_dropdown("sub_cat", $sc_catlist, set_value('sub_cat',''), "tabindex='5' id='sub_cat'  required class='form-control'")?>	
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
						</div> 
						<div class="col-md-12">
							<div class="form-group">
								<label>Enter Promocode</label>
								<input class="form-control" name="promocode" id="promo" required onchange="check_promocode();" />
								<p class="help-block" id="errorMessage" style="color:red; height:20px;"></p>
								<p class="error_msg" id="Error-msg-promocode"></p>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Enter Discount</label>
								<input type='number' class="form-control" name="discount" required />
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
						</div> 
						<div class="col-md-12">
							<div class="form-group">
									<label for="pdescription"  >Description</label> 
									<?=form_textarea(array('name'=>'subcat_desc', 'id'=>'permant_desc', 'value'=>set_value('subcat_desc'), 'class'=>'form-control', 'cols'=>'10', 'rows'=>'5', 'maxlength'=>'2000', 'placeholder'=>'Enter Product Description','required'=>'true'))?>
									<?=form_error('permant_desc'); ?><label class="error" id="pdesc_error"></label>
							</div> 
						</div>
						<div class="col-md-12">
							<input type="submit" value="Save" class="btn btn-success" /> 
							<a onclick="location.reload();" class="btn btn-warning">cancel</a>
						</div>
					</div>
					<div class="clearfix"></div>
					<hr />
				</div>
			<?=form_close();?>
			
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Image</th>
							<th>Promocode</th>
							<th>Status</th>
							<th><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						
						<tr>
						<?php  
						if(count($promo_list)!==0)
							{	
							$index=1;
						   foreach ($promo_list as $plist)  
						  {  
							
						?>	
						<tr>
							<td><?=$index?></td>
							<td class="text-center" ><img src="<?=IMAGEACCESSPATH.$plist->promo_image_url?>" style="height:75px"/></td>
							<td><?=$plist->promocode?></td>
							<?php if($plist->status== 1){?>
							<td class="text-success"><b>Active</b></td>
							<?php } else { ?>
							<td class="text-danger"><b>Inactive</b></td>
							<?php } ?>
							<td class="text-center">
								<?php if($plist->status== 0){?>
								<?=form_open('promocode/active_promocode_subcat')?>
								<button type="submit" class="btn btn-success" name="active"value="<?=$plist->promo_id?>">Active</button>
								<?=form_close();?>
								<?php } else{ ?>
								<?=form_open('promocode/inactive_promocode_subcat')?>
								<button type="submit" class="btn btn-warning" name="inactive"value="<?=$plist->promo_id?>">Inactive </button>
								<?=form_close();?>
								<?php } ?>
							</td>
							<!--td class="text-center"><a onclick="$('#delete< ?=$parent_cat->parent_cat_id?>').show();" class="btn btn-danger">Delete</a></td-->
						</tr>
						
					   <?php $index++;
						 
						} }else { ?>
							<tr class="warning no-result">
								<td colspan="7"><i class="fa fa-warning"/> No records found...</td>
							</tr>
						<?php } ?> 
					</tbody>
				</table> 
					<?php echo $this->pagination->create_links();?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>

<div class="insert_success" id="confirm_delete" style="display:none">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 id="dlt_msg" class="text-center">Are you sure you want to delete</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a href="" id="deleteMAinCatHref" id="" class="btn btn-primary">Yes</a>
					<a onclick="$('#confirm_delete').hide()" id="" class="btn btn-primary">No</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/promo.js"></script>
<script src="<?=base_url();?>assets/js/add/parent_category.js"></script>
	
<style>
	input{margin-bottom:7px;}
	.error_msg{
		color: red;
		position: absolute;
		font-size: 12px;
		bottom:0;
	}
</style>