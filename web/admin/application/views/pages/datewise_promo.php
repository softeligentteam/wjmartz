<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h5 style="padding-bottom:0;">
					Datewise Promocode
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Add New Promocode</a>
				</h5>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr style="margin-top:0;" />
		<div class="row">
			<?=form_open_multipart('promocode/add_datewise_promocode');?>
				<div id="addNewSubCat" style="display:none;">
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="panel panel-primary text-center no-boder bg-color-blue">
							<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
								<img id="DateWisePrmoImagePreview" src="" style="width:100%;" alt="" />
							</div>
							<div class="panel-footer back-footer-blue">
							<input type='file' name="datewise_promo_img" id="datewise_promo_img" onchange="previewImage(datewise_promo_img,'DateWisePrmoImagePreview');" class="" style="width:100%" required /> 
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="col-md-12">
							<div class="form-group">
								<label>Select Child Category</label>
									<?=form_dropdown('pccat_id', $pc_catlist, set_value('pccat_id'), "id='pccat_id' class='form-control' =''")?>
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
							<div class="form-group">
								<label>Enter Promocode</label>
								<input class="form-control" name="promocode" required />
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
							<div class="form-group">
								<label>Enter End Date</label>
								  <input type="date" name="date" required/>
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
							<div class="form-group">
								<label>Enter discount</label>
								<input class="form-control" name="discount" required />
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
							<div class="form-group">
									<label for="pdescription"  >Description</label> 
									<?=form_textarea(array('name'=>'datewise_desc', 'id'=>'datewise_desc', 'value'=>set_value('datewise_desc'), 'class'=>'form-control', 'cols'=>'10', 'rows'=>'5', 'maxlength'=>'2000', 'placeholder'=>'Enter Product Description','required'=>'true'))?>
									<?=form_error('pdesc'); ?><label class="error" id="pdesc_error"></label>
							</div> 
						</div>
						<div class="col-md-12">
							<input type="submit" value="Save" class="btn btn-success" /> 
							<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">cancel</a>
						</div>
					</div>
					<div class="clearfix"></div>
					<hr />
				</div>
			<?=form_close();?>
			
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Promocode</th>
							<th>Child Category</th>
							<th>End Date</th>
							<th>Status</th>
							<th><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						
						<tr>
							<?php  
						   foreach ($promo_list as $plist)  
						  {  
							$index=1;
						?>	
						<tr>
							<td><?=$index?></td>
							<td><?=$plist->promocode?></td>
							<td><?=$plist->child_cat_name?></td>
							<td><?=$plist->date?></td>
							<?php if($plist->status== 1){?>
							<td class="text-success"><b>Active</b></td>
							<?php } else { ?>
							<td class="text-danger"><b>Inactive</b></td>
							<?php } ?>
							<td class="text-center">
								<?php if($plist->status== 0){?>
								<?=form_open('promocode/active_promocode_date')?>
								<button type="submit" class="btn btn-success" name="active"value="<?=$plist->promo_id?>">Active</button>
								<?=form_close();?>
								<?php } else{ ?>
								<?=form_open('promocode/inactive_promocode_date')?>
								<button type="submit" class="btn btn-warning" name="inactive"value="<?=$plist->promo_id?>">Inactive </button>
								<?=form_close();?>
								<?php } ?>
							</td>
							<!--td class="text-center"><a onclick="$('#delete< ?=$parent_cat->parent_cat_id?>').show();" class="btn btn-danger">Delete</a></td-->
						</tr>
						
					   <?php $index++;
						 
						} ?>	
					</tbody>
				</table> 
					<?php echo $this->pagination->create_links();?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>

<div class="insert_success" id="confirm_delete" style="display:none">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 id="dlt_msg" class="text-center">Are you sure you want to delete</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a href="" id="deleteMAinCatHref" id="" class="btn btn-primary">Yes</a>
					<a onclick="$('#confirm_delete').hide()" id="" class="btn btn-primary">No</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<script src="<?=base_url();?>assets/js/add/parent_category.js"></script>
	
	