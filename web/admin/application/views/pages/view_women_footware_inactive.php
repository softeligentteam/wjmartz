<link href="<?php echo base_url()?>css/bootstrap-table-style.css" rel='stylesheet' type='text/css' />
<script src="<?php echo base_url()?>js/bootstrap-table-js.js"></script>
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12"> 
				<div class="col-md-4"> 
					<h5 style="padding-bottom:0;">Inactive Women Footware Products</h5>
				</div>
				<div class="col-md-8">   
					<?=form_open('products/search_women_footware_products_inactive');?>
						<div class="col-md-8">  
							<div class="form-group"> 
								<input type="text" class="form-control" id = "product_name" name = "product_name" placeholder="Enter product name for search" /> 		
							</div>  
							<button type="submit" class="btn btn-info" style="position: absolute; right: 0; top: 0;">
								<i class="fa fa-search"></i>
							</button>
						</div>  
					<?=form_close();?>
					<div class="col-md-4 py-0">  
						<form method="post" action="<?=site_url(); ?>/excel_export/export_woman_footware_product_inactive">
							<input type="submit" name="export" class="btn btn-info pull-right" value="Generate Excel Sheet" />
						</form>
					</div>
				</div>
			</div>
		</div>
		<hr style="margin-top: 0;" />
		<div class="row"> 
			<div class="col-md-12">
				<table class="table table-hover table-bordered results">
					<thead>
						<tr>
							<th>Product&nbsp;No.</th>
							<th>Product&nbsp;Image</th>
							<th>Product&nbsp;Name</th>
							<th>Product&nbsp;Price</th>
							<th>Wholesale&nbsp;Price</th>
							<th><center>Action</center></th>
						</tr>
					</thead>
					<tbody> 
						<?php 
							if(count($product_list)!==0)
							{
								foreach($product_list as $record)
								{
						?> 
							<tr>
								<th>
									<?=$record->prod_id ?>
								</th>
								<td>
									<img src="<?=IMAGEACCESSPATH.$record->image_url?>" style="height:50px;width:50px">
								</td>
								<td>
									<?=$record->prod_name ?>
								</td>
								<td>
									<?=$record->prod_price ?>
								</td>
								<td>
									<?=$record->pwholesale_price ?>
								</td>
								<td>
									<a onclick="$('#delete<?=$record->prod_id?>').show();" class="btn btn-danger">Active</a>
								</td> 
							</tr>
							<tr style="display:none; text-align:center;" colspan="6" id="delete<?=$record->prod_id?>" >
								<td colspan="7">
									<br />
									<h4>Are you sure you want to Active  <?=$record->prod_name?></h4>
									<br /><br />
									<a class="btn btn-danger" href="<?=site_url();?>products/active_product_women_footware/<?=$record->prod_id?>">Yes ! </a>
									<a class="btn btn-info" onclick="$('#delete<?=$record->prod_id?>').hide();">Cancel</a>
									<br /><br />
								</td>
							</tr> 
						<?php }  } else { ?>
							<tr class="warning no-result">
								<td colspan="7"><i class="fa fa-warning"/> No records found...</td>
							</tr>
						<?php } ?> 
					</tbody>
				</table>
				<div>
					<ul class="pagination">
					<?php foreach ($links as $link) {
						echo "<li>". $link."</li>";
					} ?>
					<ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="insert_success" id="confirm_delete" style="display:none">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 id="dlt_msg" class="text-center">Are you sure you want to delete</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a href="" id="deleteMAinCatHref" id="" class="btn btn-primary">Yes</a>
					<a onclick="$('#confirm_delete').hide()" id="" class="btn btn-primary">No</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br/>
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br/>
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>		
<?php } ?>
<script src="<?=base_url();?>assets/js/add/product.js"></script>
	
			