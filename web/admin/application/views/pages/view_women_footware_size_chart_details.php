<link href="<?php echo base_url()?>css/bootstrap-table-style.css" rel='stylesheet' type='text/css' />
<script src="<?php echo base_url()?>js/bootstrap-table-js.js"></script>
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12"> 
				<div class="col-md-4"> 
					<h5 style="padding-bottom:0;">Women footware List For Size Chart</h5>
				</div>
				<div class="col-md-8">   
					<?=form_open('products/search_women_footware_products_sizechart');?>
						<div class="col-md-8">  
							<div class="form-group"> 
								<input type="text" class="form-control" id = "product_name" name = "product_name" placeholder="Enter product name for search" /> 		
							</div>  
							<button type="submit" class="btn btn-info" style="position: absolute; right: 0; top: 0;">
								<i class="fa fa-search"></i>
							</button>
						</div>  
					<?=form_close();?>
					<!--<div class="col-md-4 py-0">  
						<form method="post" action="<?=site_url(); ?>/excel_export/export_woman_ware_product">
							<input type="submit" name="export" class="btn btn-info pull-right" value="Generate Excel Sheet" />
						</form>
					</div>-->
				</div>
			</div>
		</div>
		<hr style="margin-top: 0;" />
		<div class="row">
			<div class="col-md-12">
				<table class="table table-hover table-bordered results">
					<thead>
						<tr>
							<th>Product&nbsp;No.</th>
							<th>Product&nbsp;Image</th>
							<th>Product&nbsp;Name</th>
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody> 
						<?php 
							if(count($product_list)!==0)
							{
								foreach($product_list as $record)
								{
						?> 
							<tr>
								<th>
									<?=$record->prod_id ?>
								</th>
								<td>
									<img src="<?=IMAGEACCESSPATH.$record->image_url?>" style="height:50px;width:50px">
								</td>
								<td>
									<?=$record->prod_name ?>
								</td>
								<td class="text-center"><a onclick="$('#add<?=$record->prod_id?>').slideDown();" class="btn btn-success">Update</a></td>
								<!--<td class="text-center"><a onclick="$('#edit<?=$record->prod_id?>').slideDown();" class="btn btn-warning">Edit</a></td>-->
							</tr>
							<tr>
								<td colspan="5" style="display:none;" id="add<?=$record->prod_id?>">
									<?=form_open_multipart('products/update_women_footware_size_chart');?>
									<div class ="col-md-12 text-center">
											<span class="text-info">Note:- Max Size: 300KB | Width: 600px | Height: 900px </span>
									</div>
									<div class ="col-md-4"></div>
									<div class="col-md-4 col-sm-3 col-xs-6">
										<div class="panel panel-primary text-center no-boder bg-color-blue">
											<div class="panel-body" id="previewImgAdd<?=$record->prod_id?>" style="min-height:200px; text-align:center;">
												<img id="prevToAdd<?=$record->prod_id?>" src="<?=IMAGEACCESSPATH.$record->size_chart_image_url?>" style="width:100%;" alt="" />
											</div>
											<div class="panel-footer back-footer-blue">
												<?=form_upload(array('name'=>'size_chart_img', 'value'=>set_value('size_chart_img'), 'id'=>'add_size_chart_img'.$record->prod_id, 'required'=>'required', 'onchange'=>'previewImage(add_size_chart_img'.$record->prod_id.',\'prevToAdd'.$record->prod_id.'\')'))?>
												<?=form_error('size_chart_img');?>
											</div> 
										</div>
										<div class="col-md-12">
											<input type="text" hidden value="<?=$record->prod_id?>"  class="" name="prod_id" />
											<input type="text" hidden value="<?=$record->size_chart_image_url?>"  class="" name="size_chart_image_url" />
											<input type="submit" value="Save" class="btn btn-success" />
											<a onclick="location.reload();" class="btn btn-warning">cancel</a>
										</div>
									</div>
									<div class ="col-md-4"></div>
									<?=form_close();?>
								</td>
							</tr>
						<?php }  } else { ?>
							<tr class="warning no-result">
								<td colspan="7"><i class="fa fa-warning"/> No records found...</td>
							</tr>
						<?php } ?> 
					</tbody>
				</table>
				<?php echo $this->pagination->create_links();?>
			</div>
		</div>
	</div>
</div>
<div class="insert_success" id="confirm_delete" style="display:none">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 id="dlt_msg" class="text-center">Are you sure you want to delete</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a href="" id="deleteMAinCatHref" id="" class="btn btn-primary">Yes</a>
					<a onclick="$('#confirm_delete').hide()" id="" class="btn btn-primary">No</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br/>
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br/>
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>		
<?php } ?>
<script src="<?=base_url();?>assets/js/add/product.js"></script>
	
			