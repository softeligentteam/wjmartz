<div id="page-wrapper">
	<div id="page-inner"> 
		<div class="row">
			<div class="col-md-12">
				<table id="customers">
					<tr>
						<td colspan="4" class="text-center tableheading">
							Single customer Details
						</td>
					</tr> 
					<tr>
						<td>customer_data ID. :</td>
						<td><?= $customer_data->cust_id ?> </td>
						<td>Customer Name :</td>
						<td><?=$customer_data->first_name." ".$customer_data->last_name?> </td>
					</tr>
					<tr>
						<td>Contact No :</td>
						<td><?= $customer_data->mobile ?> </td>
						<td>Email ID :</td>
						<td><?= $customer_data->email ?> </td>
					</tr>
						<?php 
							 if(count($customer_address)>0)
							 {
								
								foreach ($customer_address as $cust_address) 
								{?>
								<tr>
									<td><?=$cust_address->address_type?> Address :</td>
									<td colspan="3"><?=$cust_address->address." ".$cust_address->area_name." ".$cust_address->city."-".$cust_address->pincode?> </td>
								</tr>  
						<?php 	}
							}
						?>
				</table>
				<div class="clearfix"></div>
				<hr />
			</div> 	
		</div> 
		<div class="row">
			<div class="col-md-12">
				<table id="customers">
					<tr>
						<td colspan="6" class="text-center tableheading">
							Order Details
						</td>
					</tr>
					<tr>
						<th>Order&nbsp;Id.</th>
						<th>Order&nbsp;Date</th>
						<th>Order&nbsp;Status</th>
						<th>Transaction&nbsp;Id</th>
						<th>Payment&nbsp;Status</th>
						<th>Promo&nbsp;Code&nbsp;Applied</th>
						<th><center>Action</center></th>
					</tr>
					<?php if(count($order_details)>0)
							 {
								foreach ($order_details as $order) {?>
					<tr>
						<td><?=$order->order_id?></td>
						<td><?=$order->order_date?></td>
						<td><?=$order->order_status?></td>
						<td><?=$order->transaction_id?></td>
						<td><?=$order->payment_status?></td>
						<td><?php if(($order->promocode_applied)==0){
							$promocode= 'Promo Code Not Applied';
							
						}else
						{
							$promocode= $order->promocode;
						}?>
						<?=$promocode?></td>
						<td>
							<center>
								<a href="<?=site_url();?>/customer/view_customer_order_details/<?= $customer_data->cust_id ?>/<?=$order->order_id?>">
									<i class="fa fa-eye istyle" aria-hidden="true"></i>
								</a>
							</center>
						</td>
					</tr>
					<?php }
					}else { ?>
							<tr class="warning no-result">
								<td colspan="7"><i class="fa fa-warning"/> No records found...</td>
							</tr>
						<?php } ?> 	
				</table>
				<div class="clearfix"></div>
				<hr />
				<div class="col-md-9">
						<?=anchor('customer/view_all', 'Back', "class='btn btn-lg btn-warning'");?> 
				</div>
			</div> 	
		</div> 
	</div>
</div>
<style>
#customers {
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
    padding-top: 8px;
    padding-bottom: 8px;
    text-align: left;
    background-color: #31969e;
    color: white;
}
.tableheading {
    font-size: 20px !important;
    font-weight: 600 !important;
    padding: 10px 0 !important;
    color: #31969e !important;
    border-top: 1px solid white !important;
    border-left: 1px solid white !important;
    border-right: 1px solid white !important;
    background-color: white !important;
}
</style>