<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Amain_categories extends CI_Controller 
{
	public function get_main_category_list()
	{
		$this->load->model('Amaincategorymodel');
		$data['main_cat_list']=$this->Amaincategorymodel->get_all_main_categories();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function slider_images()
	{
		$this->db->select('slider_id,image_path');
		$data['sliders']=$this->db->get('slider')->result();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
}
