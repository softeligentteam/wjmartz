<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->output->set_header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
		 if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Ordermodel');
	}
	
	public function view_all()
	{
		$per_page = 15;
		$url = site_url().'order/view_all';
		$number_of_rows = $this->Ordermodel->no_of_rows_for_all_order();
		
		$data['order_list']=$this->Ordermodel->get_all_order_details($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/oder',$data);
		$this->load->view('layouts/footer');
	}
	
	public function get_ready_to_dispatch()
	{
		$order_id=$this->input->post('btn');
		$data['order']=$this->Ordermodel->get_orderidwise_order_details($order_id);
		if($this->input->post())
		{
			$this->Ordermodel->ready_to_dispatch_order($order_id);
			$this->send_email_for_ready_to_dispatch($order_id);
			$this->load->library('sms_gateway');
		    $this->sms_gateway->send_sms_for_order_ready($data['order']->mobile,$order_id,$data['order']->first_name);
			redirect('order/view_all');
		}else{
			redirect('order/view_all');
		}	
	}
	
	private function send_email_for_ready_to_dispatch($order_id)
	{
		$order_data['order']=$this->Ordermodel->get_orderidwise_order_details($order_id);
		$order_data['order_product']=$this->Ordermodel->get_orderwise_products($order_id);
		$message=$this->load->view('email/order_ready',$order_data,TRUE);
		$this->load->library('email_manager');
		$this->email_manager->send_order_ready_email($order_data['order']->email,$message);
	}
	
	public function view_ready_to_dispatch()
	{
		$per_page = 15;
		$url = site_url().'order/view_ready_to_dispatch';
		$number_of_rows = $this->Ordermodel->no_of_rows_for_ready_to_dispatch();
		
		$data['ready_order_list']=$this->Ordermodel->get_ready_to_dispatch_order_details($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/ready_dispatch_order',$data);
		$this->load->view('layouts/footer');
	}
	
	public function cancellation_order()
	{
		$order_id=$this->input->post('cancel');
		$data['order']=$this->Ordermodel->get_orderidwise_order_details($order_id);
		if($this->input->post())
		{

			$this->Ordermodel->cancel_order($order_id);
			$this->send_email_for_order_cancel($order_id);
			$this->load->library('sms_gateway');
			$this->sms_gateway->send_sms_for_order_cancel($data['order']->mobile,$order_id,$data['order']->first_name);
			redirect('order/view_cancelled_order');
		}else{
			redirect('order/view_all');
		}	
	}
	
	public function view_cancelled_order()
	{
		$per_page = 15;
		$url = site_url().'order/view_cancelled_order';
		$number_of_rows = $this->Ordermodel->no_of_rows_for_canelled();
		
		
		$data['cancel_order_list']=$this->Ordermodel->get_cancelled_order_details($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/cancel_order',$data);
		$this->load->view('layouts/footer');
	}
	
	public function delivered_order()
	{
		$order_id=$this->input->post('done');
		$order_data['order']=$this->Ordermodel->get_order_data_for_mail($order_id);
		$order_data['order_product']=$this->Ordermodel->get_orderwise_products($order_id);
		if($this->input->post())
		{
			$this->Ordermodel->delivery_order($order_id);
			$this->load->library('sms_gateway');
			$this->sms_gateway->send_sms_for_order_delivered($order_data['order']->mobile,$order_id,$data['order']->first_name);
			$message=$this->load->view('email/order_delivered',$order_data,TRUE);
			$this->load->library('email_manager');
			$this->email_manager->send_order_delivery_email($order_data['order']->email,$message);
			redirect('order/view_ready_to_dispatch');
		}else{
			redirect('order/view_ready_to_dispatch');
		}	
	}
	
	public function view_delivered_order()
	{
		$per_page = 15;
		$url = site_url().'order/view_delivered_order';
		$number_of_rows = $this->Ordermodel->no_of_rows_for_delivered();
		
		$data['delivered_order_list']=$this->Ordermodel->get_deliverd_order_details($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/delivered_order',$data);
		$this->load->view('layouts/footer');
	}
	
	public function view_order_products()
	{
		$order_id = $this->uri->segment(3,0);
		$data['order']=$this->Ordermodel->get_orderidwise_order_details($order_id);
		$data['order_product']=$this->Ordermodel->get_orderwise_products($order_id);
		
		 if($data['order']!=NULL)
		 {
			 $this->load->view('layouts/header');
			 $this->load->view('pages/view_order_products',$data);
			 $this->load->view('layouts/footer');
		 }else{
			 redirect('order/view_all');
		 }	
		//var_dump($data);
	}
	
	public function view_deliverd_order_products()
	{
		$order_id = $this->uri->segment(3,0);
		$data['order']=$this->Ordermodel->get_orderidwise_order_details($order_id);
		$data['order_product']=$this->Ordermodel->get_orderwise_products($order_id);
		//echo json_encode($data);
		if($data['order']!=NULL)
		{
		$this->load->view('layouts/header');
		$this->load->view('pages/view_delivered_order_products',$data);
		$this->load->view('layouts/footer');
		}else{
			redirect('order/ view_delivered_order');
		}	
	}
	
	public function view_cancelled_order_products()
	{
		$order_id = $this->uri->segment(3,0);
		$data['order']=$this->Ordermodel->get_orderidwise_order_details($order_id);
		$data['order_product']=$this->Ordermodel->get_orderwise_products($order_id);
		//echo json_encode($data);
		if($data['order']!=NULL)
		{
		$this->load->view('layouts/header');
		$this->load->view('pages/view_cancelled_order_products',$data);
		$this->load->view('layouts/footer');
		}else{
			redirect('order/ view_cancelled_order');
		}	
	}
	
	private function send_email_for_order_cancel($order_id)
	{
		$order_data['order']=$this->Ordermodel->get_orderidwise_order_details($order_id);
		$order_data['order_product']=$this->Ordermodel->get_orderwise_products($order_id);
		$message=$this->load->view('email/order_cancel',$order_data,TRUE);
		$this->load->library('email_manager');
		$this->email_manager->send_order_cancel_email($order_data['order']->email,$message);
	}
	
	public function get_in_process()
	{
		$order_id=$this->input->post('in_process');
		if($this->input->post())
		{
			$this->Ordermodel->in_process_order($order_id);
			redirect('order/view_ready_to_dispatch');
		}else{
			redirect('order/view_ready_to_dispatch');
		}	
	}
	
	public function view_failed_order()
	{
		$per_page = 15;
		$url = site_url().'order/view_failed_order';
		$number_of_rows = $this->Ordermodel->no_of_rows_for_failed();
		
		$data['delivered_order_list']=$this->Ordermodel->get_failed_order_details($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/failed_order',$data);
		$this->load->view('layouts/footer');
	}
	
	public function view_failed_order_products()
	{
		$order_id = $this->uri->segment(3,0);
		$data['order']=$this->Ordermodel->get_orderidwise_order_details1($order_id);
		$data['order_product']=$this->Ordermodel->get_orderwise_products1($order_id);
		//echo json_encode($data);
		if($data['order']!=NULL)
		{
		$this->load->view('layouts/header');
		$this->load->view('pages/view_failed_order_products',$data);
		$this->load->view('layouts/footer');
		}else{
			redirect('order/view_failed_order');
		}	
	}
	
	
	//->Alex
	public function view_ready_to_dispatch_order()
	{
		$order_id = $this->uri->segment(3,0);
		$data['order']=$this->Ordermodel->get_orderidwise_order_details($order_id);
		$data['order_product']=$this->Ordermodel->get_orderwise_products($order_id);
		
		 if($data['order']!=NULL)
		 {
			 $this->load->view('layouts/header');
			 $this->load->view('pages/view_ready_to_dispatch_order',$data);
			 $this->load->view('layouts/footer');
		 }else{
			 redirect('order/view_all');
		 }	
	}
	//<-Alex
	//Pooja Jadhav
	public function view_refund_order()
	{
		$per_page = 15;
		$url = site_url().'order/view_refund_order';
		$number_of_rows = $this->Ordermodel->no_of_rows_for_refund();
		
		$data['refund_order_list']=$this->Ordermodel->get_refund_order_list($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/view_refund_order',$data);
		$this->load->view('layouts/footer');
	}
	//Pooja Jadhav
	public function view_refund_order_products()
	{
		$refund_id = $this->uri->segment(3,0);
		$data['order']=$this->Ordermodel->get_refund_details($refund_id);
		$data['refund_order_product']=$this->Ordermodel->get_refund_products_details($refund_id);
		//echo json_encode($data);
		if($data['order']!=NULL)
		{
		$this->load->view('layouts/header');
		$this->load->view('pages/view_refund_order_products',$data);
		$this->load->view('layouts/footer');
		}else{
			redirect('order/view_refund_order');
		}	
	}
	//Pooja Jadhav.
	public function send_email_for_refund_order($refund_id)
	{
		$data['order']=$this->Ordermodel->get_refund_details($refund_id);
		$data['refund_order_product']=$this->Ordermodel->get_refund_products_details($refund_id);
		$message=$this->load->view('email/order_returned_exchanged',$data,TRUE);
		$this->load->library('email_manager');
		$this->email_manager->send_refund_order_email($data['order']->email,$message);
	}
	//Pooja Jadhav
	public function search_inprocess_order()
	{
		$customer_details=$this->input->post('customer_details');
		//$customer = explode(' ',$customer_details);
		if(isset($customer_details) && !empty($customer_details))
		{
			$data['order_list']=$this->Ordermodel->get_search_inprocess_order_details($customer_details);
			 //echo $this->db->last_query();
			$this->load->view('layouts/header');
			$this->load->view('pages/oder',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('order/view_all');
		}
	}
	//Pooja Jadhav
	public function search_ready_to_dispatch_order()
	{
		$customer_details=$this->input->post('customer_details');
		//$customer = explode(' ',$customer_details);
		if(isset($customer_details) && !empty($customer_details))
		{
			$data['ready_order_list']=$this->Ordermodel->get_search_ready_to_dispatch_order_details($customer_details);
			//echo $this->db->last_query();
			$this->load->view('layouts/header');
			$this->load->view('pages/ready_dispatch_order',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('order/view_ready_to_dispatch');
		}
	}
	//Pooja Jadhav
	public function search_delivered_order()
	{
		$customer_details=$this->input->post('customer_details');
		//$customer = explode(' ',$customer_details);
		if(isset($customer_details) && !empty($customer_details))
		{
			$data['delivered_order_list']=$this->Ordermodel->get_search_delivered_order_details($customer_details);
			//echo $this->db->last_query();
			$this->load->view('layouts/header');
			$this->load->view('pages/delivered_order',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('order/view_delivered_order');
		}
	}
	//Pooja Jadhav
	public function search_cancel_order()
	{
		$customer_details=$this->input->post('customer_details');
		//$customer = explode(' ',$customer_details);
		if(isset($customer_details) && !empty($customer_details))
		{
			$data['cancel_order_list']=$this->Ordermodel->get_search_cancel_order_details($customer_details);
			//echo $this->db->last_query();
			$this->load->view('layouts/header');
			$this->load->view('pages/cancel_order',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('order/view_cancelled_order');
		}
	}
	//Pooja Jadhav
	public function search_failed_order()
	{
		$customer_details=$this->input->post('customer_details');
		//$customer = explode(' ',$customer_details);
		if(isset($customer_details) && !empty($customer_details))
		{
			$data['delivered_order_list']=$this->Ordermodel->get_search_failed_order_details($customer_details);
			//echo $this->db->last_query();
			$this->load->view('layouts/header');
			$this->load->view('pages/failed_order',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('order/view_failed_order');
		}
	}
	//Pooja Jadhav 
	public function search_refund_order()
	{
		$customer_details=$this->input->post('customer_details');
		//$customer = explode(' ',$customer_details);
		if(isset($customer_details) && !empty($customer_details))
		{
			$data['refund_order_list']=$this->Ordermodel->get_search_refund_order_details($customer_details);
			$this->load->view('layouts/header');
			$this->load->view('pages/view_refund_order',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('order/view_refund_order');
		}	
	}
}	