<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timeslots extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Ddlmodel');
		$this->load->model('Timeslotsmodel');
	}	
	 
	public function view_timeslots()
	{
		//$per_page = 15;
		//$url = site_url().'/timeslots/view_timeslots';
		//$number_of_rows = $this->Timeslotsmodel->get_no_of_rows();
		
		$data['time_slots'] = $this->Timeslotsmodel->get_timeslots();
		//$data['index'] = $this->uri->segment(3)+1;
		//$this->load->library('get_pagination');
		//$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		//$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/timeslots', $data);
		$this->load->view('layouts/footer');
	}
	
	public function insert_timeslots()
	{
		if($this->input->post())
		{
			$this->Timeslotsmodel->insert_timeslots();
			
			$message = 'Product is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('timeslots/view_timeslots');
		}else{
			$message = 'Product  insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('timeslots/view_timeslots');
		}
	}
	
		
	public function delete_time_slot()
	{
		$time_slot_id=$this->uri->segment(3,0);
		if($this->Timeslotsmodel->del_time_slot($time_slot_id))
		{	
			
			$message = 'Time Slot is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('timeslots/view_timeslots');
		}else{
			$message = 'Time Slot   is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('timeslots/view_timeslots');
		}
	}

	public function edit_time_slot()
	{
		if($this->input->post())
		{	
		if($this->Timeslotsmodel->update_time_slot())
		{	
			
			$message = 'Time Slot is updated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('timeslots/view_timeslots');
		}else{
			$message = 'Time Slotis not updated sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('timeslots/view_timeslots');
		}
		}	
	}
}
