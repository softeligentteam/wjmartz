<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->output->set_header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
		if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Customermodel');
		$this->load->model('Ordermodel');
	}
	
	public function view_all()
	{
		$per_page = 15;
		$url = site_url().'/customer/view_all';
		$number_of_rows = $this->Customermodel->get_no_of_rows();
		$data['customer_list']=$this->Customermodel->get_all_customer_details($per_page);
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/customer',$data);
		$this->load->view('layouts/footer');
	}
	
	public function search_customer_details()
	{
		$customer_details=$this->input->post('customer_details');
		$customer = explode(' ',$customer_details);
		print_r($customer);
		
		if(isset($customer_details) && !empty($customer_details))
		{
			$data['customer_list']=$this->Customermodel->get_search_customer_details($customer);
			$this->load->view('layouts/header');
			$this->load->view('pages/customer',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('customer/view_all');
		}
	}
	
	//Pooja Jadhav
	public function view_customer_details()
	{
		$cust_id = $this->uri->segment(3,0);
		$data['customer_data']=$this->Customermodel->get_customer_details($cust_id);
		$data['customer_address']=$this->Customermodel->get_customer_address($cust_id);
		$data['order_details']=$this->Customermodel->get_customer_order_details($cust_id);
		if($data['customer_data']!=NULL)
		 {
			 $this->load->view('layouts/header');
			 $this->load->view('pages/view_customer_details',$data);
			 $this->load->view('layouts/footer');
		 }else{
			 redirect('customer/view_all');
		 }	
	}
	//Pooja Jadhav
	public function view_customer_order_details()
	{
		$cust_id = $this->uri->segment(3,0);
		$order_id = $this->uri->segment(4,0);
		$data['cust_id_uri'] = $cust_id; 
		$data['order_product_details']=$this->Ordermodel->get_orderwise_products($order_id);
		if($data['order_product_details']!=NULL)
		 {
			$this->load->view('layouts/header');
			$this->load->view('pages/view_customer_order_details',$data);
			$this->load->view('layouts/footer');
		 }else{
			 redirect('customer/view_all');
		 }	
	}
}	