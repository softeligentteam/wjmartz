<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/Product_Controller.php");
class Products extends Product_Controller {
	public function __construct() 
	{
		parent::__construct();
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->output->set_header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
		if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Ddlmodel');
		$this->load->model('Productmodel');
		$this->load->model('Childcategorymodel');
	}	
	 
	public function add_women_wares()
	{
		$this->view_form_women_ware();
	}
	public function add_women_footwares()
	{
		$this->view_form_women_footware();
	}
	
	public function add_women_wares_submit()
	{
		if($this->input->post())
		{	
			if($this->validate_women_ware_form())
			{	
				if($this->Productmodel->add_women_ware_products() == 0)
				{
					$css_class = "text-danger";
					$message = "Product is not added successfully";
				}
				else
				{
					$css_class = "text-success";
					$message = "Product is added successfully";
				}
				$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
				
				redirect('products/add_women_wares');
			}
			else{
				/*$css_class = "text-danger";
				$message = "Form validation occoured, please input valid values !"; 
				$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));*/
				//parent:: add_women_footware(); 
				$this->view_form_women_ware();
			}
		}
	}
	
	
	public function validate_women_ware_form()
	{
		$this->form_validation->set_error_delimiters('<div class="error_msg">', '</div>');
		$this->form_validation->set_rules('brand_id', 'Brand Name', 'required|trim|numeric|max_length[3]');
		$this->form_validation->set_rules('pccat_id', 'Child Category Name', 'required|trim|numeric|max_length[3]');
		$this->form_validation->set_rules('ftype_id', 'Fabric Type', 'required|trim');
		$this->form_validation->set_rules('pname', 'Product Name', 'required|trim|alpha_numeric_spaces|is_unique[products.prod_name]|max_length[50]');
		$this->form_validation->set_rules('psize', 'Product Size', 'callback_validate_psize_checkbox');
		$this->form_validation->set_rules('pcolor', 'Product Color', 'required|trim|numeric|max_length[3]');
		$this->form_validation->set_rules('pmrp', 'Product MRP', 'required|trim');
		$this->form_validation->set_rules('pprice', 'Product Price', 'required|trim');
		$this->form_validation->set_rules('pwprice', 'Product Wholesale Price', 'trim');
		$this->form_validation->set_rules('gst', 'Product GST', 'required|trim');
		//$this->form_validation->set_rules('pfitdetails', 'Product Fit Details', 'required|trim|min_length[10]|max_length[2000]');
		//$this->form_validation->set_rules('pfabricdetails', 'Product Fabric Details', 'required|trim|min_length[10]|max_length[2000]');
		//$this->form_validation->set_rules('pdesc', 'Product Descripton', 'required|trim|min_length[10]|max_length[2000]');
		
		$this->form_validation->set_rules('prod_img1', 'Product Image 1', 'callback_upload_1st_image');
		$this->form_validation->set_rules('prod_img2', 'Product Image 2', 'callback_upload_2nd_image');
		$this->form_validation->set_rules('prod_img3', 'Product Image 3', 'callback_upload_3rd_image');
		$this->form_validation->set_rules('prod_img4', 'Product Image 4', 'callback_upload_4th_image');
		$this->form_validation->set_rules('prod_img5', 'Product Image 5', 'callback_upload_5th_image');
		$this->form_validation->set_rules('prod_img6', 'Product Image 6', 'callback_upload_6th_image');
		return $this->form_validation->run();
	}
	
	public function add_women_footwares_submit()
	{

		if($this->input->post())
		{	
			$data['pname'] = $this->input->post('pname');
			if($this->validate_women_footware_form())
			{	
				if($this->Productmodel->add_women_footware_products() == 0)
				{
					$css_class = "text-danger";
					$message = "Product is not added successfully";
				}
				else
				{
					$css_class = "text-success";
					$message = "Product is added successfully";
				}
				$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
				redirect('products/add_women_footwares');
			}
			else{
				
				/*$css_class = "text-danger";
				$message = "Form validation occoured, please input valid vlues !"; 
				$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));*/
				
				$this->view_form_women_footware($data);
			}
		}
	}
	
	
	public function validate_women_footware_form()
	{
		$this->form_validation->set_error_delimiters('<div class="error_msg">', '</div>');
		$this->form_validation->set_rules('brand_id', 'Brand Name', 'required|trim|numeric|max_length[3]');
		$this->form_validation->set_rules('pccat_id', 'Child Category Name', 'required|trim|numeric|max_length[3]');
		$this->form_validation->set_rules('ftype_id', 'Fabric Type', 'required|trim');
		$this->form_validation->set_rules('pname', 'Product Name', 'required|trim|alpha_numeric_spaces|is_unique[products.prod_name]|max_length[50]');
		$this->form_validation->set_rules('psize', 'Product Size', 'callback_validate_psize_checkbox');
		$this->form_validation->set_rules('pcolor', 'Product Color', 'required|trim|numeric|max_length[3]');
		$this->form_validation->set_rules('pmrp', 'Product MRP', 'required|trim');
		$this->form_validation->set_rules('pprice', 'Product Price', 'required|trim');
		$this->form_validation->set_rules('pwprice', 'Product Wholesale Price', 'trim');
		$this->form_validation->set_rules('gst', 'Product GST', 'required|trim');
		//$this->form_validation->set_rules('pdesc', 'Product Descripton', 'required|trim|min_length[10]|max_length[2000]');
		
		$this->form_validation->set_rules('prod_img1', 'Product Image 1', 'callback_upload_1st_image');
		$this->form_validation->set_rules('prod_img2', 'Product Image 2', 'callback_upload_2nd_image');
		$this->form_validation->set_rules('prod_img3', 'Product Image 3', 'callback_upload_3rd_image');
		$this->form_validation->set_rules('prod_img4', 'Product Image 4', 'callback_upload_4th_image');
		$this->form_validation->set_rules('prod_img5', 'Product Image 5', 'callback_upload_5th_image');
		$this->form_validation->set_rules('prod_img6', 'Product Image 6', 'callback_upload_6th_image');
		
		return $this->form_validation->run();
	}
	
	
	
	public function view_women_ware_products()
	{ 
		
		// Set array for PAGINATION LIBRARY, and show view data according to page.
		$config = array();
		$config["base_url"] = site_url() . "products/view_women_ware_products";
		$config['uri_segment'] =3;
		$config["total_rows"] = $this->Productmodel->get_record_count_women_ware();
		$config["per_page"] = 15;
		$config['use_page_numbers'] = FALSE;
		$config['num_links'] = 15;
		$config['cur_tag_open'] = '&nbsp;<a style="background-color:rgba(32, 81, 135, 0.18)" class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		
		$data['product_list'] = $this->Productmodel->fetch_product_list_women_ware($config["per_page"], $this->uri->segment(3,0));
		$str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
		$this->view_women_wares($data);
	}
	
	public function view_women_ware_products_inactive()
	{ 
		
		// Set array for PAGINATION LIBRARY, and show view data according to page.
		$config = array();
		$config["base_url"] = site_url() . "products/view_women_ware_products_inactive";
		$config['uri_segment'] =3;
		$config["total_rows"] = $this->Productmodel->get_record_count_women_ware_inactive();
		$config["per_page"] = 15;
		$config['use_page_numbers'] = FALSE;
		$config['num_links'] = 15;
		$config['cur_tag_open'] = '&nbsp;<a style="background-color:rgba(32, 81, 135, 0.18)" class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		
		$data['product_list'] = $this->Productmodel->fetch_product_list_women_ware_inactive($config["per_page"], $this->uri->segment(3,0));
		$str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
		$this->view_women_wares_inactive($data);
	}
	
	public function view_women_footware_products_inactive()
	{ 
		
		// Set array for PAGINATION LIBRARY, and show view data according to page.
		$config = array();
		$config["base_url"] = site_url() . "products/view_women_footware_products_inactive";
		$config['uri_segment'] =3;
		$config["total_rows"] = $this->Productmodel->get_record_count_women_footware_inactive();
		$config["per_page"] = 15;
		$config['use_page_numbers'] = FALSE;
		$config['num_links'] = 15;
		$config['cur_tag_open'] = '&nbsp;<a style="background-color:rgba(32, 81, 135, 0.18)" class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		
		$data['product_list'] = $this->Productmodel->fetch_product_list_women_footware_inactive($config["per_page"], $this->uri->segment(3,0));
		$str_links = $this->pagination->create_links();
		$data["links"] = explode('&nbsp;',$str_links );
		$this->view_women_footwares_inactive($data);
	}
	public function view_women_footware_products()
	{ 
		$per_page = 15;
		$url=site_url()."/products/view_women_footware_products";
		$number_of_rows=$this->Productmodel->get_record_count_women_footware();
		$data['product_list'] = $this->Productmodel->fetch_product_list_women_footware($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url,$number_of_rows,$per_page);
		$this->pagination->initialize($config);
		$this->view_women_footwares($data);	  
	}
	
	
	public function validate_psize_checkbox()
	{
		$psize_array = $this->input->post('psize[]');
		if(count($psize_array)==0){
			$this->form_validation->set_message('validate_psize_checkbox', 'The Product Size field is required');
			return FALSE;
		}
		return TRUE;
	}
	/* public function validate_pslotid_checkbox()
	{
		$pslotid_array = $this->input->post('pslotid[]');
		$sizeQty_array = $this->input->post('sizeQty[]');
		if(count($pslotid_array)==0){
			return true;
			if(count($pslotid_array)==0)
			{
				return true;
			}
			$this->form_validation->set_message('validate_psize_checkbox', 'The Product Size field is required');
			return FALSE;
		}
		return TRUE;
	} */
	
	/******************** Upload 1st Image *******************/
	function upload_1st_image()
	{
		$index = 1;
		return $this->validate_and_upload($index, 'upload_1st_image');
	}
	
	/******************** Upload 2nd Image *******************/
	function upload_2nd_image()
	{
		$index = 2;
		return $this->validate_and_upload($index, 'upload_2nd_image');
	}
	
	/******************** Upload 3rd Image *******************/
	function upload_3rd_image()
	{
		$index = 3;
		if(empty($_FILES['prod_img'.$index]['name'])) 
		{
			return TRUE;
        }
		return $this->validate_and_upload($index, 'upload_3rd_image');
	}
	
	/******************** Upload 4th Image *******************/
	function upload_4th_image()
	{
		$index = 4;
		if(empty($_FILES['prod_img'.$index]['name'])) 
		{
			return TRUE;
        }
		return $this->validate_and_upload($index, 'upload_4th_image');
	}
	/******************** Upload 5th Image *******************/
	function upload_5th_image()
	{
		$index = 5;
		if(empty($_FILES['prod_img'.$index]['name'])) 
		{
			return TRUE;
        }
		return $this->validate_and_upload($index, 'upload_5th_image');
	}

	/******************** Upload 6th Image *******************/
	function upload_6th_image()
	{
		$index = 6;
		if(empty($_FILES['prod_img'.$index]['name'])) 
		{
			return TRUE;
        }
		return $this->validate_and_upload($index, 'upload_6th_image');
	}
	
	
	
	/******************** Validate and Upload Product Images *******************/
	function validate_and_upload($index, $msg_name)
	{
		if (empty($_FILES['prod_img'.$index]['name'])) 
		{
			$this->form_validation->set_message($msg_name, 'Please select Product image');
            return FALSE;
        }
		else
		{
			$config['upload_path'] = IMAGEBASEPATH.PRODUCTUPLOADPATH;
			$config['allowed_types'] = 'png|jpg|jpeg';
			$config['max_size'] = '300';
			$config['min_width'] = '970'; $config['min_height'] = '800';
			$config['max_width'] = '970'; $config['max_height'] = '800';
			$config['encrypt_name'] = TRUE;
			$config['override'] = FALSE;
			
			$this->load->library('upload');
			$this->upload->initialize($config);
			if (!is_dir($config['upload_path'])) 
			{
				mkdir($config['upload_path'], 0777, TRUE);
				echo "<script>console.log('created.');</script>";
			}
			
			if($this->upload->do_upload('prod_img'.$index))
            {
                $upload_data = $this->upload->data();
                $_POST['prod_img'.$index] = PRODUCTUPLOADPATH . $upload_data['file_name'];
				echo "<script>console.log('Sucess.');</script>";
                return TRUE;
            }
            else
            {
                $this->form_validation->set_message($msg_name, $this->upload->display_errors());
                return FALSE;
            }
        }
	}
	
	public function update_product_women_footware()
	{
		//$product_id = $this->input->post('pid');
		$product_id=$this->uri->segment(3,0);
		//echo $product_id;
		if($this->input->post('uproduct-submit'))
		{
			if($this->validate_uproduct_women_footware_form())
			{
				if($this->Productmodel->update_product_women_footware() == 0)
				{
					$css_class = "text-danger";
					$message = "Product is not updated successfully";
					$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
					redirect(current_url());
				}
				else
				{
					$css_class = "text-success";
					$message = "Product are updated successfully";
					$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message, 'display' => 'block'));
					redirect('products/view_women_footware_products');
				} 
			}
			else
			{
				$data['product'] = $this->Productmodel->get_product2update_footware($this->input->post('pid'));
				$product_id = $this->input->post('pid');
				if($data['product'] == NULL)
				{
					redirect('products/view_women_footware_products');
				}
				//$data['size_list'] = $this->Ddlmodel->get_psize_list_footware();
				$data['size_list'] = $this->Productmodel->get_psize_qty_list_footware($product_id);
				$get_size_id = $this->Productmodel->get_sizeid_list_footware($product_id);
				$it =  new RecursiveIteratorIterator(new RecursiveArrayIterator($get_size_id));
				$size_id = iterator_to_array($it, false);
				$data['size_list_not_in_qtyslot'] = $this->Productmodel->get_psize_not_in_qty_slot_footware($size_id);
				$this->view_update_women_footware($data);
			}
		}
		else if($this->input->post('uproduct-submiti'))
		{
			
			if($this->validate_uproduct_women_footware_images())
			{
				$images_row = $this->Productmodel->update_product_images();
				if($images_row == NULL)
				{
					$css_class = "text-danger";
					$message = "Product images are not uploaded successfully";
					$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message, 'display' => 'block'));
					redirect(current_url());
				}
				else
				{
					$css_class = "text-success";
					$message = "Product images are uploaded successfully";
					$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message, 'display' => 'block'));
					$this->delete_women_ware_product_images($images_row);
					redirect('products/view_women_footware_products');
				}
			}
			else
			{
				$data['product'] = $this->Productmodel->get_product2update_footware($this->input->post('pid'));
				
				if($data['product'] == NULL)
				{
					redirect('products/view_women_footware_products');
				}
				$data['display'] = 'block';
				//$data['size_list'] = $this->Ddlmodel->get_psize_list_footware();
				$data['size_list'] = $this->Productmodel->get_psize_qty_list_footware($product_id);
				$get_size_id = $this->Productmodel->get_sizeid_list_footware($product_id);
				$it =  new RecursiveIteratorIterator(new RecursiveArrayIterator($get_size_id));
				$size_id = iterator_to_array($it, false);
				$data['size_list_not_in_qtyslot'] = $this->Productmodel->get_psize_not_in_qty_slot_footware($size_id);
				$this->view_update_women_footware($data);
			}
		}
		else if($this->input->post('insert-submitSize'))
		{
			 if($this->Productmodel->add_more_size_when_update_footware($this->input->post('pid')) == 0)
			 //echo $this->db->last_query();
				 {
					echo $this->db->last_query();
					$css_class = "text-danger";
					$message = "Product Size and Quantity is not added successfully";
					$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
					redirect('products/view_women_footware_products');
				}
				else
				{
					$css_class = "text-success";
					$message = "Product Size and Quantity is added successfully";
					$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
					redirect('products/view_women_footware_products');
				}  
		} 
		else
		{
			$product_id = $this->uri->segment(3,0);
			$data['product'] = $this->Productmodel->get_product2update_footware($product_id);
			
			if($data['product'] == NULL)
			{
				redirect('products/view_women_footware_products');
			}
			//$data['size_list'] = $this->Ddlmodel->get_psize_list_footware();
				$data['size_list'] = $this->Productmodel->get_psize_qty_list_footware($product_id);
				$get_size_id = $this->Productmodel->get_sizeid_list_footware($product_id);
				$it =  new RecursiveIteratorIterator(new RecursiveArrayIterator($get_size_id));
				$size_id = iterator_to_array($it, false);
				$data['product_for'] = $this->Ddlmodel->get_product_for_list_footware();

				$data['size_list_not_in_qtyslot'] = $this->Productmodel->get_psize_not_in_qty_slot_footware($size_id);
				$this->view_update_women_footware($data);
		}
	}
	
	public function update_product_women_ware()
	{
		$product_id=$this->uri->segment(3,0);
		if($this->input->post('uproduct-submit'))
		{
			if($this->validate_uproduct_women_ware_form())
			{
				if($this->Productmodel->update_product_women_ware() == 0)
				{
					$css_class = "text-danger";
					$message = "Product is not updated successfully";
					$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
					redirect(current_url());
				}
				else
				{
					$css_class = "text-success";
					$message = "Product is updated successfully";
					$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
					redirect('products/view_women_ware_products');
				}
			}
			else
			{
				$data['product'] = $this->Productmodel->get_product2update($this->input->post('pid'));
				if($data['product'] == NULL)
				{
					redirect('products/view_women_ware_products');
				}
				//$data['size_list'] = $this->Productmodel->get_psize_list();
				$data['size_list'] = $this->Productmodel->get_psize_qty_list($product_id);
				$get_size_id = $this->Productmodel->get_sizeid_list($product_id);
				$it =  new RecursiveIteratorIterator(new RecursiveArrayIterator($get_size_id));
				$size_id = iterator_to_array($it, false);
				$data['size_list_not_in_qtyslot'] = $this->Productmodel->get_psize_not_in_qty_slot($size_id);
				$this->view_update_women_ware($data);
			}
		}
		else if($this->input->post('uproduct-submiti'))
		{
			
			if($this->validate_uproduct_women_ware_images())
			{
				$images_row = $this->Productmodel->update_product_images();
				if($images_row == NULL)
				{
					$css_class = "text-danger";
					$message = "Product images are not uploaded successfully";
					$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message, 'display' => 'block'));
					redirect(current_url());
				}
				else
				{
					$this->delete_women_ware_product_images($images_row);
					redirect('products/view_women_ware_products');
				}
			}
			else
			{
				$data['product'] = $this->Productmodel->get_product2update($this->input->post('pid'));
				
				if($data['product'] == NULL)
				{
					redirect('products/view_women_ware_products');
				}
				$data['display'] = 'block';
				//$data['size_list'] = $this->Productmodel->get_psize_list();
				$data['size_list'] = $this->Productmodel->get_psize_qty_list($product_id);
				$get_size_id = $this->Productmodel->get_sizeid_list($product_id);
				$it =  new RecursiveIteratorIterator(new RecursiveArrayIterator($get_size_id));
				$size_id = iterator_to_array($it, false);
				$data['size_list_not_in_qtyslot'] = $this->Productmodel->get_psize_not_in_qty_slot($size_id);
				$this->view_update_women_ware($data);
			}
		}
		 else if($this->input->post('insert-submitSize'))
		{
			 if($this->Productmodel->add_more_size_when_update($this->input->post('pid')) == 0)
			 //echo $this->db->last_query();
				 {
					echo $this->db->last_query();
					$css_class = "text-danger";
					$message = "Product Size and Quantity is not added successfully";
					$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
					redirect('products/update_product_women_ware');
				}
				else
				{
					$css_class = "text-success";
					$message = "Product Size and Quantity is added successfully";
					$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
					redirect('products/update_product_women_ware');
				}  
		} 
		else
		{
			$data['product'] = $this->Productmodel->get_product2update($this->uri->segment(3,0));
			
			if($data['product'] == NULL)
			{
				redirect('products/view_women_ware_products');
			}
			//$data['size_list'] = $this->Productmodel->get_psize_list();
			$data['size_list'] = $this->Productmodel->get_psize_qty_list($product_id);
			
			$get_size_id = $this->Productmodel->get_sizeid_list($product_id);
			$it =  new RecursiveIteratorIterator(new RecursiveArrayIterator($get_size_id));
			$size_id = iterator_to_array($it, false);
			$data['size_list_not_in_qtyslot'] = $this->Productmodel->get_psize_not_in_qty_slot($size_id);
			echo $this->db->last_query();
			$this->view_update_women_ware($data);
		}
	}
	
	
	/********************** Delete Product Images ******************/
	private function delete_women_ware_product_images($pimage_row)
	{
		$image_urls = $pimage_row->prod_image_url.','.$pimage_row->prod_image_urls;
		$pimage_list = explode(',',$image_urls);
		foreach($pimage_list as $pimage_url){
			if(file_exists(IMAGEBASEPATH.$pimage_url)){
				unlink(IMAGEBASEPATH.$pimage_url);
			}
		}
	}
	
	public function validate_uproduct_women_ware_form() 
	{
		$this->form_validation->set_error_delimiters('<div class="error_msg">', '</div>');
		//$this->form_validation->set_rules('pslotid', 'Product Size', 'callback_validate_update_psize_checkbox');
		//$this->form_validation->set_rules('pfitdetails', 'Product Fit Details', 'required|trim|min_length[10]|max_length[2000]');
		//$this->form_validation->set_rules('pfabricdetails', 'Product Fabric Details', 'required|trim|min_length[10]|max_length[2000]');
		$this->form_validation->set_rules('pmrp', 'Product MRP', 'required|trim');
		$this->form_validation->set_rules('pprice', 'Product Price', 'required|trim');
		$this->form_validation->set_rules('pwprice', 'Product Wholesale Price', 'trim');
		$this->form_validation->set_rules('gst', 'Product GST', 'required|trim');
		//$this->form_validation->set_rules('pdesc', 'Product Descripton', 'required|trim|min_length[10]|max_length[2000]');
		$this->form_validation->set_rules('in_stock', 'In Stock', 'required|trim|greater_than_equal_to[0]|less_than_equal_to[1]');
		return $this->form_validation->run();
	}
	
	private function validate_uproduct_women_ware_images()
	{
		$this->form_validation->set_error_delimiters('<div class="error_msg">', '</div>');
		$this->form_validation->set_rules('prod_img1', 'Product Image 1', 'callback_upload_1st_image');
		$this->form_validation->set_rules('prod_img2', 'Product Image 2', 'callback_upload_2nd_image');
		$this->form_validation->set_rules('prod_img3', 'Product Image 3', 'callback_upload_3rd_image');
		$this->form_validation->set_rules('prod_img4', 'Product Image 4', 'callback_upload_4th_image');
		$this->form_validation->set_rules('prod_img5', 'Product Image 5', 'callback_upload_5th_image');
		$this->form_validation->set_rules('prod_img6', 'Product Image 6', 'callback_upload_6th_image');
		return $this->form_validation->run();
	}
	
	public function validate_uproduct_women_footware_form() 
	{
		$this->form_validation->set_error_delimiters('<div class="error_msg">', '</div>');
		$this->form_validation->set_rules('pmrp', 'Product MRP', 'required|trim');
		$this->form_validation->set_rules('pprice', 'Product Price', 'required|trim');
		$this->form_validation->set_rules('pwprice', 'Product Wholesale Price', 'trim');
		$this->form_validation->set_rules('gst', 'Product GST', 'required|trim');
		//$this->form_validation->set_rules('pdesc', 'Product Descripton', 'required|trim|min_length[10]|max_length[2000]');
		$this->form_validation->set_rules('in_stock', 'In Stock', 'required|trim|greater_than_equal_to[0]|less_than_equal_to[1]');
		return $this->form_validation->run();
	}
	
	private function validate_uproduct_women_footware_images()
	{
		$this->form_validation->set_error_delimiters('<div class="error_msg">', '</div>');
		$this->form_validation->set_rules('prod_img1', 'Product Image 1', 'callback_upload_1st_image');
		$this->form_validation->set_rules('prod_img2', 'Product Image 2', 'callback_upload_2nd_image');
		$this->form_validation->set_rules('prod_img3', 'Product Image 3', 'callback_upload_3rd_image');
		$this->form_validation->set_rules('prod_img4', 'Product Image 4', 'callback_upload_4th_image');
		$this->form_validation->set_rules('prod_img5', 'Product Image 5', 'callback_upload_5th_image');
		$this->form_validation->set_rules('prod_img6', 'Product Image 6', 'callback_upload_6th_image');
		return $this->form_validation->run();
	}
	
	public function delete_women_ware()
	{
		$prod_id=$this->uri->segment(3,0);
		if($this->Productmodel->del_product($prod_id))
		{	
			
			$message = 'Product is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('products/view_women_ware_products');
		}else{
			$message = 'Product is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('products/view_women_ware_products');
		}
	}
	
	public function delete_women_footware()
	{
		$prod_id=$this->uri->segment(3,0);
		if($this->Productmodel->del_product($prod_id))
		{	
			
			$message = 'Product is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('products/view_women_footware_products');
		}else{
			$message = 'Product is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('products/view_women_footware_products');
		}
	}
	
	public function active_product_women_ware()
	{
		$id=$this->uri->segment(3,0);
		if($this->Productmodel->activate_products($id))
		{	
			
			$message = 'Product is Activated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('products/view_women_ware_products_inactive');
		}else{
			$message = 'Product Activation Failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('products/view_women_ware_products_inactive');
		}
	}
	
	public function active_product_women_footware()
	{
		$id=$this->uri->segment(3,0);
		if($this->Productmodel->activate_products($id))
		{	
			
			$message = 'Product is Activated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('products/view_women_footware_products_inactive');
		}else{
			$message = 'Product Activation Failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('products/view_women_footware_products_inactive');
		}
	}
	//pooja Jadhav
	public function view_deals_of_the_day()
	{
		$data['child_cats_list']=$this->Ddlmodel->get_child_cat_list();
		$data['parent_cats_list']=$this->Ddlmodel->get_parent_category_list();
		if($this->input->post()){
			$data['current_parent_cat_id'] = $this->input->post('parent_cats');
		}
		else{
			$data['current_parent_cat_id'] = 0;
		}
		if($this->input->post()){
			$data['current_child_cat_id'] = $this->input->post('chlid_cats');
		}
		else{
			$data['current_child_cat_id'] = 0;
		}
		
		
		$data['deals_of_the_day_data']=$this->Productmodel->get_deals_of_the_day();
				
		$this->load->view('layouts/header');
		$this->load->view('pages/view_todays_deals',$data);
		$this->load->view('layouts/footer');
	}
	//pooja Jadhav
	public function get_product_list()
	{
		$child_cat_id = $this->input->get('child_cats');
		$parent_cat_id = $this->input->get('parent_cats');
		$data = $this->Ddlmodel->get_child_cat_wise_product_list($child_cat_id,$parent_cat_id);
		echo json_encode($data);
	} 
	//Pooja Jadhav	
	public function add_deals_of_the_day()
	{
		/*  echo json_encode($_POST);
		$data=$this->upload_deal_of_day_photo();
		print_r($data); */
		if($this->input->post())
		{
			if($this->upload_deal_of_day_photo())
			
				{
					$this->Productmodel->add_Today_deals();
					$css_class = "text-success";
					$message = "Deals of the day is added successfully";
					
				}else{
					$css_class = "text-danger";
					$message = "Failed to add deal of day image";
				}
		}		else
				{
					$css_class = "text-danger";
					$message = "Failed to add Deals of the day ";
				}
				$this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
				redirect('products/view_deals_of_the_day');
	}
	//Pooja Jadhav
	public function delete_deals()
	{
		$deal_id=$this->uri->segment(3,0);
		$prod_id=$this->uri->segment(4,0);
		if(($this->Productmodel->delete_deals_of_the_day($deal_id)) && ($this->Productmodel->update_deals_status($prod_id)))
		{	
			
			$message = 'Deals of the day is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('products/view_deals_of_the_day');
		}else{
			$message = 'Failed to delete deal of the day';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('products/view_deals_of_the_day');
		}

	}
	//Pooja Jadhav
	public function search_women_ware_products()
	{
		$product_name=$this->input->post('product_name');
		if(isset($product_name) && !empty($product_name))
		{
			$data['product_list']=$this->Productmodel->get_ware_product_search_details($product_name);
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
			$this->view_women_wares($data);
		}
		else
		{
			redirect('products/view_women_wares');
		}
	}
	//Pooja Jadhav
	public function search_women_ware_products_inactive()
	{
		$product_name=$this->input->post('product_name');
		if(isset($product_name) && !empty($product_name))
		{
			$data['product_list']=$this->Productmodel->get_ware_product_inactive_search_details($product_name);
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
			$this->view_women_wares_inactive($data);
		}
		else
		{
			redirect('products/view_women_wares_inactive');
		}
	}
	
	//Pooja Jadhav
	public function search_women_footware_products()
	{
		$product_name=$this->input->post('product_name');
		if(isset($product_name) && !empty($product_name))
		{
			$data['product_list']=$this->Productmodel->get_footware_product_search_details($product_name);
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
			$this->view_women_footwares($data);
		}
		else
		{
			redirect('products/view_women_footwares');
		}
	}
	//Pooja Jadhav
	public function search_women_footware_products_inactive()
	{
		$product_name=$this->input->post('product_name');
		if(isset($product_name) && !empty($product_name))
		{
			$data['product_list']=$this->Productmodel->get_footware_product_inactive_search_details($product_name);
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
			$this->view_women_footwares_inactive($data);
		}
		else
		{
			redirect('products/view_women_footwares_inactive');
		}
	}
	//Pooja Jadhav
	public function view_women_ware_size_chart()
	{
		$per_page = 15;
		$url = site_url().'products/view_women_ware_size_chart';
		$total_rows = $this->Productmodel->get_record_count_women_ware();
		
		$data['product_list']=$this->Productmodel->fetch_product_list_women_ware_sizechart($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $total_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/view_women_ware_size_chart_details',$data);
		$this->load->view('layouts/footer');
	}
	//Pooja Jadhav 
	public function view_women_footware_size_chart()
	{
		
		$per_page = 15;
		$url = site_url().'products/view_women_footware_size_chart';
		$total_rows = $this->Productmodel->get_record_count_women_footware();
		
		$data['product_list']=$this->Productmodel->fetch_product_list_women_footware_sizechart($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $total_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/view_women_footware_size_chart_details',$data);
		$this->load->view('layouts/footer');
	}
	//Pooja Jadhav 
	public function update_women_ware_size_chart()
	{
		if($this->input->post())
		{	
			$size_image= $this->Productmodel->get_size_chart_image_path();
			if($this->validate_sizechart_form())
			{
				//echo "sucessfully";
				$this->Productmodel->update_size_chart();
				$this->delete_size_chart_images($size_image);
				$message = 'Size chart uploaded sucessfully';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('products/view_women_ware_size_chart');
			}else{
				$message = 'Image upload Failed.Please upload proper image ';
				$css_class = 'text-alert';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('products/view_women_ware_size_chart');
			}
		}
	}
	//Pooja Jadhav
	public function update_women_footware_size_chart()
	{
		if($this->input->post())
		{	
			$size_image= $this->Productmodel->get_size_chart_image_path();
			if($this->validate_sizechart_form())
			{
				$this->Productmodel->update_size_chart();
				$this->delete_size_chart_images($size_image);
				$message = 'Size chart uploaded sucessfully';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('products/view_women_footware_size_chart');
			}else{
				$message = 'Image upload Failed.Please upload proper image';
				$css_class = 'text-alert';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('products/view_women_footware_size_chart');
			}
		}
	}
	//Poooja Jadhav
	public function validate_sizechart_form()
	{
		$this->form_validation->set_error_delimiters('<div class="error_msg">', '</div>');
		$this->form_validation->set_rules('size_chart_img', 'Size Chart Image', 'callback_validate_and_upload_sizechart');
		return $this->form_validation->run();
	}
	//Poooja Jadhav
	function upload_sizechart_image()
	{
		//echo"hiiii...........";
		return $this->validate_and_upload_sizechart('upload_sizechart_image');
	}
	//KT[18-04-18]
	function validate_and_upload_sizechart()
	{
		if (empty($_FILES['size_chart_img'])) 
		{
			$this->form_validation->set_message('size_chart_img', 'Please select product image');
            return FALSE;
        }
		else
		{
			$config['upload_path'] = IMAGEBASEPATH.SIZEIMAGEUPLOAD;
			$config['allowed_types'] = 'png|jpg|jpeg';
			$config['max_size'] = '300';
			$config['min_width'] = '599'; 
			$config['min_height'] = '10';
			$config['max_width'] = '600'; 
			$config['max_height'] = '900';
			$config['encrypt_name'] = TRUE;
			$config['override'] = FALSE;
			
			$this->load->library('upload');
			$this->upload->initialize($config);
			if (!is_dir($config['upload_path'])) 
			{
				mkdir($config['upload_path'], 0777, TRUE);
				echo "<script>console.log('created.');</script>";
			}
			
			if($this->upload->do_upload('size_chart_img'))
            {
                $upload_data = $this->upload->data();
                $_POST['size_chart_img'] = SIZEIMAGEUPLOAD . $upload_data['file_name'];
				echo "<script>console.log('Sucess.');</script>";
                return TRUE;
            }
            else
            {
                $this->form_validation->set_message('validate_and_upload_sizechart', $this->upload->display_errors());
                return false;
            }
        }
	}
	
	//Pooja Jadhav
	public function search_women_ware_products_sizechart()
	{
		$product_name=$this->input->post('product_name');
		if(isset($product_name) && !empty($product_name))
		{
			$data['product_list']=$this->Productmodel->get_women_ware_sizechart_search_details($product_name);
			$this->load->view('layouts/header');
			$this->load->view('pages/view_women_ware_size_chart_details',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('products/view_women_ware_size_chart');
		}
	}
	//Pooja Jadhav
	public function search_women_footware_products_sizechart()
	{
		$product_name=$this->input->post('product_name');
		if(isset($product_name) && !empty($product_name))
		{
			$data['product_list']=$this->Productmodel->get_women_footware_sizechart_search_details($product_name);
			$this->load->view('layouts/header');
			$this->load->view('pages/view_women_footware_size_chart_details',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('products/view_footware_ware_size_chart');
		}
	}
	//KT[18-04-18]
	public function upload_deal_of_day_photo()
	{
		
			
			if (empty ( $_FILES ['deal_img'] ['name'])) {
           return FALSE;
       } else {
           $config ['upload_path'] = IMAGEBASEPATH . DEALIMAGEUPLOAD;
            $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
            $config['max_size'] = '300';
			$config['min_width'] = '300'; 
			$config['min_height'] = '300';
			$config['max_width'] = '300'; 
			$config['max_height'] = '300';
           $config ['encrypt_name'] = TRUE;
           $config ['override'] = FALSE;
               
           $this->load->library ( 'upload' );
           $this->upload->initialize ( $config );
           if (! is_dir ( $config ['upload_path'] )) {
               mkdir ( $config ['upload_path'], 0777, TRUE );
           }
               
           if ($this->upload->do_upload( 'deal_img')) {
               $upload_data = $this->upload->data();
               $_POST ['deal_img'] = DEALIMAGEUPLOAD . $upload_data ['file_name'];
               return TRUE;
           } else {
               return FALSE;
           }
       }
	}
	//KT[18-04-18]
	private function delete_size_chart_images($size_image)
	{
			if($size_image !== NULL){
			if(!empty($size_image[0]->size_chart_image_url)){
				if(file_exists(IMAGEBASEPATH.$size_image[0]->size_chart_image_url)){
					unlink(IMAGEBASEPATH.$size_image[0]->size_chart_image_url);
				}
			}
		}
		
	}
}
