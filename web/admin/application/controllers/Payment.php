<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->output->set_header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
		if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Paymentmodel');
	}
	
	public function successful_transaction()
	{
		$per_page = 15;
		$url = site_url().'/payment/successful_transaction';
		$number_of_rows = $this->Paymentmodel->get_no_of_rows();
		
		$data['success_data']=$this->Paymentmodel->get_successful_transaction($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url,$number_of_rows,$per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/success_transaction',$data);
		$this->load->view('layouts/footer');
	}
	
	public function failure_transaction()
	{
		$per_page = 15;
		$url = site_url().'/payment/failure_transaction';
		$number_of_rows = $this->Paymentmodel->get_no_of_rows1();
		
		$data['fail_data']=$this->Paymentmodel->get_failure_transaction($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url,$number_of_rows,$per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/fail_transaction',$data);
		$this->load->view('layouts/footer');
	}
	//Pooja Jadhav
	public function search_success_transaction_details()
	{
		$payment_details=$this->input->post('payment_details');
		$payment = explode(' ',$payment_details);
		if(isset($payment_details) && !empty($payment_details))
		{
			$data['success_data']=$this->Paymentmodel->get_search_success_transaction_details($payment);
			$this->load->view('layouts/header');
			$this->load->view('pages/success_transaction',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('/payment/successful_transaction');
		}
	}
	//Pooja Jadhav
	public function search_fail_transaction_details()
	{
		$payment_details=$this->input->post('payment_details');
		$payment = explode(' ',$payment_details);
		if(isset($payment_details) && !empty($payment_details))
		{
			$data['fail_data']=$this->Paymentmodel->get_search_fail_transaction_details($payment);
			$this->load->view('layouts/header');
			$this->load->view('pages/fail_transaction',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('/payment/failure_transaction');
		}
	}
}	
	 