<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parent_categories extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Parentcategorymodel');
		$this->load->model('Ddlmodel');
		
	}	
	 
	public function view_all()
	{
		$per_page = 5;
		$url = site_url().'/parent_categories/view_all';
		$number_of_rows = $this->Parentcategorymodel->get_no_of_rows();
		
		$data['parent_cat_list']=$this->Parentcategorymodel->get_parent_cat_list($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/parent_categories',$data);
		$this->load->view('layouts/footer');
	}
	 
	
	public function add_parent_category()
	{
		if($this->input->post())
		{
			$this->Parentcategorymodel->insert_parent_categories();
			$message = 'Parent Category is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('parent_categories/view_all');
		}else{
			$message = 'Parent category insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('parent_categories/view_all');
		}
			
	}
	
	
	
	public function delete_parent_category()
	{
		$parent_cat_id=$this->uri->segment(3,0);
		if($this->Parentcategorymodel->del_parent_category($parent_cat_id))
		{	
			
			$message = 'Parent Category is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('parent_categories/view_all');
		}else{
			$message = 'Parent Category is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('parent_categories/view_all');
		}
	}
	
	/***** KC[date:-24/04/18] *****/
	public function update_parent_category()
	{
		if($this->input->post())
		{	
			$parent_cat_image = null;
			$image_upload_status = $this->update_image();
			if($image_upload_status)
			{
				$parent_cat_image = $this->Parentcategorymodel->get_parent_cat_image_path();
			}
			
			if($this->Parentcategorymodel->update_parent_category())
			{
				if($image_upload_status) 
					$this->delete_parent_cat_images($parent_cat_image);
				$message = 'Parent category is updated sucessfully';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message
				));	
			}
			else
			{
				$message = 'Parent category is failed to update, Please try again';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));	
			}
		}
		redirect('parent_categories/view_all');
	}
	/***** KC[date:-24/04/18] *****/
	function update_image() {
		$_POST ['parent_cat_img'] = null;
		if (empty ( $_FILES ['parent_cat_img'] ['name'])) {
           return FALSE;
       } else {
           $config ['upload_path'] = IMAGEBASEPATH . PARENTCATUPLOADPATH;
            $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
             $config['max_size'] = 300;
		    $config['max_width'] = 900;
		    $config['max_height'] = 700;
			$config['min_width'] = 900;
		    $config['min_height'] = 700;;
           $config ['encrypt_name'] = TRUE;
           $config ['override'] = FALSE;
               
           $this->load->library ( 'upload' );
           $this->upload->initialize ( $config );
           if (! is_dir ( $config ['upload_path'] )) {
               mkdir ( $config ['upload_path'], 0777, TRUE );
           }
               
           if ($this->upload->do_upload( 'parent_cat_img')) {
               $upload_data = $this->upload->data();
               $_POST ['parent_cat_img'] = PARENTCATUPLOADPATH . $upload_data ['file_name'];
               return TRUE;
           } else {
               return FALSE;
           }
    }
	}
	/***** KT[date:-24/04/18] *****/
	public function section_wise_pc()
   
    { 
        $data['parent_categories']=$this->Ddlmodel->get_parent_category_list_for_home();
          $data['section_parent_cat']=$this->Parentcategorymodel->get_section_pat_cat();
          $this->load->view('layouts/header');
     
        $this->load->view('pages/section_wise_pc',$data);
   
          $this->load->view('layouts/footer');
 
    }
	/***** KT[date:-24/04/18] *****/
	public function add_section_wise_pc()
    {
        
        if($this->input->post())
        {
            if($this->Parentcategorymodel->add_section_wise_pc() == 0)
            {
                $css_class = "text-danger";
                $message = "Parent Category is not set to section please try again....";
            }
            else
            {
                $css_class = "text-success";
                $message = "Parent Category is set to section";
            }
            $this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
            redirect('parent_categories/section_wise_pc');
        }
        else{
            $message = "Fill-up the form properly !";
            $this->session->set_flashdata(array('css_class' => $css_class, 'message' => $message));
            redirect('parent_categories/section_wise_pc');
        } 
    }
	private function delete_parent_cat_images($parent_cat_image)
	{
			if($parent_cat_image !== NULL){
			if(!empty($parent_cat_image[0]->parent_cat_img_url)){
				if(file_exists(IMAGEBASEPATH.$parent_cat_image[0]->parent_cat_img_url)){
					unlink(IMAGEBASEPATH.$parent_cat_image[0]->parent_cat_img_url);
				}
			}
		}
		
	}
	/***** KT[date:-24/04/18] *****/
	public function check_pat_cat()
    {
        $pcat=$this->input->post('pcat');
        
        if($this->input->post())
        {    
            $data=$this->Parentcategorymodel->parent_cat_is_exist($pcat);
        }
        else
        {
            $data=NULL;
        }
        echo json_encode($data);
    
    }
}
