<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		//$this->load->model('Productmodel');
		$this->load->model('Childcategorymodel');
	}
	
	public function get_products()
	{
		$sub_cat_id=$this->input->post('sub_cat_id');
		$rows=$this->Productmodel->get_products_list($sub_cat_id);
		echo json_encode($rows);
	}
	
	public function get_child_categories()
	{
		$parent_cat_id=$this->input->post('parent_cat_id');
		$rows=$this->Childcategorymodel->get_child_cat_list($parent_cat_id);
		echo json_encode($rows);
	}
	
	
}	
