<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personalizepackages extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Personalizepackagemodel');
		$this->load->model('Ddlmodel');
	}	
	 
	public function view_personalizepackages()
	{
		$data['main_products'] = $this->Personalizepackagemodel->get_personalizepackage_list();
		$this->load->view('layouts/header');
		$this->load->view('pages/personalizepackages', $data);
		$this->load->view('layouts/footer');
	}
	
	public function personalizepackages_products()
	{
		$data['sub_products'] = $this->Personalizepackagemodel->get_sub_product_list();
		$data['ddl_main_products'] = $this->Ddlmodel->get_personalizedpackages_main_product_list();
		$this->load->view('layouts/header');
		$this->load->view('pages/personalizepackages_products', $data);
		$this->load->view('layouts/footer');
	}
	
	
	
	public function addons()
	{
		$data['addons'] = $this->Personalizepackagemodel->get_addons_list();
		$this->load->view('layouts/header');
		$this->load->view('pages/addons', $data);
		$this->load->view('layouts/footer');
	}
	
	
	
	public function insert_main_product()
	{
		if($this->input->post())
		{
			$this->Personalizepackagemodel->insert_main_product();
			
			$message = 'Product is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('Personalizepackages/view_personalizepackages');
		}else{
			$message = 'Product  insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('Personalizepackages/view_personalizepackages');
		}
	}
	
	public function delete_main_product()
	{
		$prod_id=$this->uri->segment(3,0);
		if($this->Personalizepackagemodel->delete_main_product($prod_id))
		{	
			
			$message = 'Product is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('Personalizepackages/view_personalizepackages');
		}else{
			$message = 'Product is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('Personalizepackages/view_personalizepackages');
		}			
	}
	
	public function update_main_product()
	{
		if($this->input->post())
		{	
			$this->Personalizepackagemodel->update_main_product();
			
			$message = 'Product updated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('Personalizepackages/view_personalizepackages');
		}else{
			$message = 'Product is not updated sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('Personalizepackages/view_personalizepackages');
		}			
	}
	
	
	
	
	
	
	
	
	
	
	
	public function insert_sub_product()
	{
		if($this->input->post())
		{
			$this->Personalizepackagemodel->insert_sub_product();
			
			$message = 'Sub Product is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('personalizepackages/personalizepackages_products');
		}else{
			$message = 'Sub Product insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('personalizepackages/personalizepackages_products');
		}
	}
	
	public function update_sub_product()
	{
		if($this->input->post())
		{	
			$this->Personalizepackagemodel->update_sub_product();
			
			$message = 'Product updated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('Personalizepackages/personalizepackages_products');
		}else{
			$message = 'Product is not updated sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('Personalizepackages/personalizepackages_products');
		}			
	}
	
	public function delete_sub_product()
	{
		$prod_id=$this->uri->segment(3,0);
		if($this->Personalizepackagemodel->delete_sub_product($prod_id))
		{	
			
			$message = 'Product is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('Personalizepackages/personalizepackages_products');
		}else{
			$message = 'Product is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('Personalizepackages/personalizepackages_products');
		}			
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function insert_addon_product()
	{
		if($this->input->post())
		{
			$this->Personalizepackagemodel->insert_addon_product();
			
			$message = 'Product is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('personalizepackages/addons');
		}else{
			$message = 'Product  insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('personalizepackages/addons');
		}
	}
		
	public function update_addon_product()
	{
		if($this->input->post())
		{	
			$this->Personalizepackagemodel->update_addon_product();
			
			$message = 'Product updated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('Personalizepackages/addons');
		}else{
			$message = 'Product is not updated sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('Personalizepackages/addons');
		}			
	}
	
	public function delete_addon_product()
	{
		$prod_id=$this->uri->segment(3,0);
		if($this->Personalizepackagemodel->delete_addon_product($prod_id))
		{	
			$message = 'Product is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('Personalizepackages/addons');
		}else{
			$message = 'Product is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('Personalizepackages/addons');
		}			
	}
	
	
	
	
	
	
	
	
	
}
