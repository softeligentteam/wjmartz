<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct() 
	{
		parent::__construct();
		if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
	}
	public function index()
	{
		$this->load->model('Ordermodel');
		$this->load->model('Customermodel');
		$this->load->model('Support_Services_model');
		$data['order_list']=$this->Ordermodel->get_recent_order_details();
		$data['customer_list']=$this->Customermodel->get_recent_customer_details();
		$data['support_chat']=$this->Support_Services_model->get_recent_support_chat();
		$data['customer_count']=$this->Customermodel->get_customer_count();
		$data['order_paced']=$this->Customermodel->get_order_paced_count();
		$data['order_delivered']=$this->Customermodel->get_order_delivered_count();
		$data['order_cancelled']=$this->Customermodel->get_order_cancelled_count();
		$this->load->view('layouts/header');
		$this->load->view('pages/home',$data);
		$this->load->view('layouts/footer');
	}
	public function log_in()
	{
		$this->load->view('login');
	}
	public function under_construction($page)
	{
		$name['name'] = $page; 
		$this->load->view('layouts/header');
		$this->load->view('pages/blank', $name);
		$this->load->view('layouts/footer');
	}
}
