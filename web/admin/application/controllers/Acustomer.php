<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acustomer extends CI_Controller 
{
	 public function __construct() 
	 {
        parent::__construct();
		$this->load->model('Acustomermodel');
	 }	
	 
	 
	 public function customer_login_with_otp()
	{
		$otp = 100000;
		$mobile = $this->input->post('mobile_number');
		if($mobile!==NULL){
			$this->db->where('mobile',$mobile);
			$result=$this->db->get('customer')->row();
			
			if($result!=NULL){
				//$this->session->set_userdata(array('mobile'=>$mobile,'otp'=>$otp));
				$data['otp']=$otp;
				$data['status']=1;
				$data['customer_data']=$this->Acustomermodel->get_customer_data($mobile);
				echo json_encode($data);
			}
			else{
				$data['otp']=$otp;
				$data['status']=0;
				echo json_encode($data);
			}
		}
		else{
			$data['status']=0;
			echo json_encode($data);
		}
		
	}
	
	public function new_customer_registration()
	{
		if($this->input->post())
		{	
			if($this->Acustomermodel->insert_customer())
			{
				echo 1;
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}	
		
	}
	
	public function customer_data_for_update_profile()
	{
		$cust_id=$this->uri->segment(3,0);
		$data['customer_data']=$this->Acustomermodel->get_cutomer_data_to_update($cust_id);
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function update_customer_profile()
	{
			if($this->Acustomermodel->update_customer_data()>0)
			{
				echo 1;
			}else{
				echo 0;
			}
		
	}
	
	public function support_service_list()
	{
		$data['support_service_list']=$this->Acustomermodel->get_support_services_list();
		echo json_encode($data);
	}
	
	public function support_chat()
	{
		if($this->input->post())
		{
			if($this->Acustomermodel->insert_support_chat())
			{
				echo 1;
			}else{
				echo 0;
			}	
		}else{
			echo 0;
		}	
	}
	
	
}	 