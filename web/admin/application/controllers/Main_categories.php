<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_categories extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Maincategorymodel');
		
	}	
	 
	public function view_all()
	{
		$data['main_cat_list']=$this->Maincategorymodel->get_all_main_categories();
		$this->load->view('layouts/header');
		$this->load->view('pages/main_categories',$data);
		$this->load->view('layouts/footer');
	}
	 
}
