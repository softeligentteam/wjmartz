<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Child_categories extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->output->set_header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
		  if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Ddlmodel');
		$this->load->model('Childcategorymodel');
	}	
	 
	public function view_all()
	{
		$data['parent_cat_list']=$this->Ddlmodel->get_parent_category_list();
		if($this->input->post()){
			$data['current_parent_cat_id'] = $this->input->post('parent_cat');
		}
		else{
			$data['current_parent_cat_id'] = 0;
		}
		
		$data['child_cats']=$this->Childcategorymodel->get_child_cat_list($data['current_parent_cat_id']);
		//$data['child_cat_list']=$this->Childcategorymodel->get_child_categories();
		$this->load->view('layouts/header');
		$this->load->view('pages/child_categories',$data);
		$this->load->view('layouts/footer');
	}
	
	public function view_all_inactive()
	{
		$data['parent_cat_list']=$this->Ddlmodel->get_parent_category_list();
		if($this->input->post()){
			$data['current_parent_cat_id'] = $this->input->post('parent_cat');
		}
		else{
			$data['current_parent_cat_id'] = 0;
		}
		$per_page = 10;
		$url = site_url().'/Child_categories/view_all_inactive';
		$number_of_rows = $this->Childcategorymodel->get_child_cat_list_inactive_no_of_rows();
		$data['child_cats'] =$this->Childcategorymodel->get_child_cat_list_inactive($data['current_parent_cat_id'],$per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/child_categories_inactive',$data);
		$this->load->view('layouts/footer');
		
		
		//$data['child_cats']=$this->Childcategorymodel->get_child_cat_list_inactive($data['current_parent_cat_id']);
		//$data['child_cat_list']=$this->Childcategorymodel->get_child_categories();
		//$this->load->view('layouts/header');
		//$this->load->view('pages/child_categories_inactive',$data);
		//$this->load->view('layouts/footer');
	}
	
	
	public function add_child_category()
	{
		if($this->input->post())
		{
			if($this->validate_child_cat_form())
			{
			$this->Childcategorymodel->insert_child_categories();
			$message = 'Child Category is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('child_categories/view_all');
			}else{
			$message = 'Child category insertion failed. Please upload proper image...';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('child_categories/view_all');
			}
		}else{
			$message = 'Child category insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('child_categories/view_all');
		}
			
	}
	
	public function validate_child_cat_form()
	{
		$this->form_validation->set_error_delimiters('<div class="error_msg">', '</div>');
		$this->form_validation->set_rules('child_cat_img', 'Child Category Image', 'callback_validate_and_upload_child_cat');
		return $this->form_validation->run();
	}
	//Pooja Jadhav
	function validate_and_upload_child_cat()
	{
		
		if (empty ( $_FILES ['child_cat_img'] ['name'])) {
           $this->form_validation->set_message ( 'validate_and_upload_child_cat', 'Please select Child Category Image' );
           return FALSE;
       } else {
           $config ['upload_path'] = IMAGEBASEPATH . CHILDCATUPLOADPATH;
            $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
            $config['max_size'] = '300';
			$config['min_width'] = '600'; 
			$config['min_height'] = '450';
			$config['max_width'] = '600'; 
			$config['max_height'] = '450';
           $config ['encrypt_name'] = TRUE;
           $config ['override'] = FALSE;
               
           $this->load->library ( 'upload' );
           $this->upload->initialize ( $config );
           if (! is_dir ( $config ['upload_path'] )) {
               mkdir ( $config ['upload_path'], 0777, TRUE );
           }
               
           if ($this->upload->do_upload( 'child_cat_img')) {
               $upload_data = $this->upload->data();
               $_POST ['child_cat_img'] = CHILDCATUPLOADPATH . $upload_data ['file_name'];
               return TRUE;
           } else {
               $this->form_validation->set_message ( 'validate_and_upload_child_cat', $this->upload->display_errors ());
               return FALSE;
           }
       }
	}
	
	
	
	public function update_child_category()
	{
		//echo json_encode($_POST);
		//$image = $this->update_chlid_cat_photo();
		//print_r($data);
		if($this->input->post())
		{	$child_cat_image = $this->Childcategorymodel->get_child_cat_image_path();
			if($this->update_chlid_cat_photo())
			{
				$this->Childcategorymodel->update_child_categories();
				$this->delete_child_cat_images($child_cat_image);
				
				$message = 'Child Category is updates sucessfully';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('child_categories/view_all');
			}else{
				$message = 'Child Category  Image is Failed to Update ';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('child_categories/view_all');
			}	
		}else{
			$message = 'Child category updation failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('child_categoriess/view_all');
		}
			
	}
	
	function update_chlid_cat_photo() {
    	
			if (empty ( $_FILES ['child_cat_img'] ['name'])) {
           return FALSE;
       } else {
           $config ['upload_path'] = IMAGEBASEPATH . CHILDCATUPLOADPATH;
            $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
            $config['max_size'] = '300';
			$config['min_width'] = '600'; 
			$config['min_height'] = '450';
			$config['max_width'] = '600'; 
			$config['max_height'] = '450';
           $config ['encrypt_name'] = TRUE;
           $config ['override'] = FALSE;
               
           $this->load->library ( 'upload' );
           $this->upload->initialize ( $config );
           if (! is_dir ( $config ['upload_path'] )) {
               mkdir ( $config ['upload_path'], 0777, TRUE );
           }
               
           if ($this->upload->do_upload( 'child_cat_img')) {
               $upload_data = $this->upload->data();
               $_POST ['child_cat_img'] = CHILDCATUPLOADPATH . $upload_data ['file_name'];
               return TRUE;
           } else {
               return FALSE;
           }
       }
    }
	
	
	
	
	public function delete_child_category()
	{
		$child_cat_id=$this->uri->segment(3,0);
		if($this->Childcategorymodel->del_child_category($child_cat_id))
		{	
			
			$message = 'Child Category is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('child_categories/view_all');
		}else{
			$message = 'Child Category is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('child_categories/view_all');
		}
	}
	
	public function active_child_category()
	{
		$child_cat_id=$this->uri->segment(3,0);
		if($this->Childcategorymodel->activated_child_category($child_cat_id))
		{	
			
			$message = 'Child Category is Activated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('child_categories/view_all_inactive');
		}else{
			$message = 'Child Category Activation Failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('child_categories/view_all_inactive');
		}
	}
	
	private function delete_child_cat_images($child_cat_image)
	{
			if($child_cat_image !== NULL){
			if(!empty($child_cat_image[0]->child_cat_img_url)){
				if(file_exists(IMAGEBASEPATH.$child_cat_image[0]->child_cat_img_url)){
					unlink(IMAGEBASEPATH.$child_cat_image[0]->child_cat_img_url);
				}
			}
		}
		
	}
	/***** PJ[date:-26/04/18] *****/
	public function get_child_cat_list()
	{
		$parent_cat_id = $this->input->get('parent_cats');
		$data = $this->Ddlmodel->get_parent_cat_wise_child_list($parent_cat_id);
		echo json_encode($data);
	}
	
}
