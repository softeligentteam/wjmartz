<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customermodel extends CI_Model
{
	public function get_all_customer_details($per_page)
	{
		$this->db->select('c.cust_id,first_name,last_name,mobile,email')->order_by('c.cust_id','DESC');
		return $this->db->get('customer c',$per_page, $this->uri->segment(3))->result();
	}
	
	public function get_recent_customer_details()
	{
		$this->db->select('c.cust_id,first_name,last_name,mobile,email');
		$this->db->order_by('c.cust_id', 'desc');
        $this->db->limit(5);
		return $this->db->get('customer c')->result();
	}
	
	
	public function get_no_of_rows()
	{
		return $this->db->get('customer')->num_rows();
	}
	
	//Alex ->
	public function get_customer_count()
	{
		$this->db->where('user_status',1);
		return $this->db->count_all('customer');
	}
	public function get_order_paced_count()
	{
		// $this->db->where('ord_status_id',1);
		return $this->db->count_all('order');
	}
	public function get_order_delivered_count()
	{
		$this->db->where('ord_status_id',3);
		return $this->db->get('order')->num_rows();
	}
	public function get_order_cancelled_count()
	{
		$failed = $this->cancelled_orders_count(); 
		$cancled = $this->failed_orders_count();
		$all = $failed + $cancled;
		return $all;
	}
	 private function cancelled_orders_count(){
		$this->db->where('ord_status_id',5); 
		return $this->db->get('temp_order')->num_rows();
	 }
	 
	 private function failed_orders_count(){
		$this->db->where('ord_status_id',4); 
		return $this->db->get('order')->num_rows();
	 }
	//<- Alex
	//Pooja Jadhav
	public function get_search_customer_details($customer)
	{
		$this->db->select('c.cust_id,first_name,last_name,mobile,email');
		foreach($customer as $cust)
		{
			$this->db->like('mobile',$cust);
			$this->db->or_like('first_name',$cust);
			$this->db->or_like('last_name',$cust);
		}
		return $this->db->get('customer c')->result();
	}
	//Pooja Jadhav
	public function get_customer_details($cust_id)
	{
		$this->db->select('c.cust_id,first_name,last_name,mobile,email,dob');
		$this->db->where('c.cust_id',$cust_id);
		return $this->db->get('customer c')->row();
	}
	public function get_customer_address($cust_id)
	{
		$this->db->select('ca.cust_id,address,ca.pincode,ca.area as area_name,ci.city,s.state,at.address_type');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->join('address_type at','at.address_type_id=ca.address_type');
		$this->db->where('ca.cust_id',$cust_id);
		return $this->db->get('customer_address ca')->result();
	}
	public function get_customer_order_details($cust_id)
	{
		$this->db->select('to.order_id,order_date,pd.transaction_id,pd.payment_status,os.order_status,pc.promocode,to.promocode_applied');
		//$this->db->where('ord_status_id',1);
		$this->db->join('order_status os','os.ord_status_id=to.ord_status_id');
		$this->db->join('payment_details pd','pd.order_id=to.order_id');
		$this->db->join('promocode pc','pc.promo_id=to.promocode_applied');
		$this->db->where('to.cust_id',$cust_id);
		return $this->db->get('order to')->result();
	}
}