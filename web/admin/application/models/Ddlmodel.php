<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ddlmodel extends CI_Model

{

	public function get_main_category_list()

	{

		$this->db->where_not_in('main_cat_id ',0)->order_by ( 'main_cat_name' );

		$list = $this->db->get ( 'main_category' )->result ();

		$filtered_list [''] = 'Select Main Category ';

		if (count ( $list ) > 0) {

			foreach ( $list as $object ) {

				$filtered_list [$object->main_cat_id] = $object->main_cat_name;

			}

		}

		return $filtered_list;

	

	}

	

	/***** KT[date:-24/04/18]   PJ[date:-26/04/18] *****/
	public function get_parent_category_list()

    {

        $this->db->where_not_in('parent_cat_id ',0)->order_by ( 'parent_cat_id' );
		$this->db->where('status ',1);
        $list = $this->db->get ( 'parent_category' )->result ();

        $filtered_list [''] = 'Select Parent Category ';

        if (count ( $list ) > 0) {

            foreach ( $list as $object ) {

                $filtered_list [$object->parent_cat_id] = $object->parent_cat_name;

            }

        }

        return $filtered_list;

    

    }
    
    public function get_parent_category_list_for_home()

    {

        $this->db->select('parent_cat_id,parent_cat_name')->order_by ( 'parent_cat_id' );
        //$this->db->where('status',1);

        $list = $this->db->get ( 'parent_category' )->result ();

        $filtered_list [''] = 'Select Parent Category ';

        if (count ( $list ) > 0) {

            foreach ( $list as $object ) {

                $filtered_list [$object->parent_cat_id] = $object->parent_cat_name;

            }

        }

        return $filtered_list;

    

    }

	
	/***** PJ[date:-26/04/18] *****/
	public function get_child_category_list()

	{

		$this->db->where_not_in('child_cat_id ',0)->order_by ( 'child_cat_name' );
		$this->db->where('status ',1);
		$list = $this->db->get ( 'child_category' )->result ();

		$filtered_list [''] = 'Select child Category ';

		if (count ( $list ) > 0) {

			foreach ( $list as $object ) {

				$filtered_list [$object->child_cat_id] = $object->child_cat_name;

			}

		}

		return $filtered_list;

	

	}

	

	public function get_brand_list()

	{

		$this->db->order_by( 'brand' );

		$this->db->where('parent_cat_id',2);

		$this->db->where('brand_status',1);

		$list = $this->db->get ( 'brands' )->result();

		$filtered_list [''] = 'Select Brand ';

		if (count ( $list ) > 0) {

			foreach ( $list as $object ) {

				$filtered_list [$object->brand_id] = $object->brand;

			}

		}

		return $filtered_list;

	

	}

	

	public function get_fixpackages_list(){

		$this->db->where_not_in('package_id ',0)->order_by ( 'package_name' );

		$list = $this->db->get ( 'package' )->result();

		$filtered_list [''] = 'Select Package';

		if (count ( $list ) > 0) {

			foreach ( $list as $object ) {

				$filtered_list [$object->package_id] = $object->package_name;

			}

		}

		return $filtered_list;

	}

	

	public function get_personalizedpackages_main_product_list(){

		$this->db->where_not_in('per_package_id',0)->order_by ( 'per_package_id' );

		$list = $this->db->get ( 'personalised_package' )->result();

		$filtered_list [''] = 'Select Main Product';

		if (count ( $list ) > 0) {

			foreach ( $list as $object ) {

				$filtered_list [$object->per_package_id] = $object->package_main_product;

			}

		}

		return $filtered_list;

	}

	

	

	public function get_product_types_list(){

		$this->db->where_not_in('product_type_id',0)->order_by ( 'product_type_id' );

		$list = $this->db->get ( 'product_type' )->result();

		$filtered_list [''] = 'Select Product Type';

		if (count ( $list ) > 0) {

			foreach ( $list as $object ) {

				$filtered_list [$object->product_type_id] = $object->product_type;

			}

		}

		return $filtered_list;

	}

	

	public function get_psize_list()

	{

		$this->db->where('parent_cat_id',2);

		$this->db->select('size_id, size')->order_by('size');

		return $this->db->get('product_sizes')->result();

	}

	

	public function get_pcolor_list()

	{

		$this->db->select('color_id, color_name')->order_by('color_name');

		return $this->db->get('product_colors')->result();

	}

	

	public function get_child_category_list_for_women_ware()

	{

		$this->db->where_not_in('child_cat_id ',0)->order_by ( 'child_cat_name' );

		$this->db->where_not_in('parent_cat_id ',1);

		$this->db->where('status',1);

		//$this->db->where('parent_cat_id',1);

		$list = $this->db->get ( 'child_category' )->result ();

		$filtered_list [''] = 'Select child Category ';

		if (count ( $list ) > 0) {

			foreach ( $list as $object ) {

				$filtered_list [$object->child_cat_id] = $object->child_cat_name;

			}

		}

		return $filtered_list;

	

	}

	

	public function  get_parent_category_list_for_women_ware()

	{

		$this->db->where_not_in('parent_cat_id ',1)->order_by ( 'parent_cat_name' );

		$list = $this->db->get ( 'parent_category' )->result ();

		$filtered_list [''] = 'Select Parent Category ';

		if (count ( $list ) > 0) {

			foreach ( $list as $object ) {

				$filtered_list [$object->parent_cat_id] = $object->parent_cat_name;

			}

		}

		return $filtered_list;

	

	}

	

	public function get_area_list() {

		$this->db->where_not_in('area_id ',0)->order_by( 'area_name' );

		$list = $this->db->get ( 'area' )->result ();

		$filtered_list [''] = 'Select Area';

		if (count ( $list ) > 0) {

			foreach ( $list as $object ) {

				$filtered_list [$object->area_id] = $object->area_name;

			}

		}

		return $filtered_list;

	}

	

	public function get_psize_list_footware()

	{

		$this->db->where('parent_cat_id',1);

		$this->db->select('size_id, size')->order_by('size');

		return $this->db->get('product_sizes')->result();

	}

	

	public function get_brand_list_footware()

	{

		$this->db->order_by( 'brand' );

		$this->db->where('parent_cat_id',1);

		$this->db->where('brand_status',1);

		$list = $this->db->get ( 'brands' )->result();

		$filtered_list [''] = 'Select Brand ';

		if (count ( $list ) > 0) {

			foreach ( $list as $object ) {

				$filtered_list [$object->brand_id] = $object->brand;

			}

		}

		return $filtered_list;

	

	}

	

	public function get_child_category_list_for_women_footware()

	{

		$this->db->where_not_in('child_cat_id ',0)->order_by ( 'child_cat_name' );

		$this->db->where('parent_cat_id',1);

		$this->db->where('status',1);

		$list = $this->db->get ( 'child_category' )->result ();

		$filtered_list [''] = 'Select child Category ';

		if (count ( $list ) > 0) {

			foreach ( $list as $object ) {

				$filtered_list [$object->child_cat_id] = $object->child_cat_name;

			}

		}

		return $filtered_list;


	}
	//Pooja Jadhav
	public function get_child_cat_list()
	{
		$this->db->where_not_in('child_cat_id ',0)->order_by ( 'child_cat_name' );
		$this->db->where('status',1);
		$list = $this->db->get ( 'child_category' )->result ();
		$filtered_list [''] = 'Select Category ';
		if (count ( $list ) > 0) {
			foreach ( $list as $object ) {
				$filtered_list [$object->child_cat_id] = $object->child_cat_name;
			}
		}
		return $filtered_list;
	}
	//Pooja Jadhav
	public function get_Prodcut_list_in_deal_of_day()
	{
		$this->db->select('deal_product_id');
		return $this->db->get('deals_of_day_product')->result_array('deal_product_id');
	}
	/***** PJ[date:-26/04/18] *****/
	public function get_child_cat_wise_product_list($child_cat_id,$parent_cat_id)
	{
		$this->db->select('prod_id, prod_name');
		$this->db->where_not_in('prod_id ',0)->order_by ('prod_name');
		$this->db->where('deal_status',0);
		$this->db->where('status',1);
		$this->db->where('in_stock',1);
		$this->db->where('fk_pcat_id',$parent_cat_id);
		$this->db->where('fk_pchild_id',$child_cat_id);
		return $this->db->get ( 'products' )->result ();
	}

	 public function get_child_cat_list_promocode()	
 {				
  $this->db->where_not_in('parent_cat_id',1)->order_by ( 'child_cat_name' );		
  $this->db->where_not_in('child_cat_id ',0);		
  $this->db->where('status',1);	
	$list = $this->db->get ( 'child_category' )->result ();		
  $filtered_list [''] = 'Select child Category ';		
  if (count ( $list ) > 0) 
 {			
  foreach ( $list as $object ) 
 {				
   $filtered_list [$object->child_cat_id] = $object->child_cat_name;			
 }		
 }	
	
 return $filtered_list;	 
 }
 
 /**********************Code by JSK STPL**************************/
 	public function get_product_for_list_footware()
	{
		$this->db->order_by('ptype_id');
		return $this->db->get('product_types')->result();
	}
	

	/***** PJ[date:-26/04/18] *****/
	public function get_parent_cat_wise_child_list($parent_cat_id)
	{
		$this->db->select('child_cat_id, child_cat_name');
		$this->db->where_not_in('child_cat_id ',0)->order_by ( 'child_cat_name' );
		$this->db->where('status',1);
		$this->db->where('parent_cat_id',$parent_cat_id);
		return $this->db->get ( 'child_category' )->result ();
	}
	
	 //KT[date 7may 2018]
  public function get_child_cat_list_subcat_promocode()	
 {		
  // $subcat=$this->get_child_cat_promo(); 
   //$sub=implode(" ",$subcat);
  $this->db->where_not_in('child_cat_id',0)->order_by ( 'child_cat_name' );		
  //$this->db->where_not_in('child_cat_id ',0);		
  $this->db->where('status',1);	
	$list = $this->db->get ( 'child_category' )->result ();		
  $filtered_list [''] = 'Select child Category ';		
  if (count ( $list ) > 0) 
 {			
  foreach ( $list as $object ) 
 {				
   $filtered_list [$object->child_cat_id] = $object->child_cat_name;			
 }		
 }	
	
 return $filtered_list;	 
 }
 
 //KT[date 7may 2018]
 public function get_child_cat_promo()
	{
		$this->db->select('child_cat_id');
		return $this->db->get ('promocode' )->result_array();
	}
}