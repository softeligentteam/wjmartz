<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pincodemodel extends CI_Model
{
	public function get_all_pincodes_with_charges($per_page)
	{
		$this->db->select('p.pincode_id,pincode,delivery_charges,delivery_day,status,is_cod');
		$this->db->where('pincode_id!=',0);
		//$this->db->join('area a','a.area_id=p.area_id');
		return $this->db->get('pincode p',$per_page, $this->uri->segment(3))->result();
	}	
	public function get_active_pincodes_with_charges($per_page)
	{
		$this->db->select('p.pincode_id,pincode,delivery_charges,delivery_day,status');
		$this->db->where('pincode_id!=',0);
		$this->db->where('status=',1);
		//$this->db->join('area a','a.area_id=p.area_id');
		return $this->db->get('pincode p',$per_page, $this->uri->segment(3))->result();
	}	
	public function get_inactive_pincodes_with_charges($per_page)
	{
		$this->db->select('p.pincode_id,pincode,delivery_charges,delivery_day,status');
		$this->db->where('pincode_id!=',0);
		$this->db->where('status=',0);
		//$this->db->join('area a','a.area_id=p.area_id');
		return $this->db->get('pincode p',$per_page, $this->uri->segment(3))->result();
	}	
	public function insert_delivery_charges()
	{
		$pincode=$this->input->post('pincode');
		$delivery_charge=$this->input->post('charge');
		$delivery_day=$this->input->post('day');
		$payment=$this->input->post('payment');
		//$area_id=$this->input->post('area_name');
		
		$data=array(
		'pincode'=>$pincode,
		'delivery_charges'=>$delivery_charge,
		'delivery_day'=>$delivery_day,      					//KT[date:-13/04/18]
		'is_cod'=>$payment
		);
		
		return $this->db->insert('pincode',$data);
	}
	
	public function del_pincode($pincode_id)
	{
		$this->db->where('pincode_id',$pincode_id);
		return $this->db->delete('pincode');
	}
	
	public function get_no_of_rows()
	{
		
		return $this->db->get('pincode')->num_rows();
	}
	public function get_active_no_of_rows()
	{
		$this->db->where('status=',1);
		return $this->db->get('pincode')->num_rows();
	}
	public function get_inactive_no_of_rows()
	{
		$this->db->where('status=',0);
		return $this->db->get('pincode')->num_rows();
	}
	
	public function update_pincode()
	{
		$pincode_id=$this->input->post('pin_id');
		$pincode=$this->input->post('pin');
		//$area=$this->input->post('area');
		$delivery_charge=$this->input->post('del_charge');
		$delivery_day=$this->input->post('del_day');
		$payment=$this->input->post('payment1');
		
		$data=array(
		'pincode'=>$pincode,
		'delivery_charges'=>$delivery_charge,
		'delivery_day'=>$delivery_day,
		'is_cod'=>$payment
		);
		
		$this->db->where('pincode_id',$pincode_id);
		$this->db->update('pincode',$data);
		return $this->db->affected_rows();
	}
	
	/**** KT[18-04-2018] ****/
	public function get_pincode($pincode)
	{
		$this->db->select('pincode_id');
		$this->db->where('pincode',$pincode);
		return $this->db->get('pincode')->row();
	}
	
	/****** inactive Pincode ******/
	public function inactivePincode($Pincode_id)
	{
        $cmd = "UPDATE pincode SET status = 0 WHERE pincode_id = $Pincode_id;";
		return $this->db->query($cmd);
	}
	/****** active Pincode ******/
	public function activePincode($Pincode_id)
	{
        $cmd = "UPDATE pincode SET status = 1 WHERE pincode_id = $Pincode_id;";
		return $this->db->query($cmd);
	}
}