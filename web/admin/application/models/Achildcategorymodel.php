<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Achildcategorymodel extends CI_Model
{
	public function get_parent_categorywise_child_categories($parent_cat_id)
	{
		$this->db->select('child_cat_id,child_cat_name,child_cat_img_url');
		$this->db->where('parent_cat_id',$parent_cat_id);
		return $this->db->get('child_category')->result();
	}
}	