<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordermodel extends CI_Model
{
	public function get_all_order_details($per_page)
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name,city,state,tp.discount_amount');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->where('ord_status_id',1)->order_by('to.order_date  DESC, to.order_id DESC');
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		return $this->db->get('order to',$per_page, $this->uri->segment(3))->result();
	}
	
	public function no_of_rows_for_all_order()
	{
		$this->db->where('ord_status_id',1);
		return $this->db->get('order')->num_rows();
	}
	public function get_recent_order_details()
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,ca.address,pi.pincode,ca.area as area_name,city,state,tp.discount_amount');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		//$this->db->join('area ar','ar.area_id=ca.area');
		//$this->db->join('city ci','ci.city_id=ar.city_id');
		//$this->db->join('state sa','sa.state_id=sa.state_id');
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		$this->db->order_by('to.order_date  DESC, to.order_id DESC');
       	$this->db->limit(5);
		return $this->db->get('order to')->result();
	}
	
	public function ready_to_dispatch_order($order_id)
	{
		
		$data=array(
			'ord_status_id'=>2,
			'modified_on'=>date('Y-m-d H:i:s'),
			'modified_by'=>1
		);
		$this->db->where('order_id',$order_id);
		$this->db->update('order',$data);
	}
	
	public function get_mobile($order_id)
	{
		$this->db->select('mobile');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->where('to.order_id',$order_id);
		return $this->db->get('order to')->row();
	}
	
	public function cancel_order($order_id)
	{
		
		$data=array(
			'ord_status_id'=>4,
			'modified_on'=>date('Y-m-d H:i:s'),
			'modified_by'=>1
		);
		$this->db->where('order_id',$order_id);
		$this->db->update('order',$data);
	}
	
	public function delivery_order($order_id)
	{
		
		$data=array(
			'ord_status_id'=>3,
			'modified_on'=>date('Y-m-d H:i:s'),
			'modified_by'=>1
		);
		$this->db->where('order_id',$order_id);
		$this->db->update('order',$data);
	}
	
	public function get_ready_to_dispatch_order_details($per_page)
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name,tp.discount_amount');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->where('ord_status_id',2)->order_by('to.order_date  DESC, to.order_id DESC');;
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		return $this->db->get('order to',$per_page, $this->uri->segment(3))->result();
	}
	
	public function no_of_rows_for_ready_to_dispatch()
	{
		$this->db->where('ord_status_id',2);
		return $this->db->get('order')->num_rows();
	}
	
	public function get_cancelled_order_details($per_page)
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name ,modified_by,modified_by_cust,tp.discount_amount');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->where('ord_status_id',4)->order_by('to.order_date  DESC, to.order_id DESC');;
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		return $this->db->get('order to',$per_page, $this->uri->segment(3))->result();
	}
	
	public function no_of_rows_for_canelled()
	{
		$this->db->where('ord_status_id',4);
		return $this->db->get('order')->num_rows();
	}
	
	public function get_deliverd_order_details($per_page)
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name,tp.discount_amount');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->where('ord_status_id',3)->order_by('to.order_date  DESC, to.order_id DESC');
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		return $this->db->get('order to',$per_page, $this->uri->segment(3))->result();
	}
	
	public function no_of_rows_for_delivered()
	{
		$this->db->where('ord_status_id',3);
		return $this->db->get('order')->num_rows();
	}
	public function get_orderwise_products($order_id)
	{
		$this->db->select('prod_name,tod.product_id,tod.price,tod.quantity,tod.amount,size,prod_image_url,tod.sgst_amount,tod.cgst_amount');
		$this->db->join('products p','p.prod_id=tod.product_id');
		$this->db->join('product_sizes ps','ps.size_id=tod.size_id');
		$this->db->where('tod.order_id',$order_id);
		return $this->db->get('temp_order_details tod')->result();
	}
	
	public function get_orderwise_products1($order_id)
	{
		$this->db->select('prod_name,tod.product_id,tod.price,tod.quantity,tod.amount,size,prod_image_url,tod.sgst_amount,tod.cgst_amount');
		$this->db->join('products p','p.prod_id=tod.product_id');
		$this->db->join('product_sizes ps','ps.size_id=tod.size_id');
		$this->db->where('tod.ref_order_id',$order_id);
		return $this->db->get('temp_order_details tod')->result();
	}
	
	public function get_orderidwise_order_details1($order_id)
	{
		$this->db->select('to.ref_order_id as order_id,to.first_name,to.last_name,email,mobile,to.address,to.area as area_name,tp.amount as prod_amt,to.pincode,p.delivery_charges,ci.city,s.state,transaction_id,payment_datetime,payment_status,amount,instruction,refered_by,trans_reason,mihpayid,promocode_applied,pc.promocode,pc.discount,tp.discount_amount');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.cust_id=c.cust_id');
		$this->db->join('pincode p','p.pincode_id=ca.pincode');
		//$this->db->join('area ar','ar.area_id=ca.area');
		//$this->db->join('city ci','ci.city_id=ar.city_id');
		//$this->db->join('state s','s.state_id=ci.state_id');
		$this->db->join('promocode pc','pc.promo_id=to.promocode_applied');
		$this->db->join('temp_payment_details tp','tp.ref_order_id=to.ref_order_id');
		$this->db->where('to.ref_order_id',$order_id);
		return $this->db->get('temp_order to')->row();
	}
	
	public function get_orderidwise_order_details($order_id)
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,email,mobile,ca.address,ca.area as area_name,p.pincode,p.delivery_charges,ci.city,s.state,transaction_id,payment_datetime,payment_status,tp.amount,tp.amount as prod_amt,instruction,refered_by,order_date,promocode_applied,pc.promocode,pc.discount,tp.discount_amount');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.cust_id=c.cust_id');
		$this->db->join('pincode p','p.pincode_id=ca.pincode');
		//$this->db->join('area ar','ar.area_id=ca.area');
		//$this->db->join('city ci','ci.city_id=ar.city_id');
		//$this->db->join('state s','s.state_id=ci.state_id');
		$this->db->join('promocode pc','pc.promo_id=to.promocode_applied');
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		$this->db->where('to.order_id',$order_id);
		return $this->db->get('order to')->row();
	}
	
	public function get_order_data_for_mail($order_id)
	{
		$this->db->select('o.order_id,o.first_name,o.last_name,email,order_date,delivery_date,amount,mobile,pd.amount,pd.amount as prod_amt,,promocode_applied,pc.promocode,pc.discount,pd.discount_amount,p.pincode,p.delivery_charges');
		$this->db->join('customer c','c.cust_id=o.cust_id');
		$this->db->join('customer_address ca','ca.cust_id=c.cust_id');
		$this->db->join('pincode p','p.pincode_id=ca.pincode');
		$this->db->join('promocode pc','pc.promo_id=o.promocode_applied');
		$this->db->join('payment_details pd','pd.order_id=o.order_id');
		$this->db->where('o.order_id',$order_id);
		return $this->db->get('order o')->row();
	}
	
	public function in_process_order($order_id)
	{
		
		$data=array(
			'ord_status_id'=>1,
			'modified_on'=>date('Y-m-d H:i:s'),
			'modified_by'=>1
		);
		$this->db->where('order_id',$order_id);
		$this->db->update('order',$data);
	}
	
	public function get_failed_order_details($per_page)
	{
		$this->db->select('to.ref_order_id as order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name,pi.delivery_charges,tp.discount_amount');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->where('ord_status_id',5)->order_by('order_date DESC, to.ref_order_id DESC');
		$this->db->join('temp_payment_details tp','tp.ref_order_id=to.ref_order_id');
		return $this->db->get('temp_order to',$per_page, $this->uri->segment(3))->result();
	}
	public function no_of_rows_for_failed()
	{
		$this->db->where('ord_status_id',5);
		return $this->db->get('temp_order')->num_rows();
	}
	//Pooja Jadhav
	public function no_of_rows_for_refund()
	{
		return $this->db->get('refund_order')->num_rows();
	}
	//Pooja Jadhav
	public function get_refund_order_list($per_page)
	{
		$this->db->select('ro.refund_id,ro.order_id,mobile,o.first_name,o.last_name,refund_date,amount,return_reason')->order_by('ro.refund_date  DESC, ro.refund_id DESC');
		$this->db->join('customer c','c.cust_id=ro.cust_id');
		$this->db->join('order o','o.order_id=ro.order_id');
		return $this->db->get('refund_order ro',$per_page, $this->uri->segment(3))->result();
	}
	//Pooja Jadhav
	public function get_refund_details($refund_id)
	{
		$this->db->select('rod.refund_id,ro.cust_id,ro.order_id,o.first_name,o.last_name,email,mobile,ro.order_date');
		$this->db->join('refund_order ro','ro.refund_id=rod.refund_id');
		$this->db->join('customer c','c.cust_id=ro.cust_id');
		$this->db->join('order o','o.order_id=ro.order_id');
		$this->db->where('rod.refund_id',$refund_id);
		return $this->db->get('refund_order_details rod')->row();
	}
	//Pooja Jadhav
	public function get_refund_products_details($refund_id)
	{
		$this->db->select('prod_name,rod.product_id,rod.price,rod.quantity,rod.amount,size,prod_image_url');
		$this->db->join('products p','p.prod_id=rod.product_id');
		$this->db->join('product_sizes ps','ps.size_id=rod.size_id');
		$this->db->where('rod.refund_id',$refund_id);
		return $this->db->get('refund_order_details rod')->result();
	}
	//Pooja Jadhav
	public function get_search_inprocess_order_details($mobile)
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name,city,state,tp.discount_amount');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		$this->db->like('mobile',$mobile);
		$this->db->where('ord_status_id',1)->order_by('to.order_date  DESC, to.order_id DESC');
		$search = $this->db->get('order to');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//Pooja Jadhav
	public function get_search_ready_to_dispatch_order_details($mobile)
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name,tp.discount_amount');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		$this->db->like('mobile',$mobile);
		$this->db->where('ord_status_id',2)->order_by('to.order_date  DESC, to.order_id DESC');
		$search = $this->db->get('order to');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//Pooja Jadhav
	public function get_search_delivered_order_details($mobile)
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name,tp.discount_amount');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		$this->db->like('mobile',$mobile);
		$this->db->where('ord_status_id',3)->order_by('to.order_date  DESC, to.order_id DESC');
		$search = $this->db->get('order to');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//Pooja Jadhav
	public function get_search_cancel_order_details($mobile)
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name ,modified_by,modified_by_cust,tp.discount_amount');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		$this->db->like('mobile',$mobile);
		$this->db->where('ord_status_id',4)->order_by('to.order_date  DESC, to.order_id DESC');
		$search = $this->db->get('order to');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//Pooja Jadhav
	public function get_search_failed_order_details($mobile)
	{
		$this->db->select('to.ref_order_id as order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name,pi.delivery_charges,tp.discount_amount');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->join('temp_payment_details tp','tp.ref_order_id=to.ref_order_id');
		$this->db->like('mobile',$mobile);
		$this->db->where('ord_status_id',5)->order_by('to.order_date  DESC, to.order_id DESC');
		$search = $this->db->get('temp_order to');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//Pooja Jadhav
	public function get_search_refund_order_details($mobile)
	{
		$this->db->select('ro.refund_id,ro.order_id,mobile,o.first_name,o.last_name,refund_date,amount,return_reason')->order_by('ro.refund_date DESC, ro.refund_id DESC');
		$this->db->join('customer c','c.cust_id=ro.cust_id');
		$this->db->join('order o','o.order_id=ro.order_id');
		$this->db->like('mobile',$mobile);
		$search = $this->db->get('refund_order ro');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
}