<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support_Services_model extends CI_Model
{
	public function get_all_support_services($per_page)
	{
		$this->db->select('service_id,support_service,service_description');
		return $this->db->get('support_services',$per_page,$this->uri->segment(3))->result();
	}	
	
	public function insert_support_services()
	{
		$service_name=$this->input->post('service_name');
		$service_description=$this->input->post('service_desc');
		
		$data=array(
		'support_service'=>$service_name,
		'service_description'=>$service_description
		);
		
		return $this->db->insert('support_services',$data);
	}
	
	public function get_no_of_rows_chat()
	{
		return $this->db->get('support_chat')->num_rows();
	}
	
	public function get_no_of_rows_services()
	{
		return $this->db->get('support_services')->num_rows();
	}
	
	
	public function get_support_chat($per_page)
	{
		$this->db->select('first_name,last_name,mobile,email,message');
		$this->db->join('support_chat sc','sc.cust_id=c.cust_id');
		return $this->db->get('customer c',$per_page,$this->uri->segment(3))->result();
	}
	
	public function del_support_service($service_id)
	{
		$this->db->where('service_id',$service_id);
		return $this->db->delete('support_services');
	}
	
	public function update_service()
	{
		$service_id=$this->input->post('ser_id');
		$service_name=$this->input->post('name');
		$service_description=$this->input->post('desc');
		
		$data=array(
		'support_service'=>$service_name,
		'service_description'=>$service_description
		);
		
		$this->db->where('service_id',$service_id);
		$this->db->update('support_services',$data);
		return $this->db->affected_rows();
	}
	
	public function get_recent_support_chat()
	{
		$this->db->select('first_name,last_name,mobile,email,message');
		$this->db->join('support_chat sc','sc.cust_id=c.cust_id');
		$this->db->order_by('sc.support_chat_id', 'desc');
        $this->db->limit(3);
		return $this->db->get('customer c')->result();
	}	
}