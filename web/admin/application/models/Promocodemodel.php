<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promocodemodel extends CI_Model
{
	public function insert_userwise_promo()
	{
		//$userwise_promo_img=$this->upload_userwise_promo_photo();
		
		$data=array(
		'promo_type_id'=>4,
		'promocode'=>strtoupper($this->input->post('promocode')),
		'discount'=>$this->input->post('discount'),
		'description'=>$this->input->post('userwise_desc'),
		'promo_image_url'=>$this->input->post('userwise_promo_img'),
		);
		
		return $this->db->insert('promocode',$data);
	}
	//Pooja Jadhav
	public function get_userwise_promocode_no_of_rows()
	{
		$this->db->where('promo_type_id',4);
		return $this->db->get('promocode')->num_rows();
	}
	public function get_userwise_promo_list($per_page)
	{
		$this->db->select('promo_id,promocode,child_cat_name,p.status,p.promo_image_url');
		$this->db->join('child_category cc','cc.child_cat_id=p.child_cat_id');
		$this->db->where('promo_type_id',4);
		$this->db->where_not_in('promo_id',0);
		$this->db->where('p.status',1);
		return $this->db->get('promocode p',$per_page, $this->uri->segment(3))->result();
	}
	
	public function insert_datewise_promo()
	{
		//$datewise_promo_img=$this->upload_datewise_promo_photo();
		
		$data=array(
		'child_cat_id'=>$this->input->post('pccat_id'),
		'promo_type_id'=>2,
		'promocode'=>strtoupper($this->input->post('promocode')),
		'date'=>$this->input->post('date'),
		'discount'=>$this->input->post('discount'),
		'description'=>$this->input->post('datewise_desc'),
		'promo_image_url'=>$this->input->post('datewise_promo_img'),
		);
		
		return $this->db->insert('promocode',$data);
	}
	
	public function get_datewise_promo_list()
	{
		$this->db->select('promo_id,promocode,date,child_cat_name,p.status,p.promo_image_url');
		$this->db->join('child_category cc','cc.child_cat_id=p.child_cat_id');
		$this->db->where('promo_type_id',2);
		$this->db->where_not_in('promo_id',0);
		return $this->db->get('promocode p')->result();
	}
	
	public function insert_permant_promo()
	{
		//$permant_promo_img=$this->upload_permant_promo_photo();
		
		$data=array(
		'child_cat_id'=>0,
		'promo_type_id'=>3,
		'promocode'=>strtoupper($this->input->post('promocode')),
		'discount'=>$this->input->post('discount'),
		'description'=>$this->input->post('permant_desc'),
		'promo_image_url'=>$this->input->post('permant_promo_img')
		);
		
		return $this->db->insert('promocode',$data);
	}
	//Pooja Jadhav
	public function get_permant_promocode_no_of_rows()
	{
		$this->db->where('promo_type_id',3);
		return $this->db->get('promocode')->num_rows();
	}
	public function get_permant_promo_list($per_page)
	{
		$this->db->select('promo_id,promocode,date,child_cat_name,p.status,p.promo_image_url');
		$this->db->join('child_category cc','cc.child_cat_id=p.child_cat_id');
		$this->db->where('promo_type_id',3);
		$this->db->where_not_in('promo_id',0);
		return $this->db->get('promocode p',$per_page, $this->uri->segment(3))->result();
	}
	
	public function inactivate_promocode()
	{
		$id=$this->input->post('inactive');
		$data=array('status'=>0);
		$this->db->where('promo_id',$id);
		return $this->db->update('promocode', $data);
	}
	
	public function activate_promocode()
	{
		$id=$this->input->post('active');
		$data=array('status'=>1);
		$this->db->where('promo_id',$id);
		return $this->db->update('promocode', $data);
	}
	
	
	//Pooja Jadhav
	public function get_search_userwise_promocode_details($promocode)
	{
		$this->db->select('promo_id,promocode,child_cat_name,p.status,p.promo_image_url');
		$this->db->join('child_category cc','cc.child_cat_id=p.child_cat_id');
		$this->db->where('promo_type_id',4);
		$this->db->where_not_in('promo_id',0);
		$this->db->like('promocode',$promocode);
		$search = $this->db->get('promocode p');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//Pooja Jadhav
	public function get_search_permant_promocode_details($promocode)
	{
		$this->db->select('promo_id,promocode,child_cat_name,p.status,p.promo_image_url');
		$this->db->join('child_category cc','cc.child_cat_id=p.child_cat_id');
		$this->db->where('promo_type_id',3);
		$this->db->where_not_in('promo_id',0);
		$this->db->like('promocode',$promocode);
		$search = $this->db->get('promocode p');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//KT[date 4may 2018]
	public function is_promocode_unique($promocode)
	{
		$this->db->select('promocode');
		$this->db->where('promocode', $promocode);
		$this->db->where('status',1);
		return $this->db->get('promocode')->row();
	}
	
	//KT[date 4may 2018]
	public function insert_delivery_promo()
	{
		//$userwise_promo_img=$this->upload_userwise_promo_photo();
		
		$data=array(
		'promo_type_id'=>5,
		'promocode'=>strtoupper($this->input->post('promocode')),
		'discount'=>0,
		'description'=>$this->input->post('delivery_desc'),
		'promo_image_url'=>$this->input->post('delivery_promo_img'),
		);
		
		return $this->db->insert('promocode',$data);
	}
	//KT[date 4may 2018]
	public function get_delivery_promocode_no_of_rows()
	{
		$this->db->where('promo_type_id',5);
		return $this->db->get('promocode')->num_rows();
	}
	
	//KT[date 4may 2018]
	public function get_delivery_promo_list($per_page)
	{
		$this->db->select('promo_id,promocode,child_cat_name,p.status,p.promo_image_url');
		$this->db->join('child_category cc','cc.child_cat_id=p.child_cat_id');
		$this->db->where('promo_type_id',5);
		$this->db->where_not_in('promo_id',0);
		$this->db->where('p.status',1);
		return $this->db->get('promocode p',$per_page, $this->uri->segment(3))->result();
	}
	
	//KT[date 4may 2018]
	public function get_search_delivery_promocode_details($promocode)
	{
		$this->db->select('promo_id,promocode,child_cat_name,p.status,p.promo_image_url');
		$this->db->join('child_category cc','cc.child_cat_id=p.child_cat_id');
		$this->db->where('promo_type_id',5);
		$this->db->where_not_in('promo_id',0);
		$this->db->like('promocode',$promocode);
		$search = $this->db->get('promocode p');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	
			//KT[date 7may 2018]
		
	public function insert_subcat_promo()
	{
		//$userwise_promo_img=$this->upload_userwise_promo_photo();
		
		$data=array(
		'promo_type_id'=>6,
		'promocode'=>strtoupper($this->input->post('promocode')),
		'discount'=>$this->input->post('discount'),
		'description'=>$this->input->post('subcat_desc'),
		'promo_image_url'=>$this->input->post('subcat_promo_img'),
		'child_cat_id'=>$this->input->post('sub_cat')
		);
		
		return $this->db->insert('promocode',$data);
	}
	//KT[date 7may 2018]
	public function get_subcat_promocode_no_of_rows()
	{
		$this->db->where('promo_type_id',6);
		return $this->db->get('promocode')->num_rows();
	}
	
	//KT[date 7may 2018]
	public function get_subcat_promo_list($per_page)
	{
		$this->db->select('promo_id,promocode,child_cat_name,p.status,p.promo_image_url');
		$this->db->join('child_category cc','cc.child_cat_id=p.child_cat_id');
		$this->db->where('promo_type_id',6);
		$this->db->where_not_in('promo_id',0);
		$this->db->where('p.status',1);
		return $this->db->get('promocode p',$per_page, $this->uri->segment(3))->result();
	}
	
	//KT[date 7may 2018]
	public function get_search_subcat_promocode_details($promocode)
	{
		$this->db->select('promo_id,promocode,child_cat_name,p.status,p.promo_image_url');
		$this->db->join('child_category cc','cc.child_cat_id=p.child_cat_id');
		$this->db->where('promo_type_id',6);
		$this->db->where_not_in('promo_id',0);
		$this->db->like('promocode',$promocode);
		$search = $this->db->get('promocode p');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
}