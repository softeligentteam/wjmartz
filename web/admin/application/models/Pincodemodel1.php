<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pincodemodel extends CI_Model
{
	public function get_all_pincodes_with_charges($per_page)
	{
		$this->db->select('p.pincode_id,pincode,delivery_charges');
		//$this->db->join('area a','a.area_id=p.area_id');
		return $this->db->get('pincode p',$per_page, $this->uri->segment(3))->result();
	}	
	
	public function insert_delivery_charges()
	{
		$pincode=$this->input->post('pincode');
		$delivery_charge=$this->input->post('charge');
		//$area_id=$this->input->post('area_name');
		
		$data=array(
		'pincode'=>$pincode,
		'delivery_charges'=>$delivery_charge
		);
		
		return $this->db->insert('pincode',$data);
	}
	
	public function del_pincode($pincode_id)
	{
		$this->db->where('pincode_id',$pincode_id);
		return $this->db->delete('pincode');
	}
	
	public function get_no_of_rows()
	{
		return $this->db->get('pincode')->num_rows();
	}
	
	public function update_pincode()
	{
		$pincode_id=$this->input->post('pin_id');
		$pincode=$this->input->post('pin');
		//$area=$this->input->post('area');
		$delivery_charge=$this->input->post('del_charge');
		
		$data=array(
		'pincode'=>$pincode,
		'delivery_charges'=>$delivery_charge
		);
		
		$this->db->where('pincode_id',$pincode_id);
		$this->db->update('pincode',$data);
		return $this->db->affected_rows();
	}
}