<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personalizepackagemodel extends CI_Model
{
	public function get_personalizepackage_list()
	{
		$this->db->select('per_package_id,package_main_product,main_product_img'); 
		return $this->db->get('personalised_package')->result();
	}
	
	public function get_main_product_list($child_cat_id, $per_page)
	{	
		$this->db->select('prod_id,prod_name,prod_price,prod_img_url');
		$this->db->where_not_in('prod_id',0);
		$this->db->where('child_cat_id',$child_cat_id);
		return $this->db->get('products',$per_page, $this->uri->segment(3))->result();
	}
	
	public function get_sub_product_list(){
		$this->db->select('sub_product_id,pp.per_package_id,product_name,pp.per_product_img,product_price,quantity,in_stock,p.package_main_product');
		$this->db->where_not_in('sub_product_id',0);
		$this->db->join('personalised_package p','p.per_package_id = pp.per_package_id');
		$this->db->where_not_in('pp.per_package_id',0);
		
		return $this->db->get('personalised_package_products pp')->result();
	}
	
	public function get_addons_list(){
		$this->db->select('sub_product_id,product_name,product_description,per_product_img,quantity,product_price,in_stock');
		$this->db->where('is_addon',1);
		return $this->db->get('personalised_package_products')->result();
	}
	
	public function get_no_of_rows($child_cat_id)
	{
		$this->db->where('child_cat_id',$child_cat_id);
		return $this->db->get('products')->num_rows();
	}

	
	/******************************************************************************************/
	/******************************************************************************************/
	
	
	
	public function insert_main_product()
	{
		$package_main_product = $this->input->post('package_main_product');
		$main_product_img = $this->upload_package_photo();
		
		
		$data=array(
		'package_main_product'=>$package_main_product,
		'main_product_img'=>$main_product_img,
		'created_on'=>date('Y-m-d H:i:s'),
		'created_by'=>0,
		);
		
		return $this->db->insert('personalised_package',$data);
	}
	
	public function delete_main_product($per_package_id)
	{
		$this->db->where('per_package_id',$per_package_id);
		return $this->db->delete('personalised_package');
	}
	
	public function update_main_product()
	{
		$id=$this->input->post('id');
		$package_main_product=$this->input->post('package_main_product');
		
		$image=$this->input->post('old_image');
		$main_product_img=$this->upload_photo($image);
			
		$created_on = date('d-m-y H:i:s');
		
		$data=array(
			'package_main_product'=>$package_main_product,
			'main_product_img'=>$main_product_img,
			'created_on'=>$created_on
		);
		
		$this->db->where('per_package_id',$id);
		return $this->db->update('personalised_package', $data);
	}
	
	
	
	
	
	/******************************************************************************************/
	/******************************************************************************************/
	
	
	
	
	
	public function insert_sub_product()
	{
		$per_package_id=$this->input->post('per_package_id');
		$personalizedpackage_product_name=$this->input->post('personalizedpackage_product_name');
		$fixpackage_product_img=$this->upload_package_product_photo();
		$personalizedpackage_product_price=$this->input->post('personalizedpackage_product_price');
		$personalizedpackage_product_quantity=$this->input->post('personalizedpackage_product_quantity');
		
		$data=array(
		'per_package_id'=>$per_package_id,
		'product_name'=>$personalizedpackage_product_name,
		'per_product_img'=>$fixpackage_product_img,
		'product_price'=>$personalizedpackage_product_price, 
		'in_stock'=>1
		);
		
		return $this->db->insert('personalised_package_products',$data);
	}
	
	
	public function delete_sub_product($sub_product_id)
	{
		$this->db->where('sub_product_id',$sub_product_id);
		return $this->db->delete('personalised_package_products');
	}
	
	public function update_sub_product()
	{
		$sub_product_id=$this->input->post('sub_product_id');
		$sub_product_image=$this->input->post('sub_product_image');
		
		$per_package_id=$this->input->post('per_package_id');
		$sub_product_name=$this->input->post('sub_product_name'); 
		$main_product_img=$this->upload_photo($sub_product_image);
		$product_price=$this->input->post('product_price');  
		
		$data=array(
			'per_package_id'=>$per_package_id,
			'product_name'=>$sub_product_name,
			'per_product_img'=>$main_product_img,
			'product_price'=>$product_price, 
			'in_stock'=>1
		);
		$this->db->where('sub_product_id',$sub_product_id);
		return $this->db->update('personalised_package_products', $data);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/******************************************************************************************/
	/******************************************************************************************/
	
	
	public function insert_addon_product()
	{
		$package_addon=$this->input->post('package_addon');
		$addon_description=$this->input->post('addon_description'); 
		$addon_img=$this->upload_package_product_photo();
		$addon_quantity=$this->input->post('addon_quantity');
		$addon_price=$this->input->post('addon_price');
		
		$data=array(
		'per_package_id'=>0,
		'product_name'=>$package_addon,
		'per_product_img'=>$addon_img,
		'product_description'=>$addon_description,
		'product_price'=>$addon_price, 
		'quantity'=>$addon_quantity,
		'is_addon'=>1,
		'in_stock'=>1
		);
		
		return $this->db->insert('personalised_package_products',$data);
		
	}
	public function delete_addon_product($sub_product_id)
	{
		$this->db->where('sub_product_id',$sub_product_id);
		return $this->db->delete('personalised_package_products');
	}
	
	public function update_addon_product()
	{
		$package_addon_id=$this->input->post('package_addon_id');
		$addon_img=$this->input->post('addon_img');
		
		$package_addon=$this->input->post('package_addon');
		$addon_description=$this->input->post('addon_description');  
		$image=$this->upload_photo($addon_img);
		$addon_quantity=$this->input->post('addon_quantity'); 
		$addon_price=$this->input->post('addon_price');  
		
		$data=array(
		'product_name'=>$package_addon,
		'per_product_img'=>$addon_img,
		'product_description'=>$addon_description,
		'product_price'=>$addon_price, 
		'quantity'=>$addon_quantity,
		'is_addon'=>1,
		'in_stock'=>1
		);
		$this->db->where('sub_product_id',$package_addon_id);
		return $this->db->update('personalised_package_products', $data);
	}
	
	
	
	
	
	
	
	
	
	
	
	/******************************************************************************************/
	/******************************************************************************************/
	function upload_package_product_photo() {
    	$file_url = "";
		if ($_FILES["package_image"]["error"] == 0) {

			$file_element_name = 'package_image';

			$config['upload_path'] = PCAKAGEPRODUCTUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
			//$config['max_size'] = 1024 * 8;
			// $config['max_width'] = 512;
			// $config['max_height'] = 512;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload($file_element_name)) {
				$file_url = PCAKAGEPRODUCTUPLOADPATH."/no_image.jpg";
			   // $msg = $this->upload->display_errors('', '');
				// echo "<script>console.log($msg);</script>";
			} else {
				$data = $this->upload->data();
				$file_url = PCAKAGEPRODUCTUPLOADPATH.$data['file_name'];
				// echo "<script>console.log('File uploaded successfully.');</script>";
			}

			@unlink($_FILES[$file_element_name]);

		} else {
			 
			$file_url = PCAKAGEPRODUCTUPLOADPATH."/no_image.jpg";
		}
		
		return $file_url;
    }
	
	
	
	
	function upload_package_photo() {
    	$file_url = "";
		if ($_FILES["package_image"]["error"] == 0) {

			$file_element_name = 'package_image';

			$config['upload_path'] = PCAKAGEIMAGEUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
			//$config['max_size'] = 1024 * 8;
			// $config['max_width'] = 512;
			// $config['max_height'] = 512;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload($file_element_name)) {
				$file_url = PCAKAGEIMAGEUPLOADPATH."/no_image.jpg";
			   // $msg = $this->upload->display_errors('', '');
				// echo "<script>console.log($msg);</script>";
			} else {
				$data = $this->upload->data();
				$file_url = PCAKAGEIMAGEUPLOADPATH.$data['file_name'];
				// echo "<script>console.log('File uploaded successfully.');</script>";
			}

			@unlink($_FILES[$file_element_name]);

		} else {
			 
			$file_url = TEMPMEMBERUPLOADPATH."/no_image.jpg";
		}
		
		return $file_url;
    }
	
	
	
	
	function upload_photo($old_image){
    	$file_url = "";
		if ($_FILES["package_image"]["error"] == 0) {

			$file_element_name = 'package_image';

			$config['upload_path'] = PCAKAGEPRODUCTUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp'; 
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload($file_element_name)) {
				$file_url = PCAKAGEPRODUCTUPLOADPATH."/no_image.jpg"; 
			} else {
				$data = $this->upload->data();
				$file_url = PCAKAGEPRODUCTUPLOADPATH.$data['file_name']; 
			}

			@unlink($_FILES[$file_element_name]);

		} else {
			 
			$file_url = $old_image;
		}
		
		return $file_url;
    }
	
	function upload_main_product_photo($main_product_old_img){
    	$file_url = "";
		if ($_FILES["package_image"]["error"] == 0) {

			$file_element_name = 'package_image';

			$config['upload_path'] = PCAKAGEIMAGEUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp'; 
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload($file_element_name)) {
				$file_url = PCAKAGEIMAGEUPLOADPATH."/no_image.jpg"; 
			} else {
				$data = $this->upload->data();
				$file_url = PCAKAGEIMAGEUPLOADPATH.$data['file_name']; 
			}

			@unlink($_FILES[$file_element_name]);

		} else {
			 
			$file_url =$main_product_old_img;
		}
		
		return $file_url;
    }
	
	
}