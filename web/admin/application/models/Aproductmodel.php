<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aproductmodel extends CI_Model
{
	public function get_genral_products($per_page,$child_cat_id)
	{
		$this->db->select('prod_id,prod_name,prod_img_url,prod_price,prod_description,quantity');
		$this->db->where_not_in('prod_id',0);
		$this->db->where('child_cat_id',$child_cat_id);
		return $this->db->get('products',$per_page,$this->uri->segment(4,0))->result();
	}
	
		public function get_no_of_rows($child_cat_id)
	{
		$this->db->where('child_cat_id',$child_cat_id);
		return $this->db->get('products')->num_rows();
	}
	
	public function get_all_brands()
	{
		$this->db->select('brand_id,brand,brand_img_url');
		return $this->db->get('brands')->result();
	}
	
	public function get_no_of_rows_brands($brand_id)
	{
		$this->db->where('brand_id',$brand_id);
		return $this->db->get('products')->num_rows();
	}
	
	public function get_brand_products($per_page,$brand_id)
	{
		$this->db->select('prod_id,prod_name,prod_img_url,prod_price,prod_description,quantity');
		$this->db->where_not_in('prod_id',0);
		$this->db->where('brand_id',$brand_id);
		return $this->db->get('products',$per_page,$this->uri->segment(4,0))->result();
	}
	
	public function get_all_pune_special()
	{
		$this->db->select('brand_id,brand,brand_img_url');
		$this->db->where('is_pune_special',1);
		return $this->db->get('brands')->result();
	}
	

}