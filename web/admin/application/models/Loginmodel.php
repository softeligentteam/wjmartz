<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginmodel extends CI_Model
{
	public function authenticate_user()
	{
		$email =  $this->input->post('email');
		$pass = $this->input->post('password');
		
		$this->db->select('id, user_name');	
		$this->db->where('email',$email);
		$this->db->where('password', $pass);
		
		$this->db->where('status', 1);
		return $this->db->get('user')->row();
	}
}

