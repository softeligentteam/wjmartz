var url = "http://localhost/admin-wjmartz/index.php/";
var baseurl = "http://localhost/admin-wjmartz/";

function insert_success_close(){
	$('.insert_success').fadeOut();
}

function filter(){ 
	//debugger;
	var main_cat_id= $('#main_cat option:selected').val();  
	//alert(main_cat_id);
	var selected= $('#main_cat option:selected').html();
	$('.filtered').html(selected);
	$.ajax({
		type: "POST",
		url:  url+"ajax/get_sub_categories",
		data: {"main_cat_id" : main_cat_id},
		dataType: "html",
		beforeSend: function() {
		$('#displaySubcatList').show();
		$('#appendSubcatList').html('<span style="color:red">Please Wait.......</span>');
		},
		success: function(response) {
			$('#appendSubcatList').html('');
			var obj = jQuery.parseJSON(response);
			$(function() {
				$.each(obj, function(i, item){
					var j = i+1;
					$('#appendSubcatList').append('<tr><td class="text-center">'+j+'</td><td class="text-center"><img src="'+baseurl+item['sub_cat_img_url']+'" style="width:50px;" /></td><td class="text-center">'+item['sub_cat_name']+'</td><td class="text-center"><a onclick="" id="myBtn" class="btn btn-primary">Edit</a></td><td class="text-center"><a onclick="delete_this(\''+item['sub_cat_id']+'\',\''+item['sub_cat_name']+'\');" class="btn btn-danger">Delete</a></td></tr>');
				});
			});
		},
		error: function(error) { 
			$('#displaySubcatList').html('<tr><td colspan="4"><h4 style="color:red;"><center>Some Error Occoured, Please Try Again.</center></h3></td></tr>');
		}
	});
}

function delete_this(id, name)
{
		$('#confirm_delete').show();
		$('#dlt_msg').html('');
		$('#dlt_msg').append('Are you sure you want to delete '+name+' Category ?');
		$('#deleteMAinCatHref').prop('href',url+'parent_categories/delete_parent_category/'+id);
}