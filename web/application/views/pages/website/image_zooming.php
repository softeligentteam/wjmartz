<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="<?=base_url();?>website_assets/ImageZoom/imageviewer.css"  rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>website_assets/ImageZoom/lib/jquery-1.11.3.min.js"></script>
<script src="<?=base_url();?>website_assets/ImageZoom/imageviewer.js"></script>
 
<style>
    .mobileView{display:none !important;}
	#image-gallery {
      width: 100%;
      position: relative;
      height: 650px;
      background: #000;
    }
    #image-gallery .image-container {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 50px;
    }
    #image-gallery .prev,
    #image-gallery .next {
      position: absolute;
      height: 32px;
      margin-top: -66px;
      top: 50%;
    }
    #image-gallery .prev {
      left: 20px;
    }
    #image-gallery .next {
      right: 20px;
      cursor: pointer;
    }
    #image-gallery .footer-info {
      position: absolute;
      height: 50px;
      width: 100%;
      left: 0;
      bottom: 0;
      line-height: 50px;
      font-size: 24px;
      text-align: center;
      color: white;
      border-top: 1px solid #FFF;
    }
</style>
</head>

<body>
<div id="image-gallery">
	<a 
	href="<?=site_url();?>/website_home/product_details/<?=$pcat_id?>/<?=$ccat_id?>/<?=$prod_id?>" 
	style="position: fixed;
		padding: 2px 10px;
		background-color: white;
		top: 10px;
		right: 10px;
		border-radius: 4px;
		z-index: 9999;"
	>X</a>
    <div class="image-container"></div>
    <img src="<?=base_url();?>website_assets/ImageZoom/images/left.svg" class="prev"/>
    <img src="<?=base_url();?>website_assets/ImageZoom/images/right.svg"  class="next"/>
    <div class="footer-info">
        <span class="current"></span>/<span class="total"></span>
    </div>
</div>   
<script type="text/javascript">
$(function () {
    var images = [
		<?php $images =  explode(',', $product_details->prod_image_urls); foreach ($images as $img) {?>
			{
				small : '<?=IMAGEBASEPATH.$img ?>',
				big : '<?=IMAGEBASEPATH.$img ?>'
			},
		<?php } ?>
	];
 
    var curImageIdx = 1,
        total = images.length;
    var wrapper = $('#image-gallery'),
        curSpan = wrapper.find('.current');
    var viewer = ImageViewer(wrapper.find('.image-container'));
 
    //display total count
    wrapper.find('.total').html(total);
 
    function showImage(){
        var imgObj = images[curImageIdx - 1];
        viewer.load(imgObj.small, imgObj.big);
        curSpan.html(curImageIdx);
    }
 
    wrapper.find('.next').click(function(){
         curImageIdx++;
        if(curImageIdx > total) curImageIdx = 1;
        showImage();
    });
 
    wrapper.find('.prev').click(function(){
         curImageIdx--;
        if(curImageIdx < 0) curImageIdx = total;
        showImage();
    });
 
    //initially show image
    showImage();
});
</script>

 </body>
</html>
