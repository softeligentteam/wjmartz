<div class="page-head_agile_info_w3l desktopView">
		<div class="container">
			<h3>Contact Us</h3>
			<!--/w3_short-->
				 <div class="services-breadcrumb">
					<div class="agile_inner_breadcrumb">
						<ul class="w3_short">
							<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
							<li>About Us</span></li>
						</ul>
					</div>
				</div>
	   <!--//w3_short-->
	</div>
</div>
<br class="mobileView" /><br class="mobileView" />
<div class="banner_bottom_agile_info">
	<div class="container">
		<div class="agile-contact-grids">
			<div class="agile-contact-left">
				<div class="col-md-6 address-grid">
					<h4>For More <span>Information</span></h4>
					<div class="mail-agileits-w3layouts">
						<i class="fa fa-envelope-o" aria-hidden="true"></i>
						<div class="contact-right">
							<p>Mail </p><a href="mailto:info@example.com">support@wjmartz.com</a>
						</div>
						<div class="clearfix"> </div>
					</div>
					<div class="mail-agileits-w3layouts">
						<i class="fa fa-map-marker" aria-hidden="true"></i>
						<div class="contact-right">
							<p>Address</p><span>CST No. 56 FL 6, Sankalp Campus,<br />Shivajinagar, Pune - 411005</span>
						</div>
						<div class="clearfix"> </div>
					</div>
					<!-- Remove this comment after wjmartz is active on socaial medias.
					<ul class="social-nav model-3d-0 footer-social w3_agile_social two contact">
					  <li class="share">SHARE ON : </li>
						<li><a href="#" class="facebook">
							  <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
							  <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
						<li><a href="#" class="twitter"> 
							  <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
							  <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
						<li><a href="#" class="instagram">
							  <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
							  <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
						<li><a href="#" class="pinterest">
							  <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
							  <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
					</ul>
					-->
				</div>
				
				<div class="col-md-6 contact-form">
				<?php 
					 if(NULL !== $this->session->flashdata('message'))
					 { 
					  echo "<div class='col-sm-12 text-center'><label class='".$this->session->flashdata('css_class')."'>".$this->session->flashdata('message')."</label></div><br>";
					  }
				    ?>	
					<h4 class="white-w3ls">Contact <span>Form</span></h4>
					<form action="<?=site_url();?>/website_home/send_email_for_contact_us" method="post">
						<div class="styled-input agile-styled-input-top">
							<input type="text" name="Name" required="">
							<label>Name</label>
							<span></span>
						</div>
						<div class="styled-input">
							<input type="email" name="Email" required=""> 
							<label>Email</label>
							<span></span>
						</div> 
						<div class="styled-input">
							<input type="text" name="Subject" required="">
							<label>Subject</label>
							<span></span>
						</div>
						<div class="styled-input">
							<input type="Number" name="Mobile" required="">
							<label>Mobile</label>
							<span></span>
						</div>
						<div class="styled-input">
							<textarea name="Message" required=""></textarea>
							<label>Message</label>
							<span></span>
						</div>	 
						<input type="submit" value="SEND">
					</form>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
   </div>
</div>