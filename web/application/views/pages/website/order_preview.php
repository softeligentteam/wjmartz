<div class="page-head_agile_info_w3l desktopView">
	<div class="container">
		<h3>Order Preview</h3>
		<!--/w3_short-->
		 <div class="services-breadcrumb">
			<div class="agile_inner_breadcrumb">
				<ul class="w3_short">
					<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
					<li>Order Preview</li>
				</ul>
			</div>
		</div>
		<!--//w3_short-->
	</div>
</div>
<style>
	@media(max-width:768px){
		.banner-bootom-w3-agileits .container{padding:0;}
		.banner-bootom-w3-agileits .row{padding:0;}
		.mobilebottombtn{position:fixed;left:0;bottom:0;width:100%;padding:10px 0;background-color:black;border:none;color:white;z-index:1;}
		.nopad{padding-right:0 !important; padding-left:0 !important;}
	}
</style>
<div class="banner-bootom-w3-agileits">
	<div class="container">
       <?php  if($this->session->userdata('pay')!=1) { ?>
		    <form action="<?=$posted['action']?>" name="payuForm" method="post">
				<input type="hidden" name="key" value="<?php echo $posted['key'] ?>" />
				<input type="hidden" name="hash" value="<?php echo $posted['hash'] ?>"/>
				<input type="hidden" name="txnid" value="<?php echo $posted['txnid'] ?>" />
				<input type="hidden" name="firstname" value="<?=$posted['firstname']?>" class="width95"  required>
				<input type="hidden" name="phone" value="<?=$posted['phone']?>" required>
				<input type="hidden" name="amount" value="<?=$posted['amount']?>" required>
				<input type="hidden" name="email" value="<?=$posted['email']?>" required>
				<input type="hidden" name="productinfo" value="<?=$posted['productinfo']?>" required>
				<input type="hidden" name="surl" value="<?=$posted['surl']?>" required>
				<input type="hidden" name="furl" value="<?=$posted['furl']?>" required>
				<input type="hidden" name="udf1" value="<?=$customer_data['ref_order_id']?>">
		        <div class="row">
		            <div class="col-lg-12 mobileView">
						<br />
						<h2>Order Preview</h2>
					</div>
	                <div class="col-lg-12">
	                    <!--h2>Order Preview<a href="< ?=site_url();?>/Ordercontroller/enroll_member" class="pull-right btn btn-success">Proceed To Payment</a></h2-->
				        <hr style="margin:4px;" />
        				<div class="row">
        					<div class="col-md-10 nopad">
        						<div class="col-md-4 nopad">
        							<label>User Id :</label>
        						</div>
        						<div class="col-md-8 nopad">
        						<?=$customer_data['cust_id']?>
        						</div>
        						<div class="clearfix"></div>
        						<hr style="margin:4px;" />
        						<div class="col-md-4 nopad">
        							<label>User Name :</label>
        						</div>
        						<div class="col-md-8 nopad">
        							<?=$customer_data['first_name'].' '.$customer_data['last_name']?>
        						</div>
        						<div class="clearfix"></div>
        						<hr style="margin:4px;" />
        						<div class="col-md-4 nopad">
        							<label>Mobile No. :</label>
        						</div>
        						<div class="col-md-8 nopad">
        							<?=$customer_data['mobile']?>
        						</div>
        						<div class="clearfix"></div>
        						<hr style="margin:4px;" />
        						<div class="col-md-4 nopad">
        							<label>Email Id:</label>
        						</div>
        						<div class="col-md-8 nopad">
        							<?=$customer_data['email']?>
        						</div>
        						<div class="clearfix"></div>
        					</div>
        					<div class="col-lg-2 desktopView">
        						<img src="<?=base_url();?>/website_assets/images/icon.png" style="width: 123px; float: right;" />
        					</div>
        				</div>
        				<hr style="margin:4px;" />
        				
        				<div class="row">
        					<div class="col-md-8 nopad">
        						<div class="bs-docs-example">
        							<h2>Product Details</h2>
        							<hr>
        							<table class="table table-striped desktopView">
        								<thead>
        									<tr>
        										<th colspan="2" class="text-center">Product</th>
        										<th>Size</th>
        										<th>Quantity</th>
        										<th>Unit&nbsp;Price</th>
        										<th colspan="2">Total</th>
        									</tr>
        								</thead>
        								<tbody>
        									<?php foreach($product_data as $prod) {?>
        									<tr>
        										<td>
        											<img src="<?=IMAGEBASEPATH.$prod->prod_image_url?>" style="width:75px; float:left" />
        										</td>
        										<td><p style="float:left;"><b><?=$prod->prod_name?></b></p></td>
        										<td><?=$prod->size?></td>
        										<td>
        											<span id="qty"><?=$prod->quantity?> </span>
        										</td>
        										<td><?=$prod->price?></td>
        										<td><?=$prod->amount?></td>
        									</tr>
        									<?php } ?>
        								</tbody>
        							</table>
        						    <?php foreach($product_data as $prod) {?>
										<div class="col-sm-4 col-xs-4 mobileView" style="padding:0;font-size:12px;">
											<img src="<?=IMAGEBASEPATH.$prod->prod_image_url?>" style="width:100%;" />
										</div>
										<div class="col-sm-8 col-xs-8 mobileView">
											<p style="margin:7px 0; font-weight:600;"><?=$prod->prod_name?></p>
											<p style="margin:7px 0;">Price : <i class="fa fa-inr"></i> <?=$prod->price?></p>
											<p style="margin:7px 0;" >
												Size: <?=$prod->size?>
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
												Quantity : <?=$prod->quantity?>
											</p>
										</div>
										<div class="clearfix"></div>
										<hr style="padding:0;" />
									<?php } ?>
        						</div>
        					</div>
        					<div class="col-lg-4 nopad">
        						<div class="bs-docs-example" style="overflow-x:hidden;">
        							<h2>Delivery Details</h2>
        							<hr />
        							<table class="table table-striped">
        								<thead>
        									<!--<tr>
        										<th>Particular</th>
        										<th>Price</th>
        									</tr>-->
        									<tr>
        										<th>Ref. Order id</th>
        										<th><?=$customer_data['ref_order_id']?></th>
        										<input type="hidden" name="ref_order_id" value="<?=$customer_data['ref_order_id']?>">
        									</tr>
        									<tr>
        										<th>Order Date</th>
        										<th><?=$customer_data['order_date']?></th>
        									</tr>
        										<?php if($this->session->userdata('ptypeid')!=5)
        									{	
        									$amt_total= $customer_data['discount_amount']- $customer_data['delivery_charges'];
        									}else{
        										$amt_total= $customer_data['discount_amount']- 0;
        									}
        									?>
        									<?php if($this->session->userdata('ptypeid')!=6) {?>
        									<tr>
        										<th>Discounted amount</th>
        										<th><i class="fa fa-inr"></i><?=$customer_data['amount'] - $amt_total ?></th>
        									</tr>
        									<?php } else { ?>
        									<tr>
        										<th>Discounted amount</th>
        										<th><i class="fa fa-inr"></i><?=$this->cart->format_number($this->cart->total()) - $customer_data['amount']?></th>
        									</tr>
        									<?php } ?>
        									<?php if($this->session->userdata('ptypeid')!=5)
        									{?>	
        									<tr>
        										<th>Delivery Charges</th>
            								
        										<th><?=$customer_data['delivery_charges']?></th>
        									</tr>
        									<?php } else { ?>
        									<tr>
        										<th>Delivery Charges</th>
        										
        										<th>0</th>
        									</tr><?php } ?>
        									
        									<tr>
        										<th>Total Amount</th>
        										<th><?=$customer_data['discount_amount']?></th>
        										<input type="text" value="<?=$customer_data['discount_amount']?>" name="disc_amount" hidden>
        									</tr>
        									<tr>
        										<td colspan="2">
        											<b>Delivery Address</b><br />
        											<?=$customer_data['address'].' '.$customer_data['area_name'].' '.$customer_data['city'].' '.$customer_data['state'].'-'.$customer_data['pincode']?>
        										</td>
        									</tr>
        								</thead>
        							</table> 
        							<button type="submit" class="mobilebottombtn btn btn-success col-sm-12" style="background-color:black;border-color:black;border-radius:none;">Place Order</button>
        						</div>
        					</div>
            			</div>
            		</div>
            	</div>
			</form>
		<?php } else { ?>
    		<form action="<?=site_url()?>/ordercontroller/enroll_member" name="payuForm1" method="post">
    		    <div class="row">
					<div class="col-lg-12 mobileView">
						<br />
						<h2>Order Preview</h2>
					</div>
    				<div class="col-md-10">
    					<div class="col-md-4">
    						<label>User Id :</label>
    					</div>
    					<div class="col-md-8">
    					<?=$customer_data['cust_id']?>
    					</div>
    					<div class="clearfix"></div>
    					<hr style="margin:4px;" />
    					<div class="col-md-4">
    						<label>User Name :</label>
    					</div>
    					<div class="col-md-8">
    						<?=$customer_data['first_name'].' '.$customer_data['last_name']?>
    					</div>
    					<div class="clearfix"></div>
    					<hr style="margin:4px;" />
    					<div class="col-md-4">
    						<label>Mobile No. :</label>
    					</div>
    					<div class="col-md-8">
    						<?=$customer_data['mobile']?>
    					</div>
    					<div class="clearfix"></div>
    					<hr style="margin:4px;" />
    					<div class="col-md-4">
    						<label>Email Id:</label>
    					</div>
    					<div class="col-md-8">
    						<?=$customer_data['email']?>
    					</div>
    					<div class="clearfix"></div>
    					<hr style="margin:4px;" />
    				</div>
					<div class="col-lg-2">
    		            <br />
    					<center><img src="<?=base_url();?>/website_assets/images/icon.png" style="width:100px;" /></center>
    				</div>
				</div>
    			<hr />
    			<div class="row">
    				<div class="col-md-8">
    					<div class="bs-docs-example">
    						<h2>Product Details</h2>
    						<hr>
    						<table class="table table-striped desktopView">
    							<thead>
    								<tr>
    									<th colspan="2" class="text-center">Product</th>
    									<th>Size</th>
    									<th>Quantity</th>
    									<th>Unit&nbsp;Price</th>
    									<th colspan="2">Total</th>
    								</tr>
    							</thead>
    							<tbody>
    								<?php foreach($product_data as $prod) {?>
    								<tr>
    									<td>
    										<img src="<?=IMAGEBASEPATH.$prod->prod_image_url?>" style="width:75px; float:left" />
    									</td>
    									<td><p style="float:left;"><b><?=$prod->prod_name?></b></p></td>
    									<td><?=$prod->size?></td>
    									<td>
    										<span id="qty"><?=$prod->quantity?> </span>
    									</td>
    									<td><?=$prod->price?></td>
    									<td><?=$prod->amount?></td>
    								</tr>
    								<?php } ?>
    							</tbody>
    						</table>
    						<?php foreach($product_data as $prod) {?>
    							<div class="col-sm-4 col-xs-4 mobileView" style="padding:0;font-size:12px;">
    								<img src="<?=IMAGEBASEPATH.$prod->prod_image_url?>" style="width:100%;" />
    							</div>
    							<div class="col-sm-8 col-xs-8 mobileView">
    								<p style="margin:7px 0; font-weight:600;"><?=$prod->prod_name?></p>
    								<p style="margin:7px 0;">Price : <i class="fa fa-inr"></i> <?=$prod->price?></p>
    								<p style="margin:7px 0;" >
    									Size: <?=$prod->size?>
    									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
    									Quantity : <?=$prod->quantity?>
    								</p>
    							</div>
    							<div class="clearfix"></div>
    							<hr style="padding:0;" />
    						<?php } ?>
    					</div>
    				</div>
    				<div class="col-lg-4">
    					<div class="bs-docs-example" style="overflow-x:hidden;">
    						<h2>Delivery Details</h2>
    						<hr />
    						<table class="table table-striped">
    							<thead>
    								<!--<tr>
    									<th>Particular</th>
    									<th>Price</th>
    								</tr>-->
    								<tr>
    									<th>Ref. Order id</th>
    									<th><?=$customer_data['ref_order_id']?></th>
    								</tr>
    								<tr>
    									<th>Order Date</th>
    									<th><?=$customer_data['order_date']?></th>
    								</tr>
    								<tr>
							    	<?php 
							    	$protype = $this->session->userdata('ptypeid');
							    
							    	if($protype == 5)
									{
									print_r($this->session->userdata('ptypeid'));
									?>
    									<th>Delivery Charges</th>
    									
    								
    									<th>0</th>
    									<?php } else{
    									?>
    									<th>Delivery Charges</th>
    									
    									<th><?=$customer_data['delivery_charges']?></th>
    									<?php } ?>
    								</tr>
    								<tr>
    									<th>Total Amount</th>
    									<th><?=$customer_data['discount_amount']?></th>
    									<input type="text" value="<?=$customer_data['discount_amount']?>" name="disc_amount" hidden>
    								</tr>
    								<tr>
    									<td colspan="2">
    										<b>Delivery Address</b><br />
    										<?=$customer_data['address'].' '.$customer_data['area_name'].' '.$customer_data['city'].' '.$customer_data['state'].'-'.$customer_data['pincode']?>
    									</td>
    								</tr>
    							</thead>
    						</table> 
    					    <button onClick="return confirm('Are you sure you want to place this order with COD ?')" type="submit" class="mobilebottombtn btn btn-success col-sm-12" style="background-color:black;border-color:black;border-radius:none;">Place Order</button>
            			</div>
    				</div>
    			</div>
    		</form> 
        <?php }?>
</div>
<!--
<div class="modal fade" id="placeorder" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-- >
		<div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			  <h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to place this order ?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" data-dismiss="modal">Yes</button>
				<button type="button" class="btn btn-info" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
-->