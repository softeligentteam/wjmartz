<!-- banner_bottom_agile_info -->
<div class="page-head_agile_info_w3l">
		<div class="container">
			<h3>Terms & Conditions</h3>
			<!--/w3_short-->
				 <div class="services-breadcrumb">
					<div class="agile_inner_breadcrumb">
						<ul class="w3_short">
							<li><a href="<?=site_url();?>website_home">Home</a><i>|</i></li>
							<li>Terms & Conditions</li>
						</ul>
					</div>
				</div>
	   <!--//w3_short-->
	</div>
</div>
<!-- //banner_bottom_agile_info -->
<!--Start Work Area -->
<div class="banner-bootom-w3-agileits">
	<div class="container">
	   <div class="row">
			<div class="col-md-12 col-lg-12">
				<hr />
				<p>
					This website and application is owned and managed by WJmartz. By accessing and using www.wjmartz.com Website/Application, you are legally bound by these Terms & Conditions. The terms "you" and "User" refers to anyone who accesses the Website/Application.
				</p><br />
				<p>
					When the user is browsing through the Website/Applications a hyperlink or pop up might open another page. Such pages are bound by their own individual Terms of Use. WJmartz doesn't take any responsibility for any conflict arose due to the clash of the Terms of Use on various websites redirect from WJmartz.
				</p><br />
				<p>
					WJmartz has the authority to change the Terms & Conditions at any given time without intimation for various situations like new laws or change in the authority of the company. Such changes will be updated on the website and application under "Terms & Conditions" section.
				</p><br />
				<p>
					As the user is bound legally, please be updated with our terms and conditions before shopping. 
			   </p>
			   <hr />
			   <h3>1) USE OF THE WEBSITE / APPLICATION</h3><hr />
				<p>
					While using the website and the application you agree not to copy, reproduce, modify, create derivative works from, or store any Content, in whole or in part. And also, Display, perform, publish, distribute, transmit, broadcast or circulate any Content to anyone, or for any commercial purpose, without a prior written consent from WJmartz.
				</p>
				<!--p>
					While using the website and the application you agree not to copy, reproduce, modify, create derivative works from, or store any Content, in whole or in part. And also display, perform, publish, distribute, transmit, broadcast or circulate any Content to anyone, or for any commercial purpose, without a prior written consent from WJmartz.
				</p-->
				<p>
					Every content present on the website and application is protected by copyright and other intellectual property laws registered under WJmartz or its licensors. 
				</p>
				<p>
					All trade names, trademarks, service marks and other product and service names and logos on the Website/Application and within the Content are proprietary to their respective owners and are protected by applicable trademark and copyright laws. Any of the trademarks, service marks or logos (collectively, the "Marks") displayed on the Website/Application may be registered or unregistered marks of WJmartz or others. 
				</p>
				<p>
					Nothing contained on this Website/Application should be deciphered as granting any kind of license/right to use any of the Marks displayed on the Website/Application without the prior written permission of WJmartz or a third party owner (if involved) of such Marks. Any other unauthorized uses of the Marks or any other Content, are strictly and legally prohibited.
				</p>
				<p>
					To request permission to use any Content or other WJmartz material, please contact WJmartzaz@Info@wjmartz.com. You cannot use the Website/Application for any unlawful purposes. You shall honor and abide all the reasonable requests made by the Website/Application to protect WJmartz proprietary interests in the Website/Application.
				</p>
				<hr />
				<h3>2) REGISTRATION</h3><hr />
				<p>
					As part of the registration process, the User must select a username and password and provide the Website/Application with accurate, complete and updated information. Failing to do so counts as a breach of this Agreement. This may result in immediate termination of your access to the account created.
				</p>
				<hr />
				<h3>3) LIMITATION OF LIABILITY</h3><hr />
				<p>
					You are entirely answerable for activities carried by you in reference with your browsing and use of the Website/Application. If unhappy with the Content presented on the Website/Application or with reference to these Terms of Use, your complete and sole solution is to stop using the Content and the Website/Application. The Website/Application is not accountable to pay you any damages in connection with your browsing or use of the Website/Application
				</p>
				<p>
					Due to the numerous sources, the Content is contributed, and the potential hazards of electronic distribution, there may be delays, omissions or inaccuracies in the Content on the Website/Application.
				</p>
				<p>
					The CONTENT and the WEBSITE/APPLICATION are PROVIDED "AS IT IS", without ANY WARRANTIES. NEITHER the WEBSITE/APPLICATION nor WJmartz makes ANY GUARANTEES OR WARRANTIES on the Exactness, COMPLETENESS, TIMELINESS OR CURRENTNESS of  RESULTS to be OBTAINED from, ACCESSING and USING the WEBSITE, Any and all content and material published on the APPLICATION/WEBSITE of WJmartz that is accessed through a direct or indirect link (hyperlink) THROUGH THE WEBSITE/APPLICATION. 
				</p>
				<p>
					THE WEBSITE and APPLICATION HEREBY deny ANY AND ALL WARRANTIES, EXPRESS OR Mentioned that includes WARRANTIES given by MERCHANTABILITY OR FITNESS given as a PARTICULAR PURPOSE OR USE AND OF NON INFRINGEMENT 
				</p>
				<p>
					NEITHER THE WEBSITE/APPLICATION NOR WJmartz SHALL BE LIABLE TO THE USER OR ANYONE ELSE FOR ANY Error, DELAY, Suspension in service, Failure OR Elimination, REGARDLESS OF CAUSE showed or unshown, OR FOR ANY DAMAGES RESULTING THEREFROM. 
				</p>
				<p>
					IN NO EVENT WILL THE WEBSITE/APPLICATION, WJmartz NOR ANY OF THEIR THIRD PARTY LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES, INCLUDING BUT NOT LIMITED TO count of TIME, MONEY, PROFITS OR GOODWILL, WHETHER IN CONTRACT are STRICT LIABILITY OR OTHERWISE, AND WHETHER OR NOT SUCH DAMAGES ARE Envisioned OR not WITH RESPECT TO ANY USE OF THE WEBSITE/APPLICATION Additionally, also FOR ANY LOSS OR INJURY RESULTING FROM USE OF THE WEBSITE/APPLICATION, IN WHOLE OR PART, WHETHER CAUSED BY Neglect, Accidents BEYOND ITS CONTROL IN Obtaining, Organizing, INTERPRETING, REPORTING OR DELIVERING THE WEBSITE/APPLICATION AND ANY CONTENT AT THE WEBSITE OR OTHERWISE including ANY DECISION MADE OR ACTION TAKEN BY YOU IN RELIANCE ON SUCH CONTENT OR THE WEBSITE/APPLICATION.
				</p>
				<p>
					The Website/Application or WJmartz doesn't take responsibility for any use of third-party software to access our Website/Application and shall have no accountability whatsoever to any person or entity for the accuracy or completeness of any consequence generated by any such software.
				</p>
				<hr />
				<h3>4) LINKS TO OTHER WEBSITES</h3><hr />
				<p>
					The user may access the Website/Application following a hypertext or links to the website posted on other sites that are not related to WJmartz. These links and texts are provided for the easy usage of the user only, and these links are the exclusive responsibility of WJmartz.
				</p> 
				<p>	
					You agree that the Website/Application is not responsible for the content or operation of such websites. Except as described below, a hyperlink from this Website/Application to another Website/Application does not imply or mean that the website/application endorses the content on that Website/Application or the operator or operations of that site/app. You are solely responsible for determining the extent to which you may use any content at any other application/websites to which you link from the Website/Application.
				</p>
				<hr />
				<h3>5) THE USER'S CONTENT</h3><hr />
				<p>
					The User grants WJmartz the non-exclusive right to use all information and material used to register and browse into the Website/Application by the User (other than third-party material transmitted through private electronic mail) in any of The WJmartz print or electronic publications ("Other Content"). Users entering material into the Website/Application are responsible for the Other Content. 
				</p>
				<p>
					Neither the Website/Application nor WJmartz has any responsibility for Other Content, including the content of any messages or information posted by Users or others, or for the content of information accessible via direct or indirect hyperlinks from the Website/Application. 
				</p>
				<p>
					The Website/Application holds the right that it may or may not apply its sole discretion, to review, edit, or delete Other Content that the Website/Application considers illegal, offensive, or otherwise inappropriate. You should not input or distribute any material through the Website/Application that is promotional in nature, including solicitations for funds or business, without the prior written authorization of the Website/Application.
				</p>
				<p>
					The User agrees to indemnify the Website/Application and WJmartz from all damages, liabilities, costs, charges and expenses, including reasonable attorneys' fees, that the website/application, WJmartz, their affiliates, employees, and authorized representatives may incur as a result of either: <br />(i) the User's breach of this Agreement; or <br />(ii) material entered into the Website/Application with the use of the User's screen name or password.
				</p><hr />
				<h3>6) PAYMENTS, CANCELLATION & REFUNDS</h3><hr />
				<p>
					All information, reports, content and access rights purchased on the Website/Application are non-refundable.
				</p>
				<p>
					We as a merchant shall be under no liability whatsoever in respect of any loss or damage occurred directly or indirectly out of the decline of authorization for any Transaction, including the Cardholder surpassing the preset limit commonly agreed by us with our acquiring bank time to time.
				</p>
				<hr />
				<h3>7) ADDITIONAL LEGAL TERMS</h3><hr />
				<p>
					This Agreement will serve until terminated by either us or by you. Any of us both (we and user) can terminate the Agreement by notifying the other party by telephone or electronic mail of the decision to terminate well in advance.
				</p>
				<p>	
					WJmartz holds the authority to discontinue or change the Website/Application and its availability to you, at any time.
				</p>
				<p>
					This Agreement constitutes the entire agreement between the parties relating to Website/Application and supersede any and all other agreements, oral or in writing, with respect to the Website/Application. The failure of this Website/Application, to insist upon strict compliance with any term of this Agreement shall not be construed as a waiver with regard to any subsequent failure to comply with such term or provision. This Agreement is personal to you, and you may not assign your rights or obligations to anyone. 
				</p>
				<p>
					Due to changes in the laws abided in India, any of the above-said points in this agreement are invalid or not enforceable by law, the rest all terms will be implemented with positive force and effect. This Agreement, your rights and obligations, and all actions contemplated by this Agreement shall be governed by the laws of India and come under the jurisdiction of courts of Pune, as if the Agreement was a contract wholly entered into and wholly performed within Pune, and any litigation related to this Agreement shall be brought particularly to the courts of Pune. All rights not expressly granted herein are reserved.
				</p> 
				<hr />
				<h3>8) ANTI-HACKING PROVISION</h3><hr />
				<p>
					You expressly agree not to use this Website/ Application in any manner or for any purpose that is prohibited by these terms and conditions. In addition, you expressly agree not to:
				</p>
				<ol>
					<li>Use this Website/Application for any purpose that is prohibited by any and all laws and regulations, or to promote the violation of any law and regulation </li>
					<li>Using or attempting to use any tool, automated device, program algorithm, process or methodology or manual process having similar processes or functionality like "deep-link," "scraper," "robot," "bot," "spider," "data mining," "computer code" or bribing the employees or deceiving them to give information, to access, acquire, copy, or monitor any portion of the Website/Application or any data or content found on or accessed through the Website/Application without prior express written consent and knowledge of WJmartz is prohibited.</li>
					<li>Reaching or attempting to obtain through any means any materials or information on the Website/Application that have not been made publicly by pure intent that is not available either in our public display on the Website/Application or through others accessibility by a visible link on the Website/Application.</li>
					<li>On the counterpart, in any way bypass or circumvent or any other measure employed to limit or prevent access to the Website/Application or its content intentionally or unintentionally,</li>
					<li>Violate the security of the Website/Application or attempt to gain unauthorized access to the Website/Application, data, materials, information, computer systems or networks connected to any server associated with this WebSite/Application, through any form of hacking, password mining or by any other means.</li>
					<li>Interfere or attempt to interfere with the proper working of the Website/Application or any activities conducted on or through the Website/Application, including accessing any data, content or other information prior to the time that it is intended to be available to the public on the Website/Application;</li>
					<li>Take or attempt any action that, in the sole discretion of this Application/Website's operators, imposes or may impose an unreasonable or disproportionately large load or burden on the Website/Application or such operation's infrastructure.</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<!--End Work Area-->
