  <div class="page-head_agile_info_w3l">
		<div class="container">
			<h3>Change Password</h3>
			<!--/w3_short-->
				 <div class="services-breadcrumb">
					<div class="agile_inner_breadcrumb">
						<ul class="w3_short">
							<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
							<li>Change Password</span></li>
						</ul>
					</div>
				</div>
	   <!--//w3_short-->
	</div>
</div>
	 
<div class="banner_bottom_agile_info">
	<div class="container">
		<div class="agile-contact-grids">
			<div class="agile-contact-left">
				<div class="col-md-offset-3 col-md-6 contact-form">
					<h4 class="white-w3ls">Change <span>Password</span></h4>
					<form action="<?=site_url();?>/user_registration/fb_change_password" method="post" onsubmit="return validateForgotPassword();">
						<div class="styled-input agile-styled-input-top">
							<input class="form-control" type="password" name="oldpassword" id="password" required=""  maxlength="15" minlength="6" min="6" >
							<label>Enter New Password</label>
							<span></span>
						</div>
						<div class="styled-input agile-styled-input-top">
							<input class="form-control" type="password" name="password" id="conpass"required=""  maxlength="15" minlength="6" min="6" onblur="validate_change_password_fp();">
							<label>Confirm Password</label>
							<p class="error_msg" id="Error-msg-conpassword"></p>
							<span></span>
						</div>
						<input type="submit" value="SUBMIT">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>