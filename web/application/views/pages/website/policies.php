<!-- banner_bottom_agile_info -->
<div class="page-head_agile_info_w3l desktopView">
	<div class="container">
		<h3>Privacy & Return Policy</h3>
		<!--/w3_short-->
		<div class="services-breadcrumb">
			<div class="agile_inner_breadcrumb">
				<ul class="w3_short">
					<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
					<li>Privacy & Return Policy</li>
				</ul>
			</div>
		</div>
		<!--//w3_short-->
	</div>
</div>
<!-- //banner_bottom_agile_info -->
<!--Start Work Area -->
<div class="banner-bootom-w3-agileits">
	<div class="container">
		<div class="row">
			<br class="mobileView" />
			<h2 style="text-align:center;" class="mobileView">Privacy & Return Policy</h2>
			<hr class="mobileView" />
			<div class="col-md-12 col-lg-12">
				<h3>Terms & Conditions</h3><hr />
				<p>
Welcome to WJmartz.com. Wjmartz.com is owned and managed by WJmartz. If you continue to browse and use this website/application you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern WJmartz relationship with you in relation to this website/application.				</p><br >
				<p>
The term WJmartz or 'us' or 'we' refers to the owner of the website/application whose registered office is CST NO 50, FL 6 SANKALP SOCIETYCOMPS SHIVAJINAGAR/PUNE 411005. Our company registration number is 27AAGPF8989B1ZG India. The term 'you' refers to the user or viewer of our website/application.				</p>
				<hr />
				<h3>The use of this website/application is subject to the following terms of use:</h3>
				<br />
				<ul>
					<li>The content of the pages of this website/application is for your general information and use only. It is subject to change without notice.</li><br />
					<li>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website/application for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li><br />
					<li>Your use of any information or materials on this website/application is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website/application meet your specific requirements.</li><br />
					<li>This website/application contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions. All trademarks reproduced in this website/application which are not the property of, or licensed to WJmartz are acknowledged on the website/application.</li><br />
					<li>Unauthorized use of this website/application may give rise to a claim for damages and/or be a criminal offence. From time to time this website/application may also include links to other websites/application. These links are provided for your convenience to provide further information. They do not signify that we endorse the application/website(s). We have no responsibility for the content of the linked application/website(s).</li><br />
					<li>You may not create a link to this website/application from another website/application or document without WJmartz’s prior written consent. Your use of this website</li><br />
					<li>Website/application and any dispute arising out of such use of the website/application are subject to the laws of India or regulatory authority within the country of India.</li>
				</ul>
				<br />
				<p>
Here at WJmartz, we try to get the exemplary products for our consumer at a drop low price. As all payments are done through digital mode, we assure you that the product you pay for is the product you obtain without even a minor change.				</p>
				<br />
				<p>
Since we provide the best products possible at drop low price and deliver with efficiency, cancellation, exchange and refund of the products after placing will not be entitled.				</p>
				<br />
				<p>
For the customer to know all about the product, every detail of the product is displayed. For any further issues or queries before placing the order, reach out to us at support@wjmartz.com.				</p>
				<br />
				<p>
For the convenience of the user, every product is sorted into the criteria that define if the product is acceptable for exchange or not. The exchange and the cancellation option will not be available for the products that are not eligible according to our criteria.				</p>
				<hr />
				<h3>Cancellation</h3>
				<br />
				<p>
Orders once placed cannot be cancelled as we start processing the order as soon as items are ordered.				</p><br >
				<p>
All orders undergo a rigorous systemic and manual process in order to ensure the order reaches on time.				</p>
				<hr />
				<h3>Cancellation Charges</h3>
				<br />
				<p>
					In event of any cancellation, we will be charging Rs.30 from the order value for all cancellations, as every order has to undergo a system as well as manual process, during the event of cancellation the entire process is stopped.
				</p>
				<hr />
				<h3>Return or exchange</h3>
				<br />
				<p>
The products you see are the products we deliver exactly and certainly. We provide all the necessary details about the product under the chosen product.</p><br >
				<p>
The description of the product also involves the eligibility criteria of exchange. The products qualified under these criteria will only be authorized for exchange. Our team makes sure that the desired product is thoroughly screened and maintained in the absolute form until the product reaches your doorstep. If the customer has any other queries beyond the mentions product details reach us at support@wjmartz.com.	
</p>
				<hr />
				<h3>For products applicable for return and exchange</h3>				
				<br />
				<p>
For product eligible for return and exchange – please go to menu option – order history for placing exchange of item/s
To be qualified for return & exchange –
				</p><br >
				<p>
Your item must be unused / tampered and in the same condition that you received it. It must also be in the original packaging.			
</p><br >
				<p>
Tags attached to the product should not be removed in case you wish to return the item. Products without broken tag will not be accepted  
				</p><br >
				<p>
					Once your return is received and inspected, we will send you a notification via SMS or E-mail
				</p><br >
				<p>
					We will also notify you of the approval or rejection.
				</p><br >
				<p>
					If replacement available, the product will be dispatched and notified to you via SMS/e-mail/call.
				</p><br >
				<p>
If a replacement is not available we will refund the full purchase price of your item excluding delivery charges which is non refundable.	
			</p><br >
				
				<h3>Refund</h3><br />
				<p>For refund cases, the value of the product is eligible for refund, excluding all offer and coupon discounts.<p><br />
				<p>Amount paid by customer is the value that will be eligible for refund after excluding all types of discount.<p><br />
				<p>In cases of refund, the refund will be processed, and a credit will automatically be applied to your original method of payment, within 15working days.
<center>
Or
</center>
<br />
					You will be asked to write an email with order number, product details and account number with (Name as on account, IFSC code and Bank name) Do ensure the details provided are accurate as WJmartz will not be responsible.<p><br />
				<p>For COD purchases, customer needs to mail their Account details (Name as on account, IFSC code and Bank name) to support@wjmartz.com and the value will be deposited is customer account.<p><br />

				<hr />
				<h3>Timelines for Return</h3>
				<br />
				<p>
					Return accepted only if request given within 6 days from product delivery. For eligible products, return for exchange accepted only if request given within 6 days from product delivery.
				</p>
				<hr />
				<h3>Customer support</h3>
				<br />
				<p>
					For any Suggestions & feedback, you may write to us through contact section in our app or write to us on support@wjmartz.com and we will get back to your query.
				</p>
				<hr />
				<h3>Privacy Policy</h3>
				<br />
				<p>
					This privacy policy sets out how WJmartz uses and protects any information that you provide to WJmartz when you use this website/application.
				</p><br >
				<p>
					WJmartz is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website/application, and then you can be assured that it will only be used in accordance with this privacy statement?
				</p><br >
				<p>
					WJmartz may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is revised and effective from 28/11/2017.
				</p>
				<hr />
				<h3>What we collect</h3>
				<br />
				<p>
					We may collect the following information
				</p>
				<ul>
					<li>Name and Job title</li>
					<li>Contact Information including email address, Date of birth & phone number</li>
					<li>Demographic Information such as City, postcode, preferences and interests</li>
					<li>Other Information relevant to service enquiry, customer surveys and/or offers</li>
				</ul>
				<hr />
				<h3>What we do with the information we gather</h3>
				<br />
				<p>
					We require this information to understand your needs and provide you with a best service, and in particular for the following reasons:
				</p><br >
				<ul>
					<li>Internal record keeping.</li><br />
					<li>We may use the information to improve our products and services.</li><br />
					<li>We may periodically send promotional emails/SMS about new products, special offers or other information which we think you may find interesting using the email address which you have provided.</li><br />
					<li>From time to time, we may also use your information to contact you for feedback, market research purposes. We may contact you by email,SMS, phone, fax or mail. We may use the information to customize the website/application according to your interests.</li>
				</ul>
				<hr />
				<h3>Security</h3>
				<br />
				<p>We are committed to ensuring that your information is secure. In order to prevent unauthorized access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>
				<hr />
				<h3>How we use cookies</h3>
				<br />
				<p>
					A cookie is a small file which asks permission to be placed on your computer's hard drive. Once you agree, the file is added and the cookies helps analyze web/app traffic or lets you know when you visit a particular site. Cookies allow web/app applications to respond to you as an individual. The web/app application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.
				</p><br >
				<p>
					We use traffic log cookies to identify which pages are being used. This helps us analyse data about webpage/app traffic and improve our website/application in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system. Overall, cookies help us provide you with a better website/application, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us. You can choose to accept or decline cookies. Most web/app browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website/application.
				</p>
				<hr />
				<h3>Links to other websites/application</h3>
				<br />
				<p>
					Our website/application may contain links to other website/application of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website/application. Therefore, we cannot be responsible for the protection and privacy of any information which you provide while visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website/application in question.
				</p>
				<hr />
				<h3>Controlling your Personal Information</h3>
				<br />
				<p>
					You may choose to restrict the collection or use of your personal information in the following ways:
				</p><br >
				<ul>
					<li>
						Whenever you are asked to fill in a form on the website/application, look for the box that you can click to indicate that you do not want the information to be used by anybody for promotional purposes. If such box is not available, you may choose not to fill such form. However, by submitting the filled enquiry form, you will be construed to have foregone your right and Company may choose to send promotional emails and materials from time to time.
					</li><br />
					<li>
						If you have previously agreed to us using your personal information for promotional purposes, you may change your mind at any time by writing to or emailing us at support@wjmartz.com
					</li>
				</p>
				<br />
				<p>
					We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting.
				</p>
				<hr />
				<h3>Contacting Us</h3>
				<br />
				<p>
					If there are any questions regarding this privacy policy you may contact us using the information below:
				</p> 
				<br />
				<p>
					Email : support@wjmartz.com,
				</p>
				<br />
				<p>
					Address : WJmartz, CST NO 56 FL 6, SANKALP COMPS, SHIVAJINAGAR, PUNE 411005.
				</p>	
			</div>
		</div>
	</div>
</div>
<!--End Work Area-->      