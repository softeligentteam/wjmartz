<div class="desktopView">
    <!-- /banner_bottom_agile_info -->
    <div class="page-head_agile_info_w3l">
    		<div class="container">
    			<h3><?=$pcat_name->parent_cat_name;?></h3>
    			<!--/w3_short-->
    				 <div class="services-breadcrumb">
    					<div class="agile_inner_breadcrumb">
    						<ul class="w3_short">
    							<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
    							<li><?=$pcat_name->parent_cat_name?><i>|</i></li>
    							<li><?php if(isset($ccat_name->child_cat_name)){$ccat_name->child_cat_name;} ?></li>
    						</ul>
    					</div>
    				</div>
    	   <!--//w3_short-->
    	</div>
    </div>
    
    <!---728x90--->
    <!-- banner-bootom-w3-agileits -->
    <div class="banner-bootom-w3-agileits">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-3">
    			<?php echo form_open(current_url(), array('id'=>'filterform'));?>
				<div class="community-poll">
    					<h4><i class="fa fa-bars"></i>&nbsp;&nbsp;Sort By Type</h4>
    					<div class="swit form">	
    						 <?php
    							foreach($ptype_list as $object){
    								echo "<div class='check_box'><label>".form_radio(array('name'=>'ptype_id', 'value'=>$object->ptype_id, 'checked'=>set_checkbox('ptype_id', $object->ptype_id, false),'style'=>'margin: 15px 15px 0px 0px;','class'=>'filterby'));
    								echo form_label($object->ptype).'</label></div><hr style="margin:0;"/>';
    							}
    						?>
    					</div>
    				</div>
    				<div class="community-poll">
    					<h4><i class="fa fa-bars"></i>&nbsp;&nbsp;Sort By Categories</h4>
    					<div class="swit form">	
    						
    						 <?php
    							foreach($child_cats as $object){
    								echo "<div class='check_box'><label>".form_radio(array('name'=>'childCat', 'value'=>$object->child_cat_id, 'checked'=>set_checkbox('childCat', $object->child_cat_id, false),'style'=>'margin: 15px 15px 0px 0px;','class'=>'filterby'));
    								echo form_label($object->child_cat_name).'</label></div><hr style="margin:0;"/>';
    							}
    						?>
    					</div>
    				</div>
    				<div class="community-poll">
    					<h4><i class="fa fa-bars"></i>&nbsp;&nbsp;Sort By Size</h4>
    					<div class="swit form">	
    					    <?php
    							foreach($product_size as $object){
    								echo "<div class='check_box'><label>".form_checkbox(array('name'=>'psize[]', 'value'=>$object->size_id, 'checked'=>set_checkbox('psize[]', $object->size_id, false),'style'=>'margin: 15px 15px 0px 0px;','class'=>'filterby'));
    								echo form_label($object->size).'</label></div><hr style="margin:0;"/>';
    							}
    						?>
    					</div>
    				</div>
    				<div class="community-poll">
    					<h4><i class="fa fa-bars"></i>&nbsp;&nbsp;Sort By Color</h4>
    					<div class="swit form">	
    						<?php
    							foreach($product_color as $object){
    								echo "<div class='check_box'><label>".form_checkbox(array('name'=>'pcolor[]', 'value'=>$object->color_id, 'checked'=>set_checkbox('pcolor[]', $object->color_id, false),'style'=>'margin: 15px 15px 0px 0px;','class'=>'filterby'));
    								echo form_label($object->color_name).'</label></div><hr style="margin:0;"/>';
    							}
    						?>
    					</div>
    				</div>
					<input type='hidden' name='price_filter' id='price_filter' value=<?=$this->input->post('price_filter')?>>
					<input type="submit" id="submitfilter" class="hidden">
    			<?php  echo form_close(); ?>
    			</div>
    			<?php if(empty($product_list)){?>
    				<div class="col-md-9">
    					<div class="banner-bootom-w3-agileits">
    					   <h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span>Description</h2>
    					   <div class="tab1 resp-tab-content resp-tab-content-active" style="display:block" aria-labelledby="tab_item-0">
    							<div class="single_page_agile_its_w3ls text-center">
    								<h3>No Products Found.</h3>
    							</div>	
    							<br />
    						</div>
    					</div>
    				</div>
    				<hr />
    			<?php } else{?>
    			<div class="col-md-9">
    				<br /><!--  -->
    				<div class="col-md-12 products-right">
    					<div class="sort-grid">
    						<div class="sorting">
    							<h6>Filter</h6>

								<?=form_dropdown('price_filter_list', $pp_filterlist, ($this->input->post('price_filter') ? $this->input->post('price_filter') : '0'), array('for' => 'submitfilter', 'id' => 'price_filter_list', 'class' => 'frm-field required sect'))?>
								<div class="clearfix"></div>
    						</div>
    						
    						<div class="clearfix"></div>
    					</div>
    					<div class="clearfix"></div>
    				</div>
    				<div class="clearfix"></div>
    				
    				<div class="single-pro">
    					<?php $counter2=0; foreach($product_list as $prod){$counter2++;?>
    						<div class="col-md-4 product-men">
    							<div class="men-pro-item simpleCart_shelfItem">
    								<div class="men-thumb-item">									
    									<?php
    										$images =  explode(',', $prod->prod_image_urls);
    										$counter3 = 0;
    										foreach ($images as $img) {
    											if($counter3 == 1){
    												
    												echo '<img src="'.IMAGEBASEPATH.$prod->prod_image_url.'" alt="" class="';
    												echo 'pro-image-front" />';
    											}else{
    												echo '<img src="'.IMAGEBASEPATH.$img.'" alt="" class="';
    												echo 'pro-image-back" />';
    											}
    											$counter3++;
    											if($counter3 == 2){break;}
    										}
    									?>									
    									<div class="men-cart-pro">
    										<div class="inner-men-cart-pro">
    											<a href="<?=site_url();?>/website_home/product_details/<?=$pcat_id?>/<?=$ccat_id?>/<?=$prod->prod_id?>" class="link-product-add-cart">
    												Quick View
    											</a>
    										</div>
    									</div>
    									<!--
    									-->
    									<!--<span class="product-new-top">New</span>-->
    								</div>
    								<div class="item-info-product ">
    									<h4 style="height: 22px; overflow: hidden;"><a href="single"><?=$prod->prod_name?></a></h4>
    									<div class="info-product-price">
    										<del><i class="fa fa-inr"></i> <?=$prod->prod_mrp?></del>&nbsp;&nbsp;&nbsp;
    										<span class="item_price"><i class="fa fa-inr"></i> <?=$prod->prod_price?></span>
    									</div>
    									<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
    										<input type="button" onclick="window.location = '<?=site_url();?>/website_home/product_details/<?=$pcat_id?>/<?=$ccat_id?>/<?=$prod->prod_id?>/';" value="View" class="button" />
    									</div>
    									<hr style="margin-top: -32px;" />
    								</div>
    							</div>
    						</div>
    						<?php if($counter2 == 3){ $counter2=0;?>
    							<div class="clearfix"></div>
    						<?php }?>
    					<?php }?>
    					<div class="col-lg-12">
    						<!--Space For Pagination-->
    					</div>
    					<div class="clearfix"></div>
    				</div>
    			</div>
    			<?php }?>
    		</div>	
    	</div>	
    </div>	
    <!--<h1 id="scramt">0</h1>-->
    <style>
    	#scramt{
    		position: fixed;
    		top: 50px;
    		width: 100%;
    		text-align: center;
    		background-color: #0000004a;
    	}
    </style>
    <!-- //mens -->
    <!---728x90--->
    <!--/grids-->
</div>
<div class="mobileView">
    <br /><br />
	<?php if(empty($product_list)){?>
		<br /><br />
		<div class="col-md-12">
			<div class="banner-bootom-w3-agileits">
			   <div class="tab1 resp-tab-content resp-tab-content-active" style="display:block" aria-labelledby="tab_item-0">
					<div class="single_page_agile_its_w3ls text-center">
						<h3>No Products Found.</h3>
					</div>	
					<br />
				</div>
			</div>
		</div>
		<hr />
	<?php } else{?>
		<div class="clearfix"></div>
		<div class="row" style="margin-bottom: 45px;">
			<?php $cnt = 0; foreach($product_list as $prod){ $cnt++;?>
				<div class="col-sm-6 col-xs-6" style="padding:0; border-top:1px solid #efefef; border-right:1px solid #efefef;">					
					<a href="<?=site_url();?>/website_home/product_details/<?=$pcat_id?>/<?=$ccat_id?>/<?=$prod->prod_id?>" style="width:100%">
						<img src="<?=IMAGEBASEPATH.$prod->prod_image_url?>" style="width:100%;margin-bottom: 10px;" />
					</a>
					
					<h4 style="height: 22px; overflow: hidden; text-align:center;">
						<a href="<?=site_url();?>/website_home/product_details/<?=$pcat_id?>/<?=$ccat_id?>/<?=$prod->prod_id?>" style="color: #424141; font-size: 14px; font-weight: 800; font-family: sans-serif;">
							<?=$prod->prod_name?>
						</a>
					</h4>
					<center style="font-size:14px;margin-bottom: 7px;">
						<span>
							<small style="color:#555">
								
								<?php 
								$sizes = explode(',',$prod->fk_psize_ids);
								$ci = 0;
								foreach($sizes as $sz){
									foreach($product_size as $asz){
										if($asz->size_id == $sz){
											if($ci >0 && $ci<sizeof($sizes)){
												echo ', ';
											}
											$ci++;
											echo $asz->size;
										}
									}
								}
							?>
							</small>
						</span>
						<br />
						<del><i class="fa fa-inr"></i> <?=$prod->prod_mrp?></del>
						&nbsp;&nbsp;&nbsp;
						<span>
							<?=(int)((($prod->prod_mrp-$prod->prod_price)*100)/$prod->prod_mrp);?>% off
						</span>
						&nbsp;&nbsp;&nbsp;
						<span>
							<i class="fa fa-inr"></i> <?=$prod->prod_price?>
						</span>
					</center>
				</div>
				<?php if($cnt == 2){$cnt=0; echo"<div class='clearfix'></div> "; }?>
			<?php }?>
		</div>
		<div class="clearfix"></div>
	<?php }?>











	

	<div id="sorting" style="position:fixed; bottom: 38px; display:none; padding: 15px;background-color: white; box-shadow: 0px -5px 15px #b3b0b0; width: 100%;z-index:1;">
	    <span style="padding: 3px 8px; right: 10px; position: absolute; border: 1px solid #ccc; border-radius: 4px;" onclick="$('#sorting').hide();">X</span>
		<p id="0" for="submitfilter2" class="submitform2">
		<label style="margin-bottom:15px; width:100%;cursor: pointer; color: black;">No Filteration</label>
		</p>
		<p id="1" for="submitfilter2" class="submitform2">
		<label style="margin-bottom:15px; width:100%;cursor: pointer; color: black;">Price - High To Low</label>
		</p>
		<p id="-1" for="submitfilter2" class="submitform2">
		<label style="width:100%;cursor: pointer; color: black;">Price - Low To High</label>
		</p>
	</div>
	<div id="filter" style="position:fixed; bottom: 38px; height:87%; overflow-y: scroll; display:none; padding: 15px;background-color: white; box-shadow: 0px -5px 15px #b3b0b0; width: 100%;">
		<span style="position: absolute;
    padding: 3px 8px;
    top: 10px;
    left: 48%;
    border: 1px solid #ccc;
    border-radius: 4px;" onclick="$('#filter').hide();">X</span>
		<br />
		<?=form_open(current_url(), array('id'=>'filterform2'));?>
		<div style="margin-bottom:15px; width:100%;curser:pointer;">
			<label style="width:100%;" onclick="$('#ptype').toggle();">Type <i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></label>
			<div style="display:none" id="ptype">
				<div class="swit form">	
					<?php
						foreach($ptype_list as $object){
							echo "<div class='check_box'><label>".form_radio(array('name'=>'ptype_id', 'value'=>$object->ptype_id, 'checked'=>set_checkbox('ptype_id', $object->ptype_id, false),'style'=>'margin: 15px 15px 0px 0px;','class'=>'filterby2'));
							echo form_label($object->ptype).'</label></div><hr style="margin:0;"/>';
						}
					?>
				</div>
			</div>
		</div><hr />
		<div style="margin-bottom:15px; width:100%;curser:pointer;">
			<label style="width:100%;" onclick="$('#sizes').toggle();">Size <i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></label>
			<div style="display:none" id="sizes">
				<div class="swit form">	
					<?php
						foreach($product_size as $object){
							echo "<div class='check_box'><label>".form_checkbox(array('name'=>'psize[]', 'value'=>$object->size_id, 'checked'=>set_checkbox('psize[]', $object->size_id, false),'style'=>'margin: 15px 15px 0px 0px;','class'=>'filterby2'));
							echo form_label($object->size).'</label></div><hr style="margin:0;"/>';
						}
					?>
				</div>
			</div>
		</div><hr />
		<div style="margin-bottom:15px; width:100%;curser:pointer;">
			<label style="width:100%;" onclick="$('#colors').toggle();">Color <i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></label>
			<div style="display:none" id="colors">
				<div class="swit form">	
					<?php
						foreach($product_color as $object){
							echo "<div class='check_box'><label>".form_checkbox(array('name'=>'pcolor[]', 'value'=>$object->color_id, 'checked'=>set_checkbox('pcolor[]', $object->color_id, false),'style'=>'margin: 15px 15px 0px 0px;','class'=>'filterby2'));
							echo form_label($object->color_name).'</label></div><hr style="margin:0;"/>';
						}
					?>
				</div>
			</div>
		</div><hr />
		<div style="width:100%;curser:pointer;">
			<label style="width:100%;" onclick="$('#types').slideToggle();">Category <i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></label>
			<div style="display:none" id="types">
				<div class="swit form">
					 <?php
						foreach($child_cats as $object){
							echo "<div class='check_box'><label>".form_radio(array('name'=>'childCat', 'value'=>$object->child_cat_id, 'checked'=>set_checkbox('childCat', $object->child_cat_id, false),'style'=>'margin: 15px 15px 0px 0px;','class'=>'filterby2'));
							echo form_label($object->child_cat_name).'</label></div><hr style="margin:0;"/>';
						}
					?>
				</div>
			</div>
		</div><hr />
		<input type='hidden' name='price_filter' id='price_filter2' value=<?=$this->input->post('price_filter')?>>
		<input type="submit" id="submitfilter2" class="hidden">
		<?=form_close(); ?>
	</div>
	<table style="width: 100%; position: fixed; bottom: 0; text-align: center; border: 1px solid gray;background-color: rgba(255, 255, 255, 0.8);">
		<tr>
			<td style="border-right:1px solid gray; padding: 7px 0;" onclick="$('#sorting').toggle();">
				<i class="fa fa-sort" aria-hidden="true"></i>&nbsp;&nbsp;Sort
			</td>
			<td style="padding: 7px 0;" onclick="$('#filter').toggle();">
				<i class="fa fa-filter" aria-hidden="true"></i>&nbsp;&nbsp;Filter
			</td>
		</tr>
	</table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	var x = 0;
	$(document).ready(function(){
		$(document).scroll(function(){
			x++;
			$("#scramt").text( x);
			// if(x==101){alert(x); x=0;}
		});
	});
	
	$(".filterby").click(function() {
		sessionStorage.setItem('isshow', $(this).val());
		$("#filterform").submit();
	});
	
	$("#price_filter_list").change(function() {
		sessionStorage.setItem('isshow', $(this).val());
		$("#price_filter").val($('#price_filter_list').val())
		$("#filterform").submit();
	});
	
	$(".filterby2").click(function() {
		sessionStorage.setItem('isshow', $(this).val());
		$("#filterform2").submit();
	});

	$(".submitform2").click(function() {
		sessionStorage.setItem('isshow', $(this).prop('id'));
		$("#price_filter2").val($(this).prop('id'));
		$("#filterform2").submit();
	});

</script>