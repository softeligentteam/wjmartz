<!-- /banner_bottom_agile_info -->
<div class="desktopView">
	<div class="page-head_agile_info_w3l">
		<div class="container">
			<h3 style="font-size:1.3em">Search Result for : <?php echo isset($keyword) ? $keyword : "" ?></h3>
			<!--/w3_short-->
			<div class="services-breadcrumb">
				<div class="agile_inner_breadcrumb">
					<ul class="w3_short">
						<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
						<li><?php echo isset($keyword) ? $keyword : "" ?></li>
					</ul>
				</div>
			</div>
		   <!--//w3_short-->
		</div>
	</div>
	<?php if(empty($product_list)){?>
	<div class="banner-bootom-w3-agileits">
		<div class="container">
			<div class="responsive_tabs_agileits"> 
				<div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
					<div class="resp-tabs-container">
					<!--/tab_one-->
					   <h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span>Description</h2>
					   <div class="tab1 resp-tab-content resp-tab-content-active" style="display:block" aria-labelledby="tab_item-0">
							<div class="single_page_agile_its_w3ls text-center">
								<h3>No Products Found.</h3>
							</div>
						</div>
					</div>
				</div>	
			</div>
			<br />
		</div>
	</div>
	<hr />
	<?php } else{?>
	<!---728x90--->
	<!-- banner-bootom-w3-agileits -->
	<div class="banner-bootom-w3-agileits">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="single-pro">
						<?php $counter2=0; foreach($product_list as $prod){$counter2++;?>
							<div class="col-md-3 product-men">
								<div class="men-pro-item simpleCart_shelfItem">
									<div class="men-thumb-item">									
										<?php
											$images =  explode(',', $prod->prod_image_urls);
											$counter3 = 0;
											foreach ($images as $img) {
												if($counter3 == 1){
													
													echo '<img src="'.IMAGEBASEPATH.$prod->prod_image_url.'" alt="" class="';
													echo 'pro-image-front" />';
												}else{
													echo '<img src="'.IMAGEBASEPATH.$img.'" alt="" class="';
													echo 'pro-image-back" />';
												}
												$counter3++;
												if($counter3 == 2){break;}
											}
										?>									
										
										<!--
										-->
										<!--<span class="product-new-top">New</span>-->
									</div>
									<div class="item-info-product ">
										<h4 style="height: 22px; overflow: hidden;"><a href="single"><?=$prod->prod_name?></a></h4>
										<div class="info-product-price">
											<del><i class="fa fa-inr"></i> <?=$prod->prod_mrp?></del>&nbsp;&nbsp;&nbsp;
											<span class="item_price"><i class="fa fa-inr"></i> <?=$prod->prod_price?></span>
										</div>
										<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
											<input type="button" onclick="window.location = '<?=site_url();?>/website_home/product_details/<?=$prod->fk_pcat_id?>/<?=$prod->fk_pchild_id?>/<?=$prod->prod_id?>/';" value="View" class="button" />
										</div>
										<hr style="margin-top: -32px;" />
									</div>
								</div>
							</div>
							<?php if($counter2 == 4){ $counter2=0;?>
								<div class="clearfix"></div>
							<?php }?>
						<?php }?>
						<div class="col-lg-12">
							<!--Space For Pagination-->
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>	
		</div>	
	</div>	
	<!--<h1 id="scramt">0</h1>-->
</div>































	
<style>
	#scramt{
		position: fixed;
		top: 50px;
		width: 100%;
		text-align: center;
		background-color: #0000004a;
	}
</style>
<!-- //mens -->
<!---728x90--->
<!--/grids-->
<?php }?>
</div>




<div class="mobileView">
    <br /><br />
	<?php if(empty($product_list)){?>
		<br /><br />
		<div class="col-md-12">
			<div class="banner-bootom-w3-agileits">
			   <div class="tab1 resp-tab-content resp-tab-content-active" style="display:block" aria-labelledby="tab_item-0">
					<div class="single_page_agile_its_w3ls text-center">
						<h3>No Products Found.</h3>
					</div>	
					<br />
				</div>
			</div>
		</div>
		<hr />
	<?php } else{?>
		<div class="clearfix"></div>
		<div class="row" style="margin-bottom: 45px;">
			<?php $cnt = 0; foreach($product_list as $prod){ $cnt++;?>
				<div class="col-sm-6 col-xs-6" style="padding:0; border-top:1px solid #efefef; border-right:1px solid #efefef;">					
					<a href="<?=site_url();?>/website_home/product_details/<?=$prod->fk_pcat_id?>/<?=$prod->fk_pchild_id?>/<?=$prod->prod_id?>" style="width:100%">
						<img src="<?=IMAGEBASEPATH.$prod->prod_image_url?>" style="width:100%;margin-bottom: 10px;" />
					</a>
					
					<h4 style="height: 22px; overflow: hidden; text-align:center;">
						<a href="<?=site_url();?>/website_home/product_details/<?=$prod->fk_pcat_id?>/<?=$prod->fk_pchild_id?>/<?=$prod->prod_id?>" style="color: #424141; font-size: 14px; font-weight: 800; font-family: sans-serif;">
							<?=$prod->prod_name?>
						</a>
					</h4>
					<center style="font-size:14px;margin-bottom: 7px;">
						<span>
							<small style="color:#555">
								
								<?php 
								$sizes = explode(',',$prod->fk_psize_ids);
								$ci = 0;
								foreach($sizes as $sz){
									foreach($product_size as $asz){
										if($asz->size_id == $sz){
											if($ci >0 && $ci<sizeof($sizes)){
												echo ', ';
											}
											$ci++;
											echo $asz->size;
										}
									}
								}
							?>
							</small>
						</span>
						<br />
						<del><i class="fa fa-inr"></i> <?=$prod->prod_mrp?></del>
						&nbsp;&nbsp;&nbsp;
						<span>
							<?=(int)((($prod->prod_mrp-$prod->prod_price)*100)/$prod->prod_mrp);?>% off
						</span>
						&nbsp;&nbsp;&nbsp;
						<span>
							<i class="fa fa-inr"></i> <?=$prod->prod_price?>
						</span>
					</center>
				</div>
				<?php if($cnt == 2){$cnt=0; echo"<div class='clearfix'></div> "; }?>
			<?php }?>
		</div>
		<div class="clearfix"></div>
	<?php }?>











<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	var x = 0;
	$(document).ready(function(){
		$(document).scroll(function(){
			x++;
			$("#scramt").text( x);
			// if(x==101){alert(x); x=0;}
		});
	});
</script>