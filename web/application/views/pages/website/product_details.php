<div class="desktopView">
    <br /><!-- banner-bootom-w3-agileits -->
    <div class="banner-bootom-w3-agileits">
    	<div class="container">
    	<!---728x90--->
    		<div class="col-md-5 single-right-left ">
    			<div class="grid images_3_of_2">
    				<div class="flexslider">					
    					<ul class="slides">
    						<li data-thumb="<?=IMAGEBASEPATH.$product_details->prod_image_url ?>">
    							<div class="thumb-image"> <img src="<?=IMAGEBASEPATH.$product_details->prod_image_url ?>" data-imagezoom="true" class="img-responsive"> </div>
    						</li>
    						<?php $images =  explode(',', $product_details->prod_image_urls); foreach ($images as $img) {?>
    							<li data-thumb="<?=IMAGEBASEPATH.$img ?>">
    								<div class="thumb-image"> <img src="<?=IMAGEBASEPATH.$img ?>" data-imagezoom="true" class="img-responsive"> </div>
    							</li>
    						<?php } ?>						
    					</ul>
    					<div class="clearfix"></div>
    				</div>	
    			</div>
    		</div>
    		<div class="col-md-7 single-right-left simpleCart_shelfItem">
    			<h3><?=$product_details->prod_name?></h3>
    			<p>
    				<del><i class="fa fa-inr"></i><?=$product_details->prod_mrp?></del> &nbsp;&nbsp;&nbsp;&nbsp;
    				<span class="item_price"> <?=$product_details->discount?>% off</span> &nbsp;&nbsp;&nbsp;&nbsp;
    				<span class="item_price"><i class="fa fa-inr"></i> <?=$product_details->prod_price?></span>
    			</p>
    			<hr />
    			<div class="description">
    				<h5>Check delivery, payment options and charges at your location</h5>
    				 <form action="#" method="post">
    				<input type="text" maxlength="6" value="Enter pincode" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter pincode';}" required="" name="pincode" id="pincode">
    				<input type="button" value="Check" onclick="check_pincode();">
    				<div id="message1"></div>
    				<p hidden id="message2"><span class="text-danger">Sorry we can't deliver this location currently</span></p>
    				</form>
    			</div>
    			<hr />
    			<div class="color-quality">
    				<div class="row">
    					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
    						<b>Color : </b><?=$product_details->color_name?>
    					</div>
    					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
    						<b>Type : </b><?=$product_details->fabric_type?>
    					</div>
    					<div class="clearfix"> </div>
    					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="margin-top:12px;">
    						<b>Brand : </b><?=$product_details->brand?>
    					</div>
    					<div class="col-lg-5 col-md-4 col-sm-6 col-xs-12" style="margin-top:12px;">
    						<b>Availability : </b>Available In Stock
    					</div>
    					<div class="clearfix"> </div>
    					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:12px;">
    						<b>Return & Exchange : </b><?php if($product_details->refund_status == 1){echo 'Eligible for return and exchange';}else{echo 'Not eligible for return and exchange';}?>
    					</div>
    					<div class="clearfix"> </div>
    				</div>
    			</div>
    			<hr />
    			<div class="row">
    				<div class="col-md-6 col-xs-12">
    					<div class="occasional">
    						<h5>
    							Select Size : 
    							<button class="btn" style="background:transparent; font-size: 12px; margin-left:45px;" type="button" data-toggle="modal" data-target="#sizechart">
    								(View Size Chart)
    							</button>
    						</h5>
    						<?php foreach($product_sizes as $size){ ?>
    							<button class="sizeselectbtn select" id="<?=$size['size_id']?>" onclick="selectedSize(this.id, '<?=$size['size_id'] ?>','<?=$size['size'] ?>');"><?=$size['size'] ?></button>
    						<?php }?>
    						<div class="clearfix"> </div>
    						<hr />
    					</div>
    				</div>
    				<div class="col-md-6 col-xs-12">				
    					<div class="occasional">
    						<h5>Select Quantity :</h5>
    						<i class="fa fa-minus" aria-hidden="true" id="qty_m" onclick="uptog(this);" style="float:left;margin:10px;"></i>
    						<input type="text" value="1" readonly class="form-control" id="qty_d" style="width:42px; float:left;" />
    						<i class="fa fa-plus" aria-hidden="true" id="qty_p" onclick="uptog(this);" style="float:left;margin:10px;"></i>
    					</div>
    				</div>
    			</div>
    			<div class="occasion-cart">
    				<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
    					<?=form_open('Cartcontroller/add_product_to_cart','onsubmit="return validatePrdDet();"')?>
    						<fieldset>
    							<input type="hidden" name="pcat_id" value="<?=$pcat_id?>">
    							<input type="hidden" name="ccat_id" value="<?=$ccat_id?>">
    							<input type="hidden" name="prod_id" value="<?=$product_details->prod_id?>">
    							<input type="hidden" name="prod_name" value="<?=$product_details->prod_name?>">
    							<input type="hidden" name="prod_price" value="<?=$product_details->prod_price?>">
    							<input type="hidden" id="product_size_id" name="product_size_id" value="0">
    							<input type="hidden" name="color_name" value="<?=$product_details->color_name?>">
    							<input type="hidden" name="brand" value="<?=$product_details->brand?>">
    							<input type="hidden" name="fabric_type" value="<?=$product_details->fabric_type?>">
    							<input type="hidden" name="discount" value="<?=$product_details->discount?>">
    							<input type="hidden" name="prod_image_url" value="<?=$product_details->prod_image_url?>">
    							<input type="hidden" name="prod_quantity" id="prod_qty" value="1">
    							<input type="hidden" id="product_size_name" name="product_size_name" value="">
    							<input type="submit" name="submit" value="Add to cart" class="button">
    						</fieldset>
    					<?=form_close()?>
    				</div>											
    			</div>
    			<hr />
    		</div>
    		<div class="clearfix"> </div>
    		<!---728x90--->
    		<!-- /new_arrivals --> 
    		<div class="responsive_tabs_agileits"> 
    			<div id="horizontalTab">
    					<ul class="resp-tabs-list">
    						<li>Description</li>
    						<li>Fit Details</li>
    						<li>Delivery & Return</li>
    					</ul>
    				<div class="resp-tabs-container">
    				<!--/tab_one-->
    				   <div class="tab1">
    						<div class="single_page_agile_its_w3ls">
    							<?=$product_details->prod_description ?>
    						</div>
    					</div>
    					<div class="tab2">
    						<div class="single_page_agile_its_w3ls">
    							<?=$product_details->prod_fit_details ?>
    						</div>
    					</div>
    					<div class="tab3">							
    						<div class="single_page_agile_its_w3ls">
    							<?=$product_details->delivery_n_return ?>
    						</div>
    					</div>
    				</div>
    			</div>	
    		</div>
    		<!-- //new_arrivals --> 
    		<!--/slider_owl-->
    		<!-- New-arrivals -->
        	<div class="banner-bootom-w3-agileits">
        		<div class="w3_agile_latest_arrivals">
        			<div class="container">
        			    <?php 
                                $recommended_products_details_size = sizeof($recommended_products_details);
                                if($recommended_products_details_size != 0){
                            ?>
        				<h3 class="wthree_text_info"><hr /><span>Recommended Products</span></h3>
        				<div class="row">
        					<div class="col-md-12">
        						<div class="carousel slide multi-item-carousel" id="theCarousel">
        							<div class="carousel-inner">
        								<div class="item active">
        								<?php $ele_count=0; $cnt = 0; foreach($recommended_products_details as $prod){ $cnt++; $ele_count++;?>
        									<div class="col-md-3 product-men single">
        										<div class="men-pro-item simpleCart_shelfItem">
        											<div class="men-thumb-item">
        												<?php
        													$images =  explode(',', $prod->prod_image_urls);
        													$counter3 = 0;
        													foreach ($images as $img) {
        														if($counter3 == 1){
        															
        															echo '<img src="'.IMAGEBASEPATH.$prod->img_url.'" alt="" class="';
        															echo 'pro-image-front" />';
        														}else{
        															echo '<img src="'.IMAGEBASEPATH.$img.'" alt="" class="';
        															echo 'pro-image-back" />';
        														}
        														$counter3++;
        														if($counter3 == 2){break;}
        													}
        												?>	
        											</div>
        											<div class="item-info-product ">
        												<h4><a href="#single.html"><?=$prod->pname?></a></h4>
        												<div class="info-product-price">
        													<span class="item_price"><i class="fa fa-inr"></i> <?=$prod->pprice?></span>
        													<del><i class="fa fa-inr"></i> <?=$prod->prod_mrp?></del>
        												</div>
        													<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
        														<input type="button" onclick="window.location = '<?=site_url();?>/website_home/product_details/<?=$prod->fk_pcat_id?>/<?=$prod->fk_pchild_id?>/<?=$prod->pid?>/';" value="View" class="button" />
        													</div>					
        											</div>
        										</div>
        									</div>
        								<?php if($cnt == 4){$cnt=0; ?>
                                                            <div class="clearfix"> </div>                                                
                                                        <?php if($ele_count != $recommended_products_details_size){?>
                                                            </div>
                                                            <div class="item">
                                                        <?php }?>
                                                    <?php }?>
                                                <?php }}?>
        								</div>
        								<!--  Example item end -->
        							</div>
        							<a style="width: 35px; margin-left: -25px;" class="left carousel-control" href="#theCarousel" data-slide="prev">
										<img src="<?=base_url();?>website_assets/images/white-icons/left-arrow-gray.png" style="top: 46%; position: absolute; right:5px" />
									</a>
        							<a style="width: 35px; margin-right: -25px;" class="right carousel-control" href="#theCarousel" data-slide="next">
										<img src="<?=base_url();?>website_assets/images/white-icons/right-arrow-gray.png" style="top: 46%; position: absolute; left:5px" />
									</a>
        						</div>
        					</div>
        				</div>
        				<!--//slider_owl-->
        			</div>
        		</div>
        	</div>
        	<!-- //New-arrivals -->
    	</div>
     </div>
    <!--//single_page-->
    <!---728x90--->
    <!--/grids-->
    <!--/KT[date:-13/04/18]-->
</div>
<div class="mobileView">
	<style>
		.mobileView .flex-viewport{
			border:none;
		}
		.nopad{padding:0 !important;}
	</style>
	<br /><!-- banner-bootom-w3-agileits -->
	<div class="banner-bootom-w3-agileits">
		<!---728x90--->
		<div class="col-md-5 single-right-left" style="border-bottom: 2px dashed black;margin-bottom: 20px;">
			<div class="grid images_3_of_2">
				<div class="flexslider">					
					<ul class="slides" onclick="location.href='<?=site_url();?>/website_home/product_image/<?=$this->uri->segment(3,0);?>/<?=$this->uri->segment(4,0);?>/<?=$product_details->prod_id?>';">
						<li data-thumb="<?=IMAGEBASEPATH.$product_details->prod_image_url ?>">
							<div class="thumb-image"> <img src="<?=IMAGEBASEPATH.$product_details->prod_image_url ?>" data-imagezoom="true" class="img-responsive"> </div>
						</li>
						<?php $images =  explode(',', $product_details->prod_image_urls); foreach ($images as $img) {?>
							<li data-thumb="<?=IMAGEBASEPATH.$img ?>">
								<div class="thumb-image">
									<img src="<?=IMAGEBASEPATH.$img ?>" data-imagezoom="true" class="img-responsive">
								</div>
							</li>
						<?php } ?>
					</ul>
					<div class="clearfix"></div>
				</div>	
			</div>
			<div id="zview" class="grid images_3_of_2" style="position: fixed; z-index: 9999; top: 0; padding-top: 50%; background-color: white; height:100%; width:100%;display:none;">
				<div class="flexslider">					
					<ul class="slides">
						<li data-thumb="<?=IMAGEBASEPATH.$product_details->prod_image_url ?>">
							<div class="thumb-image">
								<img src="<?=IMAGEBASEPATH.$product_details->prod_image_url ?>" data-imagezoom="true" class="img-responsive" onclick="">
							</div>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>	
			</div>
		</div>
		<div class="col-md-7 single-right-left simpleCart_shelfItem">
			<h3 style="text-align:center; margin-bottom:10px;"><?=$product_details->prod_name?></h3>
			<p style="text-align: center;border-bottom: 2px dashed black; padding-bottom:20px;font-size:14px;">
				<del><i class="fa fa-inr"></i><?=$product_details->prod_mrp?></del> &nbsp;&nbsp;&nbsp;&nbsp;
				<span class="item_price"> <?=$product_details->discount?>% off</span> &nbsp;&nbsp;&nbsp;&nbsp;
				<span class="item_price"><i class="fa fa-inr"></i> <?=$product_details->prod_price?></span>
			</p>
			<div class="row">
				<div class="col-md-6 col-xs-12" style="border-bottom: 2px dashed black; padding-bottom:7px;">
					<h3 style="text-align:center;">
						Select Size
						<button class="btn" style="background:transparent; font-size: 12px;position: absolute; right: 0; top: -4px;" type="button" data-toggle="modal" data-target="#sizechart" style="width:60px; font-size:14px;">
							(View Size Chart)
						</button>
					</h3><br />
					<center>
					<?php $size_size = sizeof($product_sizes)*75;?>
					<?php foreach($product_sizes as $size){ ?>
						<button class="sizeselectbtn select" id="<?=$size['size_id']?>" onclick="selectedSize(this.id, '<?=$size['size_id'] ?>','<?=$size['size'] ?>');"><?=$size['size'] ?></button>
					<?php }?>
					
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-6 col-xs-12" style="border-bottom: 2px dashed black; padding-bottom:20px;">				
					<div class="occasional">
						<h3 style="text-align:center;">Select Quantity</h3>
						<br />
						<div class="col-xs-offset-3 col-sm-offset-3 col-xs-2 col-sm-2 nopad">
							<i class="fa fa-minus pull-right" aria-hidden="true" id="qty_mm" onclick="uptog(this);" style="margin: 7px 25px 0px 0; background-color: #000; padding: 5px 7px; border-radius: 100%; color: white;"></i>
						</div>
						<div class="col-xs-2 col-sm-2 nopad">
							<input type="text" value="1" readonly class="form-control" id="qty_dm" style="width: 100%; padding-left: 40%; float: left; background-color: transparent; border: none; box-shadow: none; font-size: 20px;" />
						</div>
						<div class="col-xs-2 col-sm-2 nopad">
							<i class="fa fa-plus" aria-hidden="true" id="qty_pm" onclick="uptog(this);" style="margin: 7px 0px 0px 25px;background-color: #000; padding: 5px 7px; border-radius: 100%; color: white;"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="row" style="margin-bottom:20px;">
				<h3 style="text-align:center;margin-top:20px;margin-bottom: 15px;">Product Details</h3>
				<div class="col-xs-6 col-sm-6"  style="margin-bottom:12px; font-size:15px;">
					<b>Color</b>
				</div>
				<div class="col-xs-6 col-sm-6"  style="margin-bottom:12px; font-size:15px;">
					<?=$product_details->color_name?>
				</div>
				<div class="col-xs-6 col-sm-6"  style="margin-bottom:12px; font-size:15px;">
					<b>Type</b>
				</div>
				<div class="col-xs-6 col-sm-6"  style="margin-bottom:12px; font-size:15px;">
					<?=$product_details->fabric_type?>
				</div>
				<div class="col-xs-6 col-sm-6"  style="margin-bottom:12px; font-size:15px;">
					<b>Brand</b>
				</div>
				<div class="col-xs-6 col-sm-6"  style="margin-bottom:12px; font-size:15px;">
					<?=$product_details->brand?>
				</div>
				<div class="col-xs-6 col-sm-6" style="margin-bottom:12px; font-size:15px;">
					<b>Availability</b>
				</div>
				<div class="col-xs-6 col-sm-6" style="margin-bottom:12px; font-size:15px;">
					Available In Stock
				</div>
				<div class="col-xs-6 col-sm-6" style="margin-bottom:12px; font-size:15px;">
					<b>Return & Exchange</b>
				</div>
				<div class="col-xs-6 col-sm-6" style="margin-bottom:12px; font-size:15px;">
					<?php if($product_details->refund_status == 1){echo 'Not Applicable';}else{echo 'Applicable';}?>
				</div>
				<div class="clearfix"> </div>
			</div>
			<?=form_open('Cartcontroller/add_product_to_cart','onsubmit="return validatePrdDet();"')?>
				<input type="hidden" name="pcat_id" value="<?=$pcat_id?>">
				<input type="hidden" name="ccat_id" value="<?=$ccat_id?>">
				<input type="hidden" name="prod_id" value="<?=$product_details->prod_id?>">
				<input type="hidden" name="prod_name" value="<?=$product_details->prod_name?>">
				<input type="hidden" name="prod_price" value="<?=$product_details->prod_price?>">
				<input type="hidden" id="product_size_idm" name="product_size_id" value="0">
				<input type="hidden" name="color_name" value="<?=$product_details->color_name?>">
				<input type="hidden" name="brand" value="<?=$product_details->brand?>">
				<input type="hidden" name="fabric_type" value="<?=$product_details->fabric_type?>">
				<input type="hidden" name="discount" value="<?=$product_details->discount?>">
				<input type="hidden" name="prod_image_url" value="<?=$product_details->prod_image_url?>">
				<input type="hidden" name="prod_quantity" id="prod_qtym" value="1">
				<input type="hidden" id="product_size_namem" name="product_size_name" value="">
				<input type="submit" name="submit" value="Add to cart" class="button" style="width: 100%; border: none; padding: 7px; background-color: black; color: white;">
			<?=form_close()?>
		</div>
		<div class="clearfix"> </div>
		<!---728x90--->
		<!-- /new_arrivals --> 
		<div class="color-quality">
			<div class="row">
				<div class="col-xs-12 col-sm-12" onclick="$('#prod_descriptionm').slideToggle();" style="margin:10px 0; font-size:15px;">
					<b>Description<i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></b>
				</div>
				<div class="col-xs-12 col-sm-12" id="prod_descriptionm" style="display:none; margin-top:7px; font-size:15px;">
					<?=$product_details->prod_description ?>
				</div>
				<div class="clearfix"></div>
				<hr style="margin:0;" />
				<div class="col-xs-12 col-sm-12" onclick="$('#prod_fit_details').slideToggle();" style="margin:10px 0; font-size:15px;">
					<b>Fit Details<i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></b>
				</div>
				<div class="col-xs-12 col-sm-12" id="prod_fit_details" style="display:none; margin-top:7px; font-size:15px;">
					<?=$product_details->prod_fit_details ?>
				</div>
				<div class="clearfix"></div>
				<hr style="margin:0;" />
				<div class="col-xs-12 col-sm-12" onclick="$('#delivery_n_return').slideToggle();"  style="margin:10px 0; font-size:15px;">
					<b>Delivery & Return<i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></b>
				</div>
				<div class="col-xs-12 col-sm-12" id="delivery_n_return" style="display:none; margin-top:7px; font-size:15px;">
					<?=$product_details->delivery_n_return ?>
				</div>
				<div class="clearfix"></div>
				<hr style="margin:0;" />
			</div>
		</div>
		<!-- //new_arrivals --> 
		<!--/slider_owl-->
		<!-- New-arrivals -->
		<?php $recommended_products_details_sec_width = sizeof($recommended_products_details)*210;?>
		<?php if($recommended_products_details_sec_width != 0){?>
		<div class="color-quality">
			<div class="row">
				<h4 style="font-weight: 600; margin: 15px 14px;">View Similar</h4>
				<div style="width:100%; overflow:scroll;margin-bottom:20px;">
				<div style="width:<?=$recommended_products_details_sec_width?>px; overflow:scroll;margin-bottom:20px;">
					<?php $cnt = 0; foreach($recommended_products_details as $prod){ $cnt++;?>
						<div style="width:200px; margin-right:10px; float:left; background-color:#efefef;" >
							<a href="<?=site_url();?>/website_home/product_details/<?=$prod->fk_pcat_id?>/<?=$prod->fk_pchild_id?>/<?=$prod->pid?>">
								<img src="<?=IMAGEBASEPATH.$prod->img_url?>" style="width:100%" />
							</a>
							<center style="font-size:14px;">
								<p style="max-width: 100%; max-height: 20px; overflow: hidden; margin: 1px 0; font-weight:700">
									<?=$prod->pname?>
								</p>
								<span>
								<small style="color:#555">
									
									<?php 
									$sizes = explode(',',$prod->fk_psize_ids);
									$ci = 0;
									foreach($sizes as $sz){
										foreach($product_sizes as $asz){
											if($asz['size_id'] == $sz){
												if($ci >0 && $ci<sizeof($sizes)){
													echo ', ';
												}
												$ci++;
												echo $asz['size'];
											}
										}
									}
								?>
								</small><br />
							</span>
								<del><i class="fa fa-inr"></i> <?=$prod->prod_mrp?></del>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="item_price">
									<?=(int)((($prod->prod_mrp-$prod->pprice)*100)/$prod->prod_mrp);?>% off
								</span>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<span class="item_price"><i class="fa fa-inr"></i> <?=$prod->pprice?></span>
							</center>
						</div>
					<?php }?>
				</div>
			</div>
			</div>
		</div>
		<?php }?>
		<!-- //New-arrivals -->
	 </div>
</div>

<!-- Modal content for sizes-->
<div id="sizechart" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title text-center">Product Size Chart</h4>
			</div>
			<div class="modal-body">
				<img src="<?=IMAGEBASEPATH.$product_details->size_chart_image_url?>" style="width:100%;" />
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- /Modal content for sizes--> 

<script>
    function check_pincode()
    {
    	// var siteUrl="http://localhost/wj/web/index.php/";
    	var siteUrl="<?php print(site_url());?>";
    	mobileStatus = false;
    	if($('#pincode').val() == '' || $('#pincode').val() == null ) {
    		$('#p1').text("");
    	}else
    	{
    		//debugger;
    		var pin = $('#pincode').val(); 
    		$.ajax({
    		type: "POST",
    		url: siteUrl+'/website_home/check_pincode_charge',
    		data:{'pin': pin },
    		success: function(data){
    			//debugger;
    			// alert(data);
    			var obj = $.parseJSON(data); 
    			 //alert(obj.delivery_charges);
    			 if(obj!=null){
    				 //alert("kanchan");
    				//$('#message1').show();
    				$('#message1').html("<span style='color:green'>Delivery Is available with delivery charges "+obj.delivery_charges+" Within "+obj.delivery_day+" day </span>");
    				mobileStatus = false;
    				return false;
    			}
    			else{
    				$('#message1').html("<span style='color:red'>Sorry we can't deliver this location currently </span>");
    				mobileStatus = true;
    				return true;
    			} 
    		}
    	});
    	}
    }
</script>
<script>
	$('#pincode').keypress(function(e) {
		var a = [];
		var k = e.which;
		for (i = 48; i < 58; i++){
			a.push(i);
		}
		if (!(a.indexOf(k)>=0)){
			e.preventDefault();
		}
	});
</script>
<style>
	.sizeselectbtn{
		background-color: #ffffff;
		border: 3px solid #ccc;
		width: 90px;
		margin-right: 7px;
		margin-bottom: 5px;
	}
	.sizeselectbtn:hover{
		background-color: #41c4e2;
		border: 3px solid #41c4e2;
		color: white;
		font-weight: 600;
	}
	
	.selectedsizeselectbtn{
		background-color: #41c4e2 !important;
		color: white !important;
		font-weight: 600 !important;
		border-color: #41c4e2;
	}
</style>
<script>
	function validatePrdDet(){
		if($('#product_size_id').val() == '0'){
			alert('please Select Size');
			return false;
		}
	}
	function selectedSize(id, siz, v){
		$('.select').removeClass('selectedsizeselectbtn');
		$('#'+id).addClass('selectedsizeselectbtn');
		$('#product_size_id').val(siz);
		$('#product_size_idm').val(siz);
		$('#product_size_name').val(v);
		$('#product_size_namem').val(v);
	}
	
	function uptog(ele){
		var qty =parseInt($('#qty_d').val());
		if(ele.id == 'qty_m' || ele.id == 'qty_mm'){
			if(qty > 1){
				qty = qty-1;
			}
		}
		if(ele.id == 'qty_p' || ele.id == 'qty_pm'){
			if(qty < 5){
				qty = qty+1;
			}
		}
		$('#qty_d').val(qty);
		$('#qty_dm').val(qty);
		$('#prod_qty').val(qty);
		$('#prod_qtym').val(qty);
	}
</script>