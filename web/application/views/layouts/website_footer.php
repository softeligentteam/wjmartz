		<?php if(NULL != $this->session->flashdata('flash_msg')){?>
		<!-- Common Modal -->
		<div class="commonModal">
			<div class="row">
				<div class="col-sm-offset-2 col-md-offset-4 col-lg-offset-4 col-xs-12 col-sm-8 col-md-4 col-lg-4 text-center sec">
					<hr />
					<h3><?=$this->session->flashdata('flash_title');?></h3>
					<br />
					<p>
						<?=$this->session->flashdata('flash_msg');?>
					</p>
					<br />
					<button onclick="$('.commonModal').hide();" class="btn btn-md btn-info">Ok</button>
					<hr />
				</div>
			</div>
		</div>
		<!-- //Common Modal -->
		<?php }?>
		
		<div class="commonModal" id="logoutModal" style="display:none;">
			<div class="row">
				<div class="col-sm-offset-2 col-md-offset-4 col-lg-offset-4 col-xs-12 col-sm-8 col-md-4 col-lg-4 text-center sec">
					<hr />
					<h3 style="color:brown">Are You Sure?</h3>
					<br />
					<p>
						You will be logged out of this session, Are you sure you want to logout ?
					</p>
					<br />
					<a href="<?=site_url();?>/LoginController/logout" class="btn btn-md btn-default">Logout</a>
					<button onclick="$('.commonModal').hide();" class="btn btn-md btn-default">Cancel</button>
					<hr />
				</div>
			</div>
		</div>
		<!-- footer -->
		<div class="footer desktopView">
			<div class="footer_agile_inner_info_w3l">
				<div class="col-md-3 footer-left">
					<h2>
						<a href="<?=site_url();?>/website_home">
							<img src="<?=base_url();?>/website_assets/images/icon.png" style="width: 100px; border-radius:7px" />
						</a>
					</h2>
					<p>
						We thrive to bring  to you the best products at best possible price and that’s what we’re all about.
					</p>
					<ul class="social-nav model-3d-0 footer-social w3_agile_social two">
						<li><a href="##" class="facebook">
							  <div class="front"><i class="fa fa-facebook" aria-hidden="true"></i></div>
							  <div class="back"><i class="fa fa-facebook" aria-hidden="true"></i></div></a></li>
						<li><a href="##" class="instagram">
							  <div class="front"><i class="fa fa-instagram" aria-hidden="true"></i></div>
							  <div class="back"><i class="fa fa-instagram" aria-hidden="true"></i></div></a></li>
						<!--
						<li><a href="##" class="twitter"> 
							  <div class="front"><i class="fa fa-twitter" aria-hidden="true"></i></div>
							  <div class="back"><i class="fa fa-twitter" aria-hidden="true"></i></div></a></li>
						<li><a href="##" class="pinterest">
							  <div class="front"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
							  <div class="back"><i class="fa fa-linkedin" aria-hidden="true"></i></div></a></li>
						-->
					</ul>
				</div>
				<div class="col-md-9 footer-right">
					<div class="sign-grds">
						<div class="col-md-3 sign-gd">
							<h4>The <span>Site-map</span> </h4>
							<ul>
								<li><a href="<?=site_url();?>/website_home">Home</a></li>
								<?php foreach($parent_categories as $pcat){?>
								<li><a href="<?=site_url();?>/website_home/product_list/<?=$pcat->parent_cat_id?>"><?=$pcat->parent_cat_name?></a></li>
								<?php }?>
								<li><a href="<?=site_url();?>/website_home/about">About</a></li>
								<li><a href="<?=site_url();?>/website_home/contact">Contact</a></li>
							</ul>
						</div>
						<div class="col-md-4 sign-gd">
							<h4>Important <span>Links</span> </h4>
							<ul>
								<?php if(NULL == $this->session->userdata('user_id')){?>
								<li><a href="#" data-toggle="modal" data-target="#signinModal">Sign In</a></li>
								<li><a href="#" data-toggle="modal" data-target="#regmodal">Register</a></li>
								<?php }?>
								<li><a href="<?=site_url();?>/website_home/privacy_policies">Privacy & Return Policy</a></li>
								<li><a href="<?=site_url();?>/website_home/terms_conditions">Terms & Conditions</a></li>
							</ul>
							<hr />
							<a href="https://play.google.com/store/apps/details?id=wjmartz.com.wjmartz" target="_blank">
							    <img src="<?=base_url();?>/website_assets/images/playstore.png" style="width: 200px; border-radius:7px" />
							</a>
						</div>
						
						<div class="col-md-5 sign-gd-two">
							<h4>Store <span>Information</span></h4>
							<div class="w3-address">
								<!--
								<div class="w3-address-grid">
									<div class="w3-address-left">
										<i class="fa fa-phone" aria-hidden="true"></i>
									</div>
									<div class="w3-address-right">
										<h6>Phone Number</h6>
										<p>+1 234 567 8901</p>
									</div>
									<div class="clearfix"> </div>
								</div>
								-->
								<div class="w3-address-grid">
									<div class="w3-address-left">
										<i class="fa fa-map-marker" aria-hidden="true"></i>
									</div>
									<div class="w3-address-right">
										<h6>Location</h6>
										<p>
											CST No. 56 FL 6, Sankalp Campus,Shivajinagar,<br />
											Pune - 411005
										</p>
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="w3-address-grid">
									<div class="w3-address-left">
										<i class="fa fa-envelope" aria-hidden="true"></i>
									</div>
									<div class="w3-address-right">
										<h6>Email Address</h6>
										<p>Email :<a href="#mailto:support@bhoolnanahi.com"> support@wjmartz.com</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
				
			</div>
		</div>
		<!-- //footer -->


		<a href="#home" class="scroll desktopView" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
		<script  type="application/x-javascript" src="<?=base_url();?>website_assets/js/custom.js"></script>
		<!-- js -->
		<script type="text/javascript" src="<?=base_url();?>website_assets/js/jquery-2.1.4.min.js"></script>
		<!-- //js -->
		<script src="<?=base_url();?>website_assets/js/modernizr.custom.js"></script>
		<script src="<?=base_url();?>website_assets/js/imagezoom.js"></script>
		<!-- script for responsive tabs -->						
		<script src="<?=base_url();?>website_assets/js/easy-responsive-tabs.js"></script>
		<script>
			$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
			type: 'default', //Types: default, vertical, accordion           
			width: 'auto', //auto or any width like 600px
			fit: true,   // 100% fit in a container
			closed: 'accordion', // Start closed if in accordion view
			activate: function(event) { // Callback function if tab is switched
			var $tab = $(this);
			var $info = $('#tabInfo');
			var $name = $('span', $info);
			$name.text($tab.text());
			$info.show();
			}
			});
			$('#verticalTab').easyResponsiveTabs({
			type: 'vertical',
			width: 'auto',
			fit: true
			});
			});
		</script>
		<!-- FlexSlider -->
		<script src="<?=base_url();?>website_assets/js/jquery.flexslider.js"></script>
		<script>
		// Can also be used with $(document).ready()
			$(window).load(function() {
				$('.flexslider').flexslider({
				animation: "slide",
				controlNav: "thumbnails"
				});
			});
		</script>
		<!-- //FlexSlider-->
		<!-- //script for responsive tabs -->		
		<!-- stats -->
			<script src="<?=base_url();?>website_assets/js/jquery.waypoints.min.js"></script>
			<script src="<?=base_url();?>website_assets/js/jquery.countup.js"></script>
			<script>
				$('.counter').countUp();
			</script>
		<!-- //stats -->
		<!-- start-smoth-scrolling -->
		<script type="text/javascript" src="<?=base_url();?>website_assets/js/move-top.js"></script>
		<script type="text/javascript" src="<?=base_url();?>website_assets/js/jquery.easing.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
		<!-- for bootstrap working -->
		<script type="text/javascript" src="<?=base_url();?>website_assets/js/bootstrap.js"></script>
	</body>
</html>