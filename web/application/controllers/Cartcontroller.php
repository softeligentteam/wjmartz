<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cartcontroller extends CI_Controller {
	
	public function __construct() 
	{
        parent::__construct();
		$this->load->model('Ddlmodel');
		$this->load->model('HeaderData');
		$this->load->model('UserModel');
		$this->load->model('Products');
		// $this->menu['cart']=$this->cart->contents();
		$this->header_data['parent_categories'] = $this->HeaderData->get_all_parent_cat_menu_items();
		$this->header_data['child_categories']  = $this->HeaderData->get_all_child_cat_menu_items();
		$this->header_data['pincodes']  = $this->Ddlmodel->get_all_pincodes();
		$this->active_page  = array(
			'homepage' => null,
			'about' => null,
			'contact' => null,
			'active_cat' => null
		);
	}	
	
	public function index()
	{
		$data['cart_data']= $this->cart->contents();
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/website/cart',$data);
		$this->load->view('layouts/website_footer');
	}
	
	public function checkout()
	{
		if(NULL != $this->session->userdata('user_id'))
		{
			//$data['addresses1']= $this->Ddlmodel->get_all_addresses_of_user();
			$data['cart_data']= $this->cart->contents();
			$data['addresses'] = $this->UserModel->get_users_addresses($this->session->userdata('user_id'));
			$this->load->model('UserModel');
			$data['customer_name']=$this->UserModel->get_users_profile();
			$this->header_data['pincodes']  = $this->Ddlmodel->get_all_pincodes();
			$data['cities']= $this->Ddlmodel->get_all_cities();
			$data['states']= $this->Ddlmodel->get_all_states();
			$data['timeslots']= $this->Ddlmodel->get_time_slots();
			$this->load->view('layouts/website_header',$this->header_data);		
			$this->load->view('pages/website/checkout',$data);
			$this->load->view('layouts/website_footer');
		}
		else{
			$this->session->set_flashdata(array('flash_title' => 'Need login!', 'flash_msg' => 'Please login first to continue.'));
			redirect('cartcontroller');
		}
	}
	
	public function add_product_to_cart()
	{
		if($this->input->post())
		{	
			$prod_id = $this->input->post('prod_id');
			$size_id = $this->input->post('product_size_id');
			if($this->check_product_allready_in_cart($prod_id,$size_id))
			{
				$this->session->set_flashdata(array('flash_title' => 'Already In Cart', 'flash_msg' => 'Same product with this size is already added to the cart'));
				redirect('website_home/product_details/'.$this->input->post('pcat_id').'/'.$this->input->post('ccat_id').'/'.$this->input->post('prod_id').'/');
			}
			else
			{
				$product_stock= $this->Products->check_product_stock($prod_id,$size_id);
				$product_details= $this->Products->get_single_product_details($prod_id);
				if($product_stock->quantity >= $this->input->post('prod_quantity'))
				{	
					 $product = array(array(
							'id'=>$this->input->post('prod_id'),
							'name'=>$this->input->post('prod_name'),
							'price'=>$this->input->post('prod_price'),
							'qty'=>$this->input->post('prod_quantity'),
							'options'=>array('prod_id'=>$this->input->post('prod_id'),'prod_image_url'=>$this->input->post('prod_image_url'),'color_name'=>$this->input->post('color_name'),'brand'=>$this->input->post('brand'),'fabric_type'=>$this->input->post('fabric_type'),'product_size_id'=>$this->input->post('product_size_id'),'product_size_name'=>$this->input->post('product_size_name'),'sgst'=>$product_details->sgst,'cgst'=>$product_details->cgst)
						));
					if(!empty($product))
					{
						//$this->cart->destroy();
						$this->cart->insert($product);
						$this->session->set_flashdata(array('flash_title' => 'Add To Cart', 'flash_msg' => 'Product added to cart successfully'));
						redirect('Cartcontroller');
					}
					else
					{
						$this->session->set_flashdata(array('flash_title' => 'Add To Cart Failed', 'flash_msg' => 'Product add to cart failed'));
						redirect('Cartcontroller');
					}
				}
				else
				{
					$this->session->set_flashdata(array('flash_title' => 'Less Quantity', 'flash_msg' => 'Product has less Quantity!'));
					redirect('website_home/product_details/'.$this->input->post('pcat_id').'/'.$this->input->post('ccat_id').'/'.$this->input->post('prod_id').'/');
				}
			}
		}
		else
		{
			$this->session->set_flashdata(array('flash_title' => 'Add To Cart Failed', 'flash_msg' => 'Failed to add product to cart, please try again'));
		}
	}
	private function check_product_allready_in_cart($prod_id,$size_id)
    {
        foreach ($this->cart->contents() as $items)
        {
            if(($items['id']===$prod_id)){
                if(($items['options']['product_size_id']===$size_id))
                {
                    return 1;
                }
            }
            else
            {
                return 0;
            }
        }
    }
	public function decrease_item_qty()
	{
		$rowid = $this->uri->segment(3,0);
		$qty = $this->uri->segment(4,0);
		$data = array(array(
        'rowid' => $rowid,
        'qty'   => --$qty,
		));
		$this->cart->update($data);
		redirect('Cartcontroller');
	}
	public function increase_item_qty()
	{
		$rowid = $this->uri->segment(3,0);
		$qty = $this->uri->segment(4,0);
		foreach ($this->cart->contents() as $items)
		{
			if($items['rowid']===$rowid)
			{
				$product_stock= $this->Products->check_product_stock($items['id'],$items['options']['product_size_id']);
				if($product_stock->quantity > $items['qty'])
				{
					$data = array(array(
					'rowid' => $rowid,
					'qty'   => ++$qty,
					)); 
					$this->cart->update($data);
					redirect('Cartcontroller');
				}
				else
				{
					$this->session->set_flashdata(array('flash_title' => 'Less Quantity', 'flash_msg' => 'Product has less Quantity!'));
					redirect('Cartcontroller');
				}
			}
		}
	}
	public function remove_item()
	{
		
		$rowid = $this->uri->segment(3,0);
		$this->cart->remove($rowid);
		redirect('Cartcontroller');
	
	}
}
