<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UpdateProfile extends CI_Controller {
	
	public function __construct() 
	{
        parent::__construct();
		if(NULL == $this->session->userdata('user_id')){
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="">Not Logged In</span>',
					'flash_msg' => 'Please login first to access the user dashboard.',
				)
			);
            redirect('website_home');
        }
		$this->load->model('UpdateProfileModel');
	}	
	
	
	public function update_name()
	{
		if($this->input->post()){
			if($this->UpdateProfileModel->update_name()){
				$this->session->set_flashdata(
					array(
						'flash_title' => '<span style="">Name Updated</span>',
						'flash_msg' => 'You have successfully updated your user name.',
					)
				);
				redirect('user/view_my_profile');
			}else{
				$this->session->set_flashdata(
					array(
						'flash_title' => '<span style="">Failure</span>',
						'flash_msg' => 'Something went wrong, please try again later.',
					)
				);
				redirect('user/view_my_profile');
			}
		}else{
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="">Failure!</span>',
					'flash_msg' => 'Something went wrong, please try again later.',
				)
			);
			redirect('user/view_my_profile');
		}
	}
	
	public function update_dob()
	{
		if($this->input->post()){
			if($this->UpdateProfileModel->update_dob()){
				$this->session->set_flashdata(
					array(
						'flash_title' => '<span style="">D.O.B Updated</span>',
						'flash_msg' => 'You have successfully updated your user D.O.B.',
					)
				);
				redirect('user/view_my_profile');
			}else{
				$this->session->set_flashdata(
					array(
						'flash_title' => '<span style="">Failure</span>',
						'flash_msg' => 'Something went wrong, please try again later.',
					)
				);
				redirect('user/view_my_profile');
			}
		}else{
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="">Failure!</span>',
					'flash_msg' => 'Something went wrong, please try again later.',
				)
			);
			redirect('user/view_my_profile');
		}
	}
	
	public function update_email()
	{
		if($this->input->post()){
			if($this->UpdateProfileModel->update_email()){
				$this->session->set_flashdata(
					array(
						'flash_title' => '<span style="">Email Updated</span>',
						'flash_msg' => 'You have successfully updated your user email.',
					)
				);
				redirect('user/view_my_profile');
			}else{
				$this->session->set_flashdata(
					array(
						'flash_title' => '<span style="">Failure</span>',
						'flash_msg' => 'Something went wrong, please try again later.',
					)
				);
				redirect('user/view_my_profile');
			}
		}else{
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="">Failure!</span>',
					'flash_msg' => 'Something went wrong, please try again later.',
				)
			);
			redirect('user/view_my_profile');
		}
	}

	public function update_mobile()
	{
		if($this->input->post()){
			if($this->UpdateProfileModel->update_number()){
				
				$this->session->set_flashdata(
					array(
						'flash_title' => '<span style="">Mobile Number Updated</span>',
						'flash_msg' => 'You have successfully updated your mobile number.',
					)
				);
				redirect('user/view_my_profile');
			}else{
				$this->session->set_flashdata(
					array(
						'flash_title' => '<span style="">Failure</span>',
						'flash_msg' => 'Something went wrong, please try again later.',
					)
				);
				redirect('user/view_my_profile');
			}
		}else{
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="">Failure!</span>',
					'flash_msg' => 'Something went wrong, please try again later.',
				)
			);
			redirect('user/view_my_profile');
		}
	}
	public function update_change_password()
    {
        if($this->input->post()){
            $update_password = $this->UpdateProfileModel->update_change_password();
            if($update_password > 0){
                $this->session->set_flashdata(
                    array(
                        'flash_title' => '<span style="color:green">Password Changed</span>',
                        'flash_msg' => 'Your Password is changed successfully, please log-in again to continue',
                    )
                );
                $this->unset_login_session();
                $this->session->unset_userdata('otpVerified');
                redirect('Website_home/index');
            }else{
                $this->session->set_flashdata(
                    array(
                        'flash_title' => '<span style="color:red">Failure</span>',
                        'flash_msg' => 'Failed to update password, please try again.',
                    )
                );
                redirect('user/change_password');
            }
        }
		redirect('user/change_password');
    }
	
	public function update_address()
    {
		if($this->input->post('cartupdateAddress'))
		{
			if($this->input->post()){
				if($this->UpdateProfileModel->update_address()){
					$this->session->set_flashdata(
						array(
							'flash_title' => '<span style="color:green">Address Updated!</span>',
							'flash_msg' => 'Address updated successfully.',
						)
					);
					redirect('cartcontroller/checkout');
				}else{
					$this->session->set_flashdata(
						array(
							'flash_title' => '<span style="color:yellow">Failure!</span>',
							'flash_msg' => 'Something went wrong, Please be patient.',
						)
					);
					redirect('cartcontroller/checkout');
				}
			}else{
				 $this->session->set_flashdata(
					array(
						'flash_title' => '<span style="color:red">Unknown Error!</span>',
						'flash_msg' => 'Some technical issue, please try once again.',
					)
				);
				redirect('cartcontroller/checkout');
			}
		}
		else
		{
			if($this->input->post()){
				if($this->UpdateProfileModel->update_address()){
					$this->session->set_flashdata(
						array(
							'flash_title' => '<span style="color:green">Address Updated!</span>',
							'flash_msg' => 'Address updated successfully.',
						)
					);
					redirect('user/view_my_profile');
				}else{
					$this->session->set_flashdata(
						array(
							'flash_title' => '<span style="color:yellow">Failure!</span>',
							'flash_msg' => 'Something went wrong, Please try again.',
						)
					);
					redirect('user/view_my_profile');
				}
			}else{
				 $this->session->set_flashdata(
					array(
						'flash_title' => '<span style="color:red">Unknown Error!</span>',
						'flash_msg' => 'Some technical issue, please try again.',
					)
				);
				redirect('user/view_my_profile');
			}
		}
	}
	
	public function add_new_address()
    {
		if($this->input->post()){
			if($this->UpdateProfileModel->add_new_address()){
				$this->session->set_flashdata(
					array(
						'flash_title' => '<span style="color:green">Address Added!</span>',
						'flash_msg' => 'New address is added successfully.',
					)
				);
				redirect('user/view_my_profile');
			}else{
				$this->session->set_flashdata(
					array(
						'flash_title' => '<span style="color:yellow">Failure!</span>',
						'flash_msg' => 'Something went wrong, Please try again.',
					)
				);
				redirect('user/view_my_profile');
			}
		}else{
			 $this->session->set_flashdata(
                array(
                    'flash_title' => '<span style="color:red">Unknown Error!</span>',
                    'flash_msg' => 'Some technical issue, please try once again.',
                )
            );
            redirect('user/view_my_profile');
		}
	}
	
	
    public function unset_login_session()
    {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('mobile');
        $this->cart->destroy();
        return true;
    }
}
