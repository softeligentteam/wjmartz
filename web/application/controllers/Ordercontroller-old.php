<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordercontroller extends CI_Controller {
	
	public function __construct() 
	{
        parent::__construct();
		$this->load->model('HeaderData');
		$this->load->model('Products');
		$this->load->model('OrderModel');
		// $this->menu['cart']=$this->cart->contents();
		$this->header_data['parent_categories'] = $this->HeaderData->get_all_parent_cat_menu_items();
		$this->header_data['child_categories']  = $this->HeaderData->get_all_child_cat_menu_items();
		$this->active_page  = array(
			'homepage' => null,
			'about' => null,
			'contact' => null,
			'active_cat' => null
		);
	}	

	
		public function order_preview()
	{
		if($this->input->post('check_page'))
		{
			$ref_order_id=$this->OrderModel->insert_temp_order();
			$this->session->set_userdata('ref_id',$ref_order_id);
			if($ref_order_id!=0)
			{
				$data['customer_data']=$this->OrderModel->get_customer_order_data();
				$data['product_data']=$this->OrderModel->get_product_order_data();
				$data['temp_data']=$this->OrderModel->get_reference_data($ref_order_id);
				
				$this->load->library('payment');
				$data['posted']=$this->payment->get_plan_payment_credentials($data);
				$data['delivery_address'] = $this->OrderModel->get_delivery_address_by_id();
				$this->load->view('layouts/website_header',$this->header_data); 
				$this->load->view('pages/website/order_preview',$data);
				$this->load->view('layouts/website_footer');
			}
			else
			 {
				redirect('cartcontroller/checkout');
			 } 
		}else{
		
		
				redirect('cartcontroller');
		
		}
	}
	
	public function order_details()
	{
		$data['']= null;
		$this->load->view('layouts/website_header',$this->header_data);
		// $this->load->view('layouts/nav',$this->active_page);
		$this->load->view('pages/website/order_details',$data);
		$this->load->view('layouts/website_footer');
	}
		
	public function order_failed($data){
		$this->load->view('layouts/website_header',$this->header_data); 
		$this->load->view('pages/website/order_failed',$data);
		$this->load->view('layouts/website_footer');
	}
		
	public function order_success(){
		$order_id=$this->session->flashdata('oid');
		$data['success_data']=$this->OrderModel->order_data($order_id);
		//$data['product_data']=$this->OrderModel->get_orderwise_products($order_id);
		$this->load->view('layouts/website_header',$this->header_data); 
		$this->load->view('pages/website/order_success',$data);
		$this->load->view('layouts/website_footer');
	}
	
	public function order_success_failed(){
		$reference_id=$this->input->post('udf1');
		$data['success_data']=$this->OrderModel->get_failure_data($reference_id);
		//$data['product_data']=$this->OrderModel->get_orderwise_products($order_id);
		$this->load->view('layouts/website_header',$this->header_data); 
		$this->load->view('pages/website/order_success_failed',$data);
		$this->load->view('layouts/website_footer');
	}
	
	public function invalid_transaction(){
		
		$header['title'] = 'Invalid Transaction|Wjmartz';
		$this->load->view('layouts/website_header',$this->header_data); 
		$this->load->view('pages/website/invalid_transaction');
		$this->load->view('layouts/website_footer');
	}
	
	 public function get_reference_order_data()
	  {
		    if($this->input->post())
			{	
			if($this->OrderModel->insert_temp_order())
			{
				$data['customer_data']=$this->OrderModel->get_customer_order_data();
				$data['product_data']=$this->OrderModel->get_product_order_data();
				redirect('ordercontroller/order_preview');
				//$data['imageaccesspath']=IMAGEACCESSPATH;
				//echo json_encode($data);
				
			}
			else
			 {
				echo 0;
			 } 
			}else{
				echo 0;
			}	
	  }
	  
	 public function enroll_member()
    {
		
		$reference_id = $this->session->userdata('ref_id');
		$pay_mode=$this->session->userdata('pay');
    	if($this->input->post())
    	{
			if($pay_mode!=1)
			{	
				$this->load->library('payment');
				if($this->payment->verify_success_payment()){
    			$order_data = $this->OrderModel->get_temp_member_to_create_membership($this->input->post('udf1')); // Member Reference Id
    			$this->create_membership($order_data);
				}
				else{
    			redirect('ordercontroller/invalid_transaction');
				}
			}else{
				$order_data = $this->OrderModel->get_temp_member_to_create_membership_COD($reference_id); // Member Reference Id
    			$this->create_membership($order_data);
			}
			
    	}
    	else
    	{
    		redirect('website_home/index');
    	}
    }
	
	
	
	 private function create_membership($order_data){
    	
    	$order_id = $this->OrderModel->enroll_customer_with_payment($order_data);
		
    	if($order_id !== 0){
    		//$this->move_member_photo($temp_path, $member_data->profile_photo); // moving file from member/temp to member
    		$this->OrderModel->add_order_id_for_products($order_id); // udf1 - Temp Member Reference Id
			$this->OrderModel->temp_order_delete(); 
			$this->OrderModel->temp_order_payment_delete();
			$this->OrderModel->change_stock_quantity_of_product($order_id);
			$this->OrderModel->increase_count_for_promo($order_id);
			$this->send_email_for_order_place($order_id);
			$data['order']=$this->OrderModel->order_data($order_id);
			$this->load->library('sms_gateway');
			$this->sms_gateway->send_sms_for_order_place($data['order']->mobile,$order_id,$data['order']->first_name);
			$this->cart->destroy();

    		$this->session->set_flashdata('oid', $order_id);
    		redirect('ordercontroller/order_success');
    	}
    	else{
			
    		$this->OrderModel->update_temp_payment();
			$this->send_email_for_order_place_fail();
			//$this->send_payment_failure_mail_to_member();
    		$this->session->set_flashdata('ref1_id', $this->input->post('udf1'));
    		redirect('ordercontroller/order_success_failed');
			
    	}
    }
	
	 public function order_purchase_failure(){
    	if($this->input->post())
    	{
    		$this->load->library('payment');
    		if($this->payment->verify_failure_payment()){
    			$this->OrderModel->update_temp_payment();//Update Temp Member Payment Details
    			$data['failure_data'] = $this->OrderModel->get_failure_data($this->input->post('udf1')); //$this->input->post('udf1') Member Reference Id
	    		if($data['failure_data'] !== NULL){
	    			//$data['plan'] = $this->Planmodel->get_plandata_for_enrollment($data['member']->pid);
	    			//$this->load->library('payment');
	    			//$data['posted'] = $this->payment->get_plan_payment_credentials($data);
					$this->send_email_for_order_place_fail();
	    			$this->order_failed($data);
	    		}
	    		else{
	    			redirect('website_home/index');
	    		}
    		}
    		else{
    			redirect('ordercontroller/invalid_transaction');;
    		}
    	}
    	else{
    		redirect('website_home/index');
    	}
    }
	
	private function send_email_for_order_place($order_id)
	{
		$order_data['order']=$this->OrderModel->get_order_data_for_mail($order_id);
		$order_data['order_product']=$this->OrderModel->get_orderwise_products($order_id);
		$message=$this->load->view('email/order_placed',$order_data,TRUE);
		$this->load->library('email_manager');
		$this->email_manager->send_order_place_email($order_data['order']->email,$message);
	}
	
	private function send_email_for_order_place_fail()
	{
		$order_data['order']=$this->OrderModel->get_order_data_for_order_fail_mail();
		$message=$this->load->view('email/order_fail',$order_data,TRUE);
		$this->load->library('email_manager');
		$this->email_manager->send_order_place_fail_email($order_data['order']->email,$message);
	}
	public function cancel_user_order(){
		$order_id = $this->uri->segment(3,0);
		$order_data['order']=$this->OrderModel->get_order_data_for_mail($order_id);
		$userDetails = $this->OrderModel->cancel_user_order($order_id);
		if($userDetails != null)
		{
			$this->send_email_for_order_cancel($order_id);
			$this->load->library('sms_gateway');
			$this->sms_gateway->send_sms_for_order_cancel($order_data['order']->mobile,$order_id,$order_data['order']->first_name);
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="color:green;">Cancel Order!</span>',
					'flash_msg' => 'Order cancelled Successfully',
				)
			);
			redirect('user/');
		}else{
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="color:green;">Failed To Cancel!</span>',
					'flash_msg' => 'Failed to cancel your order, please try again',
				)
			);	
			redirect('user/order_details/'.$order_id);
		}
	}
	private function send_email_for_order_cancel($order_id)
	{
		$order_data['order']=$this->OrderModel->get_order_data_for_mail_cancel($order_id);
		$order_data['order_product']=$this->OrderModel->get_orderwise_products_cancel($order_id);
		$message=$this->load->view('email/order_cancel',$order_data,TRUE);
		$this->load->library('email_manager');
		$this->email_manager->send_order_cancel_email($order_data['order']->email,$message);
	}
	public function get_refund_order_data()
	{
		$order_id = $this->uri->segment(3,0);
		if($this->OrderModel->get_refund_prod_details($order_id))
		{
			$data['refund_order_data']=$this->OrderModel->get_refund_order_data();
			$data['refund_prod_data']=$this->OrderModel->get_refund_order_prod_data();
			$this->send_email_for_refund_order();
			$data['order']=$this->OrderModel->get_refund_details();
			$this->load->library('sms_gateway');
			$this->sms_gateway->send_sms_for_order_refund($data['order']->mobile,$data['order']->refund_id,$data['order']->first_name);
			$this->session->set_flashdata(
			array(
					'flash_title' => '<span style="color:green;">Request Sent!</span>',
					'flash_msg' => 'Your request for order return is sent successfully.',
				)
			);
			redirect('user/');
		}
		else
		{
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="color:green;">Refund Failed!</span>',
					'flash_msg' => 'Failed to send request, please try again',
				)
			);	
			redirect('user/order_details/'.$order_id);
		} 
	} 
	
	public function generate_otp_refund()
    {
		$mobile = $this->Ordermodel->get_mobile_number();

		//$mobile=(int)$mobile1;
		
		$otp = rand(100000, 999999);
		if($mobile!=NULL){
			$this->db->where_in('mobile',$mobile);
			$result=$this->db->get('customer')->row();
			
			if($result!=NULL){
				//100000;
				$this->session->set_userdata(array('mobile'=>$mobile,'otp'=>$otp));
				$this->load->library('sms_gateway');
				$result1=$this->sms_gateway->genrate_otp($mobile["mobile"],$otp);
				//var_dump($result1);
				if($result1!=NULL)
				{	
					$data['otp']=$otp;
					$data['status']=1;
					echo json_encode($data);
				}	
			}
			else{
				$data['otp']=$otp;
				$data['status']=0;
				echo json_encode($data);
			}
		}
		else{
			$data['status']=0;
			echo json_encode($data);
		}
           
    }

	public function get_refunded_product_list()
	{
		$order_id=$this->uri->segment(3,0);
		$data['refund_data']=$this->OrderModel->get_refund_products($order_id);
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function send_email_for_refund_order()
	{
		$data['order']=$this->OrderModel->get_refund_details();
		$data['refund_order_product']=$this->OrderModel->get_refund_products_details();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		$message=$this->load->view('email/order_returned_exchanged.php',$data,TRUE);
		$this->load->library('email_manager');
		$this->email_manager->send_refund_order_email($data['order']->email,$message);
	}
	
}
