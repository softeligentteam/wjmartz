<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();

		$this->load->model('Promomodel');
		
	}	
	
	/*public function apply_coupon_code()
	{
		$coupon_code=$this->input->post('coupon');
		if($this->input->post())
		{	
		$result=$this->Promomodel->get_promocode($coupon_code);
		if($result!=NULL)
		{
			
			if($this->Promomodel->check_coupon_code($coupon_code)===FALSE)
			{	
				$data['status']='1';
				//$data['promo_id']=$this->Promomodel->get_data_for_promoid($coupon_code);
				$data['promo']=$this->Promomodel->get_data_for_discount($coupon_code);
				$data['message']=PROMOSUCCESSMESSAGE;
			}else
			{
			$data['status']='0';
			$data['promo']=$this->Promomodel->get_data_for_discount($coupon_code);
			if($data['promo']->promo_type_id!=5)
			{	
				$data['message']=PROMODEALMESSAGE;
			}else{
				$data['message']=PROMODEALIVERYMESSAGE;
			}	
			}			
		}else
		{
			$data['status']='0';
			$data['promo']=$this->Promomodel->get_data_for_discount($coupon_code);
			$data['message']=PROMODOESNOTEXISTMESSAGE;
		}
			
		}else
		{
			$data['status']='0';
			$data['promo']=$this->Promomodel->get_data_for_discount($coupon_code);
			$data['message']=NULL;
		}
		echo json_encode($data);
		
	}*/
	
	 //KT[date 7may 2018]
	public function apply_coupon_code()
	{
		$coupon_code=$this->input->post('coupon');
		//if($this->input->post())
		//{	
		$result=$this->Promomodel->get_promocode($coupon_code);
		if($result!=NULL)
		{
			
			if($this->Promomodel->check_coupon_code($coupon_code)==FALSE)
			{	
				$data['status']='1';
				//$data['promo_id']=$this->Promomodel->get_data_for_promoid($coupon_code);
				$data['promo']=$this->Promomodel->get_data_for_discount($coupon_code);
				if($data['promo']->promo_type_id==6)
				{	
					$data['cart_amt']=$this->session->userdata('cartamt');
				}	
				$data['message']=PROMOSUCCESSMESSAGE;
			}else
			{
			$data['status']='0';
			$data['promo']=$this->Promomodel->get_data_for_discount($coupon_code);
			if($data['promo']->promo_type_id==5)
			{
			    $data['message']=PROMODEALIVERYMESSAGE;
			    $this->session->set_userdata('ptypeid',5);
			   
			
			}elseif($data['promo']->promo_type_id==6){
						$data['message']=PROMOSUBCATMESSAGE;
			}else{
				 $data['message']=PROMODEALMESSAGE; 	
			}	
			}			
		}else
		{
			$data['status']='0';
			$data['promo']=$this->Promomodel->get_data_for_discount($coupon_code);
			$data['message']=PROMODOESNOTEXISTMESSAGE;
		}
			
		/*}else
		{
			$data['status']='0';
			$data['promo']=$this->Promomodel->get_data_for_discount($coupon_code);
			$data['message']=NULL;
		}*/
		echo json_encode($data);
		
	}

	public function get_offers()
{
	$data['offers_data']=$this->Promomodel->get_data_for_promo_offers();
	$data['imageaccesspath']=IMAGEACCESSPATH;
	echo json_encode($data);
	
}
	public function get_delivery_chrages()
	{
		$address_id=$this->input->post('add_id');
		if($this->input->post())
		{
			$data['delivery_data']=$this->Promomodel->get_delivery_charges($address_id);
		}else{
			$data['delivery_data']=null;
		}
		echo json_encode($data);
	}
}	