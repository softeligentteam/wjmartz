<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payment {

	/*************** Pass data to bind with template ****************/
	public function get_plan_payment_credentials($data)
	{
		// Merchant key here as provided by Payu
		$MERCHANT_KEY = "2kdiCU"; //secure
		//$MERCHANT_KEY = "gtKFFx"; //test
		
	
		// Merchant Salt as provided by Payu
		$SALT = "2ctVqZ5L"; //secure
		//$SALT = "eCwWELxi"; //test
		
		// End point - change to https://secure.payu.in for LIVE mode
		$PAYU_BASE_URL = "https://secure.payu.in"; //secure
		//$PAYU_BASE_URL = "https://test.payu.in"; //test
		
		$action = '';
		
		$posted = array();
		
		$posted['key'] = $MERCHANT_KEY;
		$posted['amount'] = $data['temp_data']->discount_amount;//$data['plan']->pprice;
		$posted['firstname'] = $data['temp_data']->first_name;
		$posted['email'] = $data['temp_data']->email;
		$posted['phone'] = $data['temp_data']->mobile;
		$posted['productinfo'] = 'Wjmartz';
		//$posted['surl'] = "http://www.denstagenie.com/website/index.php/enroll/enroll_member";//plan_purchase_success
		//$posted['furl'] = "http://www.denstagenie.com/website/index.php/enroll/plan_purchase_payment_failure";
		
		$posted['surl'] = site_url()."/ordercontroller/enroll_member"; // Place Order
        $posted['furl'] = site_url()."/ordercontroller/order_purchase_failure";
		$posted['udf1'] = $data['temp_data']->ref_order_id;
		//$posted['udf2'] = $data['temp_data']->amount;
		$posted['formError'] = 0;
		// Generate random transaction id
		$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$posted['txnid'] = $txnid;
		
		$hash = '';
		
		// Hash Sequence
		$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		//$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1||||||||||";
		//$hashSequence = $posted['key']."|".$posted['txnid']."|".$posted['amount']."|".$posted['productinfo']."|".$posted['firstname']."|".$posted['email']."|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		if(empty($posted['hash']) && sizeof($posted) > 0) {
		
			if(
					empty($posted['key'])
					|| empty($posted['txnid'])
					|| empty($posted['amount'])
					|| empty($posted['firstname'])
					|| empty($posted['email'])
					|| empty($posted['phone'])
					|| empty($posted['productinfo'])
					|| empty($posted['surl'])
					|| empty($posted['furl'])
					|| empty($posted['udf1'])
					//|| empty($posted['udf2'])
					) {
						$posted['formError'] = 1;
		
					} else {
						//$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
						$hashVarsSeq = explode('|', $hashSequence);
						$hash_string = '';
						foreach($hashVarsSeq as $hash_var) {
							$hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
							$hash_string .= '|';
						}
						$hash_string .= $SALT;
						
						$posted['hash'] = strtolower(hash('sha512', $hash_string));
						$posted['action'] = $PAYU_BASE_URL . '/_payment';
					}
		} else if(!empty($posted['hash'])) {
			$posted['hash'] = $posted['hash'];
			$posted['$action'] = $PAYU_BASE_URL . '/_payment';
		}
		
		return $posted;
	}
	
	/********** ON PAYU BIZZ PAYMENT SUCCESS VERIFICATION CALL *********/
	public function verify_success_payment(){
	
		$status=$_POST["status"];
		$firstname=$_POST["firstname"];
		$amount=$_POST["amount"];
		$txnid=$_POST["txnid"];
		$posted_hash=$_POST["hash"];
		$key=$_POST["key"];
		$productinfo=$_POST["productinfo"];
		$email=$_POST["email"];
		$ref_id=$_POST["udf1"];
		$salt="2ctVqZ5L";
	
		If (isset($_POST["additionalCharges"])) {
			$additionalCharges=$_POST["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'||||||||||'.$ref_id.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
	
		}
		else {
			
			$retHashSeq = $salt.'|'.$status.'||||||||||'.$ref_id.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
		
		}
	
		$hash = hash("sha512", $retHashSeq);
			
		if ($hash != $posted_hash) {
			return false;
		}
		else {
			return true;
		}
	}
	
	/********** ON PAYU BIZZ PAYMENT FAILURE VERIFICATION CALL *********/
	public function verify_failure_payment(){
	
		$status=$_POST["status"];
		$firstname=$_POST["firstname"];
		$amount=$_POST["amount"];
		$txnid=$_POST["txnid"];
		$posted_hash=$_POST["hash"];
		$key=$_POST["key"];
		$productinfo=$_POST["productinfo"];
		$email=$_POST["email"];
		$ref_id=$_POST["udf1"];
		//$amount1=$_POST["udf2"];
		$salt="2ctVqZ5L";
	
		If (isset($_POST["additionalCharges"])) {
			$additionalCharges=$_POST["additionalCharges"];
			$retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'||||||||||'.$ref_id.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
	
		}
		else {
	
			$retHashSeq = $salt.'|'.$status.'||||||||||'.$ref_id.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
	
		}
		
		$hash = hash("sha512", $retHashSeq);
		
		if ($hash != $posted_hash) {
			//echo "Invalid Transaction. Please try again";
			return false;
		}
		else {
			return true;
		}
	}
	
}