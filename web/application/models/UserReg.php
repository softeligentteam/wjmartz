<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserReg extends CI_Model{
	/***** Changes made By PJ on 13/04/2018 start *****/
	public function bind_customer_data()
    {
        $cust_details = array(
            'first_name' => $this->input->post('reg_fname'), 
            'last_name' => $this->input->post('reg_lname'), 
            'email' => $this->input->post('reg_email'), 
            'password' => md5($this->input->post('reg_password')),
            'dob' =>  ' ', 
            'mobile' => $this->input->post('reg_mobile'),
            'created_on' => date('Y-m-d H:i:s'),
        );
        return $cust_details;
    }
    private function get_area_name($area_id)
    {
        $this->db->select('area_name');
        $this->db->from('area');
        $this->db->where('area_id',$area_id);
        return $this->db->get()->row();
    }
	
	public function unset_reg_user_temp_session_data()
	{
		$this->session->unset_userdata('reg_fname');
		$this->session->unset_userdata('reg_lname');
		$this->session->unset_userdata('reg_mobile');
		$this->session->unset_userdata('reg_email');
		$this->session->unset_userdata('reg_password');
		$this->session->unset_userdata('otp');
		$this->session->unset_userdata('otpVerified');
	}
	
	public function bind_customer_address_data($cust_id)
    {
        // $area_id = $this->input->post('regArea');
        // $area = $this->get_area_name($area_id);
        $address_details = array(
            'cust_id' => $cust_id,
            'address' => $this->input->post('regAddress'),
            'pincode' => $this->input->post('regPin'),
            'area' => $this->input->post('regArea'),
            'city_id' => 1,
            'state_id' => 1,
            'is_default' => 1, 
            'address_type' => 1
        );
        return $address_details;
    }
    public function register_user(){
        
        $this->db->trans_begin();
        $cust_details = $this->bind_customer_data();
        $this->db->insert('customer', $cust_details);
        $cust_id = $this->db->insert_id();
        $address_details = $this->bind_customer_address_data($cust_id);
        $this->db->insert('customer_address', $address_details);
        if($this->db->trans_status()===FALSE){
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1 ;
		}
    }
	
	/***** Changes made By PJ on 13/04/2018 end *****/
	public function get_uid_of_reg_user_and_clear_reg_session($mobile)
	{		
		$this->unset_reg_user_temp_session_data();
		$this->db->select('cust_id,mobile,email,first_name,last_name');
        $this->db->where('mobile',$mobile);
        return $this->db->get('customer')->row();
	}
	
	public function get_promocode()
	{
		$this->db->select('promocode,discount');
		$this->db->where('promo_type_id',4);
		$this->db->where('status',1);
		return $this->db->get('promocode')->row();
	}
	
	/* public function find_pincode($pincode)
	{
		$this->db->select('pincode_id, pincode');
		$this->db->where('pincode',$pincode);
		return $this->db->get('pincode')->row();
	} */
	
	public function get_state_city_pincode($pincode)
	{
		$this->db->select('pi.pincode_id,pi.pincode,ct.city_id,ct.city,st.state_id,st.state');
		$this->db->join('area ar','ar.pincode_id = pi.pincode_id');
		$this->db->join('city ct','ct.city_id = ar.city_id');
		$this->db->join('state st','st.state_id = ct.state_id');
		$this->db->where('pi.pincode_id',$pincode);
		return $this->db->get('pincode pi')->row_array();
	}
	public function get_areas($pincodeId)
	{
		$this->db->select('area_id, area_name');
		$this->db->where('pincode_id',$pincodeId);
		return $this->db->get('area')->result_array();
	}
	/***** Changes made by PJ on 13/04/2018 *****/
	
	/* public function get_pincode_wise_areas($pincodeId)
    {
        $this->db->select('area_id, area_name');
        $this->db->where('pincode_id',$pincodeId);
        return $this->db->get('area')->result();
    } */
	
	public function fp_find_mobile_in_db()
	{
		$this->db->select('cust_id');
		$this->db->where('mobile',$this->input->post('fpmobile'));
		return $this->db->get('customer')->row();
	}
	
	public function forgot_password_change()
	{
		$cpassword=$this->input->post('password');
		$cust_id=$this->session->userdata('uid');
		
		$data=array(
		'password'=>md5($cpassword)
		);
		
		$this->db->where('cust_id',$cust_id);
		$this->db->update('customer',$data);
		return $this->db->affected_rows();
	}
	
}
