<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Childcategorymodel extends CI_Model
{
	public function insert_child_categories()
	{
		$child_cat_name=$this->input->post('child_category');
		$child_cat_img=$this->upload_child_cat_photo();
		$parent_cat_id=$this->input->post('parent_cats');
		
		$data=array(
		'child_cat_name'=>$child_cat_name,
		'child_cat_img_url'=>$child_cat_img,
		'parent_cat_id'=>$parent_cat_id
		);
		
		return $this->db->insert('child_category',$data);
	}
	
	
	function upload_child_cat_photo() {
    	$file_url = "";

			if ($_FILES["child_cat_img"]["error"] == 0) {

				$file_element_name = 'child_cat_img';

				$config['upload_path'] = CHILDCATUPLOADPATH;
		        $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
		        //$config['max_size'] = 1024 * 8;
		        // $config['max_width'] = 512;
		        // $config['max_height'] = 512;
		        $config['encrypt_name'] = TRUE;

		        $this->load->library('upload', $config);

		        if (!$this->upload->do_upload($file_element_name)) {
		        	$file_url = CHILDCATUPLOADPATH."/no_image.jpg";
		           // $msg = $this->upload->display_errors('', '');
		            // echo "<script>console.log($msg);</script>";
		        } else {
		        	$data = $this->upload->data();
		            $file_url = CHILDCATUPLOADPATH.$data['file_name'];
		            // echo "<script>console.log('File uploaded successfully.');</script>";
		        }

		        @unlink($_FILES[$file_element_name]);

			} else {
				 
				$file_url = CHILDCATUPLOADPATH."/no_image.jpg";
			}
			
			return $file_url;
    }
	
	public function get_child_cat_list($parent_cat_id)
	{	
		$this->db->select('child_cat_id,child_cat_name,child_cat_img_url');
		$this->db->where_not_in('child_cat_id',0);
		$this->db->where('parent_cat_id',$parent_cat_id);
		return $this->db->get('child_category')->result();
	}
	
	public function del_child_category($child_cat_id)
	{
		$this->db->where('child_cat_id',$child_cat_id);
		return $this->db->delete('child_category');
	}

	
	
}	
	