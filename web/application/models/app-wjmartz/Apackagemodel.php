<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apackagemodel extends CI_Model
{
	public function get_all_predefined_packages()
	{
		$this->db->select('package_id,package_name,package_price,package_img');
		$this->db->where_not_in('package_id',0);
		return $this->db->get('package')->result();
	}
	
	public function get_details_for_package($package_id)
	{
		$this->db->select('package_product_id,package_product,prod_quantity,prod_price,package_product_img');
		$this->db->where('package_id',$package_id);
		return $this->db->get('package_products')->result();
	}
	
	public function get_predefined_package($package_id)
	{
		$this->db->select('package_id,package_name,package_price,package_img');
		$this->db->where_not_in('package_id',0);
		$this->db->where('package_id',$package_id);
		return $this->db->get('package')->row();
	}
	
	public function get_all_customised_package_categories()
	{
		$this->db->select('per_package_id,package_main_product,main_product_img');
		$this->db->where_not_in('per_package_id',0);
		return $this->db->get('personalised_package')->result();
	}
	
	public function get_customised_package_products($per_page,$per_package_id)
	{
		$this->db->select('sub_product_id,product_name,per_product_img,product_price,quantity,product_description');
		$this->db->where_not_in('sub_product_id',0);
		$this->db->where('is_addon',0);
		$this->db->where('per_package_id',$per_package_id);
		return $this->db->get('personalised_package_products',$per_page,$this->uri->segment(4,0))->result();
	}
	
	public function get_no_of_rows($per_package_id)
	{
		$this->db->where('per_package_id',$per_package_id);;
		return $this->db->get('personalised_package_products')->num_rows();
	}
	
	public function get_all_package_addons($per_page)
	{
		$this->db->select('sub_product_id,product_name,per_product_img,product_price,quantity,product_description');
		$this->db->where('is_addon',1);
		$this->db->where('per_package_id',0);
		$this->db->where_not_in('sub_product_id',0);
		return $this->db->get('personalised_package_products',$per_page,$this->uri->segment(3,0))->result();
	}
	
	public function get_no_of_rows_addons()
	{
		$this->db->where('is_addon',1);
		return $this->db->get('personalised_package_products')->num_rows();
	}
}