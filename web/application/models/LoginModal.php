<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModal extends CI_Model{
	
	public function check_mobile_for_login()
	{
		$this->db->select('cust_id');
		$this->db->where('mobile',$this->input->post('mobile'));
		return $this->db->get('customer')->num_rows();
	}
	
	public function check_password_for_login()
	{
		$this->db->select('cust_id');
		$this->db->where('mobile',$this->input->post('mobile'));
		$this->db->where('password',md5($this->input->post('password')));
		return $this->db->get('customer')->num_rows();
	}
		
	public function set_login_session($uid)
	{
		$userDetails = $this->get_logedin_user_details($uid);
		$this->session->set_userdata(
			array(
				'user_id'=>$userDetails->cust_id,
				'fname'=>$userDetails->first_name,
				'lname'=>$userDetails->last_name,
				'email'=>$userDetails->email,
				'mobile'=>$userDetails->mobile
			)
		);
	}
	
	public function get_logedin_user_details($uid)
	{
		$this->db->select('cust_id, first_name, last_name, email, mobile');
		$this->db->where('cust_id',$uid);
		return $this->db->get('customer')->row();
	}
	
	public function authenticate_user(){
		$mobile = $this->input->post('mobNo');
		$password = md5($this->input->post('pass'));
		$this->db->select('cust_id,');
		$this->db->where('mobile', $mobile);
		$this->db->where('password', $password);
		return $this->db->get('customer')->row();
	}
	
	public function unset_login_session()
	{
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('mobile');
		$this->session->unset_userdata('otpVerified');
		$this->cart->destroy();
		return true;
	}
}
