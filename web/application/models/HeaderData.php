<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HeaderData extends CI_Model{
	
	public function get_all_parent_cat_menu_items(){
		$this->db->select('parent_cat_id, parent_cat_name, parent_cat_img_url');
		$this->db->where('status',1);
		return $this->db->get('parent_category')->result();
	}
	
	public function get_all_child_cat_menu_items(){
		$this->db->select('parent_cat_id, child_cat_id, child_cat_name, child_cat_img_url');
		$this->db->where('status',1);
		return $this->db->get('child_category')->result();
	}
	
	public function get_sliders(){
		$this->db->select('slider_img_id, slider_image');
		$this->db->where('status',1);
		$this->db->where('slider_type','Web');
		return $this->db->get('sliders')->result();
	}

	public function get_new_arrivals(){
		$this->db->select('prod_id,prod_name,prod_mrp,prod_price,prod_image_url,prod_image_urls, fk_pcat_id,fk_pchild_id,fk_psize_ids');
		$this->db->order_by('prod_id', 'desc');
		$this->db->where('prod_id !=', 0)->where('deal_status', 0)->where('in_stock', 1)->where('status', 1)->limit(12);
		return $this->db->get('products')->result();
	}
	
	public function get_deals_of_the_day(){
		$this->db->select('dod.deal_id, dod.deal_product_id as prod_id, dod.deal_price, dod.discount, dod.deal_image_url, dod.status, pr.fk_pcat_id as pcat_id,pr.fk_pchild_id as ccat_id');
		$this->db->join('products pr','pr.prod_id = dod.deal_product_id');
		$this->db->where('dod.status', 1)->where('pr.status', 1)->where('pr.deal_status', 1)->where('in_stock', 1);
		return $this->db->get('deals_of_day_product dod')->result();
	}
	
	public function fetch_parent_categories(){
		$this->db->select('parent_cat_id, parent_cat_name, parent_cat_img_url');
		$this->db->where('status',1);
		return $this->db->get('parent_category')->result_array();
	}
	
	// END Responsive Chnages from alex : 12-05-2018
	public function fetch_all_parent_categories(){
		$this->db->select('parent_cat_id, parent_cat_name, parent_cat_img_url, status');
		return $this->db->get('parent_category')->result_array();
	}
	
	public function get_section_wise_category_preference(){
		$this->db->select('parent_cat_id')->order_by('screen_id');
		return $this->db->get('home_screen_section')->result_array();
	}
		
	public function get_app_sliders(){
		$this->db->select('slider_img_id, slider_image');
		$this->db->where('status',1);
		$this->db->where('slider_type','Android');
		return $this->db->get('sliders')->result();
	}
		
	public function get_pcat_wise_ccats($pcat_id)
	{
		$this->db->select('parent_cat_id, child_cat_id, child_cat_name, child_cat_img_url');
		$this->db->where('status',1);
		$this->db->where('parent_cat_id',$pcat_id);
		return $this->db->get('child_category')->result();
	}
	
	public function get_all_sizes()
	{
		$this->db->select('size_id,size');
		// $this->db->where('status',1);
		$this->db->where('parent_cat_id',1);
		return $this->db->get('product_sizes')->result();
	}
	// END Responsive Chnages from alex : 12-05-2018
	
}