<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Model{
	/**** Changes made by PJ on 12/04/2018 ****/
	/**** KC - Changes on 16/05/2018 ****/
	public function get_pcat_ccat_wise_product_list($pcat, $ccat){
        $this->db->select('prod_id,prod_name,prod_mrp,prod_price,prod_image_url,prod_image_urls,fk_psize_ids');
        $this->db->join('product_colors pc','pc.color_id = p.fk_pcolor_id');
		$this->db->join('product_sizes ps','ps.size_id = p.fk_psize_ids');
		if(count($_POST))
		{
			if($this->input->post('ptype_id') !== NULL)
			{
				$this->db->where('fk_ptype_id', $this->input->post('ptype_id'));
			}
			if($this->input->post('childCat') !== NULL)
			{
				$this->db->where('fk_pchild_id', $this->input->post('childCat'));
			}
			if($this->input->post('pcolor[]') !== NULL)
			{
				$this->db->where_in('fk_pcolor_id', $this->input->post('pcolor[]'));
			}
			if($this->input->post('psize[]')!==NULL)
			{
				$this->db->group_start();
				$index = 1;
				foreach($this->input->post('psize[]') as $size)
				{
					if($index == 1)
					{
						$this->db->like('fk_psize_ids',$size);
					}
					else
					{
						$this->db->or_like('fk_psize_ids',$size);
					}
					$index++;
				}
				$this->db->group_end();			
			}
		}
		else
		{
			if($ccat !==0)
			{
				$this->db->where('fk_pchild_id',$ccat);
			}
		}
        $this->db->where('fk_pcat_id',$pcat)->where('in_stock', 1);
		$this->db->where('deal_status', 0)->where('status',1);
		if($this->input->post('price_filter') == 1 || $this->input->post('price_filter') == -1)
		{
			$this->db->order_by('prod_price', $this->input->post('price_filter') == 1 ? 'DESC' : 'ASC');
		}
        return $this->db->get('products p')->result();
    }
	
	public function get_single_product_details($prod_id){
		$this->db->select('prod.prod_id,prod.prod_name,prod.prod_mrp,prod.prod_price,prod.prod_image_url,prod.prod_image_urls,prod.fk_psize_ids,prod.size_chart_image_url,prod.prod_description,prod.prod_fit_details,prod.prod_fabric_details,prod.delivery_n_return,prod_color.color_name,prod.fabric_type,prod_brand.brand, prod.status,prod.refund_status,prod.discount,sgst,cgst,igst');
		$this->db->join('product_colors prod_color','prod_color.color_id=prod.fk_pcolor_id');
		$this->db->join('brands prod_brand','prod_brand.brand_id=prod.fk_pbrand_id');
		$this->db->where('prod_id',$prod_id);
		$this->db->where('prod.status',1);
		return $this->db->get('products prod')->row();
	}
	public function get_product_sizes($sizes, $product_id){
		/*$this->db->select('size, size_id');
		$this->db->where_in('size_id',$sizes);
		return $this->db->get('product_sizes')->result_array();
		*/
		$this->db->select('ps.size, ps.size_id, psq.quantity');
		$this->db->join('product_slot_quantity psq', 'psq.size_id = ps.size_id and psq.product_id ='.$product_id);
		$this->db->where('psq.quantity !=', 0);
		$this->db->where_in('ps.size_id',$sizes);
		return $this->db->get('product_sizes ps')->result_array();
	}

	public function get_product_type_list()
	{
		$this->db->select('ptype_id, ptype')->order_by('ptype_id');
		return $this->db->get('product_types')->result();
	}

	public function get_parent_cat_name($pcat_id){
		$this->db->select('parent_cat_name');
		$this->db->where('parent_cat_id',$pcat_id);
		return $this->db->get('parent_category')->row();
	}
	public function get_child_cat_name($ccat_id){
		$this->db->select('child_cat_name');
		$this->db->where('child_cat_id',$ccat_id);
		$this->db->where('status',1);
		return $this->db->get('child_category')->row();
	}
	public function get_child_cats($pcat){
		$this->db->select('child_cat_name,child_cat_id');
		$this->db->where('parent_cat_id',$pcat);
		$this->db->where('status',1);
		return $this->db->get('child_category')->result();
	}
	/*-----code by kanchan[date:-11/04/18]-----*/
	public function check_pincodewise_charge($pincode)
	{
		$this->db->select('delivery_charges,delivery_day');
		$this->db->where('pincode',$pincode);
		return $this->db->get('pincode')->row();	
	}
	/*-----code by kanchan[date:-10/04/18]----- Commented by KC 16/05/2018*/
	/*public function get_filter_low_price($pcat,$ccat)
	{
		$this->db->select('prod_id as pid, fk_pchild_id as pccat_id, prod_name as pname, fk_psize_ids, prod_mrp as mrp, prod_price as price, prod_image_url as image_url,discount,fk_pcolor_id,refund_status,deal_status');
		$this->db->select('prod_id,prod_name,prod_mrp,prod_price,prod_image_url,prod_image_urls');
		$this->db->where('fk_pcat_id',$pcat);
		if($ccat!=0){
			$this->db->where('fk_pchild_id',$ccat);
		}
		$this->db->where('in_stock', 1)->order_by('prod_price', 'asc');
		$this->db->where('deal_status', 0);
		return $this->db->get('products')->result();
	}
	
	public function get_filter_high_price($pcat,$ccat)
	{
		$this->db->select('prod_id as pid, fk_pchild_id as pccat_id, prod_name as pname, fk_psize_ids, prod_mrp as mrp, prod_price as price, prod_image_url as image_url,discount,fk_pcolor_id,refund_status,deal_status');
		$this->db->select('prod_id,prod_name,prod_mrp,prod_price,prod_image_url,prod_image_urls');
		$this->db->where('fk_pcat_id',$pcat);
		if($ccat!=0){
			$this->db->where('fk_pchild_id',$ccat);
		}
		$this->db->where('in_stock', 1)->order_by('prod_price', 'desc');
			$this->db->where('deal_status', 0);
		return $this->db->get('products')->result();
	}*/
	/*-----code by kanchan[date:-10/04/18]-----*/
	public function get_productsizes()
	{
		$this->db->select('size, size_id');
		$this->db->where('parent_cat_id',1);
		$this->db->order_by("size", "asc");
		return $this->db->get('product_sizes')->result();
	}
	/*-----code by kanchan[date:-10/04/18]-----*/
	public function get_productcolor()
	{
		$this->db->select('color_id,color_name');
		$this->db->order_by("color_name", "asc");
		return $this->db->get('product_colors')->result();
	}
	
	/*-----code by kanchan[date:-10/04/18]----- Modified by KC - 24/05/2018-----*/
	public function search_element($search_string)
	{
		$search_words = explode(" ", $search_string);
		$this->db->select('prod_id,prod_name,prod_mrp,prod_price,prod_image_url,prod_image_urls,fk_pcat_id,fk_pchild_id,fk_psize_ids');
		$this->db->where('status', 1)->where('deal_status', 0)->where('in_stock', 1);
		$this->db->group_start();
		$index = 1;
		foreach($search_words as $word)
		{
			if($index == 1)
			{
				$this->db->like('prod_name',$word);
			}
			else
			{
				$this->db->or_like('prod_name',$word);
			}
			$index++;
		}
		$this->db->group_end();
		return $this->db->get('products')->result();
	}
	
	/***** PJ[date:-25/04/18] *****/
	public function check_product_stock($prod_id,$size_id)
	{
		$this->db->select('quantity');
		$this->db->where('product_id',$prod_id);
		$this->db->where('size_id',$size_id);
		return $this->db->get('product_slot_quantity')->row();
	}
	/***** PJ[date:-27/04/18] *****/
	public function get_recommended_products($prod_id, $pccat_id)
	{
		$prod_color = $this->get_product_color($prod_id);
		$this->db->select('prod_id as pid, prod_name as pname, prod_price as pprice, prod_image_url as img_url,prod_image_urls,prod_mrp,fk_pcolor_id,fk_pchild_id,fk_pcat_id,fk_psize_ids');
		$this->db->where('fk_pchild_id', $pccat_id);
		$this->db->where('fk_pcolor_id', $prod_color->fk_pcolor_id);
		$this->db->limit(8, 1);
		$this->db->where('status',1);
		$this->db->where('in_stock', 1)->order_by('prod_id', 'RANDOM');
		return $this->db->get('products')->result();
	}
	/***** PJ[date:-27/04/18] *****/
	public function get_product_color($prod_id)
	{
		$this->db->select('fk_pcolor_id');
		//$this->db->join('product_colors pc','pc.color_id=p.fk_pcolor_id');
		$this->db->where('prod_id',$prod_id);
		return $this->db->get('products p')->row();
	}
}
