<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UpdateProfileModel extends CI_Model{
	
	public function update_name(){
		$this->session->set_userdata('fname',$this->input->post('change_fname'));
		$this->session->set_userdata('lname',$this->input->post('change_lname'));
		$user_details = array(
			'first_name' => $this->input->post('change_fname'),
			'last_name' => $this->input->post('change_lname')
		);
		$this->db->where('cust_id',$this->session->userdata('user_id'));
		return $this->db->update('customer', $user_details);
	}
	
	public function update_dob(){
		$user_details = array(
			'dob' => $this->input->post('change_dob'),
		);
		$this->db->where('cust_id',$this->session->userdata('user_id'));
		return $this->db->update('customer', $user_details);
	}
	
	public function update_email(){
		$this->session->set_userdata('email',$this->input->post('change_email'));
		$user_details = array(
			'email' => $this->input->post('change_email'),
		);
		$this->db->where('cust_id',$this->session->userdata('user_id'));
		return $this->db->update('customer', $user_details);
	}
	
	
	public function update_number()
	{
		$this->session->set_userdata('number',$this->input->post('change_number'));
		$user_details = array(
			'mobile' => $this->input->post('change_number'),
		);
		$this->db->where('cust_id',$this->session->userdata('user_id'));
		return $this->db->update('customer', $user_details);
	}
	
	public function update_address()
	{
		$address_id = $this->input->post('addressId');
		$user_details = array(
			'address' => $this->input->post('chAddress'),
			'pincode' => $this->input->post('chPin'),
			'area' => $this->input->post('chArea')
		);
		$this->db->where('address_id',$address_id);
		return $this->db->update('customer_address', $user_details);
	}
	
	public function add_new_address()
    {
        $count_add=$this->get_address_count();
        $address_type =$count_add[0]->address_count ;
        $address_details = array(
           'cust_id' => $this->session->userdata('user_id'),
           'address' => $this->input->post('addAddress'), 
            'city_id' =>$this->input->post('addcity'),
            'state_id' =>$this->input->post('addstate'),
           'pincode' => $this->input->post('addPin'),
           'area' => $this->input->post('addArea'),
            'address_type'=>$address_type+1
       );
        return $this->db->insert('customer_address', $address_details);
    }
	
	/***** written By PJ on 10/04/2018 *****/
	public function update_change_password()
    {
        $password=md5($this->input->post('oldpassword'));
        
        $update_password=array(
        'password'=>md5($this->input->post('password')),
        );
            $this->db->where('cust_id', $this->session->userdata('user_id'));
            $this->db->where('password',$password);
            $this->db->update('customer', $update_password);
            return $this->db->affected_rows();
            //return $this->db->last_query();
    }
	/***** KT[22-april] *****/
    public function get_address_count()
    {
        $cmd="SELECT COUNT(address_id) as address_count FROM customer_address WHERE cust_id=".$this->session->userdata('user_id');
        $query=$this->db->query($cmd);
        return $query->result();
    }
		
}
