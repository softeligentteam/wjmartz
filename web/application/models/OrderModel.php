<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderModel extends CI_Model{
	
	public function set_delivery_address_temp_session()
	{
		$this->session->set_userdata(
			array(
				'delivery_fname'=>$this->input->post('delivery_fname'),
				'delivery_lname'=>$this->input->post('delivery_lname'),
				'delivery_address_id'=>$this->input->post('delivery_address'),
				'timeslots_id'=>$this->input->post('timeslots')
			)
		);
		
		return true;
	}

	public function get_delivery_address_by_id()
	{
		$this->db->select('ca.address_id, ca.address, ca.area, pi.pincode, ct.city, st.state');
		$this->db->join('pincode pi','pi.pincode_id = ca.pincode');
		$this->db->join('city ct','ct.city_id = ca.city_id');
		$this->db->join('state st','st.state_id = ca.state_id');
		$this->db->where('address_id',$this->session->userdata('delivery_address_id'));
		return $this->db->get('customer_address ca')->row_array();
	}
	
	public function get_pincode_from_address($address_id)
	{
		$this->db->select('pincode');
		$this->db->where('address_id',$address_id);
		return $this->db->get('customer_address')->row_array();
	}
	
	public function get_delivery_charges($address_id,$cust_id)
	{
		$pincode_id=$this->get_pincode_from_address($address_id);
		
		$this->db->select('p.pincode,delivery_charges,ca.area as area_name,delivery_day');
		$this->db->from('customer_address ca');
		$this->db->where('cust_id',$cust_id);
		//$this->db->join('area a','a.area_id=p.area_id');
		$this->db->where_in('p.pincode_id',$pincode_id);
		return $this->db->get('pincode p')->row();
	}

	public function get_address_for_order($address_id)
	{
	   $this->db->select('address,area,ca.pincode');
	   $this->db->join('pincode p','p.pincode_id=ca.pincode');
           $this->db->where('ca.address_id',$address_id);
	   return $this->db->get('customer_address ca')->row();
	}
	
	public function insert_temp_order()
	{
		$time=$this->input->post('current_time');
		//var_dump($time);
		$match_time = "18:59:59";
		if($time > $match_time)
		{	
			$shift=0; //after 7pm
		}else{
			$shift=1;  // before 7pm
		}	
		
		if($shift===0)
		{
			$delivery_date=Date('y:m:d', strtotime("+2 days"));
		}else{
			$delivery_date=Date('y:m:d', strtotime("+1 day"));
		}
		
		$payment=$this->input->post('payment');
		$payment_mode=$this->session->set_userdata('pay',$payment);
		
		$cust_id=$this->session->userdata('user_id');
		$fname=$this->input->post('deliveryFname');
		$lname=$this->input->post('deliveryLname');
		$promo_id=(int)$this->input->post('promo_id');
		$address_id=$this->input->post('delivery_address');
		$address_list=$this->get_address_for_order($address_id);
		$address=$address_list->address;
		$area=$address_list->area;
		$pincode_id=$address_list->pincode;
		$time_slot_id=$this->input->post('timeslots');
		$refered_by=$this->input->post('reference_name');
		$instuction=$this->input->post('remark');
		//$delivery_charges=$this->input->post('delivery_charges');
		$productdetails = //$this->input->post('product_details');
		$product_details = $this->cart->contents();//json_decode($productdetails, TRUE);
		//var_dump($product);
		/*$price=$product->product_price;
		$quantity=$product->product_quantity;
		$amount=$price * $quantity;*/
		//$product_type=$this->input->post('product_type');
		
		//$count = count($this->input->post('product_details'));
		//var_dump($count);
		
		$order_data=array(
			'cust_id'=>$cust_id,
			'first_name'=>$fname,
			'last_name'=>$lname,
			'address_id'=>$address_id,
			'address'=>$address,
			'area'=>$area,
			'pincode'=>$pincode_id,
			'delivery_date'=>$delivery_date,
			'time_slot_id'=>$time_slot_id,
			'ord_status_id'=>1,
			//'refered_by'=>$refered_by,
			//'instruction'=>$instuction,
			'promocode_applied'=>$promo_id,
			'order_date'=>date('Y-m-d'),
			'created_on'=>date('Y-m-d H:i:s')
		);
		$this->db->insert('temp_order',$order_data);
		$ref_order_id=$this->db->insert_id();
		
		$order_product_details=array();
			$order_details=array('ref_order_id'=>$ref_order_id);
			//var_dump($product_details);
			foreach($product_details as $product) 
			{
				
				$order_details['product_id']=$product['id'];
				$order_details['price']=$product['price'];
				$order_details['quantity']=$product['qty'];
				$order_details['sgst_amount']=$product['price'] * $product['options']['sgst'][0]/100;
				$order_details['cgst_amount']=$product['price'] * $product['options']['cgst'][0]/100;
				$order_details['amount']=$product['price'] * $product['qty'];
              	$order_details['size_id']=$product['options']['product_size_id'];
				$order_product_details[]=$order_details;
				
			}
		
		$this->db->insert_batch('temp_order_details',$order_product_details);
		$ptypeid = (int)$this->input->post('ptcode');
		if($ptypeid != 5)
		{
					$delivery_charges=$this->input->post('delivery_charges');		
		}
		else{
		$delivery_charges = 0;
		}
		$order_payment_details=array();
		$discount=(int)$this->input->post('discount');
		$payment_details=array(
		'ref_order_id'=>$ref_order_id,
		'payment_datetime'=>date("Y-m-d"),
		'amount'=>0,
		'discount_amount'=>0
		);
		$count=count($product_details);
		foreach($product_details as $product) 
		{
			$sgst=$product['price'] * $product['options']['sgst']/100;
			$cgst=$product['price'] * $product['options']['cgst']/100;
			$ptypeid = (int)$this->input->post('ptcode');
			if($ptypeid != 6)
			{	
			$amount=$product['price'] * $product['qty'];
			$payment_details['amount'] = $payment_details['amount'] + $amount ;
			}else{
			$amount=$this->session->userdata('cartamt');
			$payment_details['amount']= $amount ;	
			}
			$dis_amount=(int)$payment_details['amount'] * (int)$discount / 100;
			$payment_details['discount_amount']=($payment_details['amount']-$dis_amount)+ $delivery_charges;
			$order_payment_details=$payment_details;
		}
		$this->db->insert('temp_payment_details',$order_payment_details);
		$this->session->unset_userdata('cartamt');
	   if($this->db->trans_status()===FALSE){
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
		return $ref_order_id;
        }
	}
	
	public function get_last_order_id()
	{
		$query=$this->db->get('temp_order')->last_row();
		return $ref_order_id=$query->ref_order_id;
	}
	
	public function get_address_id()
	{
		$query=$this->db->get('temp_order')->last_row();
		return $ref_order_id=$query->address_id;
	}
	
	
	
	public function get_customer_order_data()
	{
		$address_id=$this->get_address_id();
		$ref_order_id=$this->get_last_order_id();
		$this->db->select('to.ref_order_id,c.cust_id,to.first_name,to.last_name,email,mobile,to.address,pi.pincode,to.area as area_name,ci.city,s.state,order_date,tp.amount,delivery_charges,pc.promocode,tp.discount_amount,pc.discount');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.cust_id=c.cust_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		//$this->db->join('area ar','ar.area_id=ca.area');
		//$this->db->join('city ci','ci.city_id=ar.city_id');
		//$this->db->join('state s','s.state_id=ci.state_id');
		$this->db->join('temp_payment_details tp','tp.ref_order_id=to.ref_order_id');
		$this->db->join('promocode pc','pc.promo_id=to.promocode_applied');
		$this->db->where('ca.address_id',$address_id);
		$this->db->where('to.ref_order_id',$ref_order_id);
		return $this->db->get('temp_order to ')->row_array();
	}
	
	public function get_product_order_data()
	{
		$ref_order_id=$this->get_last_order_id();
		$this->db->select('prod_name,prod_image_url,price,tod.quantity,tod.amount,size,sgst_amount,cgst_amount');
		$this->db->join('products pr','pr.prod_id=tod.product_id');
		//$this->db->join('package pa','pa.package_id=tod.package_id');
		//$this->db->join('package_addons paa','paa.package_addon_id=tod.addon_id');
		//$this->db->join('personalised_package_products pp','pp.sub_product_id=tod.per_package_id');
		$this->db->join('product_sizes ps','ps.size_id=tod.size_id');
		$this->db->where('tod.ref_order_id',$ref_order_id);
		return $this->db->get('temp_order_details tod')->result();
	}
	
	public function get_reference_data($ref_order_id)
	{
		
		//$payu_data=json_decode($payudata,TRUE);
		
		//$reference_id=$payu_data['udf1'];
		$this->db->trans_begin();
		$this->db->select('discount_amount,to.first_name,c.mobile,to.ref_order_id,c.email,amount');
		$this->db->join('temp_payment_details tp','tp.ref_order_id=to.ref_order_id');
		$this->db->from('customer c');
		$this->db->where('c.cust_id',$this->session->userdata('user_id'));
		$this->db->where('to.ref_order_id',$ref_order_id );
		$order_data = $this->db->get('temp_order to')->row();
		
		if($this->db->trans_status()===FALSE){
			$this->db->trans_rollback();
			return NULL;
		}
		else {
			$this->db->trans_commit();
			return $order_data;
		}
	}
	
	public function get_temp_member_to_create_membership($ref_order_id)
	{
		
		//$payu_data=json_decode($payudata,TRUE);
		
		//$reference_id=$payu_data['udf1'];
		$this->db->trans_begin();
		$this->db->select('to.cust_id,to.first_name,to.last_name,discount_amount,to.address_id,to.address,to.area,to.pincode,to.order_date,to.ord_status_id,to.time_slot_id,to.refered_by,to.instruction,to.promocode_applied,to.delivery_date,c.mobile,to.ref_order_id,c.email,amount');
		$this->db->join('temp_payment_details tp','tp.ref_order_id=to.ref_order_id');
		$this->db->from('customer c');
		//$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->where('to.ref_order_id',$this->input->post('udf1') );
		$order_data = $this->db->get('temp_order to')->row();
		
		if($this->db->trans_status()===FALSE){
			$this->db->trans_rollback();
			return NULL;
		}
		else {
			$this->db->trans_commit();
			return $order_data;
		}
	}
	
	public function get_temp_member_to_create_membership_COD($ref_order_id)
	{
		
		//$payu_data=json_decode($payudata,TRUE);
		
		//$reference_id=$payu_data['udf1'];
		$this->db->trans_begin();
		$this->db->select('to.cust_id,to.first_name,to.last_name,discount_amount,to.address_id,to.address,to.area,to.pincode,to.order_date,to.ord_status_id,to.time_slot_id,to.refered_by,to.instruction,to.promocode_applied,to.delivery_date,c.mobile,to.ref_order_id,c.email,amount');
		$this->db->join('temp_payment_details tp','tp.ref_order_id=to.ref_order_id');
		$this->db->from('customer c');
		//$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->where('to.ref_order_id',$ref_order_id);
		$order_data = $this->db->get('temp_order to')->row();
		
		if($this->db->trans_status()===FALSE){
			$this->db->trans_rollback();
			return NULL;
		}
		else {
			$this->db->trans_commit();
			return $order_data;
		}
	}
	
	public function enroll_customer_with_payment($order_data)
	{
		$this->db->trans_begin();
		$this->db->insert('order', $this->bind_order_data($order_data));
		$order_id = $this->db->insert_id();
		if($this->session->userdata('pay')!=1)
		{	
			$this->db->insert('payment_details',$this->bind_payment_details($order_id,$order_data));
		}else{
			$this->db->insert('payment_details',$this->bind_payment_details_COD($order_id));
		}	
		if($this->db->trans_status()===FALSE){
			$this->db->trans_rollback();
			return 0;
			
			
		}
		else {
			$this->db->trans_commit();
			return $order_id;
			
		}
	}
	
	private function bind_temp_order_id($order_id)
	{
		$orders_data = array(
		'order_id'=> $order_id
		);
		return $orders_data;
	}
	
	private function bind_order_data($order_data)
	{
		$orders_data = array(
				'cust_id' => $order_data->cust_id,
				'first_name'=>$order_data->first_name,
				'last_name'=>$order_data->last_name,
				'address_id' =>$order_data->address_id,
				'address'=>$order_data->address,
				'area'=>$order_data->area,
				'pincode'=>$order_data->pincode,
				'order_date' => $order_data->order_date,
				'ord_status_id'=>$order_data->ord_status_id,
				'time_slot_id' => $order_data->time_slot_id,
				'delivery_date'=>$order_data->delivery_date,
				'refered_by' => $order_data->refered_by,
				'instruction' => $order_data->instruction,
				'promocode_applied'=>$order_data->promocode_applied
		);
		return $orders_data;
	}
	
	public function add_order_id_for_products($order_id)
	{
		$payudata=$this->input->post('payu_data');
		$payu_data=json_decode($payudata,TRUE);
		
		if($this->session->userdata('pay')!=1)
		{	
			$reference_id=$this->input->post('udf1');
		}else{
		  $reference_id=$this->session->userdata('ref_id');
		}  
		
		$product_data=array(
		'order_id'=>$order_id
		);
		
		$this->db->where('ref_order_id',$reference_id);
		$this->db->update('temp_order_details',$product_data);
		return $this->db->affected_rows();
	}
	
	private function bind_payment_details($order_id,$order_data)
	{
	
		
		//$payudata=$this->input->post('payu_data');
		// $payu_data=json_decode($payudata,TRUE);
		// $mihpayid=$payu_data['id'];
		// $main_amount=$payu_data['udf2'];
		// $payment_mode=$payu_data['mode'];
		// $transaction_id=$payu_data['txnid'];
		// $amount=$payu_data['amount'];
		// $payment_datetime=$payu_data['addedon'];
		// $payment_status=$payu_data['status'];
		// $bank_ref_no=$payu_data['bank_ref_no'];
		$order_payment = array(
				
				'order_id' => $order_id,
				'mihpayid'=>$this->input->post('mihpayid'),
				'payment_mode'=>$this->input->post('mode'),
				'transaction_id'=>$this->input->post('txnid'),
				'amount'=>$this->cart->format_number($this->cart->total()),
				'discount_amount'=>$this->input->post('amount'),
				'payment_datetime'=>$this->input->post('addedon'),
				'payment_status'=>$this->input->post('status'),
				'bank_ref_no'=>$this->input->post('bank_ref_num')
		);
		return $order_payment;
	}
	
	private function bind_payment_details_COD($order_id)
	{
		$order_payment = array(
				
				'order_id' => $order_id,
				'mihpayid'=>0,
				'payment_mode'=>'COD',
				'transaction_id'=>0,
				'amount'=>$this->cart->format_number($this->cart->total()),
				'discount_amount'=>$this->input->post('disc_amount'),
				'payment_datetime'=>date('Y-m-d H:i:s'),
				'payment_status'=>'COD'
		);
		return $order_payment;
	}
	
	public function temp_order_payment_delete()
	{
		$payudata=$this->input->post('payu_data');
		$payu_data=json_decode($payudata,TRUE);
		
		$reference_id=$this->input->post('udf1');
		
		if($payu_data !=null||$payu_data !=NULL||$payu_data !=0)
		{	
			$reference_id=$this->input->post('udf1');
		}else{
		 $reference_id=$this->input->post('ref_order_id');
		}  
		 
                $this->db->where('ref_order_id',$reference_id);
                return $this->db->delete('temp_payment_details');
		//$this->db->delete('temp_order');
				
	}

	public function increase_count_for_promo($order_id)
	{
		$this->db->select('promocode_applied');
		$this->db->where('order_id',$order_id);
		$promo_id=$this->db->get('order')->row_array();
		//var_dump($promo_id);
		$data=array(
		'applied_promo'=>'applied_promo' + 1
		);
		
		$this->db->where_in('promo_id',$promo_id);
		return $this->db->update('promocode',$data);		
	}

        public function temp_order_delete()
	{
		$payudata=$this->input->post('payu_data');
		$payu_data=json_decode($payudata,TRUE);
		
		if($payu_data !=null||$payu_data !=NULL||$payu_data !=0)
		{	
			$reference_id=$this->input->post('udf1');
		}else{
		$reference_id=$this->input->post('ref_order_id');
		}  
        $this->db->where('ref_order_id',$reference_id);
             
		$this->db->delete('temp_order');
				
	}
	
	
	public function update_temp_payment()
	{
		$payudata=$this->input->post('payu_data');
		$payu_data=json_decode($payudata,TRUE);
		$mihpayid=$this->input->post('id');
		$payment_mode=$this->input->post('mode');
		$transaction_id=$this->input->post('txnid');
		$amount=$this->input->post('amount');
		$payment_datetime=$this->input->post('addedon');
		$payment_status=$this->input->post('status');
		$reference_id=$this->input->post('udf1');
		$reason=$this->input->post('Error_Message');
		$data1=array('ord_status_id'=>5);
		$data=
		array(
		'mihpayid'=>$this->input->post('id'),
		'payment_mode'=>$this->input->post('mode'),
		'transaction_id'=>$this->input->post('txnid'),
		'discount_amount'=>$this->input->post('amount'),
		'amount'=>$this->cart->format_number($this->cart->total()),
		'payment_datetime'=>$this->input->post('addedon'),
		'payment_status'=>$this->input->post('status'),
		'trans_reason'=>$this->input->post('Error_Message')	
		);
		$this->db->where('ref_order_id',$reference_id);
		$this->db->update('temp_payment_details',$data);
		$this->db->update('temp_order',$data1);
		return $this->db->affected_rows();
	}
	
	
	
	public function order_data($order_id)
	{
		$this->db->select('o.order_id,o.first_name,o.last_name,email,mobile,transaction_id,payment_datetime,payment_status,amount,o.cust_id,p.discount_amount');
		$this->db->join('customer c','c.cust_id=o.cust_id');
		$this->db->join('payment_details p','p.order_id=o.order_id');
		$this->db->where('o.order_id',$order_id);
		return $this->db->get('order o')->row();
	}
	
	public function get_failure_data($reference_id)
	{
		$payudata=$this->input->post('payu_data');
		$payu_data=json_decode($payudata,TRUE);
		
		
		
		$this->db->select('to.ref_order_id,to.first_name,to.last_name,email,mobile,transaction_id,payment_datetime,payment_status,trans_reason,amount,tp.discount_amount,tp.amount,to.cust_id');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('temp_payment_details tp','tp.ref_order_id=to.ref_order_id');
		$this->db->where('to.ref_order_id',$reference_id);
		return $this->db->get('temp_order to')->row_array();
		
	}
	
	public function get_orderwise_products($order_id)
	{
		$this->db->select('prod_name,tod.product_id,tod.price,tod.quantity,tod.amount,prod_image_url,size,cgst_amount,sgst_amount,refund_status');
		$this->db->join('products p','p.prod_id=tod.product_id');
		//$this->db->join('package pa','pa.package_id=tod.package_id');
		//$this->db->join('personalised_package_products pp','pp.sub_product_id=tod.per_package_id');
		$this->db->join('product_sizes ps','ps.size_id=tod.size_id');
		$this->db->where('tod.order_id',$order_id);
		return $this->db->get('temp_order_details tod')->result();
	}
	
	public function get_order_data_for_mail($order_id)
	{
		$this->db->select('o.order_id,o.first_name,o.last_name,email,order_date,delivery_date,amount,mobile,discount_amount');
		$this->db->join('customer c','c.cust_id=o.cust_id');
		$this->db->join('payment_details p','p.order_id=o.order_id');
		$this->db->where('o.order_id',$order_id);
		return $this->db->get('order o')->row();
	}
	
	public function get_order_data_for_order_fail_mail()
	{
		$payudata=$this->input->post('payu_data');
		$payu_data=json_decode($payudata,TRUE);
		
		if($this->session->userdata('pay')!=1)
		{	
			$reference_id=$this->input->post('udf1');
		}else{
		  $reference_id=$this->session->userdata('ref_id');
		}  
		
		$this->db->select('o.ref_order_id,o.first_name,o.last_name,email,order_date,delivery_date,amount,p.discount_amount,mobile');
		$this->db->join('customer c','c.cust_id=o.cust_id');
		$this->db->join('temp_payment_details p','p.ref_order_id=o.ref_order_id');
		$this->db->where('o.ref_order_id',$reference_id);
		return $this->db->get('temp_order o')->row();
	}
	
	public function get_stock_product_id($order_id)
	{
		$this->db->select('product_id');
		$this->db->where('order_id',$order_id);
		return $this->db->get('temp_order_details')->result();
	}

	public function get_stock_size_id($order_id)
	{
		$this->db->select('size_id');
		$this->db->where('order_id',$order_id);
		return $this->db->get('temp_order_details')->result();
	}
	
	public function get_order_product_id($order_id)
	{
		$order_slot=$this->get_stock_product_id($order_id);
		$product_id=array();
		if(sizeof($order_slot)){
		foreach($order_slot as $order)
		{
			$product_id[]=$order->product_id;
		}
			}
    	else{
    		$product_id[0] = 0;
    	}
		return $product_id;
		
	}

	public function get_order_size_id($order_id)
	{
		$order_slot=$this->get_stock_size_id($order_id);
		$size_id=array();
		if(sizeof($order_slot)){
		foreach($order_slot as $order)
		{
			$size_id[]=$order->size_id;
		}
			}
    	else{
    		$size_id[0] = 0;
    	}
		return $size_id;
		
	}
	
	public function get_order_product_quantity($order_id)
	{
		$product_id=$this->get_order_product_id($order_id);
		$size_id=$this->get_order_size_id($order_id);
		$this->db->select('quantity');
		$this->db->where('order_id',$order_id);
		$this->db->where_in('product_id',$product_id);
		$this->db->where_in('size_id',$size_id);
		return $this->db->get('temp_order_details')->result();
	}
	
	public function get_product_total_quantity($order_id)
	{
		$product_id=$this->get_order_product_id($order_id);
		$size_id=$this->get_order_size_id($order_id);
		$slot_id=$this->get_prod_slot_id($order_id);
		$new=implode(',',$slot_id);	
		$this->db->select('quantity');
		$this->db->where_in('slot_id',$slot_id);
		//$this->db->where_in('size_id',$size_id);
		return $this->db->get('product_slot_quantity')->result();
	}
	
	public function less_product_quantity($order_id)
	{
		$stock=array();
		$quantity=array();
		$total_quantity=array();
		
		$product_id=$this->get_order_product_id($order_id);
		$size_ids=$this->get_order_size_id($order_id);
		$order_quantity=$this->get_order_product_quantity($order_id);
		$product_quantity=$this->get_product_total_quantity($order_id);
		//var_dump($product_id);
		//var_dump($size_ids);
		//var_dump($order_quantity);
		//var_dump($product_quantity);
		if(sizeof($product_quantity)){
            foreach($product_quantity as $pqty)
            {
            if(sizeof($order_quantity)){
            foreach($order_quantity as $oqty)
            {
                $quantity[]=$oqty->quantity;
                $total_quantity[]=$pqty->quantity;
				$stock[] = (int)$pqty->quantity-(int)$oqty->quantity;
            }
        }
		}
		}
		return $stock;
		
	}

	public function get_product_stock($total_quantity,$quantity)
    {
    
    $size = sizeof($total_quantity);
        
        for($i=0; $i<count($total_quantity); $i++){
                
                $stock[$i] = (int)$total_quantity[$i] - (int)$quantity[$i];
                //var_dump($tqty[$i]);
                
        }
         return $stock;
    }
	public function do_out_of_stock($order_id){
		$product_id=$this->get_order_product_id($order_id);
		$qty_sum=$this->get_total_quantity_of_each_product($product_id);
		$size = sizeof($qty_sum);
		for($i=0; $i<$size; $i++){
			if($qty_sum[$i]<=0){
				$this->out_of_stock($product_id);
			}else{
				$this->in_stock($product_id);
			}
		}
	}
	public function get_total_quantity_of_each_product($product_id)
	{
			// foreach($product_id as $pid)
		// {
			// $this->db->select('quantity');
			// $this->db->where('product_id',$pid);
			// $prod_qty= $this->db->get('product_slot_quantity')->result_array();
		// }	
		// $prod_array=array();
		// foreach($product_id as $prod){
			// $prod_array[]= $prod;
		// } 
		// $this->db->select('product_id,quantity');
		// $this->db->where_in('product_id',$prod_array);
		// return $prod_qty= $this->db->get('product_slot_quantity')->result_array();
		$qty = array();
		$remQty = array();
		foreach($product_id as $prod){
			$qty[]=$this->get_its_qty($prod);
			$remQty[]=$this->get_sum($prod);
		}
		
		return $remQty;
	}
	
	private function get_its_qty($product_id){
		$this->db->select('quantity');
		$this->db->where('product_id',$product_id);
		return $this->db->get('product_slot_quantity')->result_array();
	}
	private function get_sum($product_id)
	{
		$this->db->select('quantity');
		$this->db->where('product_id',$product_id);
		$prod_qty = $this->db->get('product_slot_quantity')->result_array();
		$size = sizeof($prod_qty);
		$sum=0;
		foreach($prod_qty as $qty)
		{
			$sum=$sum+$qty['quantity'];
		}
		return $sum;
	}
	public function out_of_stock($product_id)
	{
		foreach($product_id as $pid)
		{
		$change_stock=array(
			'in_stock'=>0
		);
		$this->db->where('prod_id',$pid);
		$this->db->update('products',$change_stock);
		}
	}
	public function in_stock($product_id)
	{
		foreach($product_id as $pid)
		{
		$change_stock=array(
			'in_stock'=>1
		);
		$this->db->where('prod_id',$pid);
		$this->db->update('products',$change_stock);
		}
	}
	public function get_slot_ids($order_id)
	{
		$product_ids=$this->get_order_product_id($order_id);
		$size_ids=$this->get_order_size_id($order_id);
		$new=implode(',',$product_ids);
		//$new1=implode(',',$size_ids);
		$this->db->select('slot_id');
		$this->db->where_in('product_id',$new);
		$this->db->where_in('size_id',$size_ids);
		
		return $this->db->get('product_slot_quantity')->result();
	}
	
	public function get_prod_slot_id($order_id)
	{
		$order_slot=$this->get_slot_ids($order_id);
		$slot_id=array();
		if(sizeof($order_slot)){
		foreach($order_slot as $order)
		{
			$slot_id[]=$order->slot_id;
		}
			}
    	else{
    		$slot_id[0] = 0;
    	}
		return $slot_id;
		
	}
	public function change_stock_quantity_of_product($order_id)
	{
		$query=("update product_slot_quantity psq join temp_order_details od on od.product_id = psq.product_id AND od.size_id = psq.size_id
        join `order` o on o.order_id = od.order_id set psq.quantity = (psq.quantity - od.quantity)
		where o.order_id=").$order_id;
		return $this->db->query($query);
	}
	public function cancel_user_order($order_id)
	{
		$cust_id=$this->input->post('customer_id');
		$data=array(
			'ord_status_id'=>4,
			'modified_on_cust'=>date('Y-m-d H:i:s'),
			'modified_by_cust'=>$cust_id
		);
		$this->db->where('order_id',$order_id);
		$this->db->update('order',$data);
		return $this->db->affected_rows();
	}
	public function get_order_details($order_id)
	{
		$this->db->select('address,area,pincode,mihpayid,order_date,delivery_date,amount,discount_amount');
		$this->db->join('payment_details pd','pd.order_id=o.order_id');
		$this->db->where('o.order_id',$order_id);
		return $this->db->get('order o')->row();
	}
	public function get_orderwise_products_cancel($order_id)
	{
		$this->db->select('prod_name,tod.product_id,tod.price,tod.quantity,tod.amount,size,prod_image_url,tod.sgst_amount,tod.cgst_amount');
		$this->db->join('products p','p.prod_id=tod.product_id');
		$this->db->join('product_sizes ps','ps.size_id=tod.size_id');
		$this->db->where('tod.order_id',$order_id);
		return $this->db->get('temp_order_details tod')->result();
	}
	public function get_order_data_for_mail_cancel($order_id)
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,email,mobile,ca.address,ca.area as area_name,p.pincode,p.delivery_charges,ci.city,s.state,transaction_id,payment_datetime,payment_status,tp.amount,tp.amount as prod_amt,instruction,refered_by,order_date,promocode_applied,pc.promocode,pc.discount,tp.discount_amount');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.cust_id=c.cust_id');
		$this->db->join('pincode p','p.pincode_id=ca.pincode');
		//$this->db->join('area ar','ar.area_id=ca.area');
		//$this->db->join('city ci','ci.city_id=ar.city_id');
		//$this->db->join('state s','s.state_id=ci.state_id');
		$this->db->join('promocode pc','pc.promo_id=to.promocode_applied');
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		$this->db->where('to.order_id',$order_id);
		return $this->db->get('order to')->row();
	}
	
	 public function get_orderwise_products_refund($order_id, $proddata)
	{
		$this->db->select('prod_name,tod.product_id,tod.price,tod.quantity,tod.amount,size,prod_image_url,tod.sgst_amount,tod.cgst_amount,tod.size_id');
		$this->db->join('products p','p.prod_id=tod.product_id');
		$this->db->join('product_sizes ps','ps.size_id=tod.size_id');
		$this->db->where('tod.order_id',$order_id);
		$this->db->where('tod.product_id',$proddata);
		return $this->db->get('temp_order_details tod')->result();
	}
	
	
	public function get_refund_prod_details($order_id)
	{
		$prod_data = $this->input->post('preturn[]');
		$status = false;
		foreach($prod_data as $proddata)
		{
			if($this->refund_order($proddata, $order_id) == TRUE)
			{
				$status = TRUE;
			}
			else{
				return FALSE;
			}
		}
		return $status;
		//var_dump($value);
	}
	public function refund_order($proddata, $order_id)
	{
		$order_data=$this->get_order_details($order_id);
		//$productdetails = $this->input->post('product_details');
		$product_details = $this->get_orderwise_products_refund($order_id, $proddata);
		echo json_encode($product_details); 
		$refund_data=array(
		'order_id'=>$order_id,
		'cust_id'=>$this->session->userdata('user_id'),//$this->input->post('cust_id'),
		'address'=>$order_data->address,
		'area'=>$order_data->area,
		'pincode'=>$order_data->pincode,
		'mihpayid'=>$order_data->mihpayid,
		'order_date'=>$order_data->order_date,
		'delivery_date'=>$order_data->delivery_date,
		'refund_date'=>date('Y-m-d'),
		'amount'=>$order_data->amount,
		'discount_amount'=>$order_data->discount_amount,
		'return_reason'=>'static reason'//$this->input->post('return_reason')
		);
		$this->db->insert('refund_order',$refund_data);
		$refund_id=$this->db->insert_id();

		$order_product_details=array();
			$order_details=array('refund_id'=>$refund_id);
			foreach($product_details as $product) 
			{
				$order_details['product_id']=$product->product_id;
				$order_details['price']=$product->price;
				$order_details['quantity']=$product->quantity;
				$order_details['sgst_amount']=$product->sgst_amount;
				$order_details['cgst_amount']=$product->cgst_amount;
				$order_details['amount']=$product->price * $product->quantity;
						$order_details['size_id']=$product->size_id;
				$order_product_details[]=$order_details;
			}
		$this->db->insert_batch('refund_order_details',$order_product_details);
		$data=array(
		'refund_id'=>$refund_id
		);
		$this->db->where('order_id',$order_id);
		$this->db->update('order',$data);
	   if($this->db->trans_status()===FALSE){
			$this->db->trans_rollback();
			return FALSE;
		}
		else {
			$this->db->trans_commit();
		return TRUE;
		}
	}
	public function get_last_refund_order_id()
	{
		$query=$this->db->get('refund_order')->last_row();
		return $refund_id=$query->refund_id;
	}
	public function get_refund_order_prod_data()
	{
		$refund_id=$this->get_last_refund_order_id();
		$this->db->select('product_id,prod_name,prod_price,prod_image_url,rod.quantity,rod.amount,ps.size');
		$this->db->join('refund_order_details rod','rod.refund_id=ro.refund_id');
		$this->db->join('products p','p.prod_id=rod.product_id');
		$this->db->join('product_sizes ps','ps.size_id=rod.size_id');
		$this->db->where('ro.refund_id',$refund_id);
		return $this->db->get('refund_order ro')->result();
	}
	
	public function get_refund_order_data()
	{
		$refund_id=$this->get_last_refund_order_id();
		$this->db->select('ro.refund_id,ro.cust_id,o.first_name,o.last_name,ro.order_id,ro.address,ro.area,s.state,ci.city,p.pincode,ro.amount,ro.discount_amount');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('customer c','c.cust_id=ro.cust_id');
		$this->db->join('pincode p','p.pincode_id=ro.pincode');
		$this->db->join('order o','o.order_id=ro.order_id');
		$this->db->where('ro.refund_id',$refund_id);
		return $this->db->get('refund_order ro')->result();
	}
	public function get_refund_products_details()
	{
		$refund_id=$this->get_last_refund_order_id();
		$this->db->select('prod_name,rod.product_id,rod.price,rod.quantity,rod.amount,size,prod_image_url');
		$this->db->join('products p','p.prod_id=rod.product_id');
		$this->db->join('product_sizes ps','ps.size_id=rod.size_id');
		$this->db->where('rod.refund_id',$refund_id);
		return $this->db->get('refund_order_details rod')->result();
	} 
	
	public function get_refund_details()
	{
		$refund_id=$this->get_last_refund_order_id();
		$this->db->select('rod.refund_id,ro.cust_id,ro.order_id,o.first_name,o.last_name,email,mobile,ro.order_date');
		$this->db->join('refund_order ro','ro.refund_id=rod.refund_id');
		$this->db->join('customer c','c.cust_id=ro.cust_id');
		$this->db->join('order o','o.order_id=ro.order_id');
		$this->db->where('rod.refund_id',$refund_id);
		return $this->db->get('refund_order_details rod')->row();
	}
}
