<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sms_gateway
{

	/*********- Sms for otp -***********/
	public function genrate_otp($mobile,$otp)
	{
		$api_url = "http://www.mysmsshop.in/http-api.php?username=wjmartz&password=wjmartz@123&senderid=wjmrtz&route=1&number=".$mobile."&message=Hello%20Your%20OTP%20is%20".$otp;
		$response = file_get_contents($api_url);
		return $response;
	}
	
	/*********- Sms for Order Cancelled -***********/
	public function send_sms_for_order_cancel($mobile,$order_id,$first_name)
	{
		$api_url ="http://www.mysmsshop.in/http-api.php?username=wjmartz&password=wjmartz@123&senderid=wjmrtz&route=1&number=".$mobile."&message=Dear%20".$first_name.",%20Your%20Order%20is%20sucessfully%20cancelled%20with%20Order%20id%20".$order_id.".%20In%20case%20of%20any%20doubt%20or%20query,%20you%20can%20write%20to%20us%20on%20support@wjmartz.com";
		$response = file_get_contents($api_url);
		return $response;
	}
	
	
	/*********- Sms for Order Placed -***********/
	public function send_sms_for_order_place($mobile,$order_id,$first_name)
	{
		$api_url ="http://www.mysmsshop.in/http-api.php?username=wjmartz&password=wjmartz@123&senderid=wjmrtz&route=1&number=".$mobile."&message=Dear%20".$first_name.",%20Your%20order%20is%20sucessfully%20placed%20with%20order%20id%20".$order_id.".%20In%20case%20of%20any%20doubt%20or%20query,%20you%20can%20write%20to%20us%20on%20support@wjmartz.com";
		$response = file_get_contents($api_url);
		return $response;
	}
	
	/*********- Sms for registration -***********/
	public function send_sms_for_welcome($mobile,$first_name)
	{
		$api_url ="http://www.mysmsshop.in/http-api.php?username=wjmartz&password=wjmartz@123&senderid=wjmrtz&route=1&number=".$mobile."&message=Welcome%20".$first_name.",%20Thank%20you%20for%20choosing%20us.%20We%20are%20pleased%20to%20welcome%20you%20as%20a%20new%20member%20of%20WJMARTZ.%20We%20feel%20honored%20that%20you%20have%20chosen%20us%20to%20fill%20your%20product%20needs,%20and%20we%20are%20eager%20to%20be%20of%20service.%20In%20case%20of%20any%20doubt%20or%20query,%20you%20can%20write%20to%20us%20on%20support@wjmartz.com";
		$response = file_get_contents($api_url);
		return $response;
	}

	/*********- Sms for new user coupon code -***********/
	public function send_sms_for_promocode($mobile,$first_name,$discount,$promocode)
	{
		$api_url ="http://www.mysmsshop.in/http-api.php?username=wjmartz&password=wjmartz@123&senderid=wjmrtz&route=1&number=".$mobile."&message=Welcome%20".$first_name.",%20Thank%20you%20for%20choosing%20WJMARTZ.%20We%20are%20pleased%20to%20have%20you,%20and%20as%20a%20new%20member%20of%20WJMARTZ.%20We%20would%20like%20to%20compliment%20you%20with%20an%20additional%20".$discount."%20discount%20offer%20on%20your%20first%20purchase.%20Please%20use%20the%20code%20".$promocode."%20to%20avail%20the%20offer.%20In%20case%20of%20any%20doubt%20or%20query,%20you%20can%20write%20to%20us%20on%20support@wjmartz.com";
		$response = file_get_contents($api_url);
		return $response;
	}
	
	/*********- Sms for Return request sent -***********/
	public function send_sms_for_order_refund($mobile,$refund_id,$first_name)
	{	
		$api_url ="http://www.mysmsshop.in/http-api.php?username=wjmartz&password=wjmartz@123&senderid=wjmrtz&route=1&number=".$mobile."&message=Dear%20".$first_name.",%20Your%20request%20for%20return%20order%20is%20accepted%20with%20refund%20id%20".$refund_id.".%20In%20case%20of%20any%20doubt%20or%20query,%20you%20can%20write%20to%20us%20on%20support@wjmartz.com";
		$response = file_get_contents($api_url);
		return $response;
	}
	
	public function generate_otp_forgot_password($mobile,$otp)
	{
		$api_url ='http://www.mysmsshop.in/http-api.php?username=wjmartz&password=wjmartz@123&senderid=wjmrtz&route=1&number='.$mobile.'&message=OTP%20For%20Forgot%20Password%20is%20'.$otp;
		$response = file_get_contents($api_url);
		return $response;
	}
	
}