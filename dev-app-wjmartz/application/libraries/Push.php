<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Push {
//notification title
    private $title;

    //notification message 
    private $message;

    //notification image url 
    private $image;

    //initializing values in this constructor
    function push($title, $message, $image) {
         $this->title = $title;
         $this->message = $message; 
         $this->image = $image; 
    }
    //getting the push notification
    public function getPush($title, $message, $image) {
        $res = array();
        $res['data']['title'] = $title;
        $res['data']['message'] = $message;
        $res['data']['image'] = $image;
        return $res;
    }
}