<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PushController extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();

		$this->load->model('Pushmodel');

		
    	}	
		
	 // this function will redirect to book service page
	 function index()
	 {
		$this->subscribe();
	 }

	 // this function to load service book page
	 function subscribe()
	 {
		 
		 $this->load->view('pages/site_subscriber', $data);
	 }

	public function register_device()
	{
		$status = $this->Pushmodel->insert_pushdata();	
		echo json_encode($status);
	}
	
	  
	    
    public function get_all_device_details()
    {
    	$data['device_details'] = $this->Pushmodel->get_all_device_details();
    	echo json_encode($data['device_details']);
    }
  
        public function getPush($title, $message, $image) {
        $res = array();
        $res['data']['title'] = $title;
        $res['data']['message'] = $message;
         $res['data']['image'] = $image;
         $res['data']['extra_data'] = '0';
        return $res;
      }
        public function send_notification()
	{
	   //importing required files 
		$response = array(); 
		$message1=$this->input->post('message');
		$message=json_encode($message1);
		$title1=$this->input->post('title');
		$title=json_encode($title1);
		$email=$this->input->post('email');
		$image1=$this->input->post('image');
		$image=json_encode($image1);
		
		
		$data['devices'] = $this->Pushmodel->get_all_device_data();
		
		
		$response = array(); 
		 //getting the push from push object
		 $mPushNotification = $this->getPush($title, $message, $image); 
		 $devicetoken= array(implode('',$data['devices']));
		
		//$response= $this->send($data['devices'], $mPushNotification);
		$result =$this->send($data['devices'], $mPushNotification);
				$response['error'] = false;
				$response['message'] = $result;
				echo json_encode($response);
		 //sending push notification and displaying result 
		 
		//echo json_encode($response);
       }
       
       public function send($registration_ids, $message) {
        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message
        );
        return $this->sendPushNotification($fields);
    }
    
    /*
    * This function will make the actuall curl request to firebase server
    * and then the message is sent 
    */
    private function sendPushNotification($fields) {
         
        //importing the constant files
        
        //firebase server url to send the curl request
        $url = 'https://fcm.googleapis.com/fcm/send';
 
        //building headers for the request
        $headers = array(
            'Authorization: key=' .FIREBASE_API_KEY,
            'Content-Type: application/json'
        );

        //Initializing curl to open a connection
        $ch = curl_init();
 
        //Setting the curl url
        curl_setopt($ch, CURLOPT_URL, $url);
        
        //setting the method as post
        curl_setopt($ch, CURLOPT_POST, true);

        //adding headers 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        //disabling ssl support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        //adding the fields in json format 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
  
        //finally executing the curl request 
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        //Now close the connection
        curl_close($ch);
 
        //and return the result 
        return $result;
    }
            public function getPush_single($title, $message, $image) {
        $res = array();
        $res['data']['title'] = $title;
        $res['data']['message'] = $message;
         $res['data']['image'] = $image;
         $res['data']['extra_data'] = '1';
        return $res;
      }
  //getting all tokens to send push to all devices
	public function sendSinglePush()
	{
	
		$response = array(); 
		$message1=$this->input->post('message');
		$message=json_encode($message1);
		$title1=$this->input->post('title');
		$title=json_encode($title1);
		$email=$this->input->post('email');
		$image1=$this->input->post('image');
		$image=json_encode($image1);
		
		$mPushNotification = $this->getPush_single($title, $message, $image); 
		
		$token=$this->getTokenByEmail($email);
		
		
		if($this->input->post())
		{	
			//hecking the required params 
			/*if(isset($_POST['title']) and isset($_POST['message']) and isset($_POST['email'])){*/

				
				/*$this->load->library('Push');
				

				//getting the push from push object
				$this->load->library('push');
				$mPushNotification = $this->push->getPush(); 
				var_dump($mPushNotification);*/

				//getting the token from database object 
				$devicetoken1 =$this->getTokenByEmail($email);
				$devicetoken= array(implode('',$devicetoken1));
				

				//creating firebase class object 
				//$firebase = new Firebase(); 

				//sending push notification and displaying result
				//$this->load->library('firebase');
				$result =$this->send($devicetoken, $mPushNotification);
				$response['error'] = false;
				$response['message'] = $result;
				echo json_encode($response);
			/*}else{
				$response['error']=true;
				$response['message']='Parameters missing';*/
			
		}else{
			$response['error']=true;
			$response['message']='Invalid request';
		}

		echo json_encode($response);
	}
	
	public function getTokenByEmail($email)
	{
		$this->db->select('fcm_device_id');
		$this->db->where('email',$email);
		$rows=$this->db->get('pn_test')->result();
	        $tokens= array();
	        if(count($rows)>0)
	        {
	            foreach($rows as $row)
	            {
	                $tokens[] = $row->fcm_device_id;
	            }
	        }
			return $tokens;
		}
	
}