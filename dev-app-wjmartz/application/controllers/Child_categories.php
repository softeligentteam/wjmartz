<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Child_categories extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Ddlmodel');
		$this->load->model('Childcategorymodel');
	}	
	 
	public function view_all()
	{
		$data['parent_cat_list']=$this->Ddlmodel->get_parent_category_list();
		$this->load->view('layouts/header');
		$this->load->view('pages/child_categories',$data);
		$this->load->view('layouts/footer');
	}
	
	public function add_child_category()
	{
		if($this->input->post())
		{
			$this->Childcategorymodel->insert_child_categories();
			$message = 'Child Category is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('child_categories/view_all');
		}else{
			$message = 'Child category insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('child_categoriess/view_all');
		}
			
	}
	
	public function delete_child_category()
	{
		$child_cat_id=$this->uri->segment(3,0);
		if($this->Childcategorymodel->del_child_category($child_cat_id))
		{	
			
			$message = 'Child Category is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('child_categories/view_all');
		}else{
			$message = 'Child Category is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('child_categories/view_all');
		}
	}
	 
	
}
