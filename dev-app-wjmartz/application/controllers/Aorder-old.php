<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aorder extends CI_Controller 
{
	 public function __construct() 
	 {
        parent::__construct();
		$this->load->model('Aordermodel');
		$this->load->model('Ddlmodel');
	 }

	 public function get_time_slots()
	 {
		 $data['time_slot']=$this->Aordermodel->get_time_slot();
		 echo json_encode($data);
	 }
	 
	  public function get_reference_order_data()
	  {
		    if($this->input->post())
			{	
				$delivery_charges=$this->input->post('delivery_charges');
			if($this->Aordermodel->insert_temp_order())
			{
				$data['customer_data']=$this->Aordermodel->get_customer_order_data();
				$data['product_data']=$this->Aordermodel->get_product_order_data();
				$data['payment_mode']=$this->session->userdata('cod');
				$data['delivery_charges'] = $delivery_charges;
				$data['imageaccesspath']=IMAGEACCESSPATH;
				echo json_encode($data);
				
			}
			else
			 {
				echo 0;
			 } 
//echo json_encode($_POST);
			}else{
				echo 0;
			}	
	  }
	
	public function payment_success()
	{
		$header['title'] = 'Payment Success|Denstagenie';
		$this->load->view('layouts/header', $header);
		$this->load->view('pages/payment_success');
		$this->load->view('layouts/footer');
	}
	
	/*************** Plan Purchase Fail URL ****************/
	public function payment_fail()
	{
		$header['title'] = 'Payment Fail|Denstagenie';
		$this->load->view('layouts/header', $header);
		$this->load->view('pages/payment_fail');
		$this->load->view('layouts/footer');
		
	}
	
	public function get_order_data_success()
	{
		if($this->input->post())
		{	
		$order_data=$this->Aordermodel->get_reference_order_data();
		//var_dump($order_data);
		$order_id=$this->Aordermodel->enroll_customer_with_payment($order_data);
		
		if($order_id==0)
		{
			$this->Aordermodel->update_temp_payment();
			$this->send_email_for_order_place_fail();
			
		}else{
			$this->Aordermodel->add_order_id_for_products($order_id);
           		 $this->Aordermodel->temp_order_payment_delete();
				$this->Aordermodel->temp_order_delete();
				$this->Aordermodel->change_stock_quantity_of_product($order_id);
				$this->Aordermodel->do_out_of_stock($order_id);
				$this->Aordermodel->increase_count_for_promo($order_id);
				$this->send_email_for_order_place($order_id);
		}
		$data['order_data']=$this->Aordermodel->order_data($order_id);
		$this->load->library('sms_gateway');
		$this->sms_gateway->send_sms_for_order_place($data['order_data']->mobile,$order_id,$data['order_data']->first_name);
		echo json_encode($data);
		}
	}
	
	public function get_order_data_failure()
	{
		$this->Aordermodel->update_temp_payment();
		$data['failure_data']=$this->Aordermodel->get_failure_data();
		echo json_encode($data);
	}
	
	public function order_history()
	{
		$cust_id=$this->uri->segment(3,0);
		$data['order_history_successs']=$this->Aordermodel->get_success_order_history($cust_id);
		$data['order_history_failure']=$this->Aordermodel->get_failure_order_history($cust_id);
		echo json_encode($data);
	}
	
	public function order_history_product_details()
	{
		$order_id=$this->uri->segment(3,0);
		$payment_status=$this->uri->segment(4,0);
		if($payment_status=="success" || $payment_status=="COD")
		{	
			$data['order_history_products']=$this->Aordermodel->get_success_order_history_products($order_id);
			$data['imageaccesspath']=IMAGEACCESSPATH;
			echo json_encode($data);
		}else{
			$data['order_history_products']=$this->Aordermodel->get_failure_order_history_products($order_id);
			$data['imageaccesspath']=IMAGEACCESSPATH;
			echo json_encode($data);
		}	
	}
	
	public function order_history_details()
	{
		$order_id=$this->uri->segment(3,0);
		$payment_status=$this->uri->segment(4,0);
		if($payment_status=="success" || $payment_status=="COD")
		{	
			$data['order_history_details']=$this->Aordermodel->get_success_order_history_details($order_id);
			$delivery_charges=$data['order_history_details'][0]->delivery_charges;
			$data['imageaccesspath']=IMAGEACCESSPATH;
			if($data['order_history_details'][0]->promo_type_id==5)
			{
			 	$data['delivery_charges'] =0;
			}else{
			 $data['delivery_charges'] = $delivery_charges;
			}
			$data['order_status']=$this->Ddlmodel->get_order_status();
			echo json_encode($data);
		}else{
			$data['order_history_details']=$this->Aordermodel->get_failure_order_history_details($order_id);
			$delivery_charges=$data['order_history_details'][0]->delivery_charges;
			if($data['order_history_details'][0]->promo_type_id==5)
			{
			 	$data['delivery_charges'] =0;
			}else{
			 $data['delivery_charges'] = $delivery_charges;
			}
			$data['imageaccesspath']=IMAGEACCESSPATH;
			$data['order_status']=$this->Ddlmodel->get_order_status();
			echo json_encode($data);
		}	
	}
	
	private function send_email_for_order_place($order_id)
	{
		$order_data['order']=$this->Aordermodel->get_order_data_for_mail($order_id);
		$order_data['order_product']=$this->Aordermodel->get_orderwise_products($order_id);
		$message=$this->load->view('email/order_placed',$order_data,TRUE);
		$this->load->library('email_manager');
		$this->email_manager->send_order_place_email($order_data['order']->email,$message);
	}
	
	private function send_email_for_order_place_fail()
	{
		$order_data['order']=$this->Aordermodel->get_order_data_for_order_fail_mail;
		$message=$this->load->view('email/order_fail',$order_data,TRUE);
		$this->load->library('email_manager');
		$this->email_manager->send_order_place_fail_email($order_data['order']->email,$message);
	}
	
	public function get_stock_details()
	{
		if($this->input->post())
		{
			$product_details = json_decode($this->input->post('product_details'), TRUE);
			$returnarray = array();
			$idarray = array();
			$sizearray=array();
			if(is_array($product_details))
			{	$i=0;
				foreach($product_details["product_details"] as $product)
				{
					
					
					$this->db->select('slot_id');
					$this->db->where('size_id',$product['product_size_id']);
					$this->db->where('product_id',$product['product_id']);
					$slot_id=$this->db->get('product_slot_quantity')->row();
					$product['product_slot_id']=$slot_id;
					
					$prod[$i]['product_size_id'] = $product['product_size_id'];
					$prod[$i]['product_id'] = $product['product_id'];
					$prod[$i]['product_quantity'] = $product['product_quantity'];
					$prod[$i]['product_slot_id'] = $slot_id->slot_id;

					
					if($returnarray==NULL)
					{
						$returnarray[$prod[$i]['product_slot_id']]= $prod[$i]['product_quantity'];
						$idarray[] = $product['product_id'];
						$sizearray[]=$product['product_size_id'];
					}
					else if(!isset($returnarray[$prod[$i]['product_slot_id']]))	
					{
						$returnarray[$prod[$i]['product_slot_id']]= $prod[$i]['product_quantity'];
						$idarray[] = $product['product_id'];
						$sizearray[]=$product['product_size_id'];
					}
					else
					{
						//$returnarray['size_id']=$sizearray;
						$returnarray[$prod[$i]['product_slot_id']] = $prod[$i]['product_quantity'] + $returnarray[$prod[$i]['product_slot_id']];
					}
					$i++;
				}
			}
			$data['stock_details'] = $this->Aordermodel->get_stock_status_of_product($returnarray,$idarray,$sizearray);
			$data['array']=$prod;
			echo json_encode($data);
		}
		else
		{
			$data['stock_details'] = NULL;
			echo json_encode($data);
		}	
	}
	
	
	public function get_pincodewise_delivery_charges()
	{
		if($this->input->post())
		{	
			$address_id=$this->input->post('address_id');
			$cust_id=$this->input->post('cust_id');
			//$pincode_id=$this->Aordermodel->get_pincode_from_address($address_id);
			//var_dump($pincode_id);
			$data['delivery_charges']=$this->Aordermodel->get_delivery_charges($address_id,$cust_id);
			echo json_encode($data);
		}else{
			$data['delivery_charges'] = NULL;
			echo json_encode($data);
		}	
	}
	
	public function cancel_order()
	{
		$order_id=$this->input->post('order_id');
		$order_data['order']=$this->Aordermodel->get_order_data_for_mail($order_id);
		if($this->Aordermodel->cancel_user_order($order_id)>0)
		{
			$this->send_email_for_order_cancel($order_id);
			$this->load->library('sms_gateway');
			$this->sms_gateway->send_sms_for_order_cancel($order_data['order']->mobile,$order_id,$order_data['order']->first_name);
			echo "1";
		}else{
			echo "0";
		}	
	}
	
	private function send_email_for_order_cancel($order_id)
	{
		$order_data['order']=$this->Aordermodel->get_order_data_for_mail_cancel($order_id);
		$order_data['order_product']=$this->Aordermodel->get_orderwise_products_cancel($order_id);
		$message=$this->load->view('email/order_cancel',$order_data,TRUE);
		$this->load->library('email_manager');
		$this->email_manager->send_order_cancel_email($order_data['order']->email,$message);
	}
	

	  public function get_refund_order_data()
	  {
		    if($this->input->post())
			{	
			if($this->Aordermodel->refund_order())
			{
				$data['refund_order_data']=$this->Aordermodel->get_refund_order_data();
				$data['refund_prod_data']=$this->Aordermodel->get_refund_order_prod_data();
				$this->send_email_for_refund_order();
				$this->load->library('sms_gateway');
				//$this->sms_gateway->send_sms_for_order_refund($data['refund_order_data']->mobile,$data['refund_order_data']->refund_id,$data['refund_order_data']->first_name);
				$data['imageaccesspath']=IMAGEACCESSPATH;
				echo json_encode($data);
				
			}
			else
			 {
				echo 0;
			 } 
			}else{
				echo 0;
			}	
	  }
 public function generate_otp_refund()
    {
		$mobile = $this->Aordermodel->get_mobile_number();

		//$mobile=(int)$mobile1;
		
		$otp = rand(100000, 999999);
		if($mobile!=NULL){
			$this->db->where_in('mobile',$mobile);
			$result=$this->db->get('customer')->row();
			
			if($result!=NULL){
				//100000;
				$this->session->set_userdata(array('mobile'=>$mobile,'otp'=>$otp));
				$this->load->library('sms_gateway');
				$result1=$this->sms_gateway->genrate_otp($mobile["mobile"],$otp);
				//var_dump($result1);
				if($result1!=NULL)
				{	
					$data['otp']=$otp;
					$data['status']=1;
					echo json_encode($data);
				}	
			}
			else{
				$data['otp']=$otp;
				$data['status']=0;
				echo json_encode($data);
			}
		}
		else{
			$data['status']=0;
			echo json_encode($data);
		}
           
    }

	public function get_refunded_product_list()
	{
		$order_id=$this->uri->segment(3,0);
		$data['refund_data']=$this->Aordermodel->get_refund_products($order_id);
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function send_email_for_refund_order()
	{
		$data['order']=$this->Aordermodel->get_refund_details();
		$data['refund_order_product']=$this->Aordermodel->get_refund_products_details();
		$message=$this->load->view('email/order_returned_exchanged.php',$data,TRUE);
		$this->load->library('email_manager');
		$this->email_manager->send_refund_order_email($data['order']->email,$message);
	}
	
	public function get_ord_data()
	{
		$data = $this->Aordermodel->get_refund_order_data_test();
		echo json_encode($data);
	}
	
}	
