<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Amain_categories extends CI_Controller 
{
	public function get_main_category_list()
	{
		$this->load->model('Amaincategorymodel');
		$data['main_cat_list']=$this->Amaincategorymodel->get_all_main_categories();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function slider_images()
	{
		$this->db->select('slider_img_id,slider_image');
		$this->db->where('slider_type','Android');
		$this->db->where('status',1);
		$data['sliders']=$this->db->get('sliders')->result();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function search()
	{
		$this->load->model('Amaincategorymodel');
		if($this->input->post())
		{	
			$data['result']= $this->Amaincategorymodel->get_search_result();
			$data['category']=$this->Amaincategorymodel->get_search_result_for_category();
			echo json_encode($data);
		}	
		else{
			$data['result']=NULL;
			$data['category']=NULL;
			echo json_encode($data);
		}
	}
	
	
	public function search_product_details()
	{
		$this->load->model('Ddlmodel');
		$this->load->model('Amaincategorymodel');
		$prod_id=$this->uri->segment(3,0);
		$per_page=3;
		$count=$this->Amaincategorymodel->get_no_of_rows();
		if($count>0)
		{
		$data['product_details']= $this->Amaincategorymodel->get_product_details($prod_id,$per_page);
		$data['count']=$this->Amaincategorymodel->get_no_of_rows();
		$data['success']=1;
		$data['imageaccesspath']=IMAGEACCESSPATH;
		}else{
			$data['success']=0;
			$data['product_details']='NO records found...';
		}	
		$data['psize_list']=$this->Ddlmodel->get_psize_list();
		echo json_encode($data);
	}
}
