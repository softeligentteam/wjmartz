<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
	}	
	 
	public function view_all()
	{
		$this->load->view('layouts/header');
		$this->load->view('pages/products');
		$this->load->view('layouts/footer');
	}
	
}
