<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parent_categories extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Parentcategorymodel');
		
	}	
	 
	public function view_all()
	{
		$data['parent_cat_list']=$this->Parentcategorymodel->get_parent_cat_list();
		$this->load->view('layouts/header');
		$this->load->view('pages/parent_categories',$data);
		$this->load->view('layouts/footer');
	}
	 
	
	public function add_parent_category()
	{
		if($this->input->post())
		{
			$this->Parentcategorymodel->insert_parent_categories();
			$message = 'Parent Category is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('parent_categories/view_all');
		}else{
			$message = 'Parent category insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('parent_categories/view_all');
		}
			
	}
}
