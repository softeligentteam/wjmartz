<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Apayment extends CI_Controller {

	/*************** HASH GENERATION ****************/
	public function get_product_payment_credentials()
	{
		$PAYU_BASE_URL = "https://test.payu.in"; //test
		//$key ="gtKFFx";
		//$salt="eCwWELxi";
		$key ="2kdiCU";
		$salt= "2ctVqZ5L";
		$amount = $this->input->post('amount');
		$firstname = $this->input->post('firstname');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$productinfo = $this->input->post('productinfo');
		$txnid=$this->input->post('txnid');
		$user_credentials=$this->input->post('user_credentials');
		
		
		$udf1= $this->input->post('udf1');
		$udf2= $this->input->post('udf2');
		$udf3= $this->input->post('udf3');
		$udf4= $this->input->post('udf4');
		$udf5= $this->input->post('udf5');
		
		$payhash_str = $key . '|' . $txnid . '|' .$amount . '|' .$productinfo  . '|' . $firstname . '|' . $email . '|' .$udf1 . '|' . $udf2 . '|' . $udf3 . '|' . $udf4 . '|' . $udf5 . '||||||' . $salt;
		
		$paymentHash = strtolower(hash('sha512', $payhash_str));
		$data['payment_hash'] = $paymentHash;
		
		$cmnMobileSdk = 'vas_for_mobile_sdk';
		$mobileSdk_str = $key . '|' . $cmnMobileSdk . '|default|' . $salt;
		$mobileSdk = strtolower(hash('sha512', $mobileSdk_str));
		$data['vas_for_mobile_sdk_hash'] = $mobileSdk;
		
		$cmnPaymentRelatedDetailsForMobileSdk1 = 'payment_related_details_for_mobile_sdk';
		$detailsForMobileSdk_str1 = $key  . '|' . $cmnPaymentRelatedDetailsForMobileSdk1 . '|default|' . $salt ;
		$detailsForMobileSdk1 = strtolower(hash('sha512', $detailsForMobileSdk_str1));
		$data['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk1;
		
		$data['payhash_str'] = $payhash_str;
		echo json_encode($data);
	}
	
	public function reverse_hash()
	{
		$payudata=$this->input->post('payu_data');
		$payu_data=json_decode($payudata,TRUE);
		$hash = $payu_data['hash'];
		$reverseHash = $this->generateReverseHash($payu_data);

		if ($payu_data['hash'] == $reverseHash) {
			  # transaction is successful
			  # do the required javascript task

			  //echo("Transaction Success & Verified");
			  echo $reverseHash;
			  //$this->AndroidSuccess("<?php $result=''; foreach($payu_data as $key=> $value)$result .= $key . '=' . $value . ','; $result = rtrim($result , ','); echo $reverseHash ? >");
		}
		else{
			  # transaction is tempered
			  # handle it as required
			  echo "Invalid transaction";
		}
		
	}
	
	
		# Function to generate reverse hash
		function generateReverseHash($payu_data) {
			

		  
			//$key ="gtKFFx";
			//$salt="eCwWELxi";
			$key ="2kdiCU";
			$salt= "2ctVqZ5L";
			//$udf2='';
			$udf3='';
			$udf4='';
			$udf5='';
		  
		  $reversehash_string =  $salt . "|" . $payu_data["status"]  . "||||||" . $udf5 . "|" . $udf4 . "|" . $udf3 . "|" . $payu_data["udf2"] . "|" . $payu_data["udf1"] . "|" .
			$payu_data["email"] . "|" . $payu_data["firstname"] . "|" . $payu_data["productinfo"] . "|" . $payu_data["amount"] . "|" . $payu_data["txnid"] . "|" . $payu_data["key"] ;
		  
		  $reverseHash = strtolower(hash("sha512", $reversehash_string));

		  return $reverseHash;
		}	
		
}	