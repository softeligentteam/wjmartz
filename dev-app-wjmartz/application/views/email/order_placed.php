<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/emails/order_delivered.css"/>
    <title>Order Placed | WJMartz</title>
</head>
<body width="100%" bgcolor="#fff" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width: 100%; background: #fff; text-align: left;">

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
            WJMartz : Order Placed.
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <!-- Email Header : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
            <tr>
                <td style="padding: 20px 0; text-align: center">
                    <img src="<?=base_url();?>assets/img/2.png" alt="alt_text" border="0" style="height: 100px; background: #fff; font-family: sans-serif; font-size: 15px; line-height: 50px; color: #555555;">
                </td>
            </tr>
        </table>
        <!-- Email Header : END -->

        <!-- Email Body : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
			
            <!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 40px 20px; text-align: center;">
                    <h1 style="margin: 0; font-family: sans-serif; font-size: 24px; line-height: 27px; color: #333333; font-weight: normal;">
						ORDER PLACED SUCCESSFULLY!
					</h1>
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" style="padding: 0 40px 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                    <p style="margin: 0;">
						Dear <b><?=$order->first_name.' '.$order->last_name?></b>, your order on <?=$order->order_date?>, is placed successfully. Following are your order details.
					</p><br /><hr />
					<h3>Order Details :</h3>
                    <p style="margin: 0;"></p>
					<h5>Order Id : <span style="float:right"># <?=$order->order_id?></span></h5> 
					<h5>Order Placed On : <span style="float:right"><?=$order->order_date?></span></h5> 
					<h5>Total Amount : <span style="float:right">Rs. <?=$order->amount?></span></h5>
					<hr />
                </td>
            </tr>
			
			<?php if(count($order_product)>0){ $i=1; foreach ($order_product as $product) { ?>
        	<!-- Thumbnail Left, Text Right : BEGIN -->
			<tr>
				<td bgcolor="#ffffff" dir="ltr" align="center" valign="top" width="100%" style="padding: 10px;">
					<table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<!-- Column : BEGIN -->
							<td width="33.33%" class="stack-column-center">
								<table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td dir="ltr" valign="top" style="padding: 0 10px;">
											<img src="<?=IMAGEACCESSPATH.$product->prod_image_url?>" width="170" height="170" alt="Image unreachable" border="0" class="center-on-narrow" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
										</td>
									</tr>
								</table>
							</td>
							<!-- Column : END -->
							<!-- Column : BEGIN -->
							<td width="66.66%" class="stack-column-center">
								<table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
									<tr>
										<td dir="ltr" valign="top" style="font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; padding: 10px; text-align: left;" class="center-on-narrow">
											<h2 style="margin: 0 0 10px 0; font-family: sans-serif; font-size: 18px; line-height: 21px; color: #333333; font-weight: bold;">
												<?=$product->prod_name?>
											</h2>
											<p><b>Price : </b><?=$product->price?></p>
											<p><b>Quantity : </b><?=$product->quantity?></p>
											<p><b>Total Amount : </b><?=$product->amount?></p>
										</td>
									</tr>
								</table>
							</td>
							<!-- Column : END -->
						</tr>
					</table>  
				</td>
			</tr>
			<!-- Thumbnail Left, Text Right : END -->
			<?php $i++; } } ?> 
			
			

			<!-- Clear Spacer : BEGIN -->
			<tr>
				<td aria-hidden="true" height="40" style="font-size: 0; line-height: 0;">
					&nbsp;
				</td>
			</tr>
			<!-- Clear Spacer : END -->

			<!-- 1 Column Text : BEGIN -->
			<tr>
				<td bgcolor="#ffffff">
					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td style="padding: 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
								Incase of any doubt or query, please contact our customer care help desk, we will be happy to help.
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- 1 Column Text : END --> 
		</table>
		<!-- Email Body : END -->

		<!-- Email Footer : BEGIN -->
		<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto; font-family: sans-serif; color: #888888; line-height:18px;" class="email-container">
			<tr>
				<td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; line-height:18px; text-align: center; color: #888888;" class="x-gmail-data-detectors">
					WJMartz<br><br>CST No. 50 Fl. 6, Sankalp Campus,<br />Shivajinagar, Pune - 411005.
					<br><br>
					Email : Support@wjmartz.com 
					<br><br>
					<a href="http://wjmartz.com/index.php/home/about" target="_blank">About Us </a> | 
					<a href="http://wjmartz.com/index.php/home/terms" target="_blank">Terms & Conditions </a> | 
					<a href="http://wjmartz.com/index.php/home/policies" target="_blank">Privacy & Return Policy</a>
				</td>
			</tr>
		</table>
		<!-- Email Footer : END -->
    </center>
</body>
</html>