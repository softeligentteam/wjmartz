<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>Sub Categories</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div class="col-md-12"> 
				<div class="col-md-6"> 
					<h5 class="filtered"><i class="fa fa-filter" aria-hidden="true"></i> All sub-categories </h5>
				</div>
				<form>
					<div class="col-md-4">  
						<div class="form-group"> 
							<select name="main_cat" tabindex="5" id="main_cat" required="" class="form-control">
								<option value="" selected="selected">Select Main Category...</option>
								<option value="">Demo</option>
								<option value="">Demo</option>
								<option value="">Demo</option>
								<option value="">Demo</option>
							</select> 		
						</div>  
					</div>
					<div class="col-md-2"> 
						<a onclick="" class="btn btn-info" style="width:100%">Filter</a>
					</div>
				</form>
			</div>
		</div>
		
		<hr />
		<div class="row">
			<div class="col-md-2 col-md-offset-right-10"> 
				<a onclick="$('#addNewProduct').slideDown();" class="btn btn-info" style="width:100%">Add New Product</a>
			</div>
		</div>
		
		<hr />
		<div id="addNewProduct" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Add New Product</h2>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-2 col-sm-2">
					<h5>Upload Product Image :</h5>
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; padding:0; text-align:center;">
							<img id="productImagePreview" src="" style="width:100%;" alt="" />
						</div>
						<div class="panel-footer back-footer-blue">
							<input type='file' id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%" /> 
						</div>
					</div> 
				</div>
				<div class="col-md-10">
					<h5>Product Details :</h5> 
					<div class="col-md-4">
						<div class="form-group">
							<label>Select Sub Category</label>
							<select class="form-control">
								<option>Select Main Category...</option>
								<option>Demo</option>
								<option>Demo</option>
								<option>Demo</option>
							</select>
							<p class="help-block" id="selectMainCatError" style="color:red; height:20px;"></p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Enter New Product Name</label>
							<input class="form-control">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<label>Enter Price</label>
							<input class="form-control">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter Product Description</label>
							<textarea class="form-control"></textarea>
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label><input type="checkbox" class="" /> &nbsp;&nbsp;Check To Make This Product Available For Subscription</label> 
						</div> 
					</div> 
					<div class="col-md-6 text-right">
						<a href="#" class="btn btn-success">Save</a>
						<a onclick="$('#addNewProduct').slideUp();" class="btn btn-warning">Cancle</a>
					</div>
				</div>

			</div>
			<hr>
			<!-- /. ROW  --> 
		</div> 
		
		
		<div class="row">
			<?php for($i=1; $i<13; $i++){?>
			<div class="col-md-3">
				<div class="panel panel-primary text-center no-boder bg-color-blue">
					<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
						
					</div>
					<div class="panel-footer back-footer-blue">
						<div class="col-md-6">
						Product <?=$i?> Name 
						</div>
						<div class="col-md-6">
							Prize : 123 Rs.
						</div><div class="clearfix"></div>
					</div>
				</div>
			</div>
			<?php }?>
			<div class="col-sm-12">
				<ul class="pagination">
					<li><a href="#">&lt;</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">&gt;</a></li>
				</ul> 
			</div>
		</div>
		<hr />  
		
		<div  id="addNewSubCat" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Add New Sub-Categories</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-6">
					<h5>Upload Sub-Category Image :</h5>
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							Upload Image 
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<h5>Sub Categoriy Details :</h5> 
					<div class="col-md-12">
						<div class="form-group">
							<label>Select Main Category</label>
							<select class="form-control">
								<option>Select Main Category...</option>
								<option>Demo</option>
								<option>Demo</option>
								<option>Demo</option>
							</select>
							<p class="help-block" id="selectMainCatError" style="color:red; height:20px;"></p>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter New Sub Category Name</label>
							<input class="form-control">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-12">
						<a href="#" class="btn btn-success">Save</a>
						<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">Cancle</a>
					</div>
				</div>

			</div>
			<hr />
			<!-- /. ROW  --> 
		</div>
		
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>
	