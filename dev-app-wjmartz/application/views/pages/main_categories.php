﻿<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>Main Categories</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Main Category Name</th> 
						</tr>
					</thead>
					<tbody>
						
						<?php  if(count($main_cat_list)>0){
						$i=1;
                           foreach ($main_cat_list as $main_cat)  
                          {  
                        ?>	
						<tr>
							<td><?=$i?></td>
							<td><?=$main_cat->main_cat_name?></td>
						</tr>
					   <?php $i++;
						 
						}  } ?>	
					</tbody>
				</table>
				<!--div class="col-sm-12" style="padding-left:0;"> 
				  <ul class="pagination">
					<li><a href="#">&lt;</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">&gt;</a></li>
				  </ul> 
				</div-->
			</div>
		</div>
		<!-- /. ROW  -->
	</div>
	<!-- /. PAGE INNER  -->
</div>
	