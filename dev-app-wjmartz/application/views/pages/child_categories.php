﻿<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>Child Categories</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div class="col-md-12"> 
				<div class="col-md-6"> 
					<h5 class="filtered"><i class="fa fa-filter" aria-hidden="true"></i> All Child Categories </h5>
				</div>
				<form>
					<div class="col-md-4">  
						<div class="form-group"> 
							<?=form_dropdown("parent_cat", $parent_cat_list, set_value('parent_cat', ''), "tabindex='5' id='parent_cat'  required class='form-control'")?>		
						</div>  
					</div>
					<div class="col-md-2"> 
						<a onclick="filter();" class="btn btn-info" style="width:100%">Filter</a>
					</div>
				</form>
			</div>
		</div>
		
		<div class="row" id="displayChildcatList" style="display:none;">
			<div class="col-md-12">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th class="text-center">Child Category Image</th>
							<th class="text-center">Child Category Name</th>
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody id="appendChildcatList">
					</tbody>
				</table>
					<?php echo $this->pagination->create_links();?>
			</div>
		
		</div>
		
		<div class="row">
			<div class="col-md-offset-9 col-md-3"> 
				<br />
				<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info" style="width:100%">Add New Child Category</a>
			</div>
		</div>
		
		<!-- /. ROW  -->
		
		
		
		<div  id="addNewSubCat" style="display:none;">
			<div class="row">
			<?=form_open_multipart('child_categories/add_child_category');?>
				<div class="col-md-12">
					<h2>Add New Child-Categories</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-6">
					<h5>Upload Child-Category Image :</h5>
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							<img id="productImagePreview" src="" style="width:100%;" alt="" />
						</div>
						<div class="panel-footer back-footer-blue">
						<input type='file' name="child_cat_img" id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%" /> 
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<h5>Child Categoriy Details :</h5> 
					<div class="col-md-12">
						<div class="form-group">
							<label>Select Child Category</label>
							<?=form_dropdown("parent_cats", $parent_cat_list, set_value('parent_cats', ''), "tabindex='5' id='parent_cats'  required class='form-control'")?>		
							<p class="help-block" id="selectMainCatError" style="color:red; height:20px;"></p>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter New Child Category Name</label>
							<input class="form-control" name="child_category">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-12">
						<input type="submit" value="Save" class="btn btn-success" />
						<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">Cancle</a>
					</div>
				</div>

			</div>
			<?=form_close();?>
			<hr />
			<!-- /. ROW  --> 
		</div>
		
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Child-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>
<div class="insert_success" id="confirm_delete" style="display:none">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 id="dlt_msg" class="text-center">Are you sure you want to delete</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a href="" id="deleteMAinCatHref" id="" class="btn btn-primary">Yes</a>
					<a onclick="$('#confirm_delete').hide()" id="" class="btn btn-primary">No</a>
				</div>
			</div>
		</div>
	</div>
</div>


<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/child_category.js"></script>
	