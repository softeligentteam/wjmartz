<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

defined('IMAGEBASEPATH') OR define('IMAGEBASEPATH', '/home/wjmar5ke/public_html/static/dev/');
defined('IMAGEACCESSPATH') OR define('IMAGEACCESSPATH', 'http://static.wjmartz.com/dev/');
//defined('IMAGEBASEPATH') OR define('IMAGEBASEPATH', '/home/wjmar5ke/public_html/www.wjmartz.com/admin/');
//defined('IMAGEUSERPATH') OR define('IMAGEUSERPATH', 'http://www.wjmartz.com/dev-app-wjmartz/');
//defined('IMAGEACCESSPATH') OR define('IMAGEACCESSPATH', 'http://www.wjmartz.com/admin/');
//defined('TEMPMEMBERUPLOADPATH') OR define('TEMPMEMBERUPLOADPATH', 'static_img/uploads/products/');
//defined('PARENTCATUPLOADPATH') OR define('PARENTCATUPLOADPATH', 'static_img/uploads/parent_categories/');
//defined('CHILDCATUPLOADPATH') OR define('CHILDCATUPLOADPATH', 'static_img/uploads/child_categories/');
//defined('USERUPLOADPATH') OR define('USERUPLOADPATH', 'static_img/uploads/user_photo/');
//defined('PCAKAGEIMAGEUPLOADPATH') OR define('PCAKAGEIMAGEUPLOADPATH', 'static_img/uploads/packages/');
//defined('PCAKAGEPRODUCTUPLOADPATH') OR define('PCAKAGEPRODUCTUPLOADPATH', 'static_img/uploads/package_products/');
//defined('BRANDUPLOADPATH') OR define('BRANDUPLOADPATH', 'static_img/uploads/brands/');


defined('IMAGEUSERPATH') OR define('IMAGEUSERPATH', 'http://www.wjmartz.com/dev-app-wjmartz/');
defined('PRODUCTUPLOADPATH') OR define('PRODUCTUPLOADPATH','uploads/products/');
defined('PARENTCATUPLOADPATH') OR define('PARENTCATUPLOADPATH', 'uploads/parent_categories/');
defined('CHILDCATUPLOADPATH') OR define('CHILDCATUPLOADPATH', 'uploads/child_categories/');
defined('USERUPLOADPATH') OR define('USERUPLOADPATH', 'uploads/user_photo/');
defined('PCAKAGEIMAGEUPLOADPATH') OR define('PCAKAGEIMAGEUPLOADPATH', 'uploads/packages/');
defined('PCAKAGEPRODUCTUPLOADPATH') OR define('PCAKAGEPRODUCTUPLOADPATH', 'uploads/package_products/');
defined('BRANDUPLOADPATH') OR define('BRANDUPLOADPATH', 'uploads/brands/');
defined('APPSLIDERUPLOAD') OR define('APPSLIDERUPLOAD', 'uploads/sliders/android/');
defined('DEALIMAGEUPLOAD') OR define('DEALIMAGEUPLOAD', 'uploads/deal_of_day/');
defined('PROMOIMAGEUPLOAD') OR define('PROMOIMAGEUPLOAD', 'uploads/promocodes/');
defined('SIZEIMAGEUPLOAD') OR define('SIZEIMAGEUPLOAD', 'uploads/sizechart/');

/************Status for All************/
defined('ACTIVE')        OR define('ACTIVE', 1);
defined('INACTIVE')        OR define('INACTIVE', 0);

/************COUPON CODE************/
defined('PROMODOESNOTEXISTMESSAGE') OR define('PROMODOESNOTEXISTMESSAGE','Sorry, Coupon code does not exist!');
defined('PROMOSUCCESSMESSAGE') OR define('PROMOSUCCESSMESSAGE','Coupon  code applied successfully!');
defined('PROMODEALMESSAGE') OR define('PROMODEALMESSAGE','Either your product is in deals of the day or you are not a new user!');
defined('PROMODEALIVERYMESSAGE') OR define('PROMODEALIVERYMESSAGE','Either your product is in deals of the day or Your cart value is less than 500!');
defined('PROMOSUBCATMESSAGE') OR define('PROMOSUBCATMESSAGE','This child category product is not available in the cart!');
//defined a new constant for firebase api key
 define('FIREBASE_API_KEY', 'AIzaSyA96qFirE2rkCpxk0DLpmNzo-WjYgQ4_TI');
