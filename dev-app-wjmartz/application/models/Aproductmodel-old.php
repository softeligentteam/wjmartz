<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aproductmodel extends CI_Model
{
	public function get_genral_products($per_page,$child_cat_id)
	{
		$this->db->select('prod_id,prod_name,prod_img_url,prod_price,prod_description,quantity');
		$this->db->where_not_in('prod_id',0);
		$this->db->where('fk_pchild_id',$child_cat_id);
		return $this->db->get('products',$per_page,$this->uri->segment(4,0))->result();
	}
	
		public function get_no_of_rows($child_cat_id)
	{
		$this->db->where('fk_pchild_id',$child_cat_id);
		return $this->db->get('products')->num_rows();
	}
	
	public function get_all_brands()
	{
		$this->db->where_not_in('brand_id',0);
		$this->db->select('brand_id,brand,brand_img_url');
		return $this->db->get('brands')->result();
	}
	
	public function get_no_of_rows_brands($brand_id)
	{
		$this->db->where_not_in('brand_id',0);
		$this->db->where('brand_id',$brand_id);
		return $this->db->get('products')->num_rows();
	}
	
	public function get_brand_products($per_page,$brand_id)
	{
		$this->db->select('prod_id,prod_name,prod_img_url,prod_price,prod_description,quantity');
		$this->db->where_not_in('prod_id',0);
		$this->db->where('brand_id',$brand_id);
		return $this->db->get('products',$per_page,$this->uri->segment(4,0))->result();
	}
	
	public function get_all_pune_special()
	{
		$this->db->select('brand_id,brand,brand_img_url');
		$this->db->where('is_pune_special',1);
		return $this->db->get('brands')->result();
	}
	
	public function get_pune_special_products($per_page,$brand_id)
	{
		$this->db->select('prod_id,prod_name,prod_img_url,prod_price,prod_description,quantity');
		$this->db->where_not_in('prod_id',0);
		$this->db->where('brand_id',$brand_id);
		$this->db->where('is_pune_special',1);
		return $this->db->get('products',$per_page,$this->uri->segment(4,0))->result();
	}
	
	public function get_no_of_rows_pune_special($brand_id)
	{
		$this->db->where('brand_id',$brand_id);
		return $this->db->get('products')->num_rows();
	}
	
	public function get_catandtypewise_products($child_cat_id,$per_page)
	{
		$this->db->select('prod_id as pid, fk_pchild_id as pccat_id, prod_name as pname, fk_psize_ids, prod_mrp as mrp, prod_price as price, prod_image_url as image_url, discount,fk_pcolor_id,deal_status,refund_status');
		$this->db->join('product_colors pc', 'p.fk_pcolor_id=pc.color_id');
		//$this->get_short_and_boxer_filter($pccat_id);
		$this->db->where('fk_pchild_id', $child_cat_id);
		$this->db->where('deal_status', 0);
		$this->db->where('status', 1);
		$this->db->where('in_stock', 1)->order_by('prod_id', 'desc');
		return $this->db->get('products p',$per_page, $this->uri->segment(4))->result();
	}
	
	public function get_psize_list()
	{
		$this->db->select('size_id, size')->order_by('size');
		//$this->db->where('product_id', 3);
		return $this->db->get('product_sizes')->result();
	}
	
		public function get_product_size_list($product_id)
	{
		$this->db->select('ps.size_id, ps.size, psq.quantity')->order_by('ps.size');
		//$this->db->where('product_id', 3);
		$this->db->join('product_slot_quantity psq', 'psq.size_id = ps.size_id and psq.product_id ='.$product_id);
		$this->db->order_by('ps.size_id');
		return $this->db->get('product_sizes ps')->result();
	}
	
	public function get_psize_list1($parent_cat_id)
	{
		$this->db->select('size_id, size')->order_by('size');
		$this->db->where('parent_cat_id',$parent_cat_id);
		return $this->db->get('product_sizes')->result();
	}
	public function get_no_of_rows1($child_cat_id)
	{
		$this->db->where('fk_pchild_id', $child_cat_id);
		$this->db->where('status', 1);
		return $this->db->get('products')->num_rows();
	}
	
	public function get_no_of_rows2($pccat_id)
	{
		$this->db->where('fk_pchild_id', $pccat_id);
		return $this->db->get('products')->num_rows();
	}
	
	public function get_product2view_details($pccat_id, $pid)
	{
		$data = $this->db->select('prod_image_url,prod_image_urls')->where('prod_id', $pid)->get('products')->row();
		$image_urls = $data->prod_image_url.','.$data->prod_image_urls;
		$this->db->select('prod_id as pid, prod_name as pname, prod_mrp as mrp, prod_price as pprice, b.brand as bname, fk_pchild_id as pccat_id, fk_ptype_id as ptype_id, fk_pcolor_id as pcolor_id, fabric_type as ftype, pc.color_name as pcolor, prod_fit_details as pfitdetails, prod_fabric_details as pfabricdetails, prod_description as pdesc, prod_image_url as img_url, discount,fk_psize_ids,in_stock,deal_status,size_chart_image_url,delivery_n_return,refund_status');
		$this->db->join('brands b', 'p.fk_pbrand_id=b.brand_id');
		$this->db->join('product_colors pc', 'p.fk_pcolor_id=pc.color_id');
		$this->db->where('in_stock', 1)->where('prod_id='.$pid);
		$this->db->where('fk_pchild_id', $pccat_id);
		$prod_data = $this->db->get('products p')->row();
		$product = array(
			'product'=>$prod_data,
			'image_urls'=>explode(',',$image_urls)
			);
		//var_dump($product);
		return $product;
		
	}
	
	public function get_recommended_products($pccat_id, $pcolor_id)
	{
		$this->db->select('prod_id as pid, prod_name as pname, prod_price as pprice, prod_image_url as img_url,fk_pcolor_id,fk_pchild_id,deal_status');
		$this->db->where('fk_pchild_id', $pccat_id);
		$this->db->where('fk_pcolor_id', $pcolor_id);
		$this->db->where('status', 1);
		//$this->db->limit(8, 1);
		$this->db->where('in_stock', 1)->order_by('prod_id', 'RANDOM');
		return $this->db->get('products')->result();
	}

	public function get_product2view_details_for_deal($pccat_id, $pid)
	{
		$data = $this->db->select('prod_image_url,prod_image_urls')->where('prod_id', $pid)->get('products')->row();
		$image_urls = $data->prod_image_url.','.$data->prod_image_urls;
		$this->db->select('prod_id as pid, prod_name as pname, prod_price as mrp, dp.deal_price as pprice, b.brand as bname, fk_pchild_id as pccat_id, fk_ptype_id as ptype_id, fk_pcolor_id as pcolor_id, fabric_type as ftype, pc.color_name as pcolor, prod_fit_details as pfitdetails, prod_fabric_details as pfabricdetails, prod_description as pdesc, prod_image_url as img_url,dp.discount,fk_psize_ids,in_stock,deal_status,size_chart_image_url,delivery_n_return,refund_status');
		$this->db->join('deals_of_day_product dp', 'dp.deal_product_id=p.prod_id');
		$this->db->join('brands b', 'p.fk_pbrand_id=b.brand_id');
		$this->db->join('product_colors pc', 'p.fk_pcolor_id=pc.color_id');
		$this->db->where('in_stock', 1)->where('prod_id='.$pid);
		$this->db->where('fk_pchild_id', $pccat_id);
		$prod_data = $this->db->get('products p')->row();
		$product = array(
			'product'=>$prod_data,
			'image_urls'=>explode(',',$image_urls)
			);
		//var_dump($product);
		return $product;
		
	}
	
	public function get_recommended_products_for_deal($pccat_id, $pcolor_id,$pid)
	{
		$this->db->select('prod_id as pid, prod_name as pname, deal_price as pprice, prod_image_url as img_url,fk_pcolor_id,fk_pchild_id,deal_status,refund_status');
		$this->db->join('products p', 'p.prod_id=dp.deal_product_id');
		//$this->db->where('fk_pchild_id', $pccat_id);
		$this->db->where_not_in('deal_product_id', $pid)->order_by('prod_id', 'RANDOM');
		$this->db->where('dp.status', 1);
		//$this->db->limit(8, 1);
		//$this->db->where('in_stock', 1)
		return $this->db->get('deals_of_day_product dp')->result();
	}
	
	public function get_filtered_products()
	{
		$this->db->select('prod_id as pid, fk_pchild_id as pccat_id,prod_name as pname, fk_psize_ids, prod_mrp as mrp, prod_price as price, prod_image_url as image_url, discount,fk_pcolor_id,deal_status,refund_status');
		$filterdata = $this->input->post('filter_data');
		$filter_data = json_decode($filterdata, TRUE);
		$pccat_id = $filter_data['pccat_id'];
		$pcolor_id =$filter_data['color_details_array'];
		$psize_id = $filter_data['size_details_string'];
		$ptype_id=  $filter_data['type_details_string'];
		if(count($pcolor_id)>0)
		{
			$this->db->where_in('fk_pcolor_id', $pcolor_id);
		}
		if($psize_id!=NULL)
		{	
			$this->db->group_start();
			$this->db->like('fk_psize_ids',$psize_id );
			$this->db->group_end();
		}
		if($ptype_id!=NULL)
		{	
			$this->db->group_start();
			$this->db->where_in('fk_ptype_id',$ptype_id );
			$this->db->group_end();
		}
		
		//$this->db->where('fk_pchild_id', $pccat_id);
		$this->db->where('status', 1)->where('deal_status', 0);
		$this->db->where('in_stock', 1)->order_by('prod_id', 'asc');
		return $this->db->get('products ')->result();
		
	}
	
	public function get_filter_low_price($pccat_id,$per_page)
	{
		$this->db->select('prod_id as pid, fk_pchild_id as pccat_id, prod_name as pname, fk_psize_ids, prod_mrp as mrp, prod_price as price, prod_image_url as image_url,discount,fk_pcolor_id,refund_status,deal_status');
		$this->db->where('fk_pchild_id', $pccat_id);
		$this->db->where('in_stock', 1)->order_by('prod_price', 'asc'); 
        $this->db->where('status', 1)->where('deal_status', 0);
		return $this->db->get('products',$per_page,$this->uri->segment(4,0))->result();
	}
	
	public function get_filter_high_price($pccat_id,$per_page)
	{
		$this->db->select('prod_id as pid, fk_pchild_id as pccat_id, prod_name as pname, fk_psize_ids, prod_mrp as mrp, prod_price as price, prod_image_url as image_url,discount,fk_pcolor_id,refund_status,deal_status');
		$this->db->where('fk_pchild_id', $pccat_id);
		$this->db->where('in_stock', 1)->order_by('prod_price', 'desc');
        $this->db->where('status', 1)->where('deal_status', 0);
		return $this->db->get('products',$per_page,$this->uri->segment(4,0))->result();
	}
	
	public function get_new_arrival_products()
	{
		$this->db->select('prod_id as pid, fk_pchild_id as pccat_id, prod_name as pname, fk_psize_ids, prod_mrp as mrp, prod_price as price, prod_image_url as image_url, discount,fk_pcolor_id,deal_status,refund_status');
		$this->db->join('product_colors pc', 'p.fk_pcolor_id=pc.color_id');
		$this->db->where('status', 1);
		$this->db->where('in_stock', 1);
		$this->db->where('deal_status', 0);
		$this->db->where_not_in('prod_id',0);
		$this->db->order_by('prod_id', 'DESC');
		$this->db->limit('10');
		return $this->db->get('products p')->result();
	}
	
	public function get_recommended_products_new($child_cat_id)
	{
		$this->db->select('prod_id as pid, fk_pchild_id as pccat_id, prod_name as pname, fk_psize_ids, prod_mrp as mrp, prod_price as price, prod_image_url as image_url, discount,fk_pcolor_id,deal_status,refund_status');
		$this->db->join('product_colors pc', 'p.fk_pcolor_id=pc.color_id');
		$this->db->where('status', 1);
		$this->db->where('in_stock', 1);
		$this->db->where_not_in('prod_id',0);
		$this->db->where('fk_pchild_id', $child_cat_id);
		$this->db->order_by('prod_id', 'RANDOM');
		$this->db->limit('4');
		return $this->db->get('products p')->result();
	}
	
	public function get_deals_of_the_day_product()
	{
		$this->db->select('dp.deal_product_id,prod_name as pname,dp.deal_price,deal_image_url as image_url,fk_pcolor_id,prod_price,dp.discount,p.fk_pchild_id as pccat_id,child_cat_name,,deal_status,refund_status');
		$this->db->join('products p', 'p.prod_id=dp.deal_product_id');
		$this->db->join('product_colors pc', 'p.fk_pcolor_id=pc.color_id');
		$this->db->join('child_category cc', 'cc.child_cat_id=p.fk_pchild_id');
		$this->db->where('dp.status', 1);
		$this->db->where('in_stock', 1);
		return $this->db->get('deals_of_day_product dp')->result();
		
	}

        public function get_deals_of_the_day_product_home()
	{
		$this->db->select('dp.deal_product_id,prod_name as pname,dp.deal_price,deal_image_url as image_url,fk_pcolor_id,prod_price,dp.discount,p.fk_pchild_id as pccat_id,child_cat_name,deal_status,refund_status');
		$this->db->join('products p', 'p.prod_id=dp.deal_product_id');
		$this->db->join('product_colors pc', 'p.fk_pcolor_id=pc.color_id');
		$this->db->join('child_category cc', 'cc.child_cat_id=p.fk_pchild_id');
		$this->db->where('dp.status', 1);
		$this->db->where('in_stock', 1);
                $this->db->limit('6');
		return $this->db->get('deals_of_day_product dp')->result();
		
	}

}