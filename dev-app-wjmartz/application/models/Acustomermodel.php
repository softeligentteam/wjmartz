<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acustomermodel extends CI_Model
{
	private function bind_customer_details()
	{
		$firstname=$this->input->post('firstname');
		$lastname=$this->input->post('lastname');
		$mobile=$this->input->post('mobile');
		$email=$this->input->post('email');
		$dob=$this->input->post('dob');
                $password=md5($this->input->post('confirm_password'));
	
		$customer_data=array(
		'first_name'=>$firstname,
		'last_name'=>$lastname,
		'email'=>$email,
		'mobile'=>$mobile,
		'dob'=>$dob,
                'password'=>$password,
		'created_on'=>date('Y-m-d H:i:s')
		);
		return $customer_data;
	}
	
	private function bind_customer_address($temp_cust_id)
	{
		$address=$this->input->post('address');
		$pincode=$this->input->post('pincode_id');
		$area=$this->input->post('area');
		
		$customer_address_data=array(
		'cust_id'=>$temp_cust_id,
		'address'=>$address,
		'area'=>$area,
		'pincode'=>$pincode,
		'city_id'=>$this->input->post('city_id'),
		'state_id'=>$this->input->post('state_id'),
		'is_default'=>1,
		'address_type'=>1
		);
		
		return $customer_address_data;
	}

	
	public function insert_customer()
	{
		$this->db->trans_begin();
        $this->db->insert('customer', $this->bind_customer_details());
        $temp_cust_id = $this->db->insert_id();
		$this->db->insert('customer_address', $this->bind_customer_address($temp_cust_id));
		 if($this->db->trans_status()===FALSE){
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
		return TRUE;
        }
	}
	
	public function get_last_cust_id()
	{
		$query=$this->db->get('customer')->last_row();
		return $query;
		
	}
	
	/*public function get_new_customer_data()
	{
		$cust_id=$this->db->insert_id();
		$this->db->select('cust_id,first_name,last_name,user_photo');
		$this->db->where('cust_id',$cust_id);
		return $this->db->get('customer')->result();
	}*/
	
	public function get_customer_data($mobile)
	{
		$this->db->select('cust_id,first_name,last_name,user_photo');
		$this->db->where('mobile',$mobile);
		return $this->db->get('customer')->result();
	}
	
	
	public function get_cutomer_data_to_update($cust_id)
	{
		$this->db->select('c.cust_id,first_name,last_name,email,mobile,dob,user_photo');
		$this->db->where('c.cust_id',$cust_id);
		//$this->db->where('is_default',1);	
		return $this->db->get('customer c')->result();
	}
	
	public function get_customer_address1($cust_id)
	{
		$this->db->select('address_id,address,pi.pincode,pi.pincode_id,ca.area as area_name,c.city_id,city,s.state_id,state');
		$this->db->from('city c');
		$this->db->from('state s');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		//$this->db->join('area a','a.area_id=ca.area');
		//$this->db->join('city c','c.city_id=a.city_id');
		//$this->db->join('state s','s.state_id=c.state_id');
		$this->db->where('ca.cust_id',$cust_id);
		$this->db->where('address_type',1);
		return $this->db->get('customer_address ca ')->row();
	}
	
	public function get_customer_address2($cust_id)
	{
		$this->db->select('address_id,address,pi.pincode,pi.pincode_id,ca.area as area_name,c.city_id,city,s.state_id,state');
		$this->db->from('city c');
		$this->db->from('state s');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		//$this->db->join('area a','a.area_id=ca.area');
		//$this->db->join('city c','c.city_id=a.city_id');
		//$this->db->join('state s','s.state_id=c.state_id');
		$this->db->where('ca.cust_id',$cust_id);
		$this->db->where('address_type',2);
		return $this->db->get('customer_address ca ')->row();
	}
	
	public function get_customer_address3($cust_id)
	{
		$this->db->select('address_id,address,pi.pincode,pi.pincode_id,ca.area as area_name,c.city_id,city,s.state_id,state');
		$this->db->from('city c');
		$this->db->from('state s');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		//$this->db->join('area a','a.area_id=ca.area');
		//$this->db->join('city c','c.city_id=a.city_id');
		//$this->db->join('state s','s.state_id=c.state_id');
		$this->db->where('ca.cust_id',$cust_id);
		$this->db->where('address_type',3);
		return $this->db->get('customer_address ca ')->row();
	}
	public function update_cust_name()
	{
		$cust_id=$this->input->post('cust_id');
		$firstname=$this->input->post('first_name');
		$lastname=$this->input->post('last_name');
		
		$customer_data=array(
		'first_name'=>$firstname,
		'last_name'=>$lastname,
		'modified_on'=>date('Y-m-d H:i:s')
		);
		
		$this->db->where('cust_id',$cust_id);
		$this->db->update('customer',$customer_data);
                return $this->db->affected_rows();
		
	}
	
	public function update_birthdate()
	{
		$cust_id=$this->input->post('cust_id');
		$dob=$this->input->post('dob');
		$customer_data=array(
		'dob'=>$dob,
		'modified_on'=>date('Y-m-d H:i:s')
		);
		$this->db->where('cust_id',$cust_id);
		$this->db->update('customer',$customer_data);
                return $this->db->affected_rows();

	}
	
	public function update_email()
	{
		$cust_id=$this->input->post('cust_id');
		$email=$this->input->post('email');
		$customer_data=array(
		'email'=>$email,
		'modified_on'=>date('Y-m-d H:i:s')
		);
		$this->db->where('cust_id',$cust_id);
		$this->db->update('customer',$customer_data);
                return $this->db->affected_rows();
	}
	
	public function update_mobile_number()
	{
		$cust_id=$this->input->post('cust_id');
		$mobile=$this->input->post('mobile');
		$customer_data=array(
		'mobile'=>$mobile,
		'modified_on'=>date('Y-m-d H:i:s')
		);
		$this->db->where('cust_id',$cust_id);
		$this->db->update('customer',$customer_data);
                return $this->db->affected_rows();
	}
	
	public function update_profile_photo()
	{
		$cust_id=$this->input->post('cust_id');
		$photo=$this->upload_user_photo();
		$user_photo=str_replace(IMAGEBASEPATH
				, '', $photo);
		
		$customer_data=array(
		'user_photo'=>$photo,
		'modified_on'=>date('Y-m-d H:i:s')
		);
		$this->db->where('cust_id',$cust_id);
		$this->db->update('customer',$customer_data);
                return $this->db->affected_rows();
	}
	
	public function update_address_home()
	{
		$address_id=$this->input->post('address_id');
		$address=$this->input->post('address1');
		$pincode=$this->input->post('pincode1');
		$area=$this->input->post('area1');
		$city_id=$this->input->post('city_id');
		$state_id=$this->input->post('state_id');
		$customer_data=array(
		'address'=>$address,
		'pincode'=>$pincode,
		'area'=>$area,
		'city_id' => $city_id,
		'state_id' => $state_id
		);
		$this->db->where('address_id',$address_id);
		$this->db->update('customer_address',$customer_data);
        return $this->db->affected_rows();
		
	}
	
	public function update_address_work()
	{
		$address_id=$this->input->post('address_id');
		$address=$this->input->post('address2');
		$pincode=$this->input->post('pincode2');
		$area=$this->input->post('area2');
		$city_id=$this->input->post('city_id');
		$state_id=$this->input->post('state_id');
		$customer_data=array(
		'address'=>$address,
		'pincode'=>$pincode,
		'area'=>$area,
		'city_id' => $city_id,
		'state_id' => $state_id
		);
		$this->db->where('address_id',$address_id);
		$this->db->update('customer_address',$customer_data);
        return $this->db->affected_rows();
	}
	
	public function update_address_other()
	{
		$address_id=$this->input->post('address_id');
		$address=$this->input->post('address3');
		$pincode=$this->input->post('pincode3');
		$area=$this->input->post('area3');
		$city_id=$this->input->post('city_id');
		$state_id=$this->input->post('state_id');
		$customer_data=array(
		'address'=>$address,
		'pincode'=>$pincode,
		'area'=>$area,
		'city_id' => $city_id,
		'state_id' => $state_id
		);
		$this->db->where('address_id',$address_id);
		$this->db->update('customer_address',$customer_data);
        return $this->db->affected_rows();

	}
	
	public function insert_address_work()
	{
		$cust_id=$this->input->post('cust_id');
		$address=$this->input->post('address2');
		$pincode=$this->input->post('pincode2');
		$area=$this->input->post('area2');
		$city_id=$this->input->post('city_id');
		$state_id=$this->input->post('state_id');
		$customer_data=array(
		'cust_id'=>$cust_id,
		'address'=>$address,
		'pincode'=>$pincode,
		'area'=>$area,
		'is_default'=>0,
		'address_type'=>2,
		'city_id' => $city_id,
		'state_id' => $state_id
		);
		
		return $this->db->insert('customer_address',$customer_data);
	}
	
	public function insert_address_other()
	{
		$cust_id=$this->input->post('cust_id');
		$address=$this->input->post('address3');
		$pincode=$this->input->post('pincode3');
		$area=$this->input->post('area3');
		$city_id=$this->input->post('city_id');
		$state_id=$this->input->post('state_id');
		$customer_data=array(
		'cust_id'=>$cust_id,
		'address'=>$address,
		'pincode'=>$pincode,
		'area'=>$area,
		'is_default'=>0,
		'address_type'=>3,
		'city_id' => $city_id,
		'state_id' => $state_id
		);
		
		return $this->db->insert('customer_address',$customer_data);
	}
	public function update_customer_data()
	{
		
		
		$email=$this->input->post('email');
		
		//$photo=$this->upload_user_photo();
		
		$address=$this->input->post('address');
		$pincode=$this->input->post('pincode_id');
		$address2=$this->input->post('address2');
		$pincode2=$this->input->post('pincode_id2');
		$address3=$this->input->post('address3');
		$pincode3=$this->input->post('pincode_id3');
		
		$customer_data=array(
		'first_name'=>$firstname,
		'last_name'=>$lastname,
		'email'=>$email,
		'mobile'=>$mobile,
		'dob'=>$dob,
		'user_photo'=>$user_photo,
		'modified_on'=>date('Y-m-d H:i:s')
		);
		
		$customer_address_data=array(
		'address'=>$address,
		'pincode'=>$pincode
		);
		$customer_new_address1 = 
		   array(
			'cust_id'=>$cust_id,
			'address'=>$address2,
			'pincode'=>$pincode2
		   );
		   $customer_new_address2=
		   array(
		    'cust_id'=>$cust_id,
			'address'=>$address3,
			'pincode'=>$pincode3
		   );
		if($address2!=NULL && $pincode2!=NULL)
		{
			$this->db->insert('customer_address',$customer_new_address1); 
		}
		if($address3!=NULL && $pincode3!=NULL)
		{
			$this->db->insert('customer_address',$customer_new_address2); 
		}
		$this->db->where('cust_id',$cust_id);
		$this->db->update('customer',$customer_data);
		$this->db->update('customer_address',$customer_address_data);
		return $this->db->affected_rows();
	}
	
	private function upload_user_photo()
	{
		/******* Uplaod Image ********/
			  $img = $this->input->post('user_photo');
			  $path = USERUPLOADPATH;
			  if(!is_dir($path)) //create the folder if it's not already exists
				{
				 mkdir($path,0755,TRUE);
			   $UPLOAD_DIR = $path ;
			   //echo "make directory:".$UPLOAD_DIR;
				} 
			  //define('UPLOAD_DIR', 'images/customers/');
			  $UPLOAD_DIR = $path; 
			  $img = str_replace('data:image/png;base64,', '', $img);
			  $img = str_replace(' ', '+', $img);
			  $data = base64_decode($img);
			  $file = $UPLOAD_DIR . uniqid() . '.png';
			  $success = file_put_contents($file, $data);
			 // echo "Success:";
			  return $success ? $file : NULL;
	}
	
	public function get_support_services_list()
	{
		$this->db->select('service_id,support_service,service_description')->order_by('service_id');
		$filtered_list['select_Support_service']='Select Support Service';
		$filtered_list['list']=$this->db->get('support_services')->result();
		return $filtered_list;
	}
	
	public function insert_support_chat()
	{
		$service_id=$this->input->post('service_id');
		$cust_id=$this->input->post('cust_id');
		$message=$this->input->post('message');
		
		$data=array(
		'service_id'=>$service_id,
		'cust_id'=>$cust_id,
		'message'=>$message
		);
		
		return $this->db->insert('support_chat',$data);
	}
	
	public function get_address_list($cust_id)
	{
		$this->db->select('address_id,address,pi.pincode,ca.area as area_name,s.state,c.city');
		$this->db->from('city c');
		$this->db->from('state s');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		//$this->db->join('area a','a.area_id=ca.area');
		//$this->db->join('city c','c.city_id=a.city_id');
		//$this->db->join('state s','s.state_id=c.state_id');
		$this->db->where('cust_id',$cust_id);
		return $this->db->get('customer_address ca')->result();
	}
	
	public function get_pincode_list()
	{
		$this->db->select('pincode_id,pincode')->order_by('pincode');
		$filtered_list['select_Pincode']='Select Pincode';
		$filtered_list['list']=$this->db->get('pincode')->result();
		return $filtered_list;
	}
	
	public function get_customer_data_for_drawer($cust_id)
	{
		$this->db->select('cust_id,first_name,last_name,user_photo');
		$this->db->where('cust_id',$cust_id);
		return $this->db->get('customer')->result();
	}
	
	public function get_promocode()
	{
		$this->db->select('promocode,discount');
		$this->db->where('promo_type_id',4);
		$this->db->where('status',1);
		return $this->db->get('promocode')->row();
	}

        public function authenticate_user()
    {
        $mobile=$this->input->post('mobile');
        $password=md5($this->input->post('password'));

        $this->db->select('cust_id,first_name,last_name,user_photo');
        $this->db->where('mobile',$mobile);
        $this->db->where('password', $password);
        $data =$this->db->get('customer')->row();
        return $data;
        
    }
    
    public function check_email()
    {
    	$this->db->select('cust_id');
    	$this->db->where('email',$this->input->post('email'));
    	return $this->db->get('customer')->row();
    }
    
    public function update_password_for_forgot_password(){
		
		$password = $this->input->post('password');
		$mobile = $this->input->post('mobile_number');
		
		$this->db->trans_begin();
		
		$data = array(
			'password'=>md5($password),
		);
		$this->db->where('mobile', $mobile);
		$this->db->update('customer', $data);
		$status = $this->db->affected_rows();
		if($this->db->trans_status()===FALSE){
			return 0;
		}
		else {
			$this->db->trans_commit();
			return $status;
		}
	}

}