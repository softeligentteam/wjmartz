<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parentcategorymodel extends CI_Model
{
	public function insert_parent_categories()
	{
		$parent_cat_name=$this->input->post('parent_category');
		$parent_cat_img=$this->upload_parent_cat_photo();
		
		
		$data=array(
		'parent_cat_name'=>$parent_cat_name,
		'parent_cat_img_url'=>$parent_cat_img
		);
		
		return $this->db->insert('parent_category',$data);
	}
	
	
	function upload_parent_cat_photo() {
    	$file_url = "";

			if ($_FILES["parent_cat_img"]["error"] == 0) {

				$file_element_name = 'parent_cat_img';

				$config['upload_path'] = PARENTCATUPLOADPATH;
		        $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
		        $config['max_size'] = 300;
		         $config['max_width'] = 900;
		         $config['max_height'] = 700;
			  $config['min_width'] = 900;
		         $config['min_height'] = 700;
		        $config['encrypt_name'] = TRUE;

		        $this->load->library('upload', $config);

		        if (!$this->upload->do_upload($file_element_name)) {
		        	$file_url = PARENTCATUPLOADPATH."/no_image.jpg";
		           // $msg = $this->upload->display_errors('', '');
		            // echo "<script>console.log($msg);</script>";
		        } else {
		        	$data = $this->upload->data();
		            $file_url = PARENTCATUPLOADPATH.$data['file_name'];
		            // echo "<script>console.log('File uploaded successfully.');</script>";
		        }

		        @unlink($_FILES[$file_element_name]);

			} else {
				 
				$file_url = PARENTCATUPLOADPATH."/no_image.jpg";
			}
			
			return $file_url;
    }
	public function get_parent_cat_list($per_page)
	{	
		$this->db->select('parent_cat_id,parent_cat_name,parent_cat_img_url');
		$this->db->where_not_in('parent_cat_id',0);
		//$this->db->where('main_cat_id',$main_cat_id);
		return $this->db->get('parent_category',$per_page, $this->uri->segment(3))->result();
	}
	
	public function get_no_of_rows()
	{
		return $this->db->get('parent_category')->num_rows();
	}
	
	public function del_parent_category($parent_cat_id)
	{
		$this->db->where('parent_cat_id',$parent_cat_id);
		return $this->db->delete('parent_category');
	}
	/***** KC[date:-24/04/18] *****/
	public function update_parent_category()
	{
		$parent_cat_id=$this->input->post('parent_cat_id');
		$data=array(
			'parent_cat_name'=>$this->input->post('parent_cat_name')
		);

		if($_POST['parent_cat_img'] !== null)
			$data['parent_cat_img_url'] = $this->input->post('parent_cat_img');

		$this->db->where('parent_cat_id',$parent_cat_id);
		$this->db->update('parent_category', $data);
		return $this->db->affected_rows();
	}


	/***** KT[date:-24/04/18] *****/
	public function add_section_wise_pc()
    {
        $updateArray = array(
            'parent_cat_id'=>$this->input->post('parent_cat')
        );
        $this->db->where('screen_id',$this->input->post('section1'));
        return $this->db->update('home_screen_section',$updateArray);
    }

	public function get_sectionwise_parent_categories()
	{
		$this->db->select('screen_id,section,hs.parent_cat_id,parent_cat_name,parent_cat_img_url')->order_by('screen_id','asc');
		$this->db->join('parent_category pc','pc.parent_cat_id=hs.parent_cat_id');
		$this->db->where('hs.status',1);
		return $this->db->get('home_screen_section hs')->result();
	}
	//PJ[19-04-18]
	public function get_parent_cat_image_path()
	{
		$this->db->select('parent_cat_img_url')->where('parent_cat_id', $this->input->post('parent_cat_id'));
		return $this->db->get('parent_category')->result();
	}
	
	/***** KT[date:-24/04/18] *****/
	public function get_section_pat_cat()
    {
        $this->db->select('section,parent_cat_name,screen_id,hs.parent_cat_id');
        $this->db->join('parent_category pc','pc.parent_cat_id=hs.parent_cat_id');
        $this->db->where('hs.status',1);
        return $this->db->get('home_screen_section hs')->result();
    }
	/***** KT[date:-24/04/18] *****/
	public function parent_cat_is_exist($pcat)
    {
        $this->db->select('section');
        $this->db->where('parent_cat_id',$pcat);
        return $this->db->get('home_screen_section hs')->row();
    }
}