<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ddlmodel extends CI_Model
{
	public function get_parent_category_list()
	{
		$this->db->where_not_in('parent_cat_id ',0)->order_by ( 'parent_cat_name' );
		$list = $this->db->get ( 'parent_category' )->result ();
		$filtered_list [''] = 'Select Parent Category ';
		if (count ( $list ) > 0) {
			foreach ( $list as $object ) {
				$filtered_list [$object->parent_cat_id] = $object->parent_cat_name;
			}
		}
		return $filtered_list;
	
	}
	
	public function get_main_category_list()
	{
		$this->db->where_not_in('main_cat_id ',0)->order_by( 'main_cat_name' );
		$this->db->where('status !=', 0);
		$list = $this->db->get ( 'main_category' )->result();
		
		$filtered_list [''] = 'Select Main Category ';
		if (count ( $list ) > 0) {
			foreach ( $list as $object ) {
				$filtered_list [$object->main_cat_id] = $object->main_cat_name;
			}
		}
		return $filtered_list;
	
	}
	
	public function state_list()
	{
		$this->db->select('state_id,state')->order_by('state');
		$this->db->where_not_in('state_id',0);
		$filtered_list['select_state']='Select State';
		$filtered_list['list']=$this->db->get('state')->result();
		return $filtered_list;
	}
	
	public function city_list()
	{
		$this->db->select('city_id,city,state_id')->order_by('city');
		$this->db->where_not_in('city_id',0);
		$filtered_list['select_city']='Select City';
		$filtered_list['list']= $this->db->get('city')->result();
		return $filtered_list;
	}
	
	public function area_list()
	{
		$this->db->select('area_id,area_name,city_id,pincode_id')->order_by('area_name');
		$this->db->where_not_in('area_id',0);
		$filtered_list['select_area']='Select Area';
		$filtered_list['list']=$this->db->get('area')->result();
		return $filtered_list;
	
	}
	
	public function pincode_list()
	{
		$this->db->select('pincode_id,pincode')->order_by('pincode');
		$this->db->where_not_in('pincode_id',0);
		$this->db->where('status !=', 0);
		$filtered_list['select_pincode']='Select Pincode';
		$filtered_list['list']=$this->db->get('pincode')->result();
		return $filtered_list;
	}
	
	public function get_psize_list()
	{
		$this->db->select('size_id, size')->order_by('size');
		return $this->db->get('product_sizes')->result();
	}
	
	public function get_pcolor_list()
	{
		$this->db->select('color_id, color_name')->order_by('color_name');
		return $this->db->get('product_colors')->result();
	}
	/**** KT[25-04-18] ****/
	public function get_ptype_list()
	{
		$this->db->select('ptype_id, ptype')->order_by('ptype_id');
		return $this->db->get('product_types')->result();
	}
	
	public function get_psize_list_for_filteration($pcat_id)
	{
		$this->db->select('size_id, size')->order_by('size');
		$this->db->where('parent_cat_id',$pcat_id);
		return $this->db->get('product_sizes')->result();
	}

	public function get_order_status()
	{
		$this->db->select('ord_status_id,order_status')->order_by('ord_status_id');
		return $this->db->get('order_status')->result();
		
	}
}