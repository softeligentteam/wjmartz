<div class="col-md-3 desktopView">
	<div class="community-poll">
		<h4><i class="fa fa-bars"></i>&nbsp;&nbsp;MENU</h4>
		<div class="swit form">	
			<div class="check_box">
				<a href="<?=site_url();?>/user">
					<i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;My orders
				</a>
			</div>
			<hr style="margin:7px 0;">
			<div class="check_box">
				<a href="<?=site_url();?>/user/view_return_orders">
					<i class="fa fa-shopping-bag" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Return Orders
				</a>
			</div>
			<hr style="margin:7px 0;">
			<div class="check_box">
				<a href="<?=site_url();?>/user/view_my_profile">
					<i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;My Profile
				</a>
			</div>	
			<hr style="margin:7px 0;">
			<div class="check_box">
				<a href="<?=site_url();?>/user/change_password">
					<i class="fa fa-unlock-alt" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Change Password
				</a>
			</div>
			<hr style="margin:7px 0;">
			<div class="check_box">
				<a href="#" onclick="$('#logoutModal').slideDown();">
					<i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Logout
				</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>