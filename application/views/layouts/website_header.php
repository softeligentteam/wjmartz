<!DOCTYPE html>
<html>
<head>
	<title>WJmartz | Weaving Joy</title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="On line Shopping" />
	<script type="application/x-javascript">
		addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } 
	</script>
	<!--//tags -->
	<link href="<?=base_url();?>website_assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?=base_url();?>website_assets/css/custom.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?=base_url();?>website_assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?=base_url();?>website_assets/css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?=base_url();?>website_assets/css/font-awesome.css" rel="stylesheet"> 
	<link href="<?=base_url();?>website_assets/css/easy-responsive-tabs.css" rel='stylesheet' type='text/css'/>
	<!-- //for bootstrap working -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="application/x-javascript">
		var siteURL = '<?=site_url();?>/';
		var baseURL = '<?=base_url();?>/';
		var past = window.location.href;
	</script>
</head>
<style>
	.dropdown:hover .dropdown-menu {
		display: block;
		margin-top: 0; 
	 }
	.nopad{}
	 @media(max-width:768px){
		.nopad{
			padding:0 !important;
		}
	 }
</style>
<script>
	function nocontext(e) {
		var clickedTag = (e==null) ? event.srcElement.tagName : e.target.tagName;
		if (clickedTag == "IMG") {
			// alert(alertMsg);
			return false;
		}
	}
	var alertMsg = "Image context menu is disabled";
	document.oncontextmenu = nocontext;
</script>
<body>

<!-- common modal -->
<div class="modal fade" id="commonModal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body modal-body-sub_agile">
				<div class="col-md-12 modal_body_left modal_body_left1">
					<h3 class="agileinfo_sign"><span id="commonModelTitle"></span></h3>
					<p id="commonModelMessage"></p><br /><br />
				</div>
				<div class="clearfix"></div>
				<div class="col-md-12 modal_body_left modal_body_left1">
					<span id="commonModelButtons"></span>
					<button type="button" class="btn btn-default" data-dismiss="modal" style="color:#fff; background-color:#000; border-radius:none;">Cancel</button>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- //Modal content-->
	</div>
</div>
<!-- //common modal-->

<div class="header-bot desktopView">
	<div class="header-bot_inner_wthreeinfo_header_mid">
		<div class="col-md-4">
			<h1>
				<a href="<?=site_url();?>/website_home">
					<img src="<?=base_url();?>/website_assets/images/logo.png" class="cust_logo_img" />
				</a>
			</h1>
		</div>
		<div class="col-md-offset-1 col-md-7 header-middle">
			
			<div class="header hidden-xs" id="home">
				<div class="">
					<ul>
						<?php if(NULL == $this->session->userdata('user_id')){?>
							<li> <a href="#" data-toggle="modal" data-target="#signinModal"><img src="<?=base_url();?>website_assets/images/white-icons/login.png" style="width: 12px; margin-top: -4px;" /> Log In </a></li>
							<li  style="border-right: none;"> <a href="#" data-toggle="modal" data-target="#regmodal"><img src="<?=base_url();?>website_assets/images/white-icons/register.png" style="width: 12px; margin-top: -4px;" /> Register</a></li>
							<li><a href="<?=site_url();?>/website_home/contact"><img src="<?=base_url();?>website_assets/images/white-icons/contact.png" style="width: 12px; margin-top: -4px;" /> Contact Us</a></li>
							<li><a href="#mailto:support@wjmartz.com"><img src="<?=base_url();?>website_assets/images/white-icons/mail.png" style="width: 12px; margin-top: -4px;" /> support@wjmartz.com</a></li>
						<?php }else{ ?>
							<li><a href="<?=site_url();?>/user"><img src="<?=base_url();?>website_assets/images/white-icons/user.png" style="width: 12px; margin-top: -4px;" /> Welcome <?=$this->session->userdata('name')?> </a></li>
							<li><a href="<?=site_url();?>/website_home/contact"><img src="<?=base_url();?>website_assets/images/white-icons/contact.png" style="width: 12px; margin-top: -4px;" /> Contact Us</a></li>
							<li><a href="#mailto:support@wjmartz.com"><img src="<?=base_url();?>website_assets/images/white-icons/mail.png" style="width: 12px; margin-top: -4px;" /> support@wjmartz.com</a></li>
							<li style="text-align:center;"><a href="#" onclick="$('#logoutModal').slideDown();"><img src="<?=base_url();?>website_assets/images/white-icons/login.png" style="width: 12px; margin-top: -4px;" /> Logout </a></li>
						<?php }?>
					</ul>
				</div>
			</div>
			<form action="<?=site_url()?>/website_home/search_product_list" method="post">
					<input type="search" name="search" placeholder="Search here with the product name..." required="">
					<input type="submit" value=" ">
					<a href="<?=site_url();?>/cartcontroller">
						<button type="button" style="color:white; background: none; border: none; font-size: 25px;">
							<i class="fa fa-cart-arrow-down" aria-hidden="true"></i> 
							<?php if(count($this->cart->contents()) != 0){?>
							<p style="position: relative; top: -40px; right: -15px; background-color: #a5dce8; color:#000; font-size: 15px; padding: 0px 5px; border-radius: 10px;">
								<?php echo count($this->cart->contents());?>
							</p>
							<?php }?>
						</button>
					</a>
				<div class="clearfix"></div>
			</form>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //header-bot -->

<!-- Sign-in modal -->
<div class="modal fade" id="signinModal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<?=form_open('LoginController/authenticate_and_login', 'onsubmit="return validateLogin();"')?>
			<div class="modal-body modal-body-sub_agile">
				<div class="col-md-8 modal_body_left modal_body_left1">
					<h3 class="agileinfo_sign">Sign In <span>Now</span></h3>
					<form action="#" method="post">
						<div class="styled-input agile-styled-input-top">
							<input type="number" name="mobNo" id="mobNo" required="">
							<label>Mobile</label>
							<p style="color:red; font-size:12px;" class="err_" id="err_mobNo"></p>
						</div>
						<div class="styled-input">
							<input type="password" maxlength="15" minlength="6" name="pass" id="pass" required=""> 
							<label>Password</label>
							<p style="color:red; font-size:12px;" class="err_" id="err_pass"></p>
						</div> 
						<input type="submit" value="Sign In">
					</form>
					<div class="clearfix"></div>
					<p style="margin-top:7px;"><a href="<?=site_url();?>/user_registration/forgot_password">Forgot your password ? Click here !</a></p>
					<p style="margin-top:7px;"><a href="#" data-toggle="modal" data-target="#regmodal,#signinModal" > Don't have an account? Register now!</a></p>
				</div>
				<div class="col-md-4 modal_body_right modal_body_right1 desktopView">
					<img src="<?=base_url();?>/website_assets/images/log_pic.jpg" alt=" "/>
				</div>
				<div class="clearfix"></div>
			</div>
			<?=form_close()?>
		</div>
		<!-- //Modal content-->
	</div>
</div>
<!-- //Sign-in modal-->

<!-- Register User Modal -->
<div class="modal fade" id="regmodal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body modal-body-sub_agile">
				<div class="col-md-8 modal_body_left modal_body_left1">
					<?=form_open('user_registration/register_user_and_log_in')?>
						<div id="reg_details" style="<?php if(null != $this->session->userdata('reg_mobile')){echo 'display:none;';}?>">
							<h3 class="agileinfo_sign">Sign Up <span>Now</span></h3>
								<div class="row">
									<div class="col-sm-6 nopad">
										<div class="styled-input agile-styled-input-top">
											<input type="text" name="reg_fname" id="reg_fname" required="" value="<?php if(null != $this->session->userdata('reg_mobile')){echo $this->session->userdata('reg_fname');}?>" />
											<label>First Name</label>
											<p style="color:red; font-size:12px;" class="err_" id="err_reg_fname"></p>
										</div>
									</div>
									<div class="col-sm-6 nopad">
										<div class="styled-input agile-styled-input-top">
											<input type="text" name="reg_lname" id="reg_lname" required="" value="<?php if(null != $this->session->userdata('reg_mobile')){echo $this->session->userdata('reg_lname');}?>" />
											<label>Last Name</label>
											<p style="color:red; font-size:12px;" class="err_" id="err_reg_lname"></p>
										</div>
									</div>
								</div>
								<div class="styled-input  agile-styled-input-top">
									<input type="email" name="reg_email" id="reg_email" placeholder = "email" required="" value="<?php if(null != $this->session->userdata('reg_mobile')){echo $this->session->userdata('reg_email');}?>" /> 
									<!--<label>Email</label>-->
									<p style="color:red; font-size:12px;" class="err_" id="err_reg_email"></p>
								</div> 
								<div class="styled-input  agile-styled-input-top">
									<input onchange="updateLabel();" type="number" placeholder = "Mobile No" name="reg_mobile" id="reg_mobile" required="" value="<?php if(null != $this->session->userdata('reg_mobile')){echo $this->session->userdata('reg_mobile');}?>" /> 
									<!--<label>Mobile No</label>-->
									<p style="color:red; font-size:12px;" class="err_" id="err_reg_mobile"></p>
								</div> 
								<div class="styled-input">
									<input type="password" onblur="validate_pass();" name="reg_password" id="reg_password" required="" value="<?php if(null != $this->session->userdata('reg_mobile')){echo $this->session->userdata('reg_password');}?>" />
									<label>Password</label>
									<p style="color:red; font-size:12px;" class="err_" id="err_reg_password"></p>
								</div> 
								<input type="button" id="throwOTP" value="Continue Signing Up">
							<div class="clearfix"></div>
							<p style="margin-top:7px;"><a href="<?=site_url();?>/website_home/terms_conditions">By clicking 'Sign Up', I agree to your terms and conditions.</a></p>
						</div>
						
						<div id="reg_otp_verification" style="<?php if(null != $this->session->userdata('otp') && null == $this->session->userdata('otpVerified') ){echo 'display:block;';}else{echo'display:none;';}?>">
							<h3 class="agileinfo_sign">Verify <span>Mobile Number</span></h3>
							<p>A text with your code has been sent to your mobile number:
							<b id="regMobileNo"><?=$this->session->userdata('reg_mobile');?></b><br />
							(<a href="#" onclick="changeRegDetails();"><u>Change Registration Details</u></a>)</p>
							<br />
							<div class="styled-input agile-styled-input-top">
								<input type="text" id="regOtp" name="regOtp">
								<label>Enter OTP</label>
								<span></span>
							</div>
							<br />
							<input type="button" value="Verify Mobile No." onclick="verifyOtp();">
						</div>
						
						<div id="reg_address" style="<?php if(NULL != $this->session->userdata('otpVerified')){echo'display:block;';}else{echo'display:none;';}?>">
							<span id="abcd">
							
							</span>
							<h3 class="agileinfo_sign">Update <span>Primary Address</span></h3>
							<div id="pincodeSet" style="display:;">
								<div class="styled-input agile-styled-input-top">
									<input type="text" placeholder="Maharashtra" readonly /> 
									<span></span>
								</div>
								<div class="styled-input agile-styled-input-top">
									<input type="text" placeholder="Pune" readonly />
								</div>
								<div class="styled-input agile-styled-input-top" >
									<input type="text" id="regArea" name="regArea" placeholder="Enter Area / Landmark..." required="" />
									<span></span>
								</div>								
								<div class="styled-input agile-styled-input-top">
									<!--
									<input type="text" id="regPincode" name="regPincode" required="" onblur="pincodeEntered()" />
									<input type="text" name="regPincodeId" id="regPincodeId" hidden readonly required="" />
									<label>Enter Pincode</label>
									-->
									<?=form_dropdown('regPin', $pincodes, set_value('pincodes'), array('id'=>'regPin', 'class'=>'form-control','required'=>'required'))?>
									<p style="color:red; font-size:12px;" id="err_pincode"></p>
								</div>
							</div>
							<div class="styled-input agile-styled-input-top">
								<input type="text" name="regAddress" required="" />
								<label>Address</label>
								<span></span>
							</div>
							<br />
							<input type="submit" value="Sign Up">
							<p style="float:right;">(<a href="#" onclick="changeRegDetails();"><u>Change Registration Details</u></a>)</p>
						</div>
					<?=form_close()?>
				</div>
				<div class="col-md-4 modal_body_right modal_body_right1  desktopView">
					<img src="<?=base_url();?>/website_assets/images/log_pic2.jpg" alt=" "/>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- //Modal content-->
	</div>
</div>
<!-- //Register User Modal -->

<!-- START Responsive Chnages from alex : 12-05-2018-->
<div class="header-bot mobileView" style="margin:0; padding:0;position:fixed;z-index:999;width:100%; background-color:white; box-shadow: 0px 1px 7px #bbb;">
	<div id="mmenu">
		<span class="mmenuBtn" onclick="slideMmenu();" style="z-index:2;color: black; padding: 0px 10px; border: 1px solid #efefef; border-radius: 4px;">&#88;</span>
		<script>
			var mmenu = 0;
			function slideMmenu(){
				if($('#mmenu').css('left') == '0px'){
					$('#mmenu').css('box-shadow', 'none');
					$("#mmenu").animate({left: '-82%'});
				}else{
					$('#mmenu').css('box-shadow', '25px 0 50px gray');
					$("#mmenu").animate({left: '0px'});
				}
			}
		</script>
		<div class="row">
			<div class="col-sm-12 col-xs-12 text-center"><br />
				<img src="<?=base_url();?>/website_assets/images/icon.png" style="width: 100px; border:2px solid white">
				<hr />
			</div>
			<div class="col-sm-offset-1 col-xs-offset-1 col-sm-11 col-xs-11" id="mmenuItems">
				<?php if(NULL != $this->session->userdata('user_id')){?>
					<a href="<?=site_url();?>/user/"><img src="<?=base_url()?>/website_assets/images/Icons/order_history.png" style="width:22px;" />&nbsp;&nbsp; My Order</a><br />
					<a href="<?=site_url();?>/user/view_return_orders"><img src="<?=base_url()?>/website_assets/images/Icons/order_history.png" style="width:22px;" />&nbsp;&nbsp;Return Orders</a><br />
					<a href="<?=site_url();?>/user/view_my_profile"><img src="<?=base_url()?>/website_assets/images/Icons/update_profile.png" style="width:22px;" />&nbsp;&nbsp;My Profile</a><br />
					<a href="<?=site_url();?>/user/change_password"><img src="<?=base_url()?>/website_assets/images/Icons/change_password.png" style="width:22px;" />&nbsp;&nbsp;Change Password</a><br />
					<a href="<?=site_url();?>/website_home/about"><img src="<?=base_url()?>/website_assets/images/Icons/aboutus.png" style="width:22px;" />&nbsp;&nbsp;About Us</a><br />
					<a href="<?=site_url();?>/website_home/contact"><img src="<?=base_url()?>/website_assets/images/Icons/contactus.png" style="width:22px;" />&nbsp;&nbsp;Contact Us</a><br />
					<a href="<?=site_url();?>/website_home/terms_conditions"><img src="<?=base_url()?>/website_assets/images/Icons/termsnconditions.png" style="width:22px;" />&nbsp;&nbsp;Terms & Conditions</a><br />
					<a href="<?=site_url();?>/website_home/privacy_policies"><img src="<?=base_url()?>/website_assets/images/Icons/termsnconditions.png" style="width:22px;" />&nbsp;&nbsp;Return & Exchange Policies</a><br />
					<a href="#" onclick="$('#logoutModal').slideDown();"><img src="<?=base_url()?>/website_assets/images/Icons/logout.png" style="width:22px;" />&nbsp;&nbsp;Logout</a>
				<?php }else {?>
					<a href="<?=site_url();?>/website_home/about"><img src="<?=base_url()?>/website_assets/images/Icons/aboutus.png" style="width:22px;" />&nbsp;&nbsp;About Us</a><br />
					<a href="<?=site_url();?>/website_home/contact"><img src="<?=base_url()?>/website_assets/images/Icons/contactus.png" style="width:22px;" />&nbsp;&nbsp;Contact Us</a><br />
					<a href="<?=site_url();?>/website_home/terms_conditions"><img src="<?=base_url()?>/website_assets/images/Icons/termsnconditions.png" style="width:22px;" />&nbsp;&nbsp;Terms & Conditions</a><br />
					<a href="<?=site_url();?>/website_home/privacy_policies"><img src="<?=base_url()?>/website_assets/images/Icons/termsnconditions.png" style="width:22px;" />&nbsp;&nbsp;Return & Exchange Policies</a><br />
					<a href="#" data-toggle="modal" data-target="#regmodal"><img src="<?=base_url()?>/website_assets/images/Icons/update_profile.png" style="width:22px;" />&nbsp;&nbsp;Register New</a>
					<a href="#" data-toggle="modal" data-target="#signinModal"><img src="<?=base_url()?>/website_assets/images/Icons/login.png" style="width:22px;" />&nbsp;&nbsp;Log In</a>
				<?php }?>
			</div>
		</div>
	</div>
	<div class="header-bot_inner_wthreeinfo_header_mid">
		<div class="col-xs-6 col-sm-6">
		    <span class="mmenuBtn" onclick="slideMmenu();" style="color: black;">&#9776;</span>
			<a href="<?=site_url();?>/website_home">
			<img src="<?=base_url();?>/website_assets/images/logo2.png" style="max-width:130px; margin:12px 0 0 20px;" />
			</a>
		</div>
		<div class="col-xs-6 col-sm-6">
			<a href="<?=site_url();?>/cartcontroller" style="float:right;">
				<button type="button" style="color:white; background: none; border: none; font-size: 2rem;height: 42px;">
					<img src="<?=base_url();?>/website_assets/images/cart.png" style="width: 22px; margin-top: -10px;" />
					<?php if(count($this->cart->contents()) != 0){?>
					<p style="position: relative; top: -40px; right: -15px; background-color: #a5dce8; color:#000; font-size: 12px; padding: 0; border-radius: 10px;">
						<?php echo count($this->cart->contents());?>
					</p>
					<?php }?>
				</button>
			</a>
			<button id="mmenuSearchBtn" type="button" onclick="$('#searchmm').slideToggle();">
				<img src="<?=base_url();?>/website_assets/images/search2.png" style="width: 22px; margin-top: -7px;" />
			</button>
			<form id="searchmm" action="<?=site_url()?>/website_home/search_product_list" method="post" style="display:none;position: fixed; left: 0; width: 100%; top: 40px; background-color: #fff; padding: 10px 0;">
				<input type="search" class="form-control" name="search" placeholder="Search here with the product name..." required="" style="width: 86%; float: left; margin-left: 1%;">
				<button type="submit" class="btn btn-xs btn-info" style="padding: 2px 5px; border-radius: 4px; background-color: #a5dce8; border-left: none; margin-left: 3px;">
				    <img src="<?=base_url();?>/website_assets/images/search2.png" style="width:27px;" />
			    </button>
			</form>
				
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- END Responsive Chnages from alex : 12-05-2018-->