<div class="desktopView">
    <div class="page-head_agile_info_w3l">
		<div class="container">
			<h3>User Profile</h3>
			<!--/w3_short-->
			 <div class="services-breadcrumb">
				<div class="agile_inner_breadcrumb">
					<ul class="w3_short">
						<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
						<li>Dashboard<i>|</i></li>
						<li>Order History</li>
					</ul>
				</div>
			</div>
    	   <!--//w3_short-->
    	</div>
    </div> 
    <div class="banner-bootom-w3-agileits" style="min-height:300px;">
    	<div class="container">
    		<div class="row">
    			<?php $this->load->view('layouts/userNav')?>
    			<div class="col-md-9">
    				<div class="community-poll">
    					<table class="table table-striped" id="profile" style="margin-bottom:0;">
    						<tbody>
    							<tr>
    								<th>Name</th>
    								<td>
    									<div class="row">
    										<div class="col-sm-11">
    											<span id="disp_name" class="disp"><?=$profile['first_name'].' '.$profile['last_name']?></span>
    											<div class="row edit_" id="input_name"  style="display:none;" >
    												<?=form_open('UpdateProfile/update_name', 'onsubmit=\'return validateUpdateProfileForm("cname");\'')?>
    												<div class="col-md-5" style="padding: 0;">
    													<input type="text" id="cfname" name="change_fname" value="<?=$profile['first_name']?>" class="form-control" required style="width:98%;"/>
    													<p style="color: red;position: absolute;margin-left: 5px;" id="errCfname"></p>
    												</div>
    												<div class="col-md-5" style="padding: 0;">
    													<input type="text" id="clname" name="change_lname" value="<?=$profile['last_name']?>" class="form-control" required />
    													<p style="color: red;position: absolute;margin-left: 5px;" id="errClname"></p>
    												</div>
    												<div class="col-md-2">
    													<input type="submit" value="update" class="btn btn-info" style="width:100%" />
    												</div>
    												<?=form_close()?>
    											</div>
    										</div>
    										<div class="col-sm-1">
    											<i class="fa fa-pencil pull-right e" aria-hidden="true" id="edit_name" onclick="updd('name');"></i>
    											<i class="fa fa-close pull-right c" aria-hidden="true" id="close_name" onclick="updu('name');" style="display:none;"></i>
    										</div>
    									</div>
    								</td>
    							</tr>
    						<!--	<tr>
    								<th>D.O.B</th>
    								<td>
    									<div class="row">
    										<div class="col-sm-11">
    											<span id="disp_dob" class="disp"><?=$profile['dob']?></span>
    											<div class="row edit_" id="input_dob"  style="display:none;" >
    												<?=form_open('UpdateProfile/update_dob', 'onsubmit=\'return validateUpdateProfileForm("cdob");\'')?>
    												<div class="col-md-10" style="padding: 0;">
    													<input type="date" id="cdob" name="change_dob" value="<?=$profile['dob']?>" class="form-control" />
    													<p style="color: red;position: absolute;margin-left: 5px;" id="errCdob"></p>
    												</div>
    												<div class="col-md-2">
    													<input type="submit" value="update" class="btn btn-info" style="width:100%" />
    												</div>
    												<?=form_close()?>
    											</div>
    										</div>
    										<div class="col-sm-1">
    											<i class="fa fa-pencil pull-right e" aria-hidden="true" id="edit_dob" onclick="updd('dob');"></i>
    											<i class="fa fa-close pull-right c" aria-hidden="true" id="close_dob" onclick="updu('dob');" style="display:none;"></i>
    										</div>
    									</div>
    								</td>
    							</tr>-->
    							<tr>
    								<th>Email&nbsp;Id</th>
    								<td>
    									<div class="row">
    										<div class="col-sm-11">
    											<span id="disp_email" class="disp"><?=$profile['email']?></span>
    											<div class="row edit_" id="input_email"  style="display:none;" >
    												<?=form_open('UpdateProfile/update_email', 'onsubmit=\'return validateUpdateProfileForm("cemail");\'')?>
    												<div class="col-md-10" style="padding: 0;">
    													<input type="text" id="cemail" name="change_email" value="<?=$profile['email']?>" class="form-control" />
    													<p style="color: red;position: absolute;margin-left: 5px;" id="errCemail"></p>
    												</div>
    												<div class="col-md-2">
    													<input type="button" id="checke" value="Submit" onclick="update_email();" class="btn btn-info" style="width:100%" />
    													<input type="submit" id="updtEmail" style="display:none" value="update" class="btn btn-success" style="width:100%" />
    												</div>
    												<?=form_close()?>
    											</div>
    										</div>
    										<div class="col-sm-1">
    										<!--	<i class="fa fa-pencil pull-right e" aria-hidden="true" id="edit_email" onclick="updd('email');"></i>-->
    											<i class="fa fa-close pull-right c" aria-hidden="true" id="close_email" onclick="updu('email');" style="display:none;"></i>
    										</div>
    									</div>
    								</td>
    							</tr>
    							<tr>
    								<th>Mobile&nbsp;Number</th>
    								<td>
    									<div class="row">
    										<div class="col-sm-11">
    											<span id="disp_mobile" class="disp"><?=$profile['mobile']?></span>
    											<div class="row edit_" id="input_mobile"  style="display:none;" >
    												<?=form_open('UpdateProfile/update_mobile', 'onsubmit=\'return validateUpdateProfileForm("cnumber");\'')?>
    												<div class="col-md-10" style="padding: 0;">
    													
    													<input type="number" id="cnumber" name="change_number" value="<?=$profile['mobile']?>" class="form-control" />
    													
    													<input type="number" id="cotp" value="Enter otp sent to the new number." placeholder="Enter otp sent to the new number : " class="form-control" style="display:none;" />
    													
    													<p style="color: red;position: absolute;margin-left: 5px;" id="errCmobile"></p>
    												
    												</div>
    												<div class="col-md-2" id="upNumBtn">
    													
    													<input type="button" id="check" value="Submit" onclick="verify_number($('#cnumber').val());" class="btn btn-info" style="width:100%" />
    													
    													<input type="submit" id="updtNum" style="display:none; width:100%" value="Verify OTP" class="btn btn-success" style="width:100%" />
    													
    												</div>
    												<?=form_close()?>
    											</div>
    										</div>
    										<div class="col-sm-1">
    											<i class="fa fa-pencil pull-right e" aria-hidden="true" id="edit_mobile" onclick="updd('mobile');"></i>
    											<i class="fa fa-close pull-right c" aria-hidden="true" id="close_mobile" onclick="updu('mobile');" style="display:none;"></i>
    										</div>
    									</div>
    								</td>
    							</tr>
    							
    							<!--Start loop-->
    							
    							<?php $i=0; foreach($addresses as $add){ $i++;?>
    							<tr>
    								<th>Address <?=$i?></th>
    								<td>
    									<div class="row">
    										<div class="col-sm-11">										
    											<span id="disp_address<?=$i?>" class="disp"><?=$add->address.', '.$add->area.', '.$add->city.', '.$add->state.' - '.$add->pincode.'.'?></span>
    											<div class="row edit_" id="input_address<?=$i?>"  style="display:none;" >
    												<?=form_open('UpdateProfile/update_address', 'onsubmit=\'return validateUpdateProfileForm("caddress");\'')?>
    													<div class="row">
    														<input type="text" hidden name="addressId" value="<?=$add->address_id?>" />
    														<div class="col-md-6 sol-xs-12">
    															<?=form_dropdown('state', $states, set_value('state',$add->state_id), array('id'=>'chState'.$i, 'class'=>'form-control','required'=>'required'))?>
    															
    														</div>
    														<div class="col-md-6 sol-xs-12">
    														<?=form_dropdown('city', $cities, set_value('city',$add->city_id), array('id'=>'chCity'.$i, 'class'=>'form-control','required'=>'required'))?>
    															<br />
    														</div>
    														<div class="clearfix"></div>
    														<div class="col-md-6 sol-xs-12">
    															<?=form_dropdown('chPin', $pincodes, set_value('pincodes',$add->pincode_id), array('id'=>'chpinId'.$i, 'class'=>'form-control','required'=>'required'))?>
    															<br />
    															<p style="position: absolute;bottom: 3px;color: red;" id="err_pin<?=$i?>"></p>
    														</div>
    														<div class="col-md-6 sol-xs-12">
    															<input type="text" value="<?=$add->area?>" id="chArea<?=$i?>" name="chArea" class="form-control" required/>
    														</div>
    														<div class="clearfix"></div>
    														<div class="col-md-12 sol-xs-12">
    															<textarea name="chAddress" required class="form-control"><?=$add->address?></textarea><br />
    															<input type="submit" value="update" class="btn btn-info" />
    														</div>
    													</div>
    												<?=form_close()?>
    											</div>											
    										</div>
    										<div class="col-sm-1">
    											<i class="fa fa-pencil pull-right e" aria-hidden="true" id="edit_address<?=$i?>" onclick="updd('address<?=$i?>');"></i>
    											<i class="fa fa-close pull-right c" aria-hidden="true" id="close_address<?=$i?>" onclick="updu('address<?=$i?>');" style="display:none;"></i>
    										</div>
    									</div>
    								</td>
    							</tr>
    							<?php }?>
    							<?php if($address_count->address_count < 3){?>
    							<tr>
    								<th>
    									<span id="addNewAddLbl" style="display:none;">New Address</span>
    								</th>
    								<td>
    									<div class="row">
    										<div class="col-sm-11">
    											<div id="addNewAddSec" style="display:none;">
    												<?=form_open('UpdateProfile/add_new_address')?>
    													<div class="row">
    														<div class="col-md-6 sol-xs-12">
    															<?=form_dropdown('addstate', $states, set_value('addstate'), array('class'=>'form-control','required'=>'required'))?>
    														</div>
    														<div class="col-md-6 sol-xs-12">
    															<?=form_dropdown('addcity', $cities, set_value('addcity'), array('class'=>'form-control','required'=>'required'))?>
    															<br />
    														</div>
    														<div class="clearfix"></div>
    														<div class="col-md-6 sol-xs-12">
    															<?=form_dropdown('addPin', $pincodes, set_value('pincodes'), array('id'=>'chpinId0', 'class'=>'form-control','required'=>'required'))?>
    															<br />
    															<p style="position: absolute;bottom: 3px;color: red;" id="err_pin0"></p>
    														</div>
    														<div class="col-md-6 sol-xs-12">
    															<input class="form-control" type="text" name="addArea" id="chArea0" required placeholder="Input Area..." />
    														</div>
    														<div class="clearfix"></div>
    														<div class="col-md-12 sol-xs-12">
    															<textarea name="addAddress" class="form-control" required></textarea><br />
    															<input type="submit" value="Save" class="btn btn-success" />
    														</div>
    													</div>
    												<?=form_close()?>
    											</div>
    										</div>
    										<div class="col-sm-1">
    											<button class="btn btn-info pull-right" id="addAdrBtn" onclick="togAddAdr();">Add New Address</button>
    										</div>
    									</div>
    								</td>
    							</tr>
    							<?php }?>
    							<!--End loop-->
    						</tbody>
    					</table> 
    				<div class="clearfix"></div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
<div class="mobileView">
	<div class="container" style="padding:0;">
		<div class="row">
			<h2 style="text-align:center; margin-top:60px;">My Profile</h2>
			<div class="col-md-12" style="overflow-x:hidden;">
				<hr />
				<b>Name : </b><span id="disp_namem" class="disp"><?=$profile['first_name'].' '.$profile['last_name']?></span>
				<i class="fa fa-pencil pull-right e" aria-hidden="true" id="edit_namem" onclick="updd('namem');"></i>
				<i class="fa fa-close pull-right c" aria-hidden="true" id="close_namem" onclick="updu('namem');" style="display:none;"></i>
				<div class="row edit_" id="input_namem"  style="display:none;" >
					<?=form_open('UpdateProfile/update_name', 'onsubmit=\'return validateUpdateProfileForm("cname");\'')?>					
						<input type="text" id="cfnamem" name="change_fname" value="<?=$profile['first_name']?>" class="form-control"/>
						<p style="color: red;position: absolute;margin-left: 5px;" id="errCfnamem"></p><br />
					
						<input type="text" id="clnamem" name="change_lname" value="<?=$profile['last_name']?>" class="form-control" />
						<p style="color: red;position: absolute;margin-left: 5px;" id="errClnamem"></p><br />
					
						<input type="submit" value="update" class="btn btn-info" style="width:100%" />
					<?=form_close()?>
				</div>
			</div>
			
			<!--
			<div class="col-md-12" style="overflow-x:hidden;">
				<hr />
				<b>D.O.B : </b>
				<span id="disp_dobm" class="disp">
					<?=$profile['dob']?>
				</span>
				
				<i class="fa fa-pencil pull-right e" aria-hidden="true" id="edit_dobm" onclick="updd('dobm');"></i>
				<i class="fa fa-close pull-right c" aria-hidden="true" id="close_dobm" onclick="updu('dobm');" style="display:none;"></i>
				
								
				<div class="row edit_" id="input_dobm"  style="display:none;" >
					<?=form_open('UpdateProfile/update_dob', 'onsubmit=\'return validateUpdateProfileForm("cdob");\'')?>
						<input type="date" id="cdobm" name="change_dob" value="<?=$profile['dob']?>" class="form-control" />
						<p style="color: red;position: absolute;margin-left: 5px;" id="errCdob"></p><br />
					
						<input type="submit" value="update" class="btn btn-info" style="width:100%" />
					<?=form_close()?>
				</div>
			</div>
			-->
			
			<div class="col-md-12" style="overflow-x:hidden;">
				<hr />
				<b>Email : </b>
				<span id="disp_emailm" class="disp">
					<?=$profile['email']?>
				</span>
                <!--
				<i class="fa fa-pencil pull-right e" aria-hidden="true" id="edit_emailm" onclick="updd('emailm');"></i>
				<i class="fa fa-close pull-right c" aria-hidden="true" id="close_emailm" onclick="updu('emailm');" style="display:none;"></i>
				-->	
				<div class="row edit_" id="input_emailm"  style="display:none;" >
					<?=form_open('UpdateProfile/update_email', 'onsubmit=\'return validateUpdateProfileForm("cemail");\'')?>
						<input type="text" id="cemailm" name="change_email" value="<?=$profile['email']?>" class="form-control" />
						<p style="color: red;" id="errCemailm"></p><br />
						
						<input type="button" id="checkem" value="Check Availability" onclick="update_email($('#cemailm').val());" class="btn btn-info" style="width:100%" />						
						<input type="submit" id="updtEmailm" style="display:none" value="update" class="btn btn-success" style="width:100%" />
						</div>
					<?=form_close()?>
				</div>
							
			<div class="col-md-12" style="overflow-x:hidden;">
				<hr />
				<b>Mobile&nbsp;Number : </b>
				<span id="disp_mobilem" class="disp">
					<?=$profile['mobile']?>
				</span>
				<i class="fa fa-pencil pull-right e" aria-hidden="true" id="edit_mobilem" onclick="updd('mobilem');"></i>
				<i class="fa fa-close pull-right c" aria-hidden="true" id="close_mobilem" onclick="updu('mobilem');" style="display:none;"></i>
				<div class="row edit_" id="input_mobilem" style="display:none;" >
					<?=form_open('UpdateProfile/update_mobile', 'onsubmit=\'return validateUpdateProfileForm("cnumberm");\'')?>
					<input type="number" id="cnumberm" name="change_number" value="<?=$profile['mobile']?>" class="form-control" />
					<input type="number" id="cotpm" value="Enter otp sent to the new number." placeholder="Enter otp sent to the new number : " class="form-control" style="display:none;" />
					<p style="color: red;" id="errCmobilem"></p><br />
					
					<div id="upNumBtn">
						<input type="button" id="checkm" value="Update" onclick="verify_number($('#cnumberm').val());" class="btn btn-info" style="width:100%;background-color:#000;" />
						<input type="submit" id="updtNumm" style="width:100%; display:none;background-color:#000" value="Verify OTP" class="btn btn-success" />
					</div>
					<?=form_close()?>
				</div>
			</div>
				
			<?php $i=0; foreach($addresses as $add){ $i++;?>
				<div class="col-md-12" style="overflow-x:hidden;">
					<hr />
					<b>Address <?=$i?> : </b><br />
					<span id="disp_addressm<?=$i?>" class="disp"><?=$add->address.', '.$add->area.', '.$add->city.', '.$add->state.' - '.$add->pincode.'.'?></span>
					
					<i class="fa fa-pencil pull-right e" aria-hidden="true" id="edit_addressm<?=$i?>" onclick="updd('addressm<?=$i?>');"></i>
					<i class="fa fa-close pull-right c" aria-hidden="true" id="close_addressm<?=$i?>" onclick="updu('addressm<?=$i?>');" style="display:none;"></i>
					
					<div class="edit_" id="input_addressm<?=$i?>"  style="display:none;" >
						<?=form_open('UpdateProfile/update_address', 'onsubmit=\'return validateUpdateProfileForm("caddress");\'')?>
							<input type="text" hidden name="addressId" value="<?=$add->address_id?>" />
							<br />
							<?=form_dropdown('state', $states, set_value('state',$add->state_id), array('id'=>'chStatem'.$i, 'class'=>'form-control','required'=>'required'))?>
							<br />
							<?=form_dropdown('city', $cities, set_value('city',$add->city_id), array('id'=>'chCitym'.$i, 'class'=>'form-control','required'=>'required'))?>
							<br />
							<?=form_dropdown('chPin', $pincodes, set_value('pincodes',$add->pincode_id), array('id'=>'chpinIdm'.$i, 'class'=>'form-control','required'=>'required'))?>
							<p style="color:red;" id="err_pinm<?=$i?>"></p>
							<br />
							<input type="text" value="<?=$add->area?>" id="chAream<?=$i?>" name="chArea" class="form-control" required/>
							<br />
							<textarea name="chAddress" class="form-control"><?=$add->address?></textarea>
							<br />
							<input type="submit" value="update" class="btn btn-info" style="width:100%; border-radius:0; background-color:#000" />
						<?=form_close()?>
					</div>
				</div>
			<?php }?><br /><br />
			
			<?php if($address_count->address_count < 3){?>
				<div class="col-md-12" style="overflow-x:hidden;">
					<hr />
					<div id="addNewAddSecm" style="display:none;">
						<b>Add New Address :</b><br /><br />
						<?=form_open('UpdateProfile/add_new_address')?>
							<?=form_dropdown('addstate', $states, set_value('addstate'), array('class'=>'form-control','required'=>'required'))?>
							<br />
							
							<?=form_dropdown('addcity', $cities, set_value('addcity'), array('class'=>'form-control','required'=>'required'))?>
							<br />
							
							<?=form_dropdown('addPin', $pincodes, set_value('pincodes'), array('id'=>'chpinId0m', 'class'=>'form-control','required'=>'required'))?>
							<br />
							<p style="color: red;" id="err_pin0m"></p>
							
							<input class="form-control" type="text" name="addArea" id="chAream" required placeholder="Input Area..." />
							<br />
							
							<textarea name="addAddress" class="form-control" required></textarea>
							<br />
							
							<input type="submit" value="Save" class="btn btn-success" style="width:100%; border-radius:0; background-color:#000"/>
							<br />
							<br />
						<?=form_close()?>
					</div>
					<div class="clearfix"></div>
				</div>
				<button class="btn btn-info" style="width:100%; border-radius:0; background-color:#000" id="addAdrBtnm" onclick="togAddAdr();">Add New Address</button>
			<?php }?>
		</div>
	</div>
</div>
<style>
	#profile th{width:200px;}
</style>