<div class="desktopView">
    <div class="page-head_agile_info_w3l">
    	<div class="container">
    		<h3>Order History</h3>
    		<!--/w3_short-->
    		 <div class="services-breadcrumb">
    			<div class="agile_inner_breadcrumb">
    				<ul class="w3_short">
    					<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
    					<li><a href="<?=site_url();?>/user">Dashboard</a><i>|</i></li>
    					<li><a href="<?=site_url();?>/user">Order History</a><i>|</i></li>
    					<li>Order Details</li>
    				</ul>
    			</div>
    		</div>
    	   <!--//w3_short-->
    	</div>
    </div>
    
    <div class="banner-bootom-w3-agileits">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-8">
    				<div class="bs-docs-example" style="overflow-x:hidden;">
    					<div class="row">
    						<div class="col-md-12">
    							<h2>Order Details</h2>
    							<hr>
    						</div>
    						<div class="row">
    							<div class="col-md-6">
    								<div class="col-md-3">
    									<label>Cust. Id</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['cust_id']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-3">
    									<label>Name</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['first_name']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-3">
    									<label>Mobile</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['mobile']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-3">
    									<label>Email</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['email']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-3">
    									<label>Address</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['address']?>
    								</div>
    								<div class="clearfix"></div>
    							</div>
    							<div class="col-md-6">
    								<div class="col-md-3">
    									<label>Ord. Id</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['order_id']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-3">
    									<label>Ord. Dt</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['order_date']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-12">
    									<label>Transaction Id</label><br />
    									<?=$order_details['transaction_id']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-3">
    									<label>Status</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['order_status']?>
    								</div>
    								<div class="clearfix"></div>
    							</div>
    						</div>
    					</div>
    					<div class="clearfix"></div>
    					<hr style="margin:4px;">
    					<?=form_open('Ordercontroller/get_refund_order_data/'.$order_details['order_id'], "class='form-horizontal'  onsubmit=' return validate(); '"); ?>
    					
    					<table class="table table-striped">
    						<thead>
    							<tr>
    								<th colspan="2">Product</th>
    								<th>Unit&nbsp;Price</th>
    								<th>Quantity</th>
    								<th>Total</th>
    								<?php if($order_details['refund_id'] == null && $order_details['order_status']=="Delivered"){ ?>
    								    <th>Return</th>
    								<?php } ?>
    							</tr>
    						</thead>
    						<tbody>
    							<?php foreach($order_products as $op){?>
    							<tr>
    								<td><img src="<?=IMAGEBASEPATH.$op->prod_image_url?>" style="width:75px; float:left" /></td>
    								<td>
    									<?=
    										'<p style="margin-bottom:7px"><b>Name :</b> '.$op->prod_name.'</p>'.
    										'<p style="margin-bottom:7px"><b>Size :</b> '.$op->size.'</p>'
    									?>
    								</td> 
    								<td><?=$op->price?></td>
    								<td><?=$op->quantity?></td>
    								<td><b><?=$op->price*$op->quantity?></b></td>
    								<?php if($order_details['refund_id'] == null && $order_details['order_status']=="Delivered"){ ?>
    								<td>
    								    <?=form_checkbox(array('name'=>'preturn[]', 'value'=>$op->product_id, 'id'=>$op->product_id, 'checked'=>set_checkbox('preturn[]', $op->product_id, 'false')));?>
    								    <label class="error preturn_error" style="color:red"></label>
    								</td>
    								<?php } ?>
    							</tr>
    							<?php } ?>
    						</tbody>
    					</table>
    					<a href="<?=site_url();?>/user" class="btn btn-sm btn-default">Back</a>
    				</div>
    			</div>
    			<div class="col-lg-4">
    				<div class="bs-docs-example" style="overflow-x:hidden;">
    					<h2>Delivery Details</h2>
    					<hr>
    					<table class="table table-striped">
    						<thead>
    							<tr>
    								<th>Delivery Date</th>
    								<th><?=$order_details['delivery_date']?></th>
    							</tr>
    							<tr>
    								<th>Time Slot</th>
    								<th><?=$order_details['time_slot'];?></th>
    							</tr>
    							<tr>
    								<th>Coupon Code</th>
    								<?php if($order_details['promocode_applied']!=0) { ?>
    									<th>Applied</i></th>
    								<?php } else { ?>
    									<th>Not Applied</i></th>
    								<?php } ?>
    							</tr>
    							<tr>
    								<th>Payment Status</th>
    								<th><?=$order_details['payment_status']?></th>
    							</tr>
    							<tr>
    								<th><b>Amount</b></th>
    								<th><?=$order_details['amount']?> <i class="fa fa-inr"></i></th>
    							</tr>
    							<tr>
    								<th><b>Delivery Charges</b></th>
    								<th><?=$order_details['delivery_charges']?> <i class="fa fa-inr"></i></th>
    							</tr>
    							<tr>
    								<th><b>Total Payable</b></th>
    								<th><?=$order_details['discount_amount']?> <i class="fa fa-inr"></i></th>
    							</tr>
    						</thead>
    					</table>
    					
    					<?php
    						//Calculate the difference between the current date and delivery date, check if it exceeds 8 dys.
    						//$date_diff = 8;
    					?>
    					
    					<?php if($order_details['order_status']=="In Process") { ?>
    						<a href="<?php echo site_url('Ordercontroller/cancel_user_order/') . $order_details['order_id']; ?>" class="btn btn-danger col-sm-12" role="button" onClick="return confirm('Are you sure you want to Cancel order?')">Cancel Order</a>
    					<?php } ?>
    					
    				<?php 
    					$delivery_date = date_create($order_details['delivery_date']);
    					$current_date = date_create(date('Y-m-d'));
    					$datediff = date_diff($delivery_date, $current_date);
    					$date_diff= $datediff->format('%a');
    					if($order_details['refund_id'] == null && $order_details['order_status']=="Delivered") {
				        if($date_diff < 8 ){ ?>
    						<button type="submit" name="submit" class="btn btn-danger col-sm-12" role="button" onClick="return confirm('Are you sure you want to Return order?')">Return Order</button>
    					<?php }} ?>
    				</div>
    			</div>
    			<?=form_close(); ?>  
    		</div>
    	</div>
    </div>

</div>

<div class="mobileView">
	<div class="container" style="padding:0;">
		<div class="row">
			<div class="col-md-12" style="overflow-x:hidden;padding:0;">
				<h2 style="text-align:center; margin-top:60px;">
					Customer Details
				</h2>
				<div style="background-color: #eee; padding: 7px 0; margin: 7px 0; width: 100%;font-size: 14px;">
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Customer Id</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?=$order_details['cust_id']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Name</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?=$order_details['first_name']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Mobile No.</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?=$order_details['mobile']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Email Id</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?=$order_details['email']?>
					</div>
					<div class="clearfix"></div>
				</div>
				<h2 style="text-align:center; margin-top:7px;">
					Delivery Details
				</h2>
				<div style="background-color: #eee; padding: 7px 0; margin: 7px 0; width: 100%;">
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>	Order Id</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?=$order_details['order_id']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Transaction Id</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?=$order_details['transaction_id']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Address</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?=$order_details['address']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Delivery Date</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?=$order_details['delivery_date']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Time Slot</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?=$order_details['time_slot']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Coupon Code</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?php if($order_details['promocode_applied']!=0) { echo'Applied'; }else{ echo 'Not Applied';} ?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Payment Status</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?=$order_details['payment_status']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Order Status</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<?=$order_details['order_status']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Total Amount</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<i class="fa fa-inr"></i> <?=$order_details['discount_amount']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Delivery Charges</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<i class="fa fa-inr"></i> <?=$order_details['delivery_charges']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<b>Payable Amount</b>
					</div>
					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
						<i class="fa fa-inr"></i> <?=$order_details['discount_amount']?>
					</div>
					<div class="clearfix"></div>
				</div>
				<?=form_open('Ordercontroller/get_refund_order_data/'.$order_details['order_id'], "class='form-horizontal'  onsubmit=' return validate(); '"); ?>
				<h2 style="text-align:center; margin-top:7px;">
					Products Details
				</h2>
				<hr />
				
				<div class="col-md-12" style="overflow-x:hidden;padding:0;">
					<?php foreach($order_products as $op) {?>
						<div class="col-sm-4 col-xs-4" style="padding:0;font-size:12px;">
							<img src="<?=IMAGEBASEPATH.$op->prod_image_url?>" style="width:100%;" />
						</div>
						<div class="col-sm-8 col-xs-8">
							<p style="margin:7px 0; font-weight:600;"><?=$op->prod_name?></p>
							<p style="margin:7px 0;">Price : <i class="fa fa-inr"></i> <?=$op->price?></p>
							<p style="margin:7px 0;" >
								Size: <?=$op->size?> 
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
								Quantity : <?=$op->quantity?>
							</p>
							<p style="margin:7px 0;" >
								<label class="error" style="color:red" id="preturn_error"></label>
							</p>
							<?php if($order_details['order_status'] == 'Delivered'){?>
							<?=form_checkbox(array('style'=>'float: right; position: absolute; top: 0; right: 15px;', 'name'=>'preturn[]', 'value'=>$op->product_id, 'id'=>$op->product_id+'m', 'checked'=>set_checkbox('preturn[]', $op->product_id, 'false')));?>
							<label class="error preturn_error" style="color:red"></label>
							<?php }?>
						</div>
						<div class="clearfix"></div>
						<hr style="padding:0;" />
					<?php } ?>
				</div>
							
				<div class="col-sm-12 col-xs-12" style="padding: 10px 14px; background-color: aliceblue;">
					<a href="<?=site_url();?>/website_home/privacy_policies">
						# Return and cancellation policies
					</a>
				</div>
				<div class="clearfix"></div>
							
				<div class="col-md-12" style="overflow-x:hidden;padding:0;">
					<?php if($order_details['order_status']=="In Process") { ?>
						<a href="<?php echo site_url('Ordercontroller/cancel_user_order/') . $order_details['order_id']; ?>" style="width: 100%; border-radius: 0;" class="btn btn-danger col-sm-12" role="button" onClick="return confirm('Are you sure you want to Cancel order?')">Cancel Order</a>
					<?php } ?>
					
					<?php 
					$delivery_date = date_create($order_details['delivery_date']);
					$current_date = date_create(date('Y-m-d'));
					$datediff = date_diff($delivery_date, $current_date);
					$date_diff= $datediff->format('%a');
				    if($order_details['refund_id'] == null && $order_details['order_status']=="Delivered") {
				    if($date_diff < 8 &&  $order_details['order_status']=="Delivered") { ?>
						<button style="width:100%; border-radius:none;background-color:black;border-color:black" type="submit" name="submit" style="width: 100%; border-radius: 0;" class="btn btn-danger col-sm-12" role="button" onClick="return confirm('Are you sure you want to Return order?')">Return Order</button>
						<!--<a href="#" class="btn btn-warning col-sm-12">Return Order</a>-->
					<?php } } ?>
				</div>
				<?=form_close(); ?>  
				</div>
				
			</div>
		</div>
	</div>
</div>


<script>
var preturn = 0;
var validated = true;
function validate()
{
	debugger;
	$('input[type=checkbox]').each(function () {
		if($(this).prop('checked'))
		{
			preturn = preturn+1;
		}
	}); 
	if(preturn == 0){validated = false; $('.preturn_error').html('You must select atleast one product');}
	else{
		validated = true;
		$('.preturn_error').html('');
	}
	return validated;
	}
</script>