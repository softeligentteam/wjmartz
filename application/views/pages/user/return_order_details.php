<div class="desktopView">
    <div class="page-head_agile_info_w3l">
    		<div class="container">
    			<h3>Return Order</h3>
    			<!--/w3_short-->
    				 <div class="services-breadcrumb">
    					<div class="agile_inner_breadcrumb">
    						<ul class="w3_short">
    							<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
    							<li><a href="<?=site_url();?>/user">Dashboard</a><i>|</i></li>
    							<li><a href="<?=site_url();?>/user">Order History</a><i>|</i></li>
    							<li>Order Details</li>
    						</ul>
    					</div>
    				</div>
    	   <!--//w3_short-->
    	</div>
    </div>
    
    <div class="banner-bootom-w3-agileits">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-8">
    				<div class="bs-docs-example" style="overflow-x:hidden;">
    					<div class="row">
    						<div class="col-md-12">
    							<h2>Order Details</h2>
    							<hr>
    						</div>
    						<div class="row">
    							<div class="col-md-6">
    								<div class="col-md-3">
    									<label>Cust. Id</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['cust_id']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-3">
    									<label>Name</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['first_name']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-3">
    									<label>Mobile</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['mobile']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-3">
    									<label>Email</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['email']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-3">
    									<label>Address</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['address']?>
    								</div>
    								<div class="clearfix"></div>
    							</div>
    							<div class="col-md-6">
    								<div class="col-md-3">
    									<label>Ord. Id</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['order_id']?>
    								</div>
    								<div class="clearfix"></div>
    								<hr style="margin:4px;">
    								<div class="col-md-3">
    									<label>Ord. Dt</label>
    								</div>
    								<div class="col-md-9">
    									<?=$order_details['order_date']?>
    								</div>
    								<div class="clearfix"></div>
    							</div>
    						</div>
    					</div>
    					<div class="clearfix"></div>
    					<hr style="margin:4px;">
    					
    					<table class="table table-striped">
    						<thead>
    							<tr>
    								<th colspan="2">Product</th>
    								<th>Unit&nbsp;Price</th>
    								<th>Quantity</th>
    								<th>Total</th>
    							</tr>
    						</thead>
    						<tbody>
    							<?php foreach($order_products as $op){?>
    							<tr>
    								<td><img src="<?=IMAGEBASEPATH.$op->prod_image_url?>" style="width:75px; float:left" /></td>
    								<td>
    									<?=
    										'<p style="margin-bottom:7px"><b>Name :</b> '.$op->prod_name.'</p>'.
    										'<p style="margin-bottom:7px"><b>Size :</b> '.$op->size.'</p>'
    									?>
    								</td> 
    								<td><?=$op->price?></td>
    								<td><?=$op->quantity?></td>
    								<td><b><?=$op->price*$op->quantity?></b></td>
    							</tr>
    							<?php } ?>
    						</tbody>
    					</table>
    					<a href="<?=site_url();?>/user" class="btn btn-sm btn-default">Back</a>
    				</div>
    			</div>
    			<div class="col-lg-4">
    				<div class="bs-docs-example" style="overflow-x:hidden;">
    					<h2>Return Details</h2>
    					<hr>
    					<table class="table table-striped">
    						<thead>
    						<tr>
    								<th>Return Id</th>
    								<th><?=$order_details['refund_id']?></th>
    							</tr>
    							<tr>
    								<th>Return Date</th>
    								<th><?=$order_details['refund_date']?></th>
    							</tr>
    						</thead>
    					</table>
    					
    					<?php
    						//Calculate the difference between the current date and delivery date, check if it exceeds 8 dys.
    						//$date_diff = 8;
    					?>
    					
    					
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
<div class="mobileView">
	<div class="container" style="padding:0;">
		<div class="row">
			<div class="col-md-12" style="overflow-x:hidden;padding:0;">
				<h2 style="text-align:center; margin-top:60px;">Order Details</h2>
				<div style="background-color: #eee; padding: 7px 0; margin: 7px 0; width: 100%;font-size: 14px;">
					<div class="col-sm-5 col-xs-5" style="margin: 3px 0;">
						<b>Customer Id</b>
					</div>
					<div class="col-sm-7 col-xs-7" style="margin: 3px 0;">
						<?=$order_details['cust_id']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-5 col-xs-5" style="margin: 3px 0;">
						<b>Name</b>
					</div>
					<div class="col-sm-7 col-xs-7" style="margin: 3px 0;">
						<?=$order_details['first_name']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-5 col-xs-5" style="margin: 3px 0;">
						<b>Mobile</b>
					</div>
					<div class="col-sm-7 col-xs-7" style="margin: 3px 0;">
						<?=$order_details['mobile']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-5 col-xs-5" style="margin: 3px 0;">
						<b>Email</b>
					</div>
					<div class="col-sm-7 col-xs-7" style="margin: 3px 0;">
						<?=$order_details['email']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-5 col-xs-5" style="margin: 3px 0;">
						<b>Address</b>
					</div>
					<div class="col-sm-7 col-xs-7" style="margin: 3px 0;">
						<?=$order_details['address']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-5 col-xs-5" style="margin: 3px 0;">
						<b>Order Id</b>
					</div>
					<div class="col-sm-7 col-xs-7" style="margin: 3px 0;">
						<?=$order_details['order_id']?>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-5 col-xs-5" style="margin: 3px 0;">
						<b>Customer Id</b>
					</div>
					<div class="col-sm-7 col-xs-7" style="margin: 3px 0;">
						<?=$order_details['order_date']?>
					</div>
					<div class="clearfix"></div>
				</div>
				<h2 style="text-align:center; margin:7px;">Order Products</h2>
				<div class="col-md-12" style="overflow-x:hidden;padding:0;">
					<?php foreach($order_products as $op){?>
						<hr style="margin:3px 0;" />
						<div class="col-sm-4 col-xs-4" style="padding:0;font-size:12px;">
							<img src="<?=IMAGEBASEPATH.$op->prod_image_url?>" style="width:75px; float:left" />
						</div>
						<div class="col-sm-8 col-xs-8">
							<p style="margin:7px 0; font-weight:600;">
								<?=$op->prod_name?>
							</p>
							<p style="margin:7px 0;">
								Price : <i class="fa fa-inr"></i> <?=$op->price?>
							</p>
							<p style="margin:7px 0;" >
								Size : <?=$op->size?>
								&nbsp;&nbsp;&nbsp;&nbsp;
								Qty. : <?=$op->quantity?>
							</p>
							<p style="margin:7px 0;" >
								Total Paid: <?=$op->price*$op->quantity?>
							</p>
						</div>
						<div class="clearfix"></div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>