<div class="desktopView">
    <div class="page-head_agile_info_w3l">
    		<div class="container">
    			<h3>Order History</h3>
    			<!--/w3_short-->
    				 <div class="services-breadcrumb">
    					<div class="agile_inner_breadcrumb">
    						<ul class="w3_short">
    							<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
    							<li>Dashboard<i>|</i></li>
    							<li>Order History</li>
    						</ul>
    					</div>
    				</div>
    	   <!--//w3_short-->
    	</div>
    </div>
    
    <div class="banner-bootom-w3-agileits" style="min-height:300px;">
    	<div class="container">
    		<div class="row">
    			<?php $this->load->view('layouts/userNav')?>
    			<div class="col-md-9">
    				<center><h2>Return Orders</h2></center>
    				<div class="community-poll">
    					<table class="table table-striped">
    						<thead>
    							<tr>
    								<th>Sr.&nbsp;No.</th>
    								<th class="text-center">Order&nbsp;Id</th>
    								<th>Order&nbsp;Date</th>
    								<th class="text-center">Return&nbsp;Order&nbsp;Id</th>
    								<th>Return&nbsp;Date</th>
    								<th class="text-center">Action</th>
    							</tr>
    						</thead>
    						<tbody>
    							<?php $index = 0; 
    							if(count($order_list)!==0)
    							{
    								foreach($order_list as $ol){$index++?>
    							<tr>
    								<td><?=$index?></td>
    								<td style = "text-align:center;"><b><?=$ol->order_id?></b></td>
    								<td><?=$ol->order_date?></td>
    								<td style = "text-align:center;"><b><?=$ol->refund_id?></b></td>
    								<td><?=$ol->refund_date?></td>
    								<td class="text-center">
    									<a href="<?=site_url();?>/user/return_order_details/<?=$ol->order_id?>" class="btn btn-sm btn-default">View</a>
    								</td>
    							</tr>
    							<?php }
    								} 
    								else { ?>
    								<tr class="warning">
    									<td colspan="7" style="text-align:center;">
    										<i class="fa fa-warning"/>&nbsp;&nbsp;No records found...
    									</td>
    								</tr>
    							<?php } ?>
    						</tbody>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
<div class="mobileView">
	<div class="container" style="padding:0;">
		<div class="row">
			<div class="col-md-12" style="overflow-x:hidden;padding:0;">
				<h2 style="text-align:center; margin-top:60px;">Return Orders</h2>
				<?php $index = 0; if(count($order_list)!==0) { foreach($order_list as $ol){$index++ ?>
				<div style="background-color: #eee; padding: 7px 0; margin: 7px 0; width: 100%;">
				    <a href="<?=site_url();?>/user/return_order_details/<?=$ol->order_id?>" style="color:black;">
    					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
    						<b>Order Id</b>
    					</div>
    					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
    						<b></b><?=$ol->order_id?></b>
    					</div>
    					<div class="clearfix"></div>
    					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
    						<b>Order Date</b>
    					</div>
    					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
    						<?=$ol->order_date?></td>
    					</div>
    					<div class="clearfix"></div>
    					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
    						<b>Refund Id</b>
    					</div>
    					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
    						<?=$ol->refund_id?></b>
    					</div>
    					<div class="clearfix"></div>
    					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
    						<b>Refund Date</b>
    					</div>
    					<div class="col-sm-6 col-xs-6" style="margin: 3px 0;">
    						<?=$ol->refund_date?>
    					</div>
    					<div class="clearfix"></div>
    					<hr style="margin:7px; border-top:1px solid #fff;" />
    					<div class="col-sm-12 col-xs-12 text-center">
    						<b>See Details</b>
    					</div>
    					<div class="clearfix"></div>	
				    </a>
					<?php }
						} 
						else { ?>
						<div class="col-sm-12 col-xs-12" style="margin: 15px 0;text-align:center;">
							<center><i class="fa fa-warning"></i>&nbsp;&nbsp;No records found...</center>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
