<div class="page-head_agile_info_w3l desktopView">
		<div class="container">
			<h3>Change Password</h3>
			<!--/w3_short-->
				 <div class="services-breadcrumb">
					<div class="agile_inner_breadcrumb">
						<ul class="w3_short">
							<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
							<li>Dashboard<i>|</i></li>
							<li>Change Password</li>
						</ul>
					</div>
				</div>
	   <!--//w3_short-->
	</div>
</div>
<style>
	@media(max-width:768px){
		.community-poll{margin-top:50px;}
		.container{padding:0;}
		#chpasssub{width:100%; border-radius:none;}
	}
</style>
<div class="banner-bootom-w3-agileits" style="min-height:300px;">
	<div class="container">
		<div class="row">
			<?php $this->load->view('layouts/userNav')?>
			<div class="col-md-9">
				<div class="community-poll">
					<form onsubmit="return onSubmitUpdate();" action="<?=site_url();?>/UpdateProfile/update_change_password"  method="post">
						<br />
						<div class="col-md-3">
							<label>Old Password</label>
						</div>
						<div class="col-md-9">
							<input class="form-control" type="password" name="oldpassword" id="oldpassword" required="" placeholder="Enter your old password here"  maxlength="15" minlength="6" min="6" onblur = "validate_oldpassword();" >
							<p class="error_msg" id="Error-msg-oldpassword"></p>
						</div>
						<div class="clearfix"></div><br />
						<div class="col-md-3">
							<label>New Password</label>
						</div>
						<div class="col-md-9">
							<input class="form-control" type="password" name="password" id="password"required="" placeholder="Enter your new password here"  maxlength="15" minlength="6" min="6" onblur = "validate_newpassword();">
							<p class="error_msg" id="Error-msg-newpassword"></p>
						</div>
						<div class="clearfix"></div><br />
						<div class="col-md-3">
							<label>Confirm  Password</label>
						</div>
						<div class="col-md-9">
							<input class="form-control" type="password" name="conpass" id="conpass" required="" placeholder="Re-enter your new password here"  maxlength="15" minlength="6" min="6" onblur = "validate_change_password();">
							<p class="error_msg" id="Error-msg-conpassword"></p>
						</div>
						<div class="clearfix"></div><br /><br />
						<input type="submit" class="btn btn-md btn-warning" value="UPDATE PASSWORD" />
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
	.agile_top_brand_left_grid1 p{text-align:left;}
	.login-form-grids{width: 100%;margin: 0;}
	.error_msg{
		color: red;
		position: absolute;
		font-size: 12px;
		bottom:-15px;
	}
</style>
