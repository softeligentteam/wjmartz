<div class="desktopView">	
	<!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<?php $i=0; foreach($sliders as $slide){?>
			<li data-target="#myCarousel" data-slide-to="<?=$i?>" class="<?php if($i == 1){ echo 'active';} ?>"></li>
			<?php $i++; } ?>
		</ol>
		<div class="carousel-inner" role="listbox">
			<?php $i=0; foreach($sliders as $slide){?>
			<div class="item <?php if($i == 1){ echo 'active';} ?>">
				<img src="<?=IMAGEBASEPATH.$slide->slider_image?>" class="img-responsive"/>
			</div>
			<?php $i++; } ?>
		</div>
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<img src="<?=base_url();?>website_assets/images/white-icons/left-arrow.png" style="top: 47%; position: absolute;" />
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<img src="<?=base_url();?>website_assets/images/white-icons/right-arrow.png" style="top: 47%; position: absolute;" />
			<span class="sr-only">Next</span>
		</a>
		<!-- The Modal -->
    </div> 
	<!-- //banner -->	
	
	<!-- New-arrivals -->
	<div class="banner-bootom-w3-agileits">
		<div class="w3_agile_latest_arrivals">
			<div class="container">
				<h3 class="wthree_text_info"><hr /><span>New Arrivals</span></h3>
				<p style="text-align:center; padding:10px 30px;margin: -20px 0 25px 0;">
					To give you a great headstart, we have listed below trending and latest products that you can consider buying.
				</p>
				<div class="row">
					<div class="col-md-12">
						<div class="carousel slide multi-item-carousel" id="theCarousel">
							<div class="carousel-inner">
								<div class="item active">
									<?php 
										$ele_count = 0;
										$count=0; 
										foreach($new_arrivals as $new){
											$count++;
											$ele_count++;
									?>
										<div class="col-md-3 product-men single">
											<div class="men-pro-item simpleCart_shelfItem">
												<div class="men-thumb-item">
													<?php
														$images =  explode(',', $new->prod_image_urls);
														$counter3 = 0;
														foreach ($images as $img) {
															if($counter3 == 1){
																
																echo '<img src="'.IMAGEBASEPATH.$new->prod_image_url.'" alt="" class="';
																echo 'pro-image-front" />';
															}else{
																echo '<img src="'.IMAGEBASEPATH.$img.'" alt="" class="';
																echo 'pro-image-back" />';
															}
															$counter3++;
															if($counter3 == 2){break;}
														}
													?>	
													<!--
													<div class="men-cart-pro">
														<div class="inner-men-cart-pro">
															<a href="<?=site_url();?>/website_home/product_details/<?=$new->fk_pcat_id?>/<?=$new->fk_pchild_id?>/<?=$new->prod_id?>" class="link-product-add-cart">Quick View</a>
														</div>
													</div>	
													-->													
												</div>
												<div class="item-info-product ">
													<h4 style="height: 22px;overflow: hidden;"><a href="#single.html"><?=$new->prod_name;?></a></h4>
													<div class="info-product-price">
														<span class="item_price"><i class="fa fa-inr"></i> <?=$new->prod_price;?></span>
														<del><i class="fa fa-inr"></i> <?=$new->prod_mrp;?></del>
													</div>
													<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
														<input type="button" onclick="window.location = '<?=site_url();?>/website_home/product_details/<?=$new->fk_pcat_id?>/<?=$new->fk_pchild_id?>/<?=$new->prod_id?>/';" value="View" class="button" />
													</div>
													<hr style="margin-top: -32px;" />
												</div>
											</div>
										</div>
										<?php if($count == 4){$count=0; ?>
												<div class="clearfix"> </div>												
											<?php if($ele_count != 12){?>
												</div>
												<div class="item">
											<?php }?>
										<?php }?>
									<?php }?>
								</div>
								<!--  Example item end -->
							</div>
							<a style="width: 35px; margin-left: -25px;" class="left carousel-control" href="#theCarousel" data-slide="prev">
								<img src="<?=base_url();?>website_assets/images/white-icons/left-arrow-gray.png" style="top: 46%; position: absolute; right:5px" />
							</a>
							<a style="width: 35px; margin-right: -25px;" class="right carousel-control" href="#theCarousel" data-slide="next">
								<img src="<?=base_url();?>website_assets/images/white-icons/right-arrow-gray.png" style="top: 46%; position: absolute; left:5px" />
							</a>
						</div>
					</div>
				</div>
				<!--//slider_owl-->
			</div>
		</div>
	</div>
	<!-- //New-arrivals -->
		
	<div class="banner-bootom-w3-agileits">
		<div class="container">
			<!---728x90--->
			<h3 class="wthree_text_info"><hr /><span>Browse Our Categories</span></h3>
			<br /> 
			<div class="col-md-6 bb-grids bb-left-agileits-w3layouts" onmouseout="graycolor('one');" onmouseover="colored('one');" >
				<a href="<?=site_url();?>/website_home/product_list/<?=$parent_cats[0]['parent_cat_id']?>">
				   <div class="bb-left-agileits-w3layouts-inner grid">
						<figure class="effect-roxy">
							<img src="<?=IMAGEBASEPATH.$parent_cats[0]['parent_cat_img_url']?>" id="one" class="img-responsive">
							<figcaption>
								<p>Explore</p>
								<!--
								<h3><?=$parent_cats[0]['parent_cat_name']?></h3>
								-->
							</figcaption>			
						</figure>
					</div>
				</a>
			</div>
			<div class="col-md-6 bb-grids bb-middle-agileits-w3layouts">
			   <div class="bb-middle-agileits-w3layouts grid" onmouseout="graycolor('two');" onmouseover="colored('two');" >
				   <figure class="effect-roxy">
						<img src="<?=base_url();?>website_assets/images/bottom3.jpg" id="two" class="img-responsive" />
						<figcaption>
							<!--
							<h3><?php if(isset($parent_cats[1]['parent_cat_name'])){ echo $parent_cats[1]['parent_cat_name'];}?></h3>
							<p>Upto 55%</p>
							-->
						</figcaption>
					</figure>
				</div>
				<div class="bb-middle-agileits-w3layouts forth grid" onmouseout="graycolor('three');" onmouseover="colored('three');" >
					<figure class="effect-roxy">
						<img src="<?=base_url();?>website_assets/images/bottom4.jpg" id="three" class="img-responsive" />
						<figcaption>
							<!--
							<h3><?php if(isset($parent_cats[2]['parent_cat_name'])){ echo $parent_cats[2]['parent_cat_name'];}?></h3>
							<p>Upto 65%</p>
							-->
						</figcaption>
					</figure>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- schedule-bottom -->
	
	<div class="schedule-bottom">
		<div class="col-md-5 agileinfo_schedule_bottom_left">
			<img src="<?=base_url();?>/website_assets/images/mid.jpg" alt=" " class="img-responsive" />
		</div>
		<div class="col-md-7 agileits_schedule_bottom_right">
			<div class="w3ls_schedule_bottom_right_grid">
				<img src="<?=base_url();?>/website_assets/images/discCpn.jpg" style="width:100%" />
				<!--
				<h3>Get <span>10%</span> off on first purchase</h3>
				<p>
					
				</p>
				<div class="col-md-12 w3l_schedule_bottom_right_grid1">
					<i class="fa fa-calendar-o" aria-hidden="true"></i>
					<h4>Use coupon code</h4>
					<h5 class="">WJMARTZ</h5>
				</div>
				-->
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
	<!-- //schedule-bottom -->
	
	<!-- New-arrivals -->
	<div class="banner-bootom-w3-agileits">
		<div class="w3_agile_latest_arrivals">
			<div class="container">
				<div class="row"> 
					<h3 class="wthree_text_info"><hr /><span>Deals Of The Day</span></h3>
					<div class="banner_bottom_agile_info_inner_w3ls">
						<div class="carousel slide multi-item-carousel" id="theCarousel2">
							<div class="carousel-inner">
								<div class="item active">
									<?php $ele_cnt=0; $i=0; foreach($deals_of_the_day as $deal){$i++; $ele_cnt++;?>
                                        <div class="col-md-3 grid customgrids">
                                            <a href="<?=site_url();?>/website_home/product_details/<?=$deal->pcat_id?>/<?=$deal->ccat_id?>/<?=$deal->prod_id?>">
                                                <img src="<?=IMAGEBASEPATH.$deal->deal_image_url?>" style="width:100%;" class="img-responsive">
                                            </a>
                                        </div>
                                      <?php if($i == 3){ $i==0;?>
                                          <div class="clearfix"> </div>
                                          <?php if($ele_cnt == sizeof($deals_of_the_day)){?>
                                              </div>
                                          <?php }else{?>
                                              </div>
                                              <div class="item">
                                           <?php }?>
                                      <?php }?>
                                    <?php }?>
								</div>
								<!--  Example item end -->
							</div>
							<a style="width: 35px; margin-left: -25px;" class="left carousel-control" href="#theCarousel2" data-slide="prev">
								<img src="<?=base_url();?>website_assets/images/white-icons/left-arrow-gray.png" style="top: 46%; position: absolute; right:5px" />
							</a>
							<a style="width: 35px; margin-right: -25px;" class="right carousel-control" href="#theCarousel2" data-slide="next">
								<img src="<?=base_url();?>website_assets/images/white-icons/right-arrow-gray.png" style="top: 46%; position: absolute; left:5px" />
							</a>
						</div>
						<div class="clearfix"></div>
						<br />
						<hr />
					</div>
					<style>
						.customgrids{width:33.3%}
						@media(max-width:768px){.customgrids{width:100%}}
					</style>
					<!--//slider_owl-->
				</div>
			</div>
		</div>
	</div>
	<!-- //New-arrivals -->
	
	<div class="coupons">
		<div class="coupons-grids text-center">
			<div class="w3layouts_mail_grid">
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<!--<i class="fa fa-truck" aria-hidden="true"></i>-->
						<img src="<?=base_url();?>/website_assets/images/Icons/Delivery.png" style="width:100%; padding:12px;" />
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>ON TIME DELIVERY</h3>
					</div>
				</div>
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<!--<i class="fa fa-gift" aria-hidden="true"></i>-->
						<img src="<?=base_url();?>/website_assets/images/Icons/Quality.png" style="width:100%; padding:12px;" />
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>Quality</h3>
					</div>
				</div>
				<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<!--<i class="fa fa-shopping-bag" aria-hidden="true"></i>-->
						<img src="<?=base_url();?>/website_assets/images/Icons/Return-exchange.png" style="width:100%; padding:12px;" />
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>Return and exchange</h3>
					</div>
				</div>
					<div class="col-md-3 w3layouts_mail_grid_left">
					<div class="w3layouts_mail_grid_left1 hvr-radial-out">
						<img src="<?=base_url();?>/website_assets/images/Icons/Support.png" style="width:100%; padding:12px;" />
					</div>
					<div class="w3layouts_mail_grid_left2">
						<h3>Support</h3>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
</div>
	
<div class="mobileView">
	<!-- banner -->
	<div id="myCarouselm" class="carousel slide" data-ride="carousel" style="padding-top:48px;margin-bottom: 10px;">
		<!-- Indicators -->
		<ol class="carousel-indicators" style="margin-bottom:0;">
			<?php $i=0; foreach($appsliders as $slide){?>
			<li data-target="#myCarouselm" data-slide-to="<?=$i?>" class="<?php if($i == 1){ echo 'active';} ?>"></li>
			<?php $i++; } ?>
		</ol>
		<div class="carousel-inner" role="listbox">
			<?php $i=0; foreach($appsliders as $slide){?>
			<div class="item <?php if($i == 1){ echo 'active';} ?>">
				<img src="<?=IMAGEBASEPATH.$slide->slider_image?>" class="img-responsive"/>
			</div>
			<?php $i++; } ?>
		</div>
		<!--
		<a class="left carousel-control" href="#myCarouselm" role="button" data-slide="prev" style="background-color: transparent;">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarouselm" role="button" data-slide="next" style="background-color: transparent;">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
		-->
		<!-- The Modal -->
	</div><br />
	<!-- //banner -->
	
	<?php if($sections[0]['parent_cat_id'] != 0){?>
		<h3 class="wthree_text_info" style="color:#000"><hr style="border-color:#ddd"><span style="color:black;font-weight:600;padding-left:10px;padding-right:10px;text-transform: initial"><?=$all_parent_cats[$sections[0]['parent_cat_id']]['parent_cat_name']; ?></span></h3>
		<a href="<?=site_url();?>/website_home/ccat_list/<?=$all_parent_cats[0]['parent_cat_id']?>">
			<img src="<?=IMAGEBASEPATH.$all_parent_cats[$sections[0]['parent_cat_id']]['parent_cat_img_url']; ?>" style="width:100%;margin-bottom:15px;" />
		</a>
		<div class="clearfix"></div>
	<?php }?>
	
	<h3 class="wthree_text_info" style="color:#000"><hr style="border-color:#ddd"><span style="color:black;font-weight:600;padding-left:10px;padding-right:10px;text-transform:initial;font-size:17px;">New Arrivals</span></h3>
	<div style="width:100%; overflow:scroll;margin-top:25px;margin-bottom:15px;">
		<?php $new_arrivals_sec_width = sizeof($new_arrivals)*260;?>
		<div style="width:<?=$new_arrivals_sec_width?>px; overflow:scroll;margin-bottom:20px;">
			<?php $count=0; foreach($new_arrivals as $new){$count++;?>
				<div style="width:250px; margin-right:10px; float:left; background-color:#efefef;" >
					<a href="<?=site_url();?>/website_home/product_details/<?=$new->fk_pcat_id?>/<?=$new->fk_pchild_id?>/<?=$new->prod_id?>">
						<img src="<?=IMAGEBASEPATH.$new->prod_image_url?>" style="width:100%; border: 1px solid #fbf5f5;" />
					</a>
					<center>
						<p style="max-width: 100%; max-height: 20px; overflow: hidden; margin: 1px 0; font-weight:700">
							<?=$new->prod_name;?>
						</p>
						<small><i class="fa fa-inr"></i> <?=$new->prod_price;?></small>
					</center>
				</div>
			<?php }?>
		</div>
	</div>
	<div class="clearfix"></div>
	
	
	<?php if($sections[1]['parent_cat_id'] != 0){?>
		<h3 class="wthree_text_info" style="color:#000"><hr style="border-color:#ddd"><span style="color:black;font-weight:600;padding-left:10px;padding-right:10px;text-transform:initial;font-size:17px;"><?=$all_parent_cats[$sections[1]['parent_cat_id']]['parent_cat_name']; ?></span></h3>
		<a href="<?=site_url();?>/website_home/ccat_list/<?=$all_parent_cats[1]['parent_cat_id']?>">
		<img src="<?=IMAGEBASEPATH.$all_parent_cats[$sections[1]['parent_cat_id']]['parent_cat_img_url']; ?>" style="width:100%;margin-top:10px;margin-bottom:35px;" />
		</a>
		<div class="clearfix"></div>
	<?php }?>
	
	
	<h3 class="wthree_text_info" style="color:#000"><hr style="border-color:#ddd"><span style="color:black;font-weight:600;padding-left:10px;padding-right:10px;text-transform:initial;font-size:17px;">Deals Of The Day</span></h3>
	<?php $dod_width = sizeof($deals_of_the_day)*310; ?>
	<div style="width:100%; overflow:scroll;margin-top:30px;margin-bottom:20px;">
		<div style="width:<?=$dod_width?>px; overflow:scroll;margin-bottom:20px;">
			<?php foreach($deals_of_the_day as $deal){ ?>
				<div style="width:300px; margin-right:10px; float:left; background-color:#ccc;" >
					<a href="<?=site_url();?>/website_home/product_details/<?=$deal->pcat_id?>/<?=$deal->ccat_id?>/<?=$deal->prod_id?>">
						<img src="<?=IMAGEBASEPATH.$deal->deal_image_url?>" style="width:100%; border: 1px solid #fbf5f5;" />
					</a>
				</div>
			<?php }?>
		</div>
	</div>
	<div class="clearfix"></div>
	
	
	
	<?php if($sections[2]['parent_cat_id'] != 0){?>
		<h3 class="wthree_text_info" style="color:#000"><hr style="border-color:#ddd"><span style="color:black;font-weight:600;padding-left:10px;padding-right:10px;text-transform:initial; font-size:17px"><?=$all_parent_cats[$sections[2]['parent_cat_id']]['parent_cat_name']; ?></span></h3>
		<a href="<?=site_url();?>/website_home/ccat_list/<?=$all_parent_cats[2]['parent_cat_id']?>">
		<img src="<?=IMAGEACCESSPATH.$all_parent_cats[$sections[2]['parent_cat_id']]['parent_cat_img_url']; ?>" style="width:100%;margin-top:20px;margin-bottom:20px;" />
		</a>
		<div class="clearfix"></div>
	<?php }?>
	
	<h3 class="wthree_text_info" style="color:#000"><hr style="border-color:#ddd"><span style="color:black;font-weight:600;padding-left:10px;padding-right:10px;text-transform:initial;font-size:17px;">Suggested Products</span></h3>
	<div style="width:100%; overflow:scroll;margin-top:35px;margin-bottom:20px;">
		<div class="col-sm-12 col-xs-12">
			<?php $count=0; foreach($new_arrivals as $new){$count++; if($count<5){?>
				<div class="row">
					<div class="col-sm-6 col-xs-6">
						<a href="<?=site_url();?>/website_home/product_details/<?=$new->fk_pcat_id?>/<?=$new->fk_pchild_id?>/<?=$new->prod_id?>">
							<img src="<?=IMAGEBASEPATH.$new->prod_image_url?>" style="width:100%; border: 1px solid #fbf5f5;" />
						</a>
					</div>
					<div class="col-sm-6 col-xs-6" style="padding:0;">
						<p style="max-width: 100%; max-height: 20px; overflow: hidden; margin: 5px 0; font-weight:800">
							<?=$new->prod_name;?>
						</p>
						<p style="margin: 5px 0;">
							<?=(int)((($new->prod_mrp-$new->prod_price)*100)/$new->prod_mrp);?>% off
						</p>
						<p  style="margin: 5px 0;">
						<small>
							<del><i class="fa fa-inr"></i> <?=$new->prod_mrp;?></del>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<b><i class="fa fa-inr"></i> <?=$new->prod_price;?></b>
						</small>
						</p>
						<p style="margin: 5px 0; font-size:10px;">
							<?php 
								$sizes = explode(',',$new->fk_psize_ids);
								$ci = 0;
								foreach($sizes as $sz){
									foreach($all_size as $asz){
										if($asz->size_id == $sz){
											if($ci >0 && $ci<sizeof($sizes)){
												echo ',';
											}
											$ci++;
											echo $asz->size;
										}
									}
								}
							?>
						</p>
					</div>
					<div class="clearfix"></div>
					<hr style="margin:7px 0" />
				</div>
			<?php }}?>
		</div>
	</div> 
	<div class="clearfix"></div>
	
</div>