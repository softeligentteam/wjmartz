<div class="desktopView">
    <div class="page-head_agile_info_w3l">
    	<div class="container">
    		<h3>Your Shopping Cart</h3>
    		<!--/w3_short-->
    		 <div class="services-breadcrumb">
    			<div class="agile_inner_breadcrumb">
    				<ul class="w3_short">
    					<li><a href="">Home</a><i>|</i></li>
    					<li>Shopping Cart</li>
    				</ul>
    			</div>
    		</div>
    	   <!--//w3_short-->
    	</div>
    </div>
    <div class="banner-bootom-w3-agileits">
    	<div class="container">
    		
    		<?php if(count($cart_data)==null) { ?>
    			<div class="row">
    				<div class="col-lg-12" style="font-size: xx-large;text-align: center;min-height: 300px;padding-top: 50px;">
    					<i class="fa fa-warning"></i>&nbsp;&nbsp;Your shopping cart is empty...
    				</div>
    			</div>
    		<?php }else{ ?>
    			<div class="row">
    				<div class="col-lg-12">
    					<div class="bs-docs-example">
    						<h2>
    							Shopping Cart Details
    							<a href="<?=site_url();?>/cartcontroller/checkout">
    								<button class="btn btn-success pull-right">Proceed To Checkout</button>
    							</a>
    						</h2>
    						<hr />
    						<table class="table table-striped">
    							<thead>
    								<tr>
    									<th colspan="2" class="text-center">Product</th>
    									<th>Size</th>
    									<th>Quantity</th>
    									<th>Unit&nbsp;Price</th>
    									<th colspan="2">Total</th>
    								</tr>
    							</thead>
    							<tbody>
    								<?php foreach($cart_data as $cart) {?>
    								<tr>
    									<td>
    										<img src="<?=IMAGEBASEPATH.$cart['options']['prod_image_url']?>" style="width:75px; float:left" />
    									</td>
    									<td><p style="float:left;"><b><?=$cart['name']?></b></p></td>
    									<td><?=$cart['options']['product_size_name']?></td>
    									<td>
    										<a <?php if ($cart['qty'] == '1'){ ?> style="display:none;" <?php   } ?> href="<?=site_url()?>/Cartcontroller/decrease_item_qty/<?=$cart['rowid']?>/<?=$cart['qty']?>" style="color:#f44336; cursor:pointer;">
    											<i class="fa fa-minus-square"></i>
    										</a>
    										<span id="qty"><?=$cart['qty']?> </span>
    										<a <?php if ($cart['qty'] == '5'){ ?> style="display:none;" <?php   } ?> href="<?=site_url()?>/Cartcontroller/increase_item_qty/<?=$cart['rowid']?>/<?=$cart['qty']?>" style="color:green; cursor:pointer;">
    												<i class="fa fa-plus-square"></i>
    										</a>
    									</td>
    									<td><?=$cart['price']?></td>
    									<td><?=$cart['subtotal']?></td>
    									<td>
    										<a href="<?=site_url()?>/Cartcontroller/remove_item/<?=$cart['rowid']?>" style="float:right;">
    											<i class="fa fa-trash-o"></i>
    										</a>
    									</td>
    								</tr>
    								<?php } ?>
    							</tbody>
    						</table>
    						<a href="<?=site_url();?>/cartcontroller/checkout">
    							<button class="btn btn-success pull-right">Proceed To Checkout</button>
    						</a>
    					</div>
    				</div>
    			</div>
    		<?php }?>
    	</div>
    </div>
</div>

<div class="mobileView">
	<div class="banner-bootom-w3-agileits">
		<div class="container" style="padding:0;">
			<?php if(count($cart_data)==null) { ?>
				<div class="row">
					<div class="col-lg-12" style="font-size: xx-large;text-align: center;min-height: 300px;padding-top: 50px;">
						<i class="fa fa-warning"></i>&nbsp;&nbsp;Your shopping cart is empty...
					</div>
				</div>
			<?php }else{ ?>
				<div class="row">
					<div class="col-lg-12">
						<h3 style="text-align:center; margin-top:20px;">
							Shopping Cart
						</h3>
						<hr />
						<?php foreach($cart_data as $cart) {?>
							<div class="col-sm-4 col-xs-4" style="padding:0;font-size:12px;">
								<img src="<?=IMAGEBASEPATH.$cart['options']['prod_image_url']?>" style="width:100%;" />
							</div>
							<div class="col-sm-8 col-xs-8">
								<p style="margin:7px 0; font-weight:600;"><?=$cart['name']?></p>
								<p style="margin:7px 0;">Price : <i class="fa fa-inr"></i> <?=$cart['price']?></p>
								<p style="margin:7px 0;" >
									Size: <?=$cart['options']['product_size_name']?> 
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
									Quantity : <?=$cart['qty']?>
								</p>
								<a href="<?=site_url()?>/Cartcontroller/remove_item/<?=$cart['rowid']?>" style="float: right; position: absolute; top: 0; right: 0;">
									<i class="fa fa-trash-o"></i>
								</a>
							</div>
							<div class="clearfix"></div>
							<hr style="padding:0;" />
						<?php } ?>
						<a href="<?=site_url();?>/cartcontroller/checkout">
							<button style="position:fixed;left:0;bottom:0;width:100%;padding:10px 0;background-color:black;border:none;color:white;">
								Proceed To Checkout
							</button>
						</a>
					</div>
				</div>
			<?php }?>
		</div>
	</div>
</div>
