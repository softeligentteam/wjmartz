<style>
	@media(max-width:768px){
		.banner-bootom-w3-agileits .container{padding:0;}
		.banner-bootom-w3-agileits .row{padding:0;}
		.mobilebottombtn{position:fixed;left:0;bottom:0;width:100%;padding:10px 0;background-color:black;border:none;color:white;z-index:1;}
		.nopad{padding-right:0 !important; padding-left:0 !important;}
	}
</style>
<div class="page-head_agile_info_w3l desktopView">
	<div class="container">
		<h3>Order Success</h3>
		<!--/w3_short-->
		<div class="services-breadcrumb">
			<div class="agile_inner_breadcrumb">
				<ul class="w3_short">
					<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
					<li>Order Successful</li>
				</ul>
			</div>
		</div>
		<!--//w3_short-->
	</div>
</div>
<div class="banner-bootom-w3-agileits">
	<div class="container nopad">
		<div class="row">
			<div class="col-lg-12">
				<br class="mobileView">
				<h1 class="text-success text-center">Order Placed Successfully</h1>
				<hr />
				<div class="row">
					<div class="col-lg-4">
						<center>
						<img src="<?=base_url();?>/website_assets/images/icon.png" style="width:175px; background-color:black" />
						</center>
						<br class="mobileView">
					</div>
					<div class="col-md-offset-1 col-md-7 nopad">
						<div class="col-md-4">
							<label>Member Name :</label>
						</div>
						<div class="col-md-8">
							<?=$success_data->first_name.' '.$success_data->last_name?>
						</div>
						<div class="clearfix"></div>
						<hr style="margin:4px;" />
						<div class="col-md-4">
							<label>Customer Id :</label>
						</div>
						<div class="col-md-8">
							<?=$success_data->cust_id?>
						</div>
						<div class="clearfix"></div>
						<hr style="margin:4px;" />
						<div class="col-md-4">
							<label>Order Id :</label>
						</div>
						<div class="col-md-8">
							<?=$success_data->order_id?>
						</div>
						<div class="clearfix"></div>
						<hr style="margin:4px;" />
						<div class="col-md-4">
							<label>Transcation Id :</label>
						</div>
						<div class="col-md-8">
							<?=$success_data->transaction_id?>
						</div>
						<div class="clearfix"></div>
						<hr style="margin:4px;" />
						<div class="col-md-4">
							<label>Transcation Date :</label>
						</div>
						<div class="col-md-8">
							<?=$success_data->payment_datetime?>
						</div>
						<div class="clearfix"></div>
						<hr style="margin:4px;" />
						<div class="col-md-4">
							<label>Transcation Status :</label>
						</div>
						<div class="col-md-8">
							<?=$success_data->payment_status?>
						</div>
						<div class="clearfix"></div>
						<hr style="margin:4px;" />
						<div class="col-md-4">
							<label>Amount :</label>
						</div>
						<div class="col-md-8">
							<?=$success_data->discount_amount?>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-4">
							<label>Mobile No. :</label>
						</div>
						<div class="col-md-8">
						<?=$success_data->mobile?>
						</div>
						<div class="clearfix"></div>
						<hr style="margin:4px;" />
						<div class="col-md-4">
							<label>Email Id:</label>
						</div>
						<div class="col-md-8">
							<?=$success_data->email?>
						</div>
						<div class="clearfix"></div>
						<hr style="margin:4px;" />
					</div>
				</div>
				<hr />
				<!--div class="row">
					<div class="col-md-8">
						<div class="bs-docs-example">
							<h2>Product Details</h2>
							<hr>
							<table class="table table-striped">
								<thead>
									<tr>
										<th colspan="2" class="text-center">Product</th>
										<th>Size</th>
										<th>Quantity</th>
										<th>Unit&nbsp;Price</th>
										<th colspan="2">Total</th>
									</tr>
								</thead>
								<tbody>
									< ?php foreach($cart_data as $cart) {?>
									<tr>
										<td>
											<img src="< ?=IMAGEBASEPATH.$cart['options']['prod_image_url']?>" style="width:75px; float:left" />
										</td>
										<td><p style="float:left;"><b>< ?=$cart['name']?></b></p></td>
										<td>< ?=$cart['options']['product_size_name']?></td>
										<td>
											<span id="qty">< ?=$cart['qty']?> </span>
										</td>
										<td>< ?=$cart['price']?></td>
										<td>< ?=$cart['subtotal']?></td>
									</tr>
									< ?php } ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="bs-docs-example" style="overflow-x:hidden;">
							<h2>Delivery Details</h2>
							<hr />
							<table class="table table-striped">
								<thead>
									<!--<tr>
										<th>Particular</th>
										<th>Price</th>
									</tr>-->
									<!--tr>
										<th>Order id</th>
										<th><i class="fa fa-inr"></i>450</th>
									</tr>
									<tr>
										<th>Order Date</th>
										<th>10/10/2018</th>
									</tr>
									<tr>
										<td colspan="2">
											<b>Delivery Address</b><br />
											Delivery Address Will be printed here
										</td>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>-->
			</div>
		</div>
	</div>
</div>