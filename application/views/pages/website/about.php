<!-- /banner_bottom_agile_info -->
<div class="page-head_agile_info_w3l desktopView">
		<div class="container">
			<h3>About Us</h3>
			<!--/w3_short-->
				 <div class="services-breadcrumb">
					<div class="agile_inner_breadcrumb">
						<ul class="w3_short">
							<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
							<li>About Us</span></li>
						</ul>
					</div>
				</div>
	   <!--//w3_short-->
	</div>
</div>
<div class="banner-bootom-w3-agileits">
	<div class="container">
		<div class="row">
			<br><br>
			<div class="col-md-offset-5 col-md-2">
				<img src="<?=base_url();?>/website_assets/images/icon.png" class="img-responsive">
			</div>
			<div class="col-md-6">
				<br><br>
				<h2>About us</h2>
				<hr />
				<p>Just over a year ago, we came up with thought of creating an online store for Punekars. And that’s exactly what we have set out to do, we are entrepreneur with high motivation and passion to make a difference and serve our customers.<br />We thrive to bring you the best products at best possible price and that’s what we’re all about. We believe in building a strong and lasting relationship with our customers</p>
			</div>
			<div class="col-md-6">
				<br><br>
				<h2>Our Team</h2>
				<hr />
				<p>A team of dynamic individuals with a passion for change management and depth in their respective area of expertise.</p>
				<p>Dedicated team players who bring energy, ideas and pride to their work. </p>
				<p>We view our culture as a competitive advantage and strive to create an environment where smart, motivated and creative people succeed.</p>
				<p>Our ultimate goal is thrive to be the best!</p>
				<br><br>
			</div>
		</div>
	</div>
</div>