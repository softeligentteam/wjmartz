<div class="page-head_agile_info_w3l desktopView">
	<div class="container">
		<h3>Checking-out</h3>
		<!--/w3_short-->
		 <div class="services-breadcrumb">
			<div class="agile_inner_breadcrumb">
				<ul class="w3_short">
					<li><a href="<?=site_url();?>/website_home">Home</a><i>|</i></li>
					<li><a href="<?=site_url();?>/cartcontroller/">Shopping Cart</a><i>|</i></li>
					<li>Check-out</li>
				</ul>
			</div>
		</div>
       <!--//w3_short-->
    </div>
</div>
<style>
	@media(max-width:768px){
		.banner-bootom-w3-agileits .container{padding:0;}
		.banner-bootom-w3-agileits .row{padding:0;}
		#mobilebottombtn1{position:fixed;left:0;bottom:0;width:100%;padding:10px 0;background-color:black !important;border:none;color:white;z-index:1;}
		.nopad{padding-right:0 !important; padding-left:0 !important;}
	}
</style>
<div class="banner-bootom-w3-agileits">
	<div class="container">
		<div class="row">
			<?=form_open('ordercontroller/order_preview')?>
			<div class="col-lg-8">
				<div class="bs-docs-example" style="overflow-x:hidden;">
					<h2>Check-out Details</h2>
					<hr />
						<div class="row">
							<div class="col-sm-12 nopad">
								<div class="row">
									<div class="col-sm-3 nopad"> 
										<label>Delivery Name</label>
									</div>
									<div class="col-sm-4 nopad">
										<input type="text"style="margin-bottom:10px;"  name="deliveryFname" class="form-control" placeholder="First Name" required value="<?=$customer_name['first_name']?>" />
									</div>
									<div class="col-sm-5 nopad">
										<input type="text" name="deliveryLname" class="form-control" placeholder="Last Name" required value="<?=$customer_name['last_name']?>"/>
									</div>
									<div class="clearfix"></div>
									<hr />
									<div class="col-sm-3 nopad"> 
										<label>Delivery Address</label>
									</div>
									<div class="col-sm-9 nopad">
										<?php $i=0; foreach($addresses as $add){ $i++;?>
										<div class="row">
											<div class="col-sm-11 nopad">
												<?=form_radio('delivery_address', $add->address_id,set_value('delivery_address'), array('id' => $i, 'class'=>'delivery_address','required'=>'required'));?>
												<span id="disp_address<?=$i?>" class="disp"><?=$add->address.', '.$add->area.', '.$add->city.', '.$add->state.' - '.$add->pincode.'.'?></span>
											</div>
											<div class="col-sm-1 nopad">
												<i class="fa fa-pencil pull-right e" aria-hidden="true" id="edit_address<?=$i?>" onclick="checkoutupdd('<?=$i?>');" style="display:none;"></i>
											</div>
											<div class="clearfix"></div>
											<hr style="margin:7px;" />
										</div>
									<?php }?>
									</div>
									<div class="clearfix"></div>
						            <hr class="desktopView"/>
					                <div class="col-sm-3 nopad">
										<b>Payment Mode : </b>
									</div>
									<div class="col-sm-9 nopad">
										<?php echo form_radio('payment', '1', false,array('id'=>'cod','style'=>'display:none')); ?>
										<?php echo form_label('Cash On Delivery', 'codlabel',array('id'=>'codlabel','style'=>'display:none;font-weight:400;'));?>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<?php echo form_radio('payment', '0', true,array('id'=>'online','style'=>'display:none')); ?>
										<?php echo form_label('Online Payment', 'onlinelabel',array('id'=>'onlinelabel','style'=>'display:none;font-weight:400;'));?>
										<br class="mobileView"/>
										<div class="clearfix"></div>
										<small>(Payment method options will be altered after selecting address)</small>
									</div>
									<div class="clearfix"></div>
									<hr />
									<div class="col-sm-3 nopad"> 
										<label>Delivery Time</label>
									</div>
									<div class="col-sm-9 nopad">
										<?=form_dropdown('timeslots', $timeslots, set_value('timeslots'), array('id'=>'timeslots', 'class'=>'form-control','required'=>'required'))?>
									</div>
									<div class="clearfix"></div>
									<hr />
									<!--
									<div class="col-sm-3"> 
										<label>Payment Mode</label>
									</div>
									<div class="col-sm-9">
										<?php echo form_radio('paymentmode', '2', true); ?></td><td><?php echo form_label('Cash On Delivery', 'paymentmode');?>&nbsp;&nbsp;&nbsp;
										<?php echo form_radio('paymentmode', '1', false); ?></td><td><?php echo form_label('Make On-line Payment', 'paymentmode');?>&nbsp;&nbsp;&nbsp;
									</div>
									<div class="clearfix"></div>
									<hr />
									-->
									<div class="col-sm-5 nopad">										
										<input type="checkbox" onclick="$('#couponcode').toggle('slow');" / id="chkbox"> Check if you have a coupon code.
									</div>
									<div class="col-sm-7 nopad">										
										<div id="couponcode" style="display:none;">
											<div class="input-group" style="margin:0;">
												<input type="text" class="form-control" name="coupon" id="code">
												<input type="text" class="form-control" name="couponnew" id="codenew" style="display:none">
													<input type="text" class="form-control" name="ptcode" id="ptcode" style="display:none">
												<input type="text" class="form-control" name="ptype" id="cartamt" name="cart_amt" style="display:none">
												<span class="input-group-btn">
													<button type="button" class="btn btn-success" onclick="checkCode();">Apply Code</button>
												</span>
											</div>
											<p id="message1"><span class="text-success" ></span></p>
											<p hidden id="message2"><span class="text-danger">Invalid coupon code</span></p>
											<p hidden id="message3"><span class="text-danger">Please enter some value</span></p>
											<input type="text" name="promo_id" id="promo_id" hidden>
											<input type="text" name="discount" id="discount" hidden>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr />
									<div class="col-sm-12 nopad">
										<!--
										<a href="<?=site_url();?>/ordercontroller/place_order" class="btn btn-success" style="width:100%">
											Checkout
										</a>
										-->
										
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
				</div>
			</div>
			
			
			<div class="col-lg-4">
				<div class="bs-docs-example" style="overflow-x:hidden;">
					<h2>Order Summary</h2>
					<hr />
					<table class="table table-striped">
						<thead>
							<!--<tr>
								<th>Particular</th>
								<th>Price</th>
							</tr>-->
							<tr>
								<th>Order subtotal	</th>
								<th id="sub"><i class="fa fa-inr"></i><?=$this->cart->format_number($this->cart->total())?></th>
								<input type="text" value="<?=$this->cart->format_number($this->cart->total())?>" name="subtotal" id="subtotal" hidden>
							</tr>
							<tr>
								<th>Discounted amount</th>
								<th id="disc_charges"><i class="fa fa-inr">0</i></th>
							</tr>
							<tr>
								<th>Delivery Charges</th>
								<th id="info"><i class="fa fa-inr">0</i></th>
								<input type="text" value="0" name="delivery_charges" id="del_charge" hidden>
							</tr>
							<!--
							<tr>
								<th>Discount</th>
								<th>0%</th>
							</tr>
							-->
							<tr>
								<th><b>Total Payable</b></th>
								<th id="totalPayable"><i class="fa fa-inr"></i><?=$this->cart->format_number($this->cart->total());?></th>
								<input type="text" value="<?=$this->cart->format_number($this->cart->total());?>" name="total_amt" id="total_amt" hidden>
							</tr>
						</thead>
					</table>
					<input type="submit" value="Checkout" onClick="return confirm('Are you sure you want continue with selected delivery details?')" class="btn btn-success col-sm-12" id="mobilebottombtn1" style="background-color:black;" name="check_page" />
				</div>
			</div>
			<?=form_close()?>
			<div class="clearfix"></div>
			<div class="col-lg-12 desktopView">
				<table class="table table-striped">
					<thead>
						<tr>
							<th colspan="2" class="text-center">Product</th>
							<th>Size</th>
							<th>Quantity</th>
							<th>Unit&nbsp;Price</th>
							<th colspan="2">Total</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($cart_data as $cart) {?>
						<tr>
							<td>
								<img src="<?=IMAGEBASEPATH.$cart['options']['prod_image_url']?>" style="width:75px; float:left" />
							</td>
							<td><p style="float:left;"><b><?=$cart['name']?></b></p></td>
							<td><?=$cart['options']['product_size_name']?></td>
							<td><?=$cart['qty']?></td>
							<td><?=$cart['price']?></td>
							<td><?=$cart['subtotal']?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="col-lg-12 mobileView">
				<h2>Order Products</h2>
				<?php foreach($cart_data as $cart) {?>
					<hr />
					<div class="col-sm-4 col-xs-4" style="padding:0;font-size:12px;">
						<img src="<?=IMAGEBASEPATH.$cart['options']['prod_image_url']?>" style="width:100%;" />
					</div>
					<div class="col-sm-8 col-xs-8">
						<p style="margin:7px 0; font-weight:600;"><?=$cart['name']?></p>
						<p style="margin:7px 0;">Price : <i class="fa fa-inr"></i> <?=$cart['price']?></p>
						<p style="margin:7px 0;" >
							Size: <?=$cart['options']['product_size_name']?> 
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							Quantity : <?=$cart['qty']?>
						</p>
						<a href="<?=site_url()?>/Cartcontroller/remove_item/<?=$cart['rowid']?>" style="float: right; position: absolute; top: 0; right: 0;">
							<i class="fa fa-trash-o"></i>
						</a>
					</div>
					<div class="clearfix"></div>
					<hr style="padding:0;" />
				<?php } ?>
			</div>
		</div>	
	</div>
</div>

<?php $i=0; foreach($addresses as $add){ $i++;?>
	<div class="commonModal" id="input_address<?=$i?>" style="display:none;">
		<div class="row">
			<div class="col-md-offset-3 col-md-6 text-center sec" style="padding:15px 10px;">
				<i class="fa fa-close pull-right c" aria-hidden="true" id="close_address<?=$i?>" onclick="checkoutupdu('<?=$i?>');" style="display:none;"></i>
				<div class="clearfix"></div>
				<?=form_open('UpdateProfile/update_address', 'onsubmit=\'return validateUpdateProfileForm("caddress");\'')?>
					<div class="row">
						<input type="text" hidden name="addressId" value="<?=$add->address_id?>" />
						<div class="col-md-6 sol-xs-12">
							<?=form_dropdown('state', $states, set_value('state',$add->state_id), array('id'=>'chState'.$i, 'class'=>'form-control','required'=>'required'))?>
						</div>
						<div class="col-md-6 sol-xs-12">
						<?=form_dropdown('city', $cities, set_value('city',$add->city_id), array('id'=>'chCity'.$i, 'class'=>'form-control','required'=>'required'))?>
							<br />
						</div>
						<div class="clearfix"></div>
						<div class="col-md-6 sol-xs-12">
							<?=form_dropdown('chPin', $pincodes, set_value('pincodes',$add->pincode_id), array('id'=>'chpinId'.$i, 'class'=>'form-control','required'=>'required'))?>
							<br />
							<p style="position: absolute;bottom: 3px;color: red;" id="err_pin<?=$i?>"></p>
						</div>
						<div class="col-md-6 sol-xs-12">
							<input type="text" value="<?=$add->area?>" id="chArea<?=$i?>" name="chArea" class="form-control" required/>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-12 sol-xs-12">
							<textarea name="chAddress" class="form-control"><?=$add->address?></textarea><br />
							<input type="submit" value="update" name ="cartupdateAddress" class="btn btn-info" />
							<button type="button" onclick="$('.commonModal').hide();" class="btn btn-md btn-warning">Cancel</button>
						</div>
					</div>								
				<?=form_close()?> 
			</div>
		</div>
	</div>
<?php }?>
<script> var site_url = "<?php print site_url(); ?>"; </script>
<script>
 var promo_type=false;
	function checkoutupdd(n){
		$('#input_address'+n).slideDown();
	}
	function checkoutupdu(n){
		$('#input_address'+n).slideUp();
	}
	/* $("input[name='delivery_address']").click(function(){
		$('.e').hide();
		$('#edit_address'+this.id).show();
	}); */
	$("input[name='delivery_address']").click(function(){
		$('.e').hide();
		$('#edit_address'+this.id).show();
		 var address_id = $("input[name='delivery_address']:checked").val();
		//alert(address_id);
		$.ajax({
			type: "POST",
			url: site_url+'/promo/get_delivery_chrages',
			data:{'add_id': address_id },
			success: function(data){
				//alert(data);
				var obj = $.parseJSON(data); 
				//alert(did);
				if(obj.delivery_data!=null){
					var did = obj.delivery_data.delivery_charges;
					if(promo_type==true)
					{
					$('#del_charge').val('0');
					$('#info').html('<i class="fa fa-inr"></i>'+'0');
					}else{	
					$('#del_charge').val(did);
					$('#info').html('<i class="fa fa-inr"></i>'+did);
					}
					
					if($('#ptcode').val()=="6")
					{	
						
						$("#cartamt").each(function(){
   
                        $(this).html($(this).html().replace(/,/g , ''));
                    });
                    
						var totalarray = $('#cartamt').val();
						var total = totalarray.replace(/,/g, "");
					}else if($('#ptcode').val()=="3"){
					    	$("#total_amt").each(function(){
   
                        $(this).html($(this).html().replace(/,/g , ''));
                    });
						var totalarray = $('#total_amt').val();
						 var total = totalarray.replace(/,/g, "");
					}else{
					
    				var totalarray = $("#total_amt").val();
    
                    var total = totalarray.replace(/,/g, "");
                     //var total = $("#total_amt").replace(',', '');
                    
    					
					//	var total = $('#subtotal').val();
					}
					var del_charge = $('#del_charge').val();
					
   
				//	var array = total.split(",");

                  // var strVale = total;
                    // var intValArray=strVale.split(',');
                    // for(var i=0;i<intValArray.length;i++){
                      //   intValArray =parseInt(intValArray.append(i));
                    //}
					var totalPayable = parseFloat(total)   + parseInt(del_charge);
					$('#totalPayable').html('<i class="fa fa-inr"></i>'+totalPayable);
						$("#total_amt").val(totalPayable);
					if(obj.delivery_data.is_cod !=0)
					{
						$('#cod').show();
						$('#online').attr('checked',true);
						$('#codlabel').show();
						$('#online').show();
						$('#onlinelabel').show();
					}
					else{
						$('#cod').hide();
						$('#cod').attr('checked',false);
						$('#codlabel').hide();
						$('#online').show();
						$('#onlinelabel').show();
					}
					return false;
				}
				else{
					$('#del_charge').val('0');
					$('#cod').hide();
					$('#codlabel').hide();
					$('#online').hide();
					$('#onlinelabel').hide();
					$("input[name='delivery_address']").attr('checked', false);
					$('#commonModelTitle').text('Delivery Alert');
					$('#commonModelMessage').text('Sorry, this address is not available for delivery, please select another, update or add new.');
					$('#commonModal').modal('show');
					return true;
				}
			}
		});
	});
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="<?=base_url()?>website_assets/js/web/promo.js"></script>