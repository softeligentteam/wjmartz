<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/emails/order_delivered.css"/>
    <title>Customer Registration | WJMartz</title>
</head>
<body width="100%" bgcolor="#fff" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width: 100%; background: #fff; text-align: left;">

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display: none; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
            WJMartz : Customer Registration.
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <!-- Email Header : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
            <tr>
                <td style="padding: 20px 0; text-align: center">
                    <img src="<?=base_url();?>/website_assets/images/icon.png" alt="alt_text" border="0" style="height: 100px; background: #fff; font-family: sans-serif; font-size: 15px; line-height: 50px; color: #555555;">
                </td>
            </tr>
        </table>
        <!-- Email Header : END -->

        <!-- Email Body : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
			
            <!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding: 40px 40px 20px; text-align: center;">
                    <h1 style="margin: 0; font-family: sans-serif; font-size: 24px; line-height: 27px; color: #333333; font-weight: normal;">
						Welcome <?=$customer_data->first_name.' '.$customer_data->last_name?>!
					</h1>
                </td>
            </tr>
            <tr>
                <td bgcolor="#ffffff" style="padding: 0 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                    <p class="lead">
						Thank you for choosing us. We are pleased to welcome you as a new member of <b>WJMartz</b>. We feel honored that you have chosen us to fill your product needs, and we are eager to be of service.
					</p> 
					<hr />
                </td>
            </tr>
			<!-- 1 Column Text : BEGIN -->
			<tr>
				<td bgcolor="#ffffff">
					<table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr>
							<td style="padding: 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
								Incase of any doubt or query, please contact our customer care help desk, we will be happy to help.
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- 1 Column Text : END --> 
		</table>
		<!-- Email Body : END -->

		<!-- Email Footer : BEGIN -->
		<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto; font-family: sans-serif; color: #888888; line-height:18px;" class="email-container">
			<tr>
				<td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; line-height:18px; text-align: center; color: #888888;" class="x-gmail-data-detectors">
					WJMartz<br><br>CST No. 50 Fl. 6, Sankalp Campus,<br />Shivajinagar, Pune - 411005.
					<br><br>
					Email : Support@wjmartz.com 
					<br><br>
					<a href="http://www.wjmartz.com/about.html">About Us </a> | 
					<a href="http://www.wjmartz.com/terms_conditions.html">Terms & Conditions </a> | 
					<a href="http://www.wjmartz.com/privacy_refund_policies.html">Privacy & Return Policy</a>
				</td>
			</tr>
		</table>
		<!-- Email Footer : END -->
    </center>
</body>
</html>