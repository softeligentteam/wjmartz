 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function __construct() 
	{
        parent::__construct();
		if(NULL == $this->session->userdata('user_id')){
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="">Not Logged In</span>',
					'flash_msg' => 'Please login first to access the user dashboard.',
				)
			);
            redirect('website_home');
        }
		$this->load->model('UserModel');
		$this->load->model('HeaderData');
		$this->load->model('OrderModel');
		$this->load->model('Ddlmodel');
		$this->header_data['parent_categories'] = $this->HeaderData->get_all_parent_cat_menu_items();
		$this->header_data['child_categories']  = $this->HeaderData->get_all_child_cat_menu_items();
		
	}	
	public function index()
	{
		$uid = $this->session->userdata('user_id');
		$data['order_list'] = $this->UserModel->get_all_orders_user_wise($uid);
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/user/dashboard', $data);
		$this->load->view('layouts/website_footer');
	}
	
	public function view_return_orders()
	{
		$uid = $this->session->userdata('user_id');
		$data['order_list'] = $this->UserModel->get_refund_order_data($uid);
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/user/view_return_orders', $data);
		$this->load->view('layouts/website_footer');
	}
	
	
	public function return_order_details(){
		$order_id = $this->uri->segment(3,0);
		$data['order_details'] = $this->UserModel->get_return_order_details($order_id);
		$data['order_products'] = $this->UserModel->get_order_products($order_id);
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/user/return_order_details', $data);
		$this->load->view('layouts/website_footer');
	}
	
	
	public function order_details(){
		$order_id = $this->uri->segment(3,0);
		$data['order_details'] = $this->UserModel->get_order_details($order_id);
		$data['order_products'] = $this->UserModel->get_order_products($order_id);
		//print_r($order_id);
		//echo'<br /><br /><br />';
		//print_r($data);
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/user/order_details', $data);
		$this->load->view('layouts/website_footer');
	}
	
	
	
	public function view_my_profile()
	{
		$this->header_data['pincodes']  = $this->Ddlmodel->get_all_pincodes();
		$data['cities']= $this->Ddlmodel->get_all_cities();
		$data['states']= $this->Ddlmodel->get_all_states();
		
		$data['profile'] = $this->UserModel->get_users_profile(); 
		$data['address_count'] = $this->UserModel->get_users_addresses_count($data['profile']['cust_id']);
		$data['addresses'] = $this->UserModel->get_users_addresses($data['profile']['cust_id']);
		// print_r($data['profile']['cust_id']);
		// echo'<br /><br />';
		// print_r($data['addresses']);
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/user/profile', $data);
		$this->load->view('layouts/website_footer');
	}
	
	public function update_profile()
	{
		$data['profile'] = $this->UserModel->get_users_profile();
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/user/edit_profile', $data);
		$this->load->view('layouts/website_footer');
	}
	
	public function change_password()
	{
		$data[] = NULL;
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/user/change_password', $data);
		$this->load->view('layouts/website_footer');
	}
	
}	
	