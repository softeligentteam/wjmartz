<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_registration extends CI_Controller {
	
	public function __construct() 
	{
        parent::__construct();
		$this->load->model('UserReg');
		$this->load->model('LoginModal');
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->output->set_header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
		$this->load->model('HeaderData');
		$this->header_data['parent_categories'] = $this->HeaderData->get_all_parent_cat_menu_items();
		
	}	
	public function set_reg_user_temp_session_data_and_send_otp()
	{
		$this->UserReg->unset_reg_user_temp_session_data();
		$otp = rand(100000, 999999);
		$this->session->set_userdata(
			array(
				'reg_fname' => $this->input->post('fname'), 
				'reg_lname' => $this->input->post('lname'), 
				'reg_mobile' => $this->input->post('mobile'), 
				'reg_email' => $this->input->post('email'), 
				'reg_password' => $this->input->post('password'),
				'last_sent_otp' => $otp
			)
		);	
		$mobile=$this->session->userdata('reg_mobile');
		$this->load->library('sms_gateway');
		$this->sms_gateway->genrate_otp($mobile,$otp);
		// echo json_encode($_POST);
		echo 'true';
	}
	
	public function check_for_mobile_number_is_unique(){
		$mobile =  $this->input->post('number');
		$this->db->select('mobile');
        $this->db->where('mobile', $mobile);
        $db_mobile_no = $this->db->get('customer')->result();
        if($db_mobile_no == null){echo '0';}
        else{echo '1';}
	}
	
	public function check_for_email_is_unique(){
		$email = $this->input->post('email');
		$this->db->select('email');
        $this->db->where('email', $email);
        $db_mobile_no = $this->db->get('customer')->result();
        if($db_mobile_no == null){echo '0';}
        else{echo '1';}
	}
	
	public function set_otp_in_temp_session()
	{
	    $mobile = $this->input->post('mobile'); 
	    $otp = rand(100000, 999999);
	    $this->load->library('sms_gateway');
		$result1=$this->sms_gateway->genrate_otp($mobile,$otp);
		if($result1 != null){
			
			$this->session->set_userdata(array('last_sent_otp' => $otp));
			echo 'true';
		}else{echo 'false';}
	}
	
	public function verify_temp_session_otp()
	{
		$userOtp = $this->input->post('otp');
		if($userOtp == $this->session->userdata('last_sent_otp')){
			echo 'true';
			$this->session->unset_userdata('otp');
			$this->session->set_userdata(array('otpVerified'=>'true'));
		}else{echo 'false';}
	}
	
	/* public function get_address_details_using_pincode()
    {
        if($this->input->post())
        {
            $pincode = $this->input->post('pincode');
            $findPincode = $this->UserReg->find_pincode($pincode);
            if(null != $findPincode){
                $addressDetails = $this->UserReg->get_state_city_pincode($findPincode->pincode_id);
                $addressDetails['areas'] = $this->UserReg->get_pincode_wise_areas($findPincode->pincode_id);
                echo json_encode($addressDetails);
            }
            else{
                echo 'false';
            }
        }
        else{
            echo 'false';
        }
    } */
	
	public function register_user_and_log_in()
	{
		if($this->UserReg->register_user()){
			$mobile = $this->session->userdata('reg_mobile');
			$uid = $this->UserReg->get_uid_of_reg_user_and_clear_reg_session($this->session->userdata('reg_mobile'));
			$data['customer_data']=$this->UserReg->get_uid_of_reg_user_and_clear_reg_session($mobile);
			$data['promo']=$this->UserReg->get_promocode();
			$this->UserReg->unset_reg_user_temp_session_data();
			$this->LoginModal->set_login_session($uid->cust_id);
			$message=$this->load->view('email/welcome',$data,TRUE);
			$this->load->library('email_manager');
			$this->email_manager->send_welcome_email($data['customer_data']->email,$message);
			$this->load->library('sms_gateway');
			$this->sms_gateway->send_sms_for_welcome($data['customer_data']->mobile,$data['customer_data']->first_name);
			//$this->sms_gateway->send_sms_for_promocode($data['customer_data']->mobile,$data['customer_data']->first_name,$data['promo']->discount,$data['promo']->promocode);
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="color:green;">Registration Successful!</span>',
					'flash_msg' => 'Welcome '.$this->session->userdata('name').'<br /> Your are registered and logged in successfully!'
				)
			);
			redirect('user');
			//var_dump($data['customer_data']);
		}
		else{
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="color:red;">Registration Failed!</span>',
					'flash_msg' => 'Registration failed, please try again.'
				)
			);
			redirect('website_home');
		}
	}
	
	
	public function forgot_password()
	{
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/website/forgotpassword');
		$this->load->view('layouts/website_footer');
	}
	
	public function fp_throw_otp()
	{
		if($this->input->post()){
			$data['user_data'] = $this->UserReg->fp_find_mobile_in_db();
			$mobile=$this->input->post('fpmobile');
			// print_r($data['uid']);
			$otp=rand(100000, 999999);
			if($data['user_data'] != NULL){
				$this->session->set_userdata(array(
					'uid'=>$data['user_data']->cust_id,
					'fpotp'=>$otp
				));
				if($this->session->userdata('fpotp') != NULL)
				{
					$this->load->library('sms_gateway');
					$this->sms_gateway->genrate_otp($mobile,$otp);
					
					$this->load->view('layouts/website_header',$this->header_data);
					$this->load->view('pages/website/fp_confirm_number', $data);
					$this->load->view('layouts/website_footer');
				}else{
					$this->session->set_flashdata(array(
						'flash_title'=>'Technical Error',
						'flash_msg'=>'Something went wrong, please try again.'
					));
					redirect('user_registration/forgot_password');
				}
			}else{
				$this->session->set_flashdata(array(
						'flash_title'=>'Not Registered',
						'flash_msg'=>'This number is not registered.'
					));
				redirect('user_registration/forgot_password');
			}
		}else{
			redirect('user_registration/forgot_password');
		}
	}
	
	public function fp_confirm_otp()
	{
		if($this->session->userdata('fpotp') == $this->input->post('fpuserotp'))
		{
    		$this->load->view('layouts/website_header',$this->header_data);
    		$this->load->view('pages/website/fp_changepassword');
    		$this->load->view('layouts/website_footer');
		}
		else{
		    	$this->session->set_flashdata(array(
						'flash_title'=>'Incorrect OTP',
						'flash_msg'=>'OTP authentication failed, please enter correct OTP.'
					));
					redirect('user_registration/forgot_password');
		}
	}
	
	public function fb_change_password()
	{
		
		if($this->UserReg->forgot_password_change()!=0){
			$this->session->set_flashdata(array(
				'flash_title'=>'Password Reset',
				'flash_msg'=>'You have successfully reset your password,<br /> Please login to continue.'
			));
			redirect('website_home');
		}else{
			$this->session->set_flashdata(array(
				'flash_title'=>'Failed To Reset',
				'flash_msg'=>'Something went wrong, can not reset your password,<br />please try aagin.'
			));
			redirect('website_home');
		}
	}
}	
	