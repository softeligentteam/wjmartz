<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website_home extends CI_Controller {
	
	public function __construct() 
	{
        parent::__construct();
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->output->set_header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
		$this->load->model('HeaderData');
		$this->load->model('Products');
		$this->load->model('Ddlmodel');
		// $this->menu['cart']=$this->cart->contents();
		$this->header_data['parent_categories'] = $this->HeaderData->get_all_parent_cat_menu_items();
		$this->header_data['child_categories']  = $this->HeaderData->get_all_child_cat_menu_items();
		$this->header_data['pincodes']  = $this->Ddlmodel->get_all_pincodes();
		$this->active_page  = array(
			'homepage' => null,
			'about' => null,
			'contact' => null,
			'active_cat' => null
		);
	}	
	
	public function index()
	{
		$data['sliders']= $this->HeaderData->get_sliders();
		$data['new_arrivals']= $this->HeaderData->get_new_arrivals();
		$data['deals_of_the_day']= $this->HeaderData->get_deals_of_the_day();
		$data['parent_cats']= $this->HeaderData->fetch_parent_categories();
	    
	    // START Responsive Chnages from alex : 12-05-2018
	    $data['appsliders']= $this->HeaderData->get_app_sliders();
		$data['all_parent_cats']= $this->HeaderData->fetch_all_parent_categories();
		$data['sections']= $this->HeaderData->get_section_wise_category_preference();
		$data['all_size']= $this->Products->get_productsizes();
	    // END Responsive Chnages from alex : 12-05-2018
		
		$this->active_page['homepage'] = 'menu__item--current';
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/website/home',$data);
		$this->load->view('layouts/website_footer');
	}
	
	public function about()
	{
		$this->active_page['about'] = 'menu__item--current';
		$this->load->view('layouts/website_header',$this->header_data);
		
		$this->load->view('pages/website/about');
		$this->load->view('layouts/website_footer');
	}	
	
	public function contact()
	{
		$this->active_page['contact'] = 'menu__item--current';
		$this->load->view('layouts/website_header',$this->header_data);
		
		$this->load->view('pages/website/contact');
		$this->load->view('layouts/website_footer');
	}
	
	public function under_construction()
	{
		$this->active_page['contact'] = 'menu__item--current';
		$this->load->view('layouts/website_header',$this->header_data);
		
		$this->load->view('pages/website/under_construction');
		$this->load->view('layouts/website_footer');
	}
	
	public function product_list()
	{
		$data=array(
			'pcat_id' => $this->uri->segment(3,0),
			'ccat_id' => $this->uri->segment(4,0)
		);
		$this->active_page['active_cat'] = $data['pcat_id'];
		$data['product_list']= $this->Products->get_pcat_ccat_wise_product_list($data['pcat_id'], $data['ccat_id']);
		if($data['product_list'] == null)
		{
			$data['pcat_id'] = 1; // Footwares id - it's default
			$data['ccat_id'] = 0;
			$data['product_list']= $this->Products->get_pcat_ccat_wise_product_list($data['pcat_id'], $data['ccat_id']);
		}
		$data['ptype_list'] = $this->Products->get_product_type_list();
		$data['pcat_name'] = $this->Products->get_parent_cat_name($data['pcat_id']);
		$data['ccat_name'] = $this->Products->get_child_cat_name($data['ccat_id']);
		$data['child_cats'] = $this->Products->get_child_cats($data['pcat_id']);
		$data['product_size'] = $this->Products->get_productsizes();
		$data['product_color'] = $this->Products->get_productcolor();
		$data['pp_filterlist'] = array('0' => 'No filter', '1' => 'Price - High To Low', '-1' => 'Price - Low To High');
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/website/product_list',$data);
		$this->load->view('layouts/website_footer');
	}
	
	public function product_details()
	{		
		$data=array(
			'pcat_id' => $this->uri->segment(3,0),
			'ccat_id' => $this->uri->segment(4,0),
			'prod_id' => $this->uri->segment(5,0)
		);
		
		$data['product_details']= $this->Products->get_single_product_details($data['prod_id']);
		$data['recommended_products_details']= $this->Products->get_recommended_products($data['prod_id'],$data['ccat_id']);
		$prod_sizes = explode(',',$data['product_details']->fk_psize_ids);
		$data['product_sizes']= $this->Products->get_product_sizes($prod_sizes, $data['prod_id']);
		$this->active_page['active_cat'] = $data['pcat_id'];
		
		$this->load->view('layouts/website_header',$this->header_data);
		
		$this->load->view('pages/website/product_details',$data);
		$this->load->view('layouts/website_footer');
	}
	
	public function privacy_policies()
	{
		$this->load->view('layouts/website_header',$this->header_data);
		
		$this->load->view('pages/website/policies');
		$this->load->view('layouts/website_footer');
	}
	
	public function terms_conditions()
	{
		$this->load->view('layouts/website_header',$this->header_data);
		
		$this->load->view('pages/website/terms');
		$this->load->view('layouts/website_footer');
	}
	/*-----code by kanchan[date:-10/04/18]----- commented by KC - 14/05/2018*/
	/*public function high_price_product_list()
	{
		$data=array(
			'pcat_id' => $this->uri->segment(3,0),
			'ccat_id' => $this->uri->segment(4,0)
		);
		$this->active_page['active_cat'] = $data['pcat_id'];
		$data['product_list']= $this->Products->get_filter_high_price($data['pcat_id'],$data['ccat_id']);
		$data['pcat_name'] = $this->Products->get_parent_cat_name($data['pcat_id']);
		$data['ccat_name'] = $this->Products->get_child_cat_name($data['ccat_id']);
		$data['child_cats'] = $this->Products->get_child_cats($data['pcat_id']);
		$data['product_size'] = $this->Products->get_productsizes();
		$data['product_color'] = $this->Products->get_productcolor();
		$data['selecthl'] = NULL;
		$data['lth'] = NULL;
		$data['htl'] = 'selected';
		
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/website/product_list',$data);
		$this->load->view('layouts/website_footer');
	}
	public function low_price_product_list()
	{
		$data=array(
			'pcat_id' => $this->uri->segment(3,0),
			'ccat_id' => $this->uri->segment(4,0)
		);
		$this->active_page['active_cat'] = $data['pcat_id'];
		$data['product_list']= $this->Products->get_filter_low_price($data['pcat_id'],$data['ccat_id']);
		$data['pcat_name'] = $this->Products->get_parent_cat_name($data['pcat_id']);
		$data['ccat_name'] = $this->Products->get_child_cat_name($data['ccat_id']);
		$data['child_cats'] = $this->Products->get_child_cats($data['pcat_id']);
		$data['product_size'] = $this->Products->get_productsizes();
		$data['product_color'] = $this->Products->get_productcolor();
		$data['selecthl'] = NULL;
		$data['lth'] = 'selected';
		$data['htl'] = NULL;
		
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/website/product_list',$data);
		$this->load->view('layouts/website_footer');
	}*/

	/*-----code by kanchan[date:-10/04/18]---------- Modified by KC - 24/05/2018-----*/
	public function search_product_list()
	{
		$data['product_list'] = null;
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$data['keyword'] = trim($this->input->post('search'));
			$data['product_list']= $this->Products->search_element($data['keyword']);
			$data['product_size'] = $this->Products->get_productsizes();
		}
		$this->load->view('layouts/website_header',$this->header_data,$data);
		$this->load->view('pages/website/search_product_list',$data);
		$this->load->view('layouts/website_footer');
	}
	
	/*-----code by kanchan[date:-11/04/18]-----*/
	public function check_pincode_charge()
	{
		$pincode=$this->input->post('pin');
		//echo $pincode;
		if($this->input->post())
		{	
			$data=$this->Products->check_pincodewise_charge($pincode);
		}
		else
		{
			$data=NULL;
		}
		echo json_encode($data);
	}
	
	
	/*----- KT[date:-27/04/18]-----*/
	public function send_email_for_contact_us()
	{
		if($this->input->post())
		{	
		 $send_welcome=$this->send_welcome_mail();
		 $send_enquiry=$this->send_enquiry_mail();
		 if($send_welcome!=NULL && $send_enquiry!=NULL) 
		 {
			$this->session->set_flashdata ( array (
						'flash_title' => '<sapn class="text-success">Success !</span>',
						'flash_msg' => '<sapn class="text-success">Your enquiry is submitted successfully!</span>'
				) );
				redirect('website_home/contact');
			
		 } else{
			 $this->session->set_flashdata ( array (
						'flash_title' => '<sapn class="text-danger">Failure !</span>',
						'flash_msg' => '<sapn class="text-danger">Something went wrong, Please try sending enquiry again.</span>'
			) ); 
			redirect('website_home/contact');
		 }
		}else{
			redirect('website_home/contact');
		}
		
	}
	/*----- KT[date:-27/04/18]-----*/
	private function send_welcome_mail()
	{
		$this->load->library('email_manager');
		$this->email_manager->initialize_email();
		$name['name']=$this->input->post('Name');
		$email=$this->input->post('Email');
		var_dump($email);
		$message= $this->load->view('email/contact_us',$name,TRUE);
		
		$this->email->from('support@wjmartz.com', 'Wjmartz');
		$this->email->to($email);
		$this->email->subject('Wjmartz Welcome');
		$this->email->message($message);
		return $this->email->send();		
		
	}
	/*----- KT[date:-27/04/18]-----*/
	private function send_enquiry_mail()
	{
		$this->load->library('email_manager');
		$this->email_manager->initialize_email();
		$remark=$this->input->post('Message');
		$name=$this->input->post('Name');
		$subject=$this->input->post('Subject');
		$email=$this->input->post('Email');
		$mobile=$this->input->post('Mobile');
		
		$message= "<html> 
						Name: ".$name." <br><br>
						Subject: ".$subject." <br><br>
						Email: ".$email." <br><br>
						Mobile: ".$mobile." <br><br>
						Message: <br><br>".$remark." 
						</html>";
		$this->email->from('support@wjmartz.com', 'Wjmartz');
		$this->email->to('support@wjmartz.com');
		$this->email->subject($this->input->post('Subject'));
		$this->email->message($message);
		return $this->email->send();		
		
	}
	
	// START Responsive Chnages from alex : 12-05-2018
	public function ccat_list()
	{
		$pcat_id = $this->uri->segment(3,0);
		$data['ccats'] = $this->HeaderData->get_pcat_wise_ccats($pcat_id);
		$this->load->view('layouts/website_header',$this->header_data);
		$this->load->view('pages/website/ccat_list',$data);
		$this->load->view('layouts/website_footer');
	}
	// END Responsive Chnages from alex : 12-05-2018

	public function product_image()
	{	
		$data['pcat_id'] = $this->uri->segment(3,0);
		$data['ccat_id'] = $this->uri->segment(4,0);
		$data['prod_id'] = $this->uri->segment(5,0);
		$data['product_details']= $this->Products->get_single_product_details($data['prod_id']);
		$this->load->view('layouts/website_header');
		$this->load->view('pages/website/image_zooming.php', $data);
	}
}