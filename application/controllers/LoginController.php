<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {
	
	public function __construct() 
	{
        parent::__construct();
		$this->load->model('UserReg');
		$this->load->model('LoginModal');
		
	}
	
	public function authenticate_and_login(){
		$userDetails = $this->LoginModal->authenticate_user();
		if($userDetails != null)
		{
			$this->LoginModal->set_login_session($userDetails->cust_id);
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="color:green;">Logged In Successfully!</span>',
					'flash_msg' => 'Welcome back '.$this->session->userdata('name')
				)
			);
			redirect('user');
		}else{
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="color:red;">Login Failed!</span>',
					'flash_msg' => 'You may have entered a wrong number or password; or you are not yet registered.<br />Please try to log-in again or register your self.'
				)
			);	
			redirect('login');
		}
	}
	
	public function logout(){
		if($this->LoginModal->unset_login_session()){
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="">Logged Out</span>',
					'flash_msg' => 'You are logged out successfully',
				)
			);
			redirect('website_home');
		}else{
			$this->session->set_flashdata(
				array(
					'flash_title' => '<span style="color:red;">Request Failed</span>',
					'flash_msg' => 'There seems to be an issue, please try logging out again.'
				)
			);	
			redirect('user');
		}
	}
	
	public function check_mobile_for_login(){
		$userCount = $this->LoginModal->check_mobile_for_login();
		echo $userCount;
	}
	
	public function check_password_for_login(){
		$userCount = $this->LoginModal->check_password_for_login();
		echo $userCount;
	}
	
}