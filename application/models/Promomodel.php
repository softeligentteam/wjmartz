<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promomodel extends CI_Model
{
	public function get_promocode($coupon_code)
	{
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	}

	public function get_promocodetype($coupon_code)
	{
		$this->db->select('promo_type_id');
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	}

	public function get_order_count()
	{
	  $cust_id=$this->session->userdata('user_id');
	  $this->db->where('cust_id',$cust_id);
	  return $this->db->get('order')->num_rows();
	}
	
	
	public function get_deal_status_array()
	{
		$prod_id=$this->get_product_id();
		$this->db->select('deal_status');
		$this->db->where_in('prod_id',$prod_id);
		$product_details_coupon= $this->db->get('products')->result();
		
		/*$productdetails = $this->input->post('product_details_coupon');
		$product_details_coupon = json_decode($productdetails, TRUE);*/
		$deal_status=array();
		 if(is_array($product_details_coupon))
		{
		foreach($product_details_coupon as $coupon)
			{
				$deal_status[]=$coupon->deal_status;
			}
		}
		return $deal_status;
	}

	
	
	
	public function get_data_for_cart($coupon_code)
	{
		$this->db->select('promo_id,promo_type_id,discount,promocode,child_cat_id');
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	}
	
	public function get_data_for_discount($coupon_code)
	{
		$this->db->select('promo_id,discount,promo_type_id');
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	}
	
	public function get_pincode_from_address($address_id)
	{
		$this->db->select('pincode');
		$this->db->where('address_id',$address_id);
		return $this->db->get('customer_address')->row_array();
	}
	
	public function get_delivery_charges($address_id)
	{
		$pincode_id=$this->get_pincode_from_address($address_id);
		
		$this->db->select('delivery_charges,is_cod');
		//$this->db->join('area a','a.area_id=p.area_id');
		$this->db->where_in('p.pincode_id',$pincode_id);
		$this->db->where('p.status',1);
		return $this->db->get('pincode p')->row();
	}

	//KT[date 7may 2018]
	public function get_childcat_promo($coupon_code)
	{
		$this->db->select('child_cat_id');
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	} 
	 //KT[date 7may 2018]
	public function get_discount_promo($coupon_code)
	{
		$this->db->select('discount');
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	} 
	
	 //KT[date 7may 2018]
	public function get_product_id()
	{
		$cart_data = $this->cart->contents();
		$prod_id=array();
		foreach($cart_data as $cart)
		{
			$prod_id[] =$cart['id']; 
		}
		return $prod_id;
	}
	
	 //KT[date 7may 2018]
	public function get_child_cat_product()
	{
		$product_id=$this->get_product_id();
		$prod_id=implode(" ",$product_id);
		$this->db->select('prod_id,fk_pchild_id,prod_price');
		$this->db->where_in('prod_id',$product_id);
		return $this->db->get('products')->result_array();
	}
	 //KT[date 7may 2018]
	public function get_child_cat_list_cart()
	{
		$productarray=$this->get_child_cat_product();
			$cartchildarray=array();
			foreach($productarray as $prod)
			{
				$cartchildarray[]=$prod['fk_pchild_id'];
			}
		return $cartchildarray;
	}
	  //KT[date 7may 2018]
	public function check_coupon_code($coupon_code)
	{
		$flag=FALSE;
		$child_cat=$this->get_childcat_promo($coupon_code);
		$child_cat_cart=$this->get_child_cat_list_cart();
		$size_new=sizeof($child_cat_cart);
		$discount_amount=$this->get_discount_promo($coupon_code);
		
		$ptype=$this->get_promocodetype($coupon_code);
		$order_count=$this->get_order_count();
		$deal_status=$this->get_deal_status_array();
		$size=sizeof($deal_status);
		$call_user=$this->get_promocode($coupon_code);
		$user_count=$call_user->applicable_promo;
		$child_cat_id=$call_user->child_cat_id;
	
		if($ptype->promo_type_id == "3")
		{
			
			for($i=0;$i<$size;$i++)
		    {
			
		 	if($deal_status[$i]=="1")
			{
				$flag=FALSE;
				break;
			}
			else{
				$flag=FALSE;	
			}
			
		    }
		}	
		if($ptype->promo_type_id=="4")
		{
				for($i=0;$i<$size;$i++)
		    {
			if($order_count!=0 )	
			{
				$flag=TRUE;
				break;
			}
			else{
				$flag=FALSE;	
			}
		    }
		}
		if($ptype->promo_type_id=="5")
		{
			for($i=0;$i<$size;$i++)
		    {
			
		 	if($this->cart->format_number($this->cart->total()) > "1000")
			{
				$flag=TRUE;
				break;
			}
			else{
				$flag=FALSE;	
			}
			
		    }
		}
		if($ptype->promo_type_id=="6")
		{
			$cart_amount=$this->cart->format_number($this->cart->total());
			$productarray=$this->get_child_cat_product();
			$total_amount=0;
			for($i=0;$i<$size_new;$i++)
			{
				if($child_cat_cart[$i] == $child_cat->child_cat_id)
				{
					foreach($productarray as $prodarray)
					{
						$total_amount=(int)$total_amount + (int)$prodarray['prod_price'];
						 (int)$cart_amount - (int)$total_amount;
						 $disc_amount= (int)$total_amount *(int)$discount_amount->discount/100;
						$amount= (int)$total_amount - (int)$disc_amount;
						//$amt=(int)$cart_amount + (int)$amount;
						$this->session->set_userdata('cartamt',$amount);
					}
					
					
					//$this->session->set_userdata('cartamt',$total_amount);
					
					$flag=FALSE;
					break;
				}
				else{
						$flag=TRUE;	
				}
			}	
				
		}	
		return $flag;
		//var_dump($flag);
	}
	
}	