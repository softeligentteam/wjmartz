<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ddlmodel extends CI_Model{

	public function get_all_addresses_of_user()
	{
		$this->db->select('address_id,address,area,pi.pincode,ct.city');
		$this->db->join('pincode pi','pi.pincode_id = cad.pincode'); 
		$this->db->join('city ct','ct.city_id = cad.city_id');
		$this->db->join('state st','st.state_id = cad.state_id');
		$this->db->where('cust_id',$this->session->userdata('user_id'));
		$list= $this->db->get('customer_address cad')->result();
        $filtered_list [''] = 'Select your delivery address... ';
        if (count ( $list ) > 0) {
            foreach ( $list as $object ) {
                $filtered_list [$object->address_id] = $object->address.', '.$object->area.', '.$object->city.', '.$object->pincode.'.';
            }
        }
        return $filtered_list;
	}
		
	public function get_time_slots()
	{		
		$this->db->select('time_slot_id,time_slot,status,shift_status');
		$this->db->where('status','1');
		$list= $this->db->get('time_slots')->result();
        $filtered_list [''] = 'Select time slot...';
        if (count ( $list ) > 0) {
            foreach ( $list as $object ) {
                $filtered_list [$object->time_slot_id] = $object->time_slot;
            }
        }
        return $filtered_list;
	}
		
	public function get_all_pincodes()
	{		
		$this->db->select('pincode_id,pincode');
		$this->db->where('pincode_id != 0');
		$this->db->where('status !=', 0);
		$list= $this->db->get('pincode')->result();
        $filtered_list [''] = 'Select Pincode...';
        if (count ( $list ) > 0) {
            foreach ( $list as $object ) {
                $filtered_list [$object->pincode_id] = $object->pincode;
            }
        }
        return $filtered_list;
	}
	
	public function get_all_cities()
	{		
		$this->db->select('city_id,city');
		$this->db->where('city_id != 0');
		$list= $this->db->get('city')->result();
        $filtered_list [''] = 'Select city...';
        if (count ( $list ) > 0) {
            foreach ( $list as $object ) {
                $filtered_list [$object->city_id] = $object->city;
            }
        }
        return $filtered_list;
	}
	
	public function get_all_states()
	{		
		$this->db->select('state_id,state');
		$this->db->where('state_id != 0');
		$list= $this->db->get('state')->result();
        $filtered_list [''] = 'Select state...';
        if (count ( $list ) > 0) {
            foreach ( $list as $object ) {
                $filtered_list [$object->state_id] = $object->state;
            }
        }
        return $filtered_list;
	}
	
}