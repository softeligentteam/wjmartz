<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promomodel extends CI_Model
{
	public function get_promocode($coupon_code)
	{
		//$coupon_code='example123';
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	}

	public function get_promocodetype($coupon_code)
	{
		$this->db->select('promo_type_id');
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	}

	public function get_order_count()
	{
	  $cust_id=$this->input->post('cust_id');
	  $this->db->where('cust_id',$cust_id);
	  return $this->db->get('order')->num_rows();
	}

	public function get_deal_status_array()
	{
		$productdetails = $this->input->post('product_details_coupon');
		$product_details_coupon = json_decode($productdetails, TRUE);
		$deal_status=array();
		 if(is_array($product_details_coupon))
		{
		foreach($product_details_coupon['product_details_coupon'] as $coupon)
			{
				$deal_status[]=$coupon['deal_status'];
			}
		}
		return $deal_status;
	}

	
	public function check_coupon_code($coupon_code)
	{
		$flag=FALSE;
		$ptype=$this->get_promocodetype($coupon_code);
		$order_count=$this->get_order_count();
		$deal_status=$this->get_deal_status_array();
		$size=sizeof($deal_status);
		$call_user=$this->get_promocode($coupon_code);
		$user_count=$call_user->applicable_promo;
		$child_cat_id=$call_user->child_cat_id;
	
		if($ptype->promo_type_id == "3")
		{
			
			for($i=0;$i<$size;$i++)
		    {
			
		 	if($deal_status[$i]=="1")
			{
				$flag=TRUE;
				break;
			}
			else{
				$flag=FALSE;	
			}
			
		    }
		}	
		if($ptype->promo_type_id=="4")
		{
				for($i=0;$i<$size;$i++)
		    {
			if($deal_status[$i]=="1" || $order_count!=0 )	
			{
				$flag=TRUE;
				break;
			}
			else{
				$flag=FALSE;	
			}
		    }
		}
		
		return $flag;
		//var_dump($flag);
	}
	
	

	public function get_data_for_cart($coupon_code)
	{
		$this->db->select('promo_id,promo_type_id,discount,promocode,child_cat_id');
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	}

	public function get_data_for_promo_offers()
	{
		$this->db->select('promo_id,promo_type_id,discount,promo_image_url,description,promocode');
		$this->db->where('status',1);
		return $this->db->get('promocode')->result();
	}
	
	
	
}