<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parentcategorymodel extends CI_Model
{
	public function insert_parent_categories()
	{
		$parent_cat_name=$this->input->post('parent_category');
		$parent_cat_img=$this->upload_parent_cat_photo();
		$main_cat_id=1;
		
		$data=array(
		'parent_cat_name'=>$parent_cat_name,
		'parent_cat_img_url'=>$parent_cat_img,
		'main_cat_id'=>$main_cat_id
		);
		
		return $this->db->insert('parent_category',$data);
	}
	
	
	function upload_parent_cat_photo() {
    	$file_url = "";

			if ($_FILES["parent_cat_img"]["error"] == 0) {

				$file_element_name = 'parent_cat_img';

				$config['upload_path'] = PARENTCATUPLOADPATH;
		        $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
		        //$config['max_size'] = 1024 * 8;
		        // $config['max_width'] = 512;
		        // $config['max_height'] = 512;
		        $config['encrypt_name'] = TRUE;

		        $this->load->library('upload', $config);

		        if (!$this->upload->do_upload($file_element_name)) {
		        	$file_url = PARENTCATUPLOADPATH."/no_image.jpg";
		           // $msg = $this->upload->display_errors('', '');
		            // echo "<script>console.log($msg);</script>";
		        } else {
		        	$data = $this->upload->data();
		            $file_url = PARENTCATUPLOADPATH.$data['file_name'];
		            // echo "<script>console.log('File uploaded successfully.');</script>";
		        }

		        @unlink($_FILES[$file_element_name]);

			} else {
				 
				$file_url = PARENTCATUPLOADPATH."/no_image.jpg";
			}
			
			return $file_url;
    }
	
	public function get_parent_cat_list()
	{	
		$this->db->select('parent_cat_id,parent_cat_name,parent_cat_img_url');
		$this->db->where_not_in('parent_cat_id',0);
		//$this->db->where('main_cat_id',$main_cat_id);
		return $this->db->get('parent_category')->result();
	}
	
	public function del_sub_category($sub_cat_id)
	{
		$this->db->where('sub_cat_id',$sub_cat_id);
		return $this->db->delete('sub_category');
	}

}