<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model{
	
	public function get_all_orders_user_wise($uid)
	{
		$this->db->select('o.order_id,order_date,amount,o.ord_status_id,order_status,payment_status,discount_amount, o.refund_id, o.promocode_applied');
		$this->db->join('payment_details p','p.order_id=o.order_id');
		$this->db->join('order_status os','os.ord_status_id=o.ord_status_id');
		$this->db->where('o.cust_id',$uid);
		return $this->db->get('order o')->result();
	}
	
	public function get_return_orders_user_wise($uid)
	{
		$this->db->select('o.order_id,order_date,amount,o.ord_status_id,order_status,payment_status,discount_amount, o.refund_id');
		$this->db->join('payment_details p','p.order_id=o.order_id');
		$this->db->join('order_status os','os.ord_status_id=o.ord_status_id');
		$this->db->where('o.cust_id',$uid);
		$this->db->where('o.refund_id !=', null);
		return $this->db->get('order o')->result();
	}
	
	public function get_order_details($order_id)
	{
		$this->db->select('o.order_id,o.cust_id,o.first_name,o.last_name,email,mobile,o.address,p.pincode,o.area as area_name,
		ca.city,s.state,time_slot,delivery_date,o.ord_status_id,order_status,payment_status,delivery_charges,transaction_id,promocode,pc.discount,pd.discount_amount,order_date,o.modified_on,o.modified_on_cust,refund_id,amount,delivery_date,time_slot,pd.transaction_id,o.promocode_applied');
		$this->db->from('city ca');
		$this->db->from('state s');
		$this->db->join('customer c','c.cust_id=o.cust_id');
		$this->db->join('customer_address a','a.address_id=o.address_id');
		$this->db->join('pincode p','p.pincode_id=o.pincode');
		//$this->db->join('area ar','ar.area_id=a.area');
		//$this->db->join('city ca','ca.city_id=ar.city_id');
		//$this->db->join('state s','s.state_id=ca.state_id');
		$this->db->join('order_status os','os.ord_status_id=o.ord_status_id');
		$this->db->join('time_slots t','t.time_slot_id=o.time_slot_id');
		$this->db->join('payment_details pd','pd.order_id=o.order_id');
		$this->db->join('promocode pc','pc.promo_id=o.promocode_applied');
		$this->db->where('o.order_id',$order_id);
		return $this->db->get('order o')->row_array();
	}
	
	public function get_order_products($order_id)
	{
		$this->db->select('product_id,prod_name,prod_image_url,price,to.quantity,to.amount,size,sgst_amount,cgst_amount,to.size_id,p.refund_status');
		$this->db->join('products p','p.prod_id=to.product_id');
		//$this->db->join('package pa','pa.package_id=to.package_id');
		//$this->db->join('personalised_package_products pp','pp.sub_product_id=to.per_package_id');
		$this->db->join('product_sizes ps','ps.size_id=to.size_id');
		$this->db->where('order_id',$order_id);
		return $this->db->get('temp_order_details to')->result();	
	}
	
	public function get_users_profile()
	{
		$this->db->select('cust_id, first_name, last_name, dob, email, mobile'); 
		$this->db->where('cust_id', $this->session->userdata('user_id'));
		return $this->db->get('customer')->row_array();
	}
	
	public function get_users_addresses_count($user_id)
	{
		$this->db->select('count(address_id) as address_count');
		$this->db->where('cust_id', $user_id);
		return $this->db->get('customer_address')->row();
	}
	
	public function get_users_addresses($user_id)
	{
		$this->db->select('ca.address_id, ca.address, ca.area, pc.pincode_id, pc.pincode, ct.city, st.state,ca.city_id,ca.state_id');
		$this->db->join('pincode pc', 'pc.pincode_id = ca.pincode');
		// $this->db->join('area ar', 'ar.area_name = ca.area');
		$this->db->join('city ct', 'ct.city_id = ca.city_id');
		$this->db->join('state st', 'st.state_id = ca.state_id');
		$this->db->where('cust_id', $user_id);
		return $this->db->get('customer_address ca')->result();
	}
	
	public function get_refund_id($order_id)
	{
		$this->db->select('refund_id');
		$this->db->where('order_id',$order_id);
		return $this->db->get('order')->row_array();
	}
	
	public function get_refund_order_data($uid)
	{
		$this->db->select('ro.refund_id,ro.cust_id,o.first_name,o.last_name,ro.order_id,ro.address,ro.area,s.state,ci.city,p.pincode,ro.amount,ro.discount_amount, ro.refund_date, ro.order_date');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('customer c','c.cust_id=ro.cust_id');
		$this->db->join('pincode p','p.pincode_id=ro.pincode');
		$this->db->join('order o','o.order_id=ro.order_id');
		$this->db->where('ro.cust_id', $uid);
		return $this->db->get('refund_order ro')->result();
	}
	
	public function get_refund_products($order_id)
	{
		$refund_id=$this->get_refund_id($order_id);
		$this->db->select('product_id,prod_name,prod_image_url,price,ro.quantity,ro.amount,size,sgst_amount,cgst_amount,ro.size_id,p.refund_status');
		$this->db->join('products p','p.prod_id=ro.product_id');
		$this->db->join('product_sizes ps','ps.size_id=ro.size_id');
		$this->db->where_in('refund_id',$refund_id);
		return $this->db->get('refund_order_details ro')->result();	
	}
	
	public function get_return_order_details($order_id)
	{
		$this->db->select('ro.order_id, ro.refund_id, ro.cust_id,o.first_name,o.last_name,email,mobile,o.address,p.pincode,o.area as area_name,
		ca.city,s.state,o.order_date,o.promocode_applied,ro.refund_date');
		$this->db->from('city ca');
		$this->db->from('state s');
		$this->db->join('order o','o.order_id=ro.order_id');
		$this->db->join('customer c','c.cust_id=ro.cust_id');
		$this->db->join('customer_address a','a.address_id=o.address_id');
		$this->db->join('pincode p','p.pincode_id=o.pincode');
		
		$this->db->where('ro.order_id',$order_id);
		return $this->db->get('refund_order ro')->row_array();
	}
}