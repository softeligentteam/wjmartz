 <!DOCTYPE html>
 <html>
	 <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" type="text/css" href="http://www.denstagenie.com/website/website_assets/css/font-awesome.min.css">
		<style>
			body{
				font-family: sans-serif;
				font-size:14px;
				color:#646464; 
			} 
			.container{
				width:100%;
				margin:0;
			 }
			.topbar{
				background-color:#606062;
				padding:3px 0;
				color:white;
				width:100%;
			}
			 .logo{
				 text-align:center;
				 margin:7px 0;
				 width:100%;
			 }
			 .logo img
			 {
				 height:100px;
			 }
			 table{
				width: 100%;
				text-align: left;
				border: 1px solid #606062;
			}
			.no_mar_top{margin-top:none;}
			.no_mar_bottom{margin-bottom:none;}
			 
			td {
				padding: 7px 20px;
			}
			th{
				padding: 7px 20px;
				width:18%;
			}
			 
			.text-left{text-align:left; width:50%; float:left}
			.text-right{text-align:right; width:50%; float:left}
			.parag{color:gray; font-size:17px;}
			hr{border-color:#606062;}
			
		</style>
	 </head>
	 <body>
		 <div class="container">
			<table>
				<tr>
					<td colspan="4" class="topbar">
						<p class="text-left" style="color:white; margin:1px 0;">&nbsp;&nbsp;&nbsp;Call us +919284001876 </p></br>
						<p>(Between 10am to 6pm)</p>
						<p class="text-right" style="color:white; margin:1px 0;">Email us : support@wjmartz.com&nbsp;&nbsp;&nbsp;</p>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="logo">
							<img src="http://www.wjmartz.com/admin/assets/img/2.png" />
						</a>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="para">
						<hr>
						<p class="parag">
							Dear <?=$name?><br><br>
							We have recived your enquiry and we are glad to have your doubts and query answered.<br><br>
							We will get back to you as soon as possible.<br><br>
							Thank you!
						</p>
					</td>
				</tr>
				<tr> <td colspan="4"> </td> </tr>
				<tr>
					<td colspan="4" class="topbar">
						<p style="float:left; width:50%; text-align:left; margin:1px 0;">
							 
						</p>
						<p style="float:left; width:50%; text-align:right;  margin:1px 0;">
							 
						</p>
					</td>
				</tr>   
			</table>
		 </div>
		 <style>
			@media only screen and (min-width: 992px) and (max-width: 1199px){}
			@media only screen and (max-width: 768px) 
			{
				body{
					font-family: sans-serif;
					font-size:12px;
				} 
				.container{
					 width:100%;
					 margin:0; 
				 }
				 
				 .logo{
					 text-align:center;
					 margin:7px 0;
					 width:100%;
				 }
				 .logo img
				 {
					 height:100px;
				 }
				 table{
					width: 100%;
					text-align: left;
				}
				 
				td {
					padding: 7px 20px;
				}
				th{
					padding: 7px 20px;
				}
				tr .treatment_name{
					border:1px solid #ccc;
				}
			}
			@media only screen and (max-width: 425px) {
				
			}
			@media only screen and (max-width: 320px) {
				
			}
		</style>
	 </body>
 </html>