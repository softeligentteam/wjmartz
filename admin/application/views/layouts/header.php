<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>WJMartz | User_Dashboard</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="<?=base_url();?>assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="<?=base_url();?>assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="<?=base_url();?>assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TITLE ICON-->
	<link rel="shortcut icon" href="<?=base_url();?>assets/img/favicon.png" />
	<script>var site_url = "<?php print site_url(); ?>";</script>
	<script src="<?=base_url();?>assets/js/add/jQuery-2.2.0.min.js"></script>
	<script src="<?=base_url();?>assets/js/add/jquery-ui.min.js"></script>
	<script src="<?=base_url();?>assets/js/add/child_category.js"></script>
</head>
<body>
    <div id="wrapper">
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-square-o "></i>&nbsp;Welcome Admin</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right"> 
                        <li><a href="<?=site_url();?>/home/log_in">Logout</a></li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li class="text-center user-image-back">
                        <img src="<?=base_url();?>assets/img/2.png" class="img-responsive" />
                    </li>
                    <li>
                        <a href="<?=site_url();?>home/index"><i class="fa fa-desktop "></i>Dashboard</a>
                    </li>
					<li>
                        <a href="#"><i class="fa fa-edit "></i>Manage Categories<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
							<li>
								<a href="<?=site_url();?>parent_categories/view_all/"><i class="fa fa-table "></i>Parent Categories</a>
							</li>
							<li>
								<a href="<?=site_url();?>parent_categories/add_section_wise_pc/"><i class="fa fa-table "></i>Set Section Parent Categories</a>
							</li>
							<li>
								<a href="<?=site_url();?>child_categories/view_all/"><i class="fa fa-table "></i>Child Categories</a>
							</li>
							<li>
								<a href="<?=site_url();?>child_categories/view_all_inactive/"><i class="fa fa-table "></i>Inactive Child Categories</a>
							</li>
						</ul>
					</li>
					<li>
                        <a href="#"><i class="fa fa-edit "></i>Manage Products<span class="fa arrow"></span></a>
						
                        <ul class="nav nav-second-level collapse">
							<li>
								<a href="#"><i class="fa fa-edit "></i>Add Products<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
								<li>
								<a href="<?=site_url();?>products/add_women_wares/"><i class="fa fa-table "></i>Add Women Ware Products</a>
								</li>
								<li>
									<a href="<?=site_url();?>products/add_women_footwares/"><i class="fa fa-table "></i>Add Women Footware Products</a>
								</li>
								</ul>
							</li>	
							<li>
								<a href="#"><i class="fa fa-edit "></i>Add Size Chart<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
								<li>
								<a href="<?=site_url();?>products/view_women_ware_size_chart/"><i class="fa fa-table "></i>Add Women Ware Size Chart</a>
								</li>
								<li>
									<a href="<?=site_url();?>products/view_women_footware_size_chart/"><i class="fa fa-table "></i>Add Women Footware Size Chart</a>
								</li>
								</ul>
							</li>	
							<li>
								<a href="#"><i class="fa fa-edit "></i>view Products<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
								<li>
									<a href="<?=site_url();?>products/view_women_ware_products/"><i class="fa fa-table "></i>View Women Ware Products</a>
								</li>
								<li>
									<a href="<?=site_url();?>products/view_women_footware_products/"><i class="fa fa-table "></i>View Women Footware Products</a>
								</li>
								</ul>
							</li>	
							
							<li>
								<a href="#"><i class="fa fa-edit "></i>Inactive Products<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
								<li>
								<a href="<?=site_url();?>products/view_women_ware_products_inactive/"><i class="fa fa-table "></i> Women Ware Products</a>
								</li>
								<li>
									<a href="<?=site_url();?>products/view_women_footware_products_inactive/"><i class="fa fa-table "></i> Women Footware Products</a>
								</li>
								</ul>
							</li>	
						
						</ul>
					</li>
					<li>
						<a href="<?=site_url();?>brand_specials/brands/"><i class="fa fa-table "></i>Brands</a>
					</li>
					<li>
						<a href="<?=site_url();?>timeslots/view_timeslots/"><i class="fa fa-table "></i>Time Slots</a>
					</li>
					<!--<li>
						<a href="<?=site_url();?>pincode/pincode_wise_price_list/"><i class="fa fa-table "></i>Pincode Wise Prizes</a>
					</li>-->
					<li>
                        <a href="#"><i class="fa fa-edit"></i>Pincode Wise Prizes<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
							<li>
								<a href="<?=site_url();?>pincode/pincode_wise_price_list/"><i class="fa fa-table "></i>All</a>
							</li>
                            <li>
                                 <a href="<?=site_url();?>pincode/active_pincode_wise_price_list/"><i class="fa fa-table "></i>Active</a>
                            </li>
							 <li>
                                <a href="<?=site_url();?>pincode/inactive_pincode_wise_price_list/"><i class="fa fa-table "></i>Inactive</a>
                            </li>
                        </ul>
                    </li>
					<li>
                        <a href="#"><i class="fa fa-edit "></i>Manage Orders<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="<?=site_url();?>order/view_all/"><i class="fa fa-table "></i>View In Process Order</a>
                            </li>
                            <li>
                                <a href="<?=site_url();?>order/view_ready_to_dispatch/"><i class="fa fa-table "></i>Ready To dispatch Orders</a>
                            </li>
                            <li>
                                <a href="<?=site_url();?>order/view_delivered_order/"><i class="fa fa-table "></i>Deliverd Orders</a>
                            </li>
                            <li>
                                <a href="<?=site_url();?>order/view_cancelled_order/"><i class="fa fa-table "></i>Cancelled Orders</a>
                            </li>
							<li>
                                <a href="<?=site_url();?>order/view_failed_order/"><i class="fa fa-table "></i>Failed Orders</a>
                            </li>
							<li>
                                <a href="<?=site_url();?>order/view_refund_order/"><i class="fa fa-table "></i>Return Exchange Orders</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?=site_url();?>customer/view_all/"><i class="fa fa-table "></i>Customer Details</a>
                    </li>
					<li>
                        <a href="#"><i class="fa fa-edit "></i>Payment<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="<?=site_url();?>payment/successful_transaction/"><i class="fa fa-table "></i>Sucessful Transaction</a>
                            </li>
                            <li>
                                <a href="<?=site_url();?>payment/failure_transaction/"><i class="fa fa-table "></i>Failed Transaction</a>
                            </li>
                        </ul>
                    </li>
					<!--<li>
                        <a href="<?=site_url();?>slider/view_slider/"><i class="fa fa-table "></i>add slider </a>
                    </li>-->
					<li>
                        <a href="#"><i class="fa fa-edit"></i>add slider<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                 <a href="<?=site_url();?>slider/slider_for_app/"><i class="fa fa-table "></i>Android</a>
                            </li>
							 <li>
                                <a href="<?=site_url();?>slider/slider_for_website/"><i class="fa fa-table "></i>Website</a>
                            </li>
                        </ul>
                    </li>
					<li>
                        <a href="<?=site_url();?>products/view_deals_of_the_day/"><i class="fa fa-table "></i>Deals Of the Day Product</a>
                    </li>
					<li>
                        <a href="#"><i class="fa fa-edit "></i>Generate Promocode<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="<?=site_url();?>promocode/view_userwise_promo/"><i class="fa fa-table "></i>New customer Promocode</a>
                            </li>
                            <!--li>
                                <a href="<?=site_url();?>promocode/view_datewise_promo/"><i class="fa fa-table "></i>Datewise Promocode</a>
                            </li-->
							 <li>
                                <a href="<?=site_url();?>promocode/view_permant_promo/"><i class="fa fa-table "></i>Permant Promocode</a>
                            </li>
							 <li>
                                <a href="<?=site_url();?>promocode/view_delivery_promo/"><i class="fa fa-table "></i>Free Delivery Promocode</a>
                            </li>
                            
                             <li>
                                <a href="<?=site_url();?>promocode/view_subcat_promo/"><i class="fa fa-table "></i>Sub Category Promocode</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div> 
        </nav>
        <!-- /. NAV SIDE  -->