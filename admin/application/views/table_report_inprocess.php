<!DOCTYPE html>
<html>
<head>
	<title>Report Table</title>
	<style>
		.mainTable{ 
			border-collapse: collapse;
			width:100%;
		}
		tr{
			display: table-row;
			vertical-align: inherit;
			border-color: inherit;
		}
		td, th{
			padding: 8px;
			line-height: 1.42857143;
			vertical-align: top;
			border: 1px solid #ddd;
			text-align:left;
		}
		.headings{ 
			text-align:center;
			font-zize:18px;
			font-weight:600;
		}
		.spacing{
			border:none;
			line-height: 1.42857143;
		}
	</style>
</head>
<body>
  <!-- In production server. If you choose this, then comment the local server and uncomment this one-->
  <!-- <img src="<?php // echo $_SERVER['DOCUMENT_ROOT']."/media/dist/img/no-signal.png"; ?>" alt=""> -->
  <!-- In your local server 
  <img src="<?php echo $_SERVER['DOCUMENT_ROOT']."/ci-dompdf8/media/dist/img/no-signal.png"; ?>" alt="">-->
	<div id="outtable">
		<table class="mainTable">
			<tr>
				<td colspan="2" class="text-left" style="border-color:white;border-bottom-color:#ccc;background-color:white;">
					<br /><img src="<?=base_url();?>/assets/img/2.png" style="width:250px;" />
				</td>
				<td colspan="2" style="text-align:right; border-color:white;border-bottom-color:#ccc;background-color:white;">
					<p class="pull-right" style="margin-right:35px; font-size:11px;">
						<b>Address :</b><br />
						CST No. 56FL6, Sankalp Campus,<br />
						Shivajinagar, Pune-411005<br />
						Contact : support@wjmartz.com<br />
						GST NO. : 27AAGPF8989B1ZG
					</p>
				</td>
			</tr>
			<tr>
				<td colspan="4" class="headings">
					Order Details
				</td>
			</tr> 
			<tr>
				<td>Order ID. :</td>
				<td><?= $order->order_id ?> </td>
				<td>Customer Name :</td>
				<td><?=$order->first_name." ".$order->last_name?> </td>
			</tr>
			<tr>
				<td>Contact No :</td>
				<td><?= $order->mobile ?> </td>
				<td>Email ID :</td>
				<td><?= $order->email ?> </td>
			</tr>
			<tr>
				<td>Customer Address :</td>
				<td colspan="3"><?=$order->address." ".$order->area_name." ".$order->city."-".$order->pincode?> </td>
			</tr>
			<tr><td colspan="4" class="spacing"></td></tr>
			<tr>
				<td colspan="4" class="headings">
					Transaction Details
				</td>
			</tr>
			<tr>
				<td>Transaction ID : </td> 
				<td> <?=$order->transaction_id?></td>
				<td>Transaction Status : </td> 
				<td><?=$order->payment_status?></td>
			</tr>
			<tr>
				<td>Transaction Date : </td>
				<td><?=$order->payment_datetime?></td>
				<td>Amount :</td> 
				<td> <i class="fa fa-inr"></i> <?=$order->amount?></td>
			</tr>
			<tr>
				<td>GST Amount: </td>
					<?php 
						 if(count($order_product)>0)
						 {
							$total_amount=0;
							foreach ($order_product as $gst) 
							{
								$total_amount= $total_amount+$gst->sgst_amount+$gst->cgst_amount;
							}
						}
						else{$total_amount=0;}
					?> 
					<td><i class="fa fa-inr"></i> <?=$total_amount?></td>
					<td>Delivery Charge:</td> 
					<td> <i class="fa fa-inr"></i> <?=$order->delivery_charges?></td>
			</tr>
			<tr><td colspan="4" class="spacing"></td></tr>
			<tr>
				<td colspan="4" class="headings">
					Product(s) Details
				</td>
			</tr>
		</table>
		<table class="mainTable" style="width:100%;">
			<?php  if(count($order_product)>0){foreach ($order_product as $product) { ?>
			<tr> 
				<td rowspan="3" style="max-width:100px;text-align:center;padding:10px 0 0 10px;">
					<img src="<?=IMAGEACCESSPATH.$product->prod_image_url?>" style="width:70px;" />
				</td>
				<th>Product Name</th> 
				<td colspan="3"><?=$product->prod_name?></td>
			</tr>
			<tr>
				<th>Size</th> 
				<td><?=$product->size?></td>
				<th>Price</th> 
				<td><?=$product->price?></td> 
			</tr>
			<tr>
				<th>Quantity</th> 
				<td><?=$product->quantity?></td>
				<th>Amount</th>
				<td><?=$product->amount?></td>
			</tr>
			<?php } } ?> 
		</table>
	 </div>
</body>
</html>