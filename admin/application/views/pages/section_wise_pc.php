<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h5 style="padding-bottom:0;">
					Section wise parent categories 
				</h5>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr style="margin-top:0;">
		<div class="row">
			<div class="clearfix"></div>
			<div class="col-md-12"> 
				<form method="POST" action="<?=site_url();?>/parent_categories/add_section_wise_pc">
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>#Section</th>
								<th>Parent&nbsp;Category</th> 
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$i=0;
							foreach($section_parent_cat as $cat){
								$i++;
								?>
								<tr>
									<td><?=$i?></td>
									<td><?=$cat->section?></td> 
									<td> <?=$cat->parent_cat_name?>
									</td>
									<td class="text-center"><a id="myBtn" onclick="edit_this('<?=$cat->section?>','<?=$cat->parent_cat_id?>','<?=$cat->screen_id?>');" class="btn btn-primary">Edit</a></td>
								</tr>
							<?php }?> 
						</tbody>
					</table>
				</form>
			</div>
			<div class="col-md-12 text-right">
				<div id="upadateNewtime" style="display:none;">
					<?=form_open('parent_categories/add_section_wise_pc','onsubmit="return onSubmitSection();"');?>
						<div class="col-md-offset-4 col-md-4" style="border:1px solid #51a7ae; padding:15px 20px;">
							<div class="form-group">
								<label>Section</label>
								<input required class="form-control" id="section" name="section">
								<input required class="form-control" id="section1" name="section1" style="display:none">
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div>
							<div class="form-group">
								<label>Parent category</label>
								<?=form_dropdown('parent_cat', $parent_categories, set_value('parent_cat',''), array('id'=>'chpatcat', 'class'=>'form-control','required'=>'required'))?>
								<span class="errorMessage" id="noofmemberserror" style="color:red;></span>
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
							<input type="submit" value="Save" class="btn btn-success" />
							<a onclick="location.reload();" class="btn btn-warning">cancel</a>
						</div>
						<div class="clearfix"></div>
						<hr />
					<?=form_close();?> 
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div> 
	<?php if(NULL !== $this->session->flashdata('message')) { ?>
		<div class="insert_success">
			<div class="row">
				<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
					<div class="row">
						<div class="col-sm-12">
							<br />
							<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
								<center><?php echo $this->session->flashdata('message')?></center>
							</h4>
							<br />
						</div>
						<div class="col-sm-12 text-center">
							<a onclick="$('.insert_success').slideUp();" id="myBtn" class="btn btn-primary">
								ok
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>	
	<script> var site_url = "<?php print site_url(); ?>"; </script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?=base_url();?>assets/js/add/parent_category.js"></script>
	<!--script>
		var section,valu,selected_option_id;
		var array=[];
		function performNullSelection(){debugger;
			alert(array[section]);
			switch(section){
				case '1':
					$('#s2v'+array[section]).show();
					$('#s3v'+array[section]).show();
					break;
				case '2':
					$('#s1v'+array[section]).show();
					$('#s3v'+array[section]).show();
					break;
				case '3':
					$('#s1v'+array[section]).show();
					$('#s2v'+array[section]).show();
					break;
			} 
		}
		$('.ddl').change(function(){
			debugger;
			valu = $(this).val();
			section = $(this).attr('ind');
			selected_option_id = 's'+section+'v'+valu;
			if(valu != 'null'){array[section] = $(this).val();}
			switch (selected_option_id){
				case 's1v1': 
					$('#s2v1').hide();
					$('#s3v1').hide();
					break;
				case 's1v2':
					$('#s2v2').hide();
					$('#s3v2').hide();
					break;
				case 's1v3':
					$('#s2v3').hide();
					$('#s3v3').hide();
					break;
				case 's1v0':
					performNullSelection();
					break;
				case 's2v1':
					$('#s1v1').hide();
					$('#s3v1').hide();
					break;
				case 's2v2':
					$('#s1v2').hide();
					$('#s3v2').hide();
					break;
				case 's2v3':
					$('#s1v3').hide();
					$('#s3v3').hide();
					break;
				case 's2v0':
					performNullSelection();
					break;
				case 's3v1':
					$('#s1v1').hide();
					$('#s2v1').hide();
					break;
				case 's3v2':
					$('#s1v2').hide();
					$('#s2v2').hide();
					break;
				case 's3v3':
					$('#s1v3').hide();
					$('#s2v3').hide();
					break;
				case 's3v0':
					performNullSelection();
			}
			// alert(array);
		});
	</script-->