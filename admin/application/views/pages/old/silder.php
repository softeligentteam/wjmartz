<div class="main-grid">
	
	<div class="agile-grids agile-Updating-grids member_info no_border">
		<div class="row">  
			<div class="table-heading">
				<h2 style="font-size:32px; margin:0 0 0 20px; text-align:left; color:gray;">Website Slider Images</h2><hr />
			</div>					
		</div>					
			
		<div class="row">  
			<div class="clearfix"></div> 
			<div id="addWebSliderImg">
				<form action="<?=site_url();?>home/add_slider_image" enctype="multipart/form-data" method="post">
					<div class="col-md-4">
						<img id="b" src="" style=" box-shadow: 2px 2px 3px grey; margin-bottom:20px; width:100%;" alt="UPLOADED IMAGE" class="img-responsive" />
						<div class="clearfix"></div>
						<input type='file' id="a" name="product_Img" onchange="previewImage(a,'b');" style="margin:10px; width:62%; float:left;" />
						<button type="submit" class="btn btn-default w3ls-button" style="margin:10px; width:100%; float:left;">Save Slider Image </button>
					</div>
					<div class="clearfix"></div>
					<hr />
				</form>
			</div>	
		</div>
			
		<div class="row">  
			<?php foreach($user_silder as $user){?>
				<div class="col-md-4"> 
				<?php 
					if(!empty($user->image_path)) {
						echo "<img src='".IMAGEACCESSPATH.$user->image_path."' class='img-responsive' style='margin-bottom:20px; box-shadow: 2px 2px 3px grey;' />";
					}
					else{
						echo "<img src='".IMAGEACCESSPATH."static/uploads/silder/no-image-available.png' class='img-responsive' style='margin-bottom:20px; box-shadow: 2px 2px 3px grey;' />";
					}
				?>  
				<button onclick="$('#editSliderimg<?=$user->slider_id?>').slideDown();" class="btn btn-default w3ls-button" style="margin:10px; width:44%; float:left;">Edit Slider</button>  
				</div>
								
				<div id="editSliderimg<?=$user->slider_id?>" style="display:none;">
					<div class="col-md-4">
						<img id="" src="<?=IMAGEACCESSPATH.$user->image_path?>" style=" box-shadow: 2px 2px 3px grey; margin-bottom:20px; width:100%;" alt="UPLOADED IMAGE" class="img-responsive" />
						<div class="clearfix"></div>
						<form action="<?=site_url();?>home/update_slider" enctype="multipart/form-data" method="post">
							<input type="hidden" name="sid" value="<?=$user->slider_id?>">
							<input type='file' name="slider_img" onchange="prev<?=$user->slider_id?>();" style="margin:10px; width:62%; float:left;" />
							<button type="submit" name="submit" value="user" class="btn btn-default w3ls-button" style="margin:10px; width:44%; float:left;">Save Slider</button>
							<a onclick="$('#editSliderimg<?=$user->slider_id?>').hide();" class="btn btn-info w3ls-button" style="margin:10px; width:44%; float:right;">Cancel</a>
						</form>
					</div>
					<div class="clearfix"></div> 
				</div>
								
			<?php } ?>
			
			<!--?php if($i==3){ echo'<div class="clearfix"></div>'; } } ?-->
			<div class="clearfix"></div>
			<hr />
		</div> 

	</div>
</div> 


<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('user_css_class')?>">					
						<center><?php echo $this->session->flashdata('user_message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="$('.insert_success').slideUp();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

	
