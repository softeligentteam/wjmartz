<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2> Delivered Order Details</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Order ID</th> 
							<th>Customer Name</th>  
							<th>Order Date</th> 
							<th>Order Amount</th> 
							<th><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						
						<?php 
                           foreach ($delivered_order_list as $order)  
                          {  
                        ?>	
						<tr>
							<td><?=$index?></td>
							<td><?=$order->order_id?></td>
							<td><?=$order->first_name .' '.$order->last_name?></td>
							<td><?=$order->order_date?></td>
							<td><?=$order->amount?></td>
							<td data-th="Action">
                                <div><a href="<?=site_url();?>order/view_deliverd_order_products/<?=$order->order_id?>" "title="View Member"><i class="fa fa-eye istyle" aria-hidden="true"></i></a></div>
							</td>	
						</tr>
					   <?php $index++;
						 
					} ?>	
					</tbody>
				</table>
				<?php echo $this->pagination->create_links();?>
				<!--div class="col-sm-12" style="padding-left:0;"> 
				  <ul class="pagination">
					<li><a href="#">&lt;</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">&gt;</a></li>
				  </ul> 
				</div-->
			</div>
		</div>
		<!-- /. ROW  -->
	</div>
	<!-- /. PAGE INNER  -->
</div>
	