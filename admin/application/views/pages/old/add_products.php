<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>Add New Product<a href="<?=site_url();?>products/view_products/" class="btn btn-info pull-right">View All Products</a></h2>
			</div>
		</div>
		<hr />
		<!-- /. ROW  -->
		<div class="row">
			<?=form_open_multipart('products/insert_product');?>
				<div class="col-md-3">
					<h5>Upload Product Image :</h5>
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; padding:0; text-align:center;">
							<img id="productImagePreview" src="" style="width:100%;" alt="" />
						</div>
						<div class="panel-footer back-footer-blue">
							<input type='file' id="productImg" name="product_Img" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%" /> 
						</div>
					</div> 
				</div>
				<div class="col-md-9">
					<h5>Product Details :</h5> 
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter New Product Name</label>
							<input class="form-control" name="product_name">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Select Child Category</label>
							<?=form_dropdown("child_cats", $sub_cat_list, set_value('child_cats', ''), "tabindex='5' id='child_cats'  required class='form-control'")?>	
							<p class="help-block" id="selectMainCatError" style="color:red; height:20px;"></p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Select Brand</label>
							<?=form_dropdown("brands", $brand_list, set_value('brands', ''), "tabindex='5' id='brands'  required class='form-control'")?>	
							<p class="help-block" id="selectMainCatError" style="color:red; height:20px;"></p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Enter Price</label>
							<input class="form-control" name="product_price" />
							<p class="help-block" id="" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Enter Quantity</label>
							<input class="form-control" name="product_qty" />
							<p class="help-block" id="product_qty" style="color:red; height:20px;"></p>
						</div> 
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Enter Product Description</label>
						<textarea class="form-control" rows="7" name="product_desc" ></textarea>
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div> 
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label><input type="checkbox" class="" name="subscription_check" /> &nbsp;&nbsp;Check To Make This Product Available For Subscription</label><br />
						<label><input type="checkbox" class="" name="pune_special" /> &nbsp;&nbsp;Check If Product Is Pune Special</label> 
					</div> 
				</div> 
				<div class="col-md-12">
					<input type="submit" value="Save" class="btn btn-success" />
					<a onclick="$('#addNewProduct').slideUp();" class="btn btn-warning">cancel</a>
				</div>
			<?=form_close();?> 
		</div>
		<hr>
		<!-- /. ROW  --> 
	</div>
	<!-- /. PAGE INNER  -->
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/product.js"></script>
