<link href="<?php echo base_url()?>css/bootstrap-table-style.css" rel='stylesheet' type='text/css' />
<script src="<?php echo base_url()?>js/bootstrap-table-js.js"></script>
<div class="agile-grids">   
	<!-- input-forms -->
	<div class="grids">
		<div class="panel panel-widget forms-panel">
			<div class="forms">
				<div class=" form-grids form-grids-right">
					<div class="widget-shadow " data-example-id="basic-forms"> 
						<div class="form-title text-center">
							<h4> View Product List </h4>
						</div>
						<div class="form-body">

							<div class="form-horizontal"> 
	
								<div class="form-group pull-right" style="padding-right:2%">
									<input type="text" class="search form-control" placeholder="What you looking for?">
								</div>
								<span class="counter pull-right"></span>
								<table class="table table-hover table-bordered results">
									<thead>
										<tr>
											<th>Product No</th>
											<th>Product Name</th>
											<th>Product Price</th>
											<th>Wholesale Price</th>
											<th>Action</th>
										</tr>
										<tr class="warning no-result">
											<td colspan="5">
												<i class="fa fa-warning"/> No records found...</td>
										</tr>
									</thead>
									<tbody>

										<?php 
										if(count($product_list)!==0)
										{
											foreach($product_list as $record)
											{
										?>

										<tr>
											<th scope="row">
												<?=$record->prod_id ?> - <img src="<?=base_url().$record->image_url?>" style="height:50px;width:50px">
											</th>
											<td>
												<?=$record->prod_name ?>
											</td>
											<td>
												<?=$record->prod_price ?>
											</td>
											<td>
												<?=$record->pwholesale_price ?>
											</td>
											<td>
												<?php echo anchor('product/update_product/'.$record->prod_id, 'Update', "class='btn btn-sm btn-primary'");?>
											</td>
										</tr>

										<?php } 
										}
										else { ?>
											<tr class="warning">
												<td colspan="5">
													<i class="fa fa-warning"/> No records found...
												</td>
											</tr>
										<?php } ?>

									</tbody>
								</table>
								<div>
									<ul class="pagination">
									<?php foreach ($links as $link) {
										echo "<li>". $link."</li>";
									} ?>
									<ul>
								</div>
							</div> 
						</div>
					</div>
				</div>
			</div>  
		</div>
	</div>
</div>