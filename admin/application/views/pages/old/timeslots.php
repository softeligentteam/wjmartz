<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Time Slots
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Add New Time Slot</a>
				</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div id="addNewSubCat" style="display:none;">
				<?=form_open('timeslots/insert_timeslots');?> 
				<div class="col-md-offset-4 col-md-4" style="border:1px solid #51a7ae; padding:15px 20px;">
					<div class="form-group">
						<label>Define Time Slot</label>
						<input required class="form-control" name="time_slot">
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div> 
					<div class="form-group">
						<label>Select Shift</label>
						<select required class="form-control" name="shift_status" >
							<option value="null">Select shift...</option>
							<option value="1">First half Shift</option>
							<option value="0">Second half Shift</option>
						</select>
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div>
					<input type="submit" value="Save" class="btn btn-success" />
					<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">cancel</a>
				</div>
				<div class="clearfix"></div>
				<hr />
				<?=form_close();?> 
			</div>
			<div class="col-md-12 text-right"> 
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Time Slot</th>
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						<?php  foreach($time_slots as $time_slot){ ?>
						<tr>
							<td><?=$index?></td>
							<td><?=$time_slot->time_slot?></td> 
							<td class="text-center"><a onclick="edit_this('<?=$time_slot->time_slot_id?>','<?=$time_slot->time_slot?>','<?=$time_slot->shift_status?>');" id="myBtn" class="btn btn-primary">Edit</a></td>
							<td class="text-center"><a onclick="delete_this('<?=$time_slot->time_slot_id?>', '<?=$time_slot->time_slot?>');" class="btn btn-danger">Delete</a></td>
						</tr>
						<?php  $index++;
						 }?>
					</tbody>
				</table> 
				<?php echo $this->pagination->create_links();?>
			</div>
		</div>
		<div id="upadateNewtime" style="display:none;">
				<?=form_open('timeslots/edit_time_slot');?>
				<div class="col-md-offset-4 col-md-4" style="border:1px solid #51a7ae; padding:15px 20px;">
					<div class="form-group">
						<label> Time Slot id</label>
						<input required class="form-control" id="time_id" name="time_id" readonly>
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div>
					<div class="form-group">
						<label> Time Slot</label>
						<input required class="form-control" id="time" name="time">
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div>
					<div class="form-group">
						<label>Select Shift</label>
						<select class="form-control" name="shift" id="shift" >
							<option value="null">Select shift...</option>
							<option value="1">First half Shift</option>
							<option value="0">Second half Shift</option>
						</select>
						<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
					</div> 
					<input type="submit" value="Save" class="btn btn-success" />
					<a onclick="$('#upadateNewtime').slideUp();" class="btn btn-warning">cancel</a>
				</div>
				<div class="clearfix"></div>
				<hr />
				<?=form_close();?> 
			</div>
		<!-- /. ROW  -->
		
	</div>
	<!-- /. PAGE INNER  -->
</div>

<div class="insert_success" id="confirm_delete" style="display:none">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 id="dlt_msg" class="text-center">Are you sure you want to delete</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a href="" id="deleteMAinCatHref" id="" class="btn btn-primary">Yes</a>
					<a onclick="$('#confirm_delete').hide()" id="" class="btn btn-primary">No</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/time_slot.js"></script>
	
	