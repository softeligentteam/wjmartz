<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Products For Fix Packages 
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Enter New Product</a>
				</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div id="addNewSubCat" style="display:none;">
				<?=form_open_multipart('fixpackages/insert_fixpackage_product');?>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							<img id="productImagePreview" src="" style="width:100%;" alt="">
						</div>
						<div class="panel-footer back-footer-blue">
							<input type="file" name="package_image" id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%"> 
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter Product Name</label>
							<input class="form-control" name="fixpackage_product_name">
							<p class="help-block" id="" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Select Package</label>
							<?=form_dropdown("package_list", $package_list, set_value('package_list', ''), "tabindex='' id='package_list' required class='form-control'")?>	
							<p class="help-block" id="" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Enter Product Prize</label>
							<input class="form-control" name="fixpackage_product_price">
							<p class="help-block" id="" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Enter Product Quantity</label>
							<input class="form-control" name="fixpackage_product_quantity">
							<p class="help-block" id="" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12 text-right">
						<input type="submit" value="Save" class="btn btn-success" />
						<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">cancel</a>
					</div>
				</div>
				<?=form_close();?> 
				<div class="clearfix"></div>
				<hr />
			</div>
			<div class="col-md-12 text-right"> 
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Product Image</th>
							<th>Product Name</th>
							<th>Product Price</th>
							<th>Quantity</th>
							<th>Belongs to</th>
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						<?php $i=0; foreach($package_products as $package_product){ $i++;?>
						<tr>
							<td><?=$i?></td>
							<td class="text-center"><img src="<?=IMAGEACCESSPATH.$package_product->package_product_img?>" style="height:60px;" /></td>
							<td><?=$package_product->package_product?></td>
							<td><?=$package_product->prod_price?></td>
							<td><?=$package_product->prod_quantity?></td>
							<td><?=$package_product->package_name?></td>
							<td class="text-center"><a onclick="$('#edit<?=$package_product->package_product_id?>').slideDown();" id="edit_package<?=$package_product->package_product_id?>" class="btn btn-warning">Edit</a></td>
							<td class="text-center"><a onclick="$('#delete<?=$package_product->package_product_id?>').slideDown();" id="delete_package<?=$package_product->package_product_id?>"class="btn btn-danger">Delete</a></td>
						</tr>
						<tr style="display:none;" id="edit<?=$package_product->package_product_id?>">
							<td colspan="8">
								<?=form_open_multipart('fixpackages/update_fixpackage_product');?>
								<div class="col-md-3 col-sm-3 col-xs-6">
									<div class="panel panel-primary text-center no-boder bg-color-blue">
										<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
											<img id="up<?=$package_product->package_product_id?>" src="<?=IMAGEACCESSPATH.$package_product->package_product_img?>" style="width:100%;" alt="">
										</div>
										<div class="panel-footer back-footer-blue">
											<input type="file" name="package_image" id="preview<?=$package_product->package_product_id?>" onchange="previewImage(preview<?=$package_product->package_product_id?>,'up<?=$package_product->package_product_id?>');" class="" style="width:100%;"> 
										</div>
									</div>
								</div>
								<div class="col-md-9">
									<div class="col-md-12">
										<div class="form-group">
											<label>Change Product Name</label>
											<input class="form-control hidden" name="id" value="<?=$package_product->package_product_id?>" />
											<input class="form-control hidden" name="url"value="<?=$package_product->package_product_img?>" />
											<input class="form-control" name="name" value="<?=$package_product->package_product?>">
											<p class="help-block" id="" style="color:red; height:20px;"></p>
										</div> 
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Select Package</label>
											<?=form_dropdown("edited_package_list", $package_list, set_value('package_list', $package_product->package_id), "tabindex='' id='package_list' required class='form-control'")?>
											<p class="help-block" id="" style="color:red; height:20px;"></p>
										</div> 
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Change Product Prize</label>
											<input class="form-control" name="price" value="<?=$package_product->prod_price?>" />
											<p class="help-block" id="" style="color:red; height:20px;"></p>
										</div> 
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Change Product Quantity</label>
											<input class="form-control" name="quantity" value="<?=$package_product->prod_quantity?>" />
											<p class="help-block" id="" style="color:red; height:20px;"></p>
										</div> 
									</div>
									<div class="clearfix"></div>
									<div class="col-md-12 text-right">
										<input type="submit" value="Save" class="btn btn-success" />
										<a onclick="$('#edit<?=$package_product->package_product_id?>').slideUp();" class="btn btn-warning">cancel</a>
									</div>
								</div>
								<?=form_close();?> 
								<div class="clearfix"></div>
								<hr />
							</td>
						</tr>
						<tr style="display:none;" id="delete<?=$package_product->package_product_id?>">
							<td colspan="8" class="text-center">
								<h3>Are you sure you want to delete package product : <?=$package_product->package_product?></h3>
								<a href="<?=site_url();?>fixpackages/delete_fixpackage_product/<?=$package_product->package_product_id?>"  class="btn btn-danger"> Yes ! </a>
								<a onclick="$('#delete<?=$package_product->package_product_id?>').slideUp();" class="btn btn-info">cancel</a>
								
							</td>
						</tr>
						<?php }?>
					</tbody>
				</table> 
			</div>
		</div>
		<!-- /. ROW  -->
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>
<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/parent_category.js"></script>
	
	