<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Parent Categories
					<a onclick="$('#addNewSubCat').slideDown();" class="hidden btn btn-info pull-right">Add New Parent Category</a>
				</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<?=form_open_multipart('parent_categories/add_parent_category');?>
				<div id="addNewSubCat" style="display:none;">
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="panel panel-primary text-center no-boder bg-color-blue">
							<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
								<img id="productImagePreview" src="" style="width:100%;" alt="" />
							</div>
							<div class="panel-footer back-footer-blue">
								<input type='file' name="parent_cat_img" id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%" required s/> 
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="col-md-12">
							<div class="form-group">
								<label>Enter New Parent Category Name</label>
								<input class="form-control" name="parent_category" required />
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
								<label>&lt; Select Parent Category Image</label>
							</div> 
						</div>
						<div class="col-md-12">
							<input type="submit" value="Save" class="btn btn-success" /> 
							<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">cancel</a>
						</div>
					</div>
					<div class="clearfix"></div>
					<hr />
				</div>
			<?=form_close();?>
			
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Parent - Category Name</th>
							<th>Parent - Category image </th>
							<th><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						
						<tr>
							<?php  
						   foreach ($parent_cat_list as $parent_cat)  
						  {  
						?>	
						<tr>
							<td><?=$index?></td>
							<td><?=$parent_cat->parent_cat_name?></td>
							<td><img src="<?=IMAGEACCESSPATH.$parent_cat->parent_cat_img_url?>" style="width:50px;" /></td>
							<td class="text-center"><a onclick="$('#edit<?=$parent_cat->parent_cat_id?>').show();" id="myBtn" class="btn btn-primary">Edit</a></td>
							<!--td class="text-center"><a onclick="$('#delete< ?=$parent_cat->parent_cat_id?>').show();" class="btn btn-danger">Delete</a></td-->
						</tr>
						<tr style="display:none; text-align:center;" id="delete<?=$parent_cat->parent_cat_id?>">
							<td colspan="5">
								<br />
								<h4>Are you sure you want to delete Parent Category : <?=$parent_cat->parent_cat_name?> ? </h4>
								<br /><br />
								<a href="<?=site_url();?>parent_categories/delete_parent_category/<?=$parent_cat->parent_cat_id?>" class="btn btn-danger">Yes !</a>
								<a onclick="$('#delete<?=$parent_cat->parent_cat_id?>').hide();" class="btn btn-info">cancel</a>
								<br /><br />
							</td>
						</tr>
						<tr style="display:none" id="edit<?=$parent_cat->parent_cat_id?>">
							<td colspan="5">
								<?=form_open_multipart('parent_categories/update_parent_category');?> 
									<div class="col-md-3 col-sm-3 col-xs-6">
										<div class="panel panel-primary text-center no-boder bg-color-blue">
											<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
												<img id="productImagePreview<?=$parent_cat->parent_cat_id?>" src="<?=IMAGEACCESSPATH.$parent_cat->parent_cat_img_url?>" style="width:100%;" alt="" />
											</div>
											<div class="panel-footer back-footer-blue">
												<input type='file' name="parent_cat_img" id="productImg<?=$parent_cat->parent_cat_id?>" onchange="previewImage(productImg<?=$parent_cat->parent_cat_id?>,'productImagePreview<?=$parent_cat->parent_cat_id?>');" class="" style="width:100%" /> 
											</div>
										</div>
									</div>
									<div class="col-md-9">
										<div class="col-md-12">
											<div class="form-group">
												<label>Enter New Parent Category Name</label>
												<input class="form-control hidden" name="parent_cat_id" value="<?=$parent_cat->parent_cat_id?>" />
												<input class="form-control hidden" name="parent_cat_img_url" value="<?=$parent_cat->parent_cat_img_url?>" />
												<input class="form-control" name="parent_cat_name" value="<?=$parent_cat->parent_cat_name?>" />
												<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
												<label>&lt; Select Parent Category Image</label>
											</div> 
										</div>
										<div class="col-md-12">
											<input type="submit" value="Save" class="btn btn-success" />
											<a onclick="$('#edit<?=$parent_cat->parent_cat_id?>').slideUp();" class="btn btn-warning">cancel</a>
										</div>
									</div>
									<div class="clearfix"></div>
									<hr /> 
								<?=form_close();?>
							</td>
						</tr>
					   <?php $index++;
						 
						} ?>	
					</tbody>
				</table> 
					<?php echo $this->pagination->create_links();?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>

<div class="insert_success" id="confirm_delete" style="display:none">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 id="dlt_msg" class="text-center">Are you sure you want to delete</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a href="" id="deleteMAinCatHref" id="" class="btn btn-primary">Yes</a>
					<a onclick="$('#confirm_delete').hide()" id="" class="btn btn-primary">No</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/parent_category.js"></script>
	
	