<div id="page-wrapper">
	<div id="page-inner" style="padding:0;">
		<div class="row"> 
			<div class="col-md-12">
				<div class="panel panel-widget forms-panel">
					<div class="forms"> 
						<ul class="tab">
							<li>
								<a href="javascript:void(0)" class="tablinks active" onclick="openTab(event, 'tab1')">
									Update Product</a>
							</li>
							<li>
								<a href="javascript:void(0)" class="tablinks" onclick="openTab(event, 'tab2')">
									Update Product Images
								</a>
							</li>
						</ul>
						<div class=" form-grids form-grids-right tabcontent" id="tab1" style="display:<?php if(null !== $this->session->flashdata('umessagei') || isset($display)) echo "none"; else echo"display";?>">
							<div class="widget-shadow">
								<div class="col-md-12">
									<?=form_open('products/update_product_women_ware', "class='form-horizontal' onsubmit='return validate1();'"); ?>
										<div class="col-md-4"><br />
											<label for="brand_name"  >Brand Name: </label> 
											<?=form_input(array('name'=>'brand_id', 'value'=>$product->bname, 'class'=>'form-control', 'readonly'=>'readonly'))?>
											<?=form_error('brand_id'); ?>
										</div>
										<div class="col-md-4"><br />
											<label for="child_cat"  >Child Category: </label> 
											<?=form_input(array('name'=>'pccat', 'value'=>$product->pccat, 'class'=>'form-control', 'readonly'=>'readonly'))?>
										</div>
										<div class="col-md-4"><br />
											<label for="product_name"  >Product Name</label>
											<?=form_input(array('name'=>'pname', 'value'=>$product->pname, 'class'=>'form-control', 'readonly'=>'readonly'))?>
										</div>
										<div class="clearfix"></div>
										<style>
											.myTxt{
												position:absolute;
												bottom:0;
												right:0;
												margin-right:25%;
												width:75px;
												display:none;
											}
										</style>
										<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
										<script>
											function viewInput(id){debugger;
												$('#sizeQty'+id).slideToggle();
											}
										</script>
										<div class="col-md-12"><br />
											<label for="product_color"  >Product Sizes : Quantity </label><br />
											<hr style="margin:4px 0;"/>
											<?php 
												foreach($size_list as $object)
												{
													$checked = FALSE;
													foreach($product->psizes as $psize)
													{
														if($object->size_id == $psize)
														{
															echo "<div class='col-md-3' style='padding:0'>".form_checkbox(array('name'=>'psize[]', 'id'=>$object->size_id, 'value'=>$object->size_id, 'onchange'=>'viewInput(this.id);', 'checked'=>'checked'));
															echo form_label($object->size).form_input(array('style'=>'display:block;', 'name'=>'sizeQty[]', 'id'=>'sizeQty'.$object->size_id, 'class'=>'myTxt', 'placeholder'=>'Quantity')).'</div>';
															$checked = TRUE; 
														}
													}
													if($checked==FALSE)
													{
														echo "<div class='col-md-3' style='padding:0'>".form_checkbox(array('name'=>'psize[]', 'id'=>$object->size_id, 'onchange'=>'viewInput(this.id);', 'value'=>$object->size_id));
														echo form_label($object->size).
														foreach($qty_list as $Object_qty)
														{
															form_input(array('name'=>'sizeQty[]', 'id'=>'sizeQty'.$object->size_id, 'class'=>'myTxt', 'placeholder'=>'Quantity', 'value'=>$object_qty->quantity)).'</div>';
														}										
													}
												}
											?>
											<div class="clearfix"></div>
											<?=form_error('psize'); ?>
											
											<label class="error" id="psize_error"></label>
											<hr style="margin:4px 0;"/>
										</div>
										<div class="col-md-3"><br />
											<label for="product_color"  >Product Color: </label> 
											<?=form_input(array('name'=>'pcolor', 'value'=>$product->pcolor, 'class'=>'form-control', 'readonly'=>'readonly'))?>
										</div>
										<div class="col-md-3"><br />
											<label for="product_pmrp"  >Product MRP</label>
											<?=form_input(array('name'=>'pmrp', 'id'=>'pmrp', 'value'=>$product->pmrp, 'class'=>'form-control', 'type'=>'number', 'min'=>'100', 'step'=>'any', ''=>'', 'placeholder'=>'Enter Product MRP'))?>
											<?=form_error('pmrp'); ?><label class="error" id="pmrp_error"></label>
										</div>
										<div class="col-md-3"><br />
											<label for="product_price"  >Product Price</label> 
											<?=form_input(array('name'=>'pprice', 'id'=>'pprice', 'value'=>$product->pprice, 'class'=>'form-control', 'type'=>'number',  'placeholder'=>'Enter Product Price'))?>
											<?=form_error('pprice'); ?><label class="error" id="pprice_error"></label>
										</div>
										<div class="col-md-3"><br />
											<label for="pwholesale_price"  >Wholesale Price</label>
											<?=form_input(array('name'=>'pwprice', 'id'=>'pwprice', 'value'=>$product->pwprice, 'class'=>'form-control', 'type'=>'number',  'placeholder'=>'Enter Product Wholesale Price'))?>
											<?=form_error('pwprice'); ?><label class="error" id="pwprice_error"></label>
										</div>
										<!--<div class="col-md-12"><br />
											<label for="pwholesale_price"  >product slot quantity</label>
											<?=form_input(array('name'=>'pquantity', 'id'=>'pquantity', 'value'=>$product->quantity, 'class'=>'form-control', 'type'=>'number', 'placeholder'=>'Enter Product slot'))?>
										</div>-->
										<div class="col-md-12"><br />
											<label for="pfitdetails"  >Fit Details</label>
											<?=form_textarea(array('name'=>'pfitdetails', 'id'=>'pfitdetails', 'value'=>$product->pfitdetails, 'class'=>'form-control', 'cols'=>'10', 'rows'=>'5', 'maxlength'=>'2000', 'placeholder'=>'Enter Product Fit Details'))?>
											<?=form_error('pdesc'); ?><label class="error" id="pfitdetails_error"></label>
										</div>
										<div class="col-md-12"><br />
											<label for="pfabricdetails"  >Fabric Details</label>
											<?=form_textarea(array('name'=>'pfabricdetails', 'id'=>'pfabricdetails', 'value'=>$product->pfabricdetails, 'class'=>'form-control', 'cols'=>'10', 'rows'=>'5', 'maxlength'=>'2000', 'placeholder'=>'Enter Product Fabric Details'))?>
											<?=form_error('pdesc'); ?><label class="error" id="pfabricdetails_error"></label>
										</div>
										<div class="col-md-12"><br />
											<label for="pdescription"  >Description</label>
											<?=form_textarea(array('name'=>'pdesc', 'id'=>'pdesc', 'value'=>$product->pdesc, 'class'=>'form-control', 'cols'=>'10', 'rows'=>'5', 'maxlength'=>'2000', 'placeholder'=>'Enter Product Description'))?>
											<?=form_error('pdesc'); ?><label class="error" id="pdesc_error"></label>
										</div>
										<div class="col-md-3"><br />
											<label for="prod_img"  >Available In Stock</label>
										</div>
										<div class="col-md-9"><br />
											<?php
												if($product->in_stock == 1)
												{
													echo "<div class='col-md-3' style='padding:0'>".form_radio(array('name'=>'in_stock', 'value'=>1, 'checked'=>'checked'));
													echo form_label('Yes').'</div>';
													echo "<div class='col-md-3' style='padding:0'>".form_radio(array('name'=>'in_stock', 'value'=>0));
													echo form_label('No').'</div>';
												}
												else
												{
													echo "<div class='col-md-3' style='padding:0'>".form_radio(array('name'=>'in_stock', 'value'=>1));
													echo form_label('Yes').'</div>';
													echo "<div class='col-md-3' style='padding:0'>".form_radio(array('name'=>'in_stock', 'value'=>0, 'checked'=>'checked'));
													echo form_label('No').'</div>';
												}
											?>
											<br>
											<?=form_error('in_stock'); ?>
										</div>
										<div class="col-md-12"><br />
											<input name="pid" type="hidden" value="<?=$product->pid?>">
											<?=form_submit('uproduct-submit', 'Update', 'class="btn btn-lg btn-primary"')?>
											<?=anchor('products/view_products_women_ware', 'Cancel', "class='btn btn-lg btn-warning'");?>
										</div>
									<?=form_close(); ?>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class=" form-grids form-grids-right tabcontent" id="tab2" style="display:<?php if(null !== $this->session->flashdata('umessagei') || isset($display)) echo "block"; else echo "none";?>">
							<div class="widget-shadow">
								<div class="form-body">
									<div class="col-md-12">
										<?php 
											if(null !== $this->session->flashdata('umessagei'))
											{ 
												echo "<div class='col-sm-12 text-center'><label class='".$this->session->flashdata('css_class')."'>".$this->session->flashdata('umessagei')."</label></div><br>";
											}
										?> 
									</div>
									<div class="col-md-12">
										<?=form_open_multipart('products/update_product_women_ware', "class='form-horizontal' onsubmit='return validate2();';"); ?>
											<div class="col-md-12"><br />
												<label for="product_name"  >Product Name</label><br /><br />
												<?=form_input(array('name'=>'pname', 'value'=>$product->pname, 'class'=>'form-control', 'readonly'=>'readonly'))?><br /><br/>
												<label for="prod_img"  >Product Images</label><hr />
												<?=form_upload(array('name'=>'prod_img1', 'value'=>set_value('prod_img1'), ''=>''))?>
												<?=form_error('prod_img1');?><br>
												<?=form_upload(array('name'=>'prod_img2', 'value'=>set_value('prod_img2'), ''=>''))?>
												<?=form_error('prod_img2');?><br>
												<?=form_upload(array('name'=>'prod_img3', 'value'=>set_value('prod_img3')))?>
												<?=form_error('prod_img3');?><br>
												<?=form_upload(array('name'=>'prod_img4', 'value'=>set_value('prod_img4')))?>
												<?=form_error('prod_img4');?>
												<label class="error" id="prod_img_error"></label><br>
												<input name="pid" type="hidden" value="<?=$product->pid?>">
												<?=form_submit('uproduct-submiti', 'Update', 'class="btn btn-lg btn-primary"')?>
												<?=anchor('products/view_products_women_ware', 'Cancel', "class='btn btn-lg btn-warning'");?><br /><br />
												<span class="text-info">Note:- Max Size:300KB | Width:400px | Height:550px </span><br /><br />
											</div>
											<div class="clearfix"></div>
										<?=form_close(); ?> 
									
								</div> 
							</div>
						</div>
						<script>
							function openTab(evt, cityName) {
								var i, tabcontent, tablinks;
								tabcontent = document.getElementsByClassName("tabcontent");
								for (i = 0; i < tabcontent.length; i++) {
									tabcontent[i].style.display = "none";
								}
								tablinks = document.getElementsByClassName("tablinks");
								for (i = 0; i < tablinks.length; i++) {
									tablinks[i].className = tablinks[i].className.replace(" active", "");
								}
								document.getElementById(cityName).style.display = "block";
								evt.currentTarget.className += " active";
							}
						</script>
					<div class="clearfix"></div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>






<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$('#ppcat_id').change(function(){
			var ppcat = $('#ppcat_id option:selected').val();
			if(ppcat!==''){
			$.ajax({
				url: '<?=site_url('product/product_child_cat')?>',
				dataType: 'json', 
				data: {'ppcat': ppcat},
				type: "get",
				success: function(data){
					$('#pccat_id').html('');
					$('#pccat_id').append($("<option/>", {
							value: '',
							text: 'Select Product Child Category'
					}));
					
					$.each(data, function(index, pccat_list){
					   $('#pccat_id').append($("<option/>", {
							value: pccat_list.pchild_cat_id,
							text: pccat_list.pchild_category
						}));
					});
				}
			});
			}
			else{
				$('#pccat_id').html('');
					$('#pccat_id').append($("<option/>", {
						value: '',
						text: 'Select Product Child Category'
				}));
			}
		});
	});
	
</script>
  
<style>
	ul.tab {
		list-style-type: none;
		margin: 0;
		padding: 0;
		overflow: hidden;
		border: 1px solid #ccc;
		background-color: #f1f1f1;
	}

	/* Float the list items side by side */
	ul.tab li {float: left; width:50%;}

	/* Style the links inside the list items */
	ul.tab li a {
		display: inline-block;
		color: black;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		transition: 0.3s;
		font-size: 17px;
		width:100%;
	}

	/* Change background color of links on hover */
	ul.tab li a:hover {
		background-color: #ddd;
	}

	/* Create an active/current tablink class */
		ul.tab li a:focus, .active {
		background-color: #ccc;
	}

	/* Style the tab content */
	.tabcontent {
		display: block;
		padding: 6px 12px;
		border: 1px solid #ccc;
		border-top: none;
	}
</style>

<script>
	$('input').change(function() {
		var id = $(this).attr('id');
		$('#'+id+'_error').html('');
	});
	$('textarea').change(function() {
		var id = $(this).attr('id');
		$('#'+id+'_error').html('');
	}); 
	$('input[type=checkbox]').change(function(){
		$('#psize_error').html('');
	}); 
	$('input[type=file]').change(function(){
		$('#prod_img_error').html('');
	});
	var validated1 = true;
	var validated2 = true;
	function validate1(){ 
		var psizes = 8;
		$('input[type=checkbox]').each(function () {
			if($(this).prop('checked'))
			{
				psizes = psizes-1;
			}
		}); 
		if(psizes == 8){validated1 = false; $('#psize_error').html('You must select atleast one product size');}
		else{$('#psize_error').html('');}
		 
		
		$('input').each(function() {
			if($(this).attr('id') == 'pmrp'){
				var id = $(this).attr('id');
				var val = $(this).val();
				if(val == ''){
					$('#'+id+'_error').html('This field cannot be blank.');
					validated1 = false;
				}
				else if(val%1 != 0){validated1 = false; $('#'+id+'_error').html('Input a valid ammount without decimals or other characters');}
				else{$('#'+id+'_error').html('');}
			}
			
			else if($(this).attr('id') == 'pprice'){
				var id = $(this).attr('id');
				var val = $(this).val();
				if(val == ''){
					$('#'+id+'_error').html('This field cannot be blank.');
					validated1 = false;
				}
				else if(val%1 != 0){validated1 = false; $('#'+id+'_error').html('Input a valid ammount without decimals or other characters');}
				else{$('#'+id+'_error').html('');}
			}
			
			else if($(this).attr('id') == 'pwprice'){
				var id = $(this).attr('id');
				var val = $(this).val();
				if(val == ''){
					$('#'+id+'_error').html('This field cannot be blank.');
					validated1 = false;
				}
				else if(val%1 != 0){validated1 = false; $('#'+id+'_error').html('Input a valid ammount without decimals or other characters');}
				else{$('#'+id+'_error').html('');}
			} 
		});
		
		$('textarea').each(function() {
			var id = $(this).attr('id');
			var val = $(this).val();
			if(val == ''){
				$('#'+id+'_error').html('This field cannot be blank.');
				validated1 = false; 
			}
			else{$('#'+id+'_error').html('');}
		}); 
		return validated1;
	}
		
	function validate2(){
		var prod_img = 4;
		$('input[type=file]').each(function () { 
			var val = $(this).val();
			if(val == ''){
				prod_img = prod_img - 1
			}
		});
		if(prod_img < 3){validated2 = false; $('#prod_img_error').html('You must upload atlease 3 images');}
		else{$('#prod_img_error').html('');}
		
		return validated2;
	}
	
	
</script>
<style>
.error{
	position: absolute;
    color: red;
    font-size: 12px;
	background: white;
}
</style>