<!--script src="<?=base_url();?>assets/js/add/fixpackages.js"></script-->
<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Fix Packages List
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Enter New Package</a>
				</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div id="addNewSubCat" style="display:none;">
				<?=form_open_multipart('fixpackages/insert_package');?>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							<img id="productImagePreview" src="" style="width:100%;" alt="">
						</div>
						<div class="panel-footer back-footer-blue">
							<input type="file" name="package_image" id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%"> 
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter Package Name</label>
							<input class="form-control" name="package_name">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Enter Package Prize</label>
							<input class="form-control" name="package_price">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-12 text-right">
					<input type="submit" value="Save" class="btn btn-success" />
					<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">cancel</a>
				</div>
				<?=form_close();?> 
				<div class="clearfix"></div>
				<hr />
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Package Image</th>
							<th>Package Name</th>
							<th>Package Price</th>
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						<?php $i=0; foreach($packages as $package){ $i++;?>
						<tr>
							<td><?=$i?></td>
							<td class="text-center"><img src="<?=IMAGEACCESSPATH.$package->package_img?>" style="height:60px;" /></td>
							<td><?=$package->package_name?></td>
							<td><?=$package->package_price?></td>
							<td class="text-center"><a onclick="$('#editsubcat<?=$package->package_id?>').slideDown();" class="btn btn-warning">Edit</a></td>
							<td class="text-center"><a onclick="$('#deletesubcat<?=$package->package_id?>').slideDown();"" id="delete_package<?=$package->package_id?>" class="btn btn-danger">Delete</a></td>
						</tr>
						<tr id="deletesubcat<?=$package->package_id?>" style="display:none;">
							<td colspan="6" class="text-center">
								<br />Are you sure you want to delete package : <?=$package->package_name?> ?<br /><br />
								<a href="<?=site_url();?>fixpackages/delete_fixpackage/<?=$package->package_id?>" class="btn btn-danger">Yes !</a>
								<a onclick="$('#deletesubcat<?=$package->package_id?>').slideUp();" class="btn btn-success">cancel.</a>
								<br /><br />
							</td>
						</tr>
						<tr id="editsubcat<?=$package->package_id?>" style="display:none;">
							<td colspan="6">
								<?=form_open_multipart('fixpackages/update_fixpackage');?>
								<div class="col-md-3 col-sm-3 col-xs-6">
									<div class="panel panel-primary text-center no-boder bg-color-blue">
										<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
											<img id="fixpackageimage<?=$package->package_id?>" src="<?=IMAGEACCESSPATH.$package->package_img?>" style="width:100%;" alt="">
										</div>
										<div class="panel-footer back-footer-blue">
											<input type="file" name="package_image" id="fixpackagenewimage<?=$package->package_id?>" onchange="previewImage(fixpackagenewimage<?=$package->package_id?>,'fixpackageimage<?=$package->package_id?>');" class="" style="width:100%">
										</div>
									</div>
								</div>
								<div class="col-md-9">
									<div class="col-md-12">
										<div class="form-group">
											<label>Change Package Name</label>
											<input class="form-control" value="<?=$package->package_name?>" name="fixpackage_name2">
											<input class="hidden form-control" value="<?=$package->package_img?>" name="fixpackage_old_img">
											<input class="hidden form-control" value="<?=$package->package_id?>" name="fixpackage_id2">
											<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
										</div> 
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Change Package Prize</label>
											<input class="form-control" value="<?=$package->package_price?>" name="fixpackage_price2">
											<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
										</div> 
									</div>
									<div class="clearfix"></div>
									<div class="col-md-12 text-right">
										<input type="submit" value="Update" class="btn btn-danger" />
										<a onclick="$('#editsubcat<?=$package->package_id?>').slideUp();" class="btn btn-info">cancel</a>
									</div>
								</div>
								<?=form_close();?> 
								<div class="clearfix"></div> 
							</td>
						</tr>
						<?php }?>
					</tbody>
				</table> 
			</div>
		</div>
		<!-- /. ROW  -->
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>
<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success" id="flash_message">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="$('#flash_message').hide();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
	
	