<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>Sucessful Transaction Details</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Order ID</th> 
							<th>Transaction ID</th> 
							<th>Customer Name</th> 
							<th>Tansaction Date</th> 
							<th>Transaction Status</th> 
						</tr>
					</thead>
					<tbody>
						
						<?php  if(count($success_data)>0){
						$i=1;
                           foreach ($success_data as $success)  
                          {  
                        ?>	
						<tr>
							<td><?=$i?></td>
							<td><?=$success->order_id?></td>
							<td><?=$success->transaction_id?></td>
							<td><?=$success->first_name .' '.$success->last_name?></td>
							<td><?=$success->payment_datetime?></td>
							<td><?=$success->payment_status?></td>
						</tr>
					   <?php $i++;
						 
						}  } ?>	
					</tbody>
				</table>
				<?php echo $this->pagination->create_links();?>
				<!--div class="col-sm-12" style="padding-left:0;"> 
				  <ul class="pagination">
					<li><a href="#">&lt;</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">&gt;</a></li>
				  </ul> 
				</div-->
			</div>
		</div>
		<!-- /. ROW  -->
	</div>
	<!-- /. PAGE INNER  -->
</div>
	