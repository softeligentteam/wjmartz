<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Addons Product List
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Enter New Addon</a>
				</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div id="addNewSubCat" style="display:none;">
				<?=form_open_multipart('personalizepackages/insert_addon_product');?>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							<img id="productImagePreview" src="" style="width:100%;" alt="">
						</div>
						<div class="panel-footer back-footer-blue">
							<input type="file" name="package_image" id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%"> 
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter Addon Product Name</label>
							<input class="form-control" name="package_addon">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Enter Addon Quantity</label>
							<input class="form-control" name="addon_quantity">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Enter Addon Price</label>
							<input class="form-control" name="addon_price">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter Addon Details</label>
							<textarea class="form-control" name="addon_description" rows="4"></textarea>
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12 text-right">
						<input type="submit" value="Save" class="btn btn-success" />
						<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">cancel</a>
					</div>
				</div>
				<?=form_close();?> 
				<div class="clearfix"></div>
				<hr />
			</div>
			<div class="col-md-12 text-right"> 
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Image</th>
							<th>Main Product Name</th> 
							<th>Prize</th> 
							<th>Quantity</th> 
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						<?php $i=0; foreach($addons as $addon){ $i++;?>
						<tr>
							<td><?=$i?></td>
							<td class="text-center"><img src="<?=IMAGEACCESSPATH.$addon->per_product_img?>" style="height:60px;" /></td>
							<td><?=$addon->product_name?></td> 
							<td><?=$addon->product_price?></td> 
							<td><?=$addon->quantity?></td> 
							<td class="text-center"><a onclick="$('#edit<?=$addon->sub_product_id?>').show()" id="edit_package<?=$addon->sub_product_id?>" class="btn btn-warning">Edit</a></td>
							<td class="text-center"><a onclick="$('#delete<?=$addon->sub_product_id?>').show();" id="delete_package<?=$addon->sub_product_id?>" class="btn btn-danger">Delete</a></td>
						</tr>
						<tr>
							<td style="display:none; text-align:center;" colspan="7" id="delete<?=$addon->sub_product_id?>" >
								<br /> 
								<h4>Are you sure you want to delete Addon : <?=$addon->product_name?></h4>
								<br /><br />
								<a class="btn btn-danger" href="<?=site_url();?>personalizepackages/delete_addon_product/<?=$addon->sub_product_id?>">Yes ! </a>
								<a class="btn btn-info" onclick="$('#delete<?=$addon->sub_product_id?>').hide();">cancel.</a> 
								<br /><br />
							</td>
						</tr>
						<tr>
							<td style="display:none;" colspan="7" id="edit<?=$addon->sub_product_id?>" >
								<?=form_open_multipart('personalizepackages/update_addon_product');?>
									<div class="col-md-3 col-sm-3 col-xs-6">
										<div class="panel panel-primary text-center no-boder bg-color-blue">
											<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
												<img id="productImagePreview<?=$addon->sub_product_id?>" src="<?=IMAGEACCESSPATH.$addon->per_product_img?>" style="width:100%;" alt="">
											</div>
											<div class="panel-footer back-footer-blue">
												<input type="file" name="package_image" id="productImg<?=$addon->sub_product_id?>" onchange="previewImage(productImg<?=$addon->sub_product_id?>,'productImagePreview<?=$addon->sub_product_id?>');" class="" style="width:100%"> 
											</div>
										</div>
									</div>
									<div class="col-md-9">
										<div class="col-md-12">
											<div class="form-group">
												<label>Enter Addon Product Name</label>
												<input class="form-control" name="package_addon_id" value="<?=$addon->sub_product_id?>">
												<input class="form-control" name="addon_img" value="<?=$addon->per_product_img?>">
												<input class="form-control" name="package_addon" value="<?=$addon->product_name?>">
												<p class="help-block" id="package_addon" style="color:red; height:20px;"></p>
											</div> 
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Enter Addon Quantity</label>
												<input class="form-control" name="addon_quantity" value="<?=$addon->quantity?>">
												<p class="help-block" id="adon_quantity" style="color:red; height:20px;"></p>
											</div> 
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Enter Addon Price</label>
												<input class="form-control" name="addon_price" value="<?=$addon->product_price?>">
												<p class="help-block" id="addon_price" style="color:red; height:20px;"></p>
											</div> 
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label>Enter Addon Details</label>
												<textarea class="form-control" name="addon_description" rows="4">
													 <?=$addon->product_description?>
												</textarea>
												<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
											</div> 
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 text-right">
											<input type="submit" value="Save" class="btn btn-success" />
											<a onclick="$('#edit<?=$addon->sub_product_id?>').hide();" class="btn btn-warning">cancel</a>
										</div>
									</div>
								<?=form_close();?> 
							</td>
						</tr>
						<?php }?>
					</tbody>
				</table> 
			</div>
		</div>
		<!-- /. ROW  -->
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>
<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/parent_category.js"></script>
	
	