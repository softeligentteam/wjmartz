<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Customizable Packages Main Product List
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Enter New Main Product</a>
				</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div id="addNewSubCat" style="display:none;">
				<?=form_open_multipart('personalizepackages/insert_main_product');?>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							<img id="productImagePreview" src="" style="width:100%;" alt="">
						</div>
						<div class="panel-footer back-footer-blue">
							<input type="file" name="package_image" id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%"> 
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter Main Product Name</label>
							<input class="form-control" name="package_main_product">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12 text-right">
						<input type="submit" value="Save" class="btn btn-success" />
						<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">cancel</a>
					</div>
				</div>
				<?=form_close();?> 
				<div class="clearfix"></div>
				<hr />
			</div>
			<div class="col-md-12 text-right"> 
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Image</th>
							<th>Main Product Name</th> 
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						<?php $i=0; foreach($main_products as $main_product){ $i++;?>
						<tr>
							<td><?=$i?></td>
							<td class="text-center"><img src="<?=IMAGEACCESSPATH.$main_product->main_product_img?>" style="height:60px;" /></td>
							<td><?=$main_product->package_main_product?></td> 
							<td class="text-center"><a onclick="$('#edit<?=$main_product->per_package_id?>').show();" id="edit_package<?=$main_product->per_package_id?>" class="btn btn-warning">Edit</a></td>
							<td class="text-center"><a onclick="$('#delete<?=$main_product->per_package_id?>').show();" id="delete_package<?=$main_product->per_package_id?>" class="btn btn-danger">Delete</a></td>
						</tr>
						<tr>
							<td class="text-center" style="display:none;" colspan="5" id="delete<?=$main_product->per_package_id?>">
								<h4>Are you sure you want to delete pacakge : <?=$main_product->package_main_product?> ?</h4>
								<a href="<?=site_url();?>personalizepackages/delete_main_product/<?=$main_product->per_package_id?>" class="btn btn-danger">Yes !</a>
								<a onclick="$('#delete<?=$main_product->per_package_id?>').hide();" class="btn btn-info">cancel.</a>
							</td>
						</tr>
						<tr>
							<td colspan="5" style="display:none;" id="edit<?=$main_product->per_package_id?>">
								<?=form_open_multipart('personalizepackages/update_main_product');?>
								<div class="col-md-3 col-sm-3 col-xs-6">
									<div class="panel panel-primary text-center no-boder bg-color-blue">
										<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
											<img id="productImagePreviewedit<?=$main_product->per_package_id?>" src="<?=IMAGEACCESSPATH.$main_product->main_product_img?>" style="width:100%;" alt="">
										</div>
										<div class="panel-footer back-footer-blue">
											<input type="file" name="package_image" id="productImgedit<?=$main_product->per_package_id?>" onchange="previewImage(productImgedit<?=$main_product->per_package_id?>,'productImagePreviewedit<?=$main_product->per_package_id?>');" class="" style="width:100%"> 
										</div>
									</div>
								</div>
								<div class="col-md-9">
									<div class="col-md-12">
										<div class="form-group">
											<label>Enter Main Product Name</label>
											<input class="form-control hidden" value="<?=$main_product->per_package_id?>" name="id">
											<input class="form-control hidden" value="<?=$main_product->main_product_img?>" name="old_image">
											<input class="form-control" value="<?=$main_product->package_main_product?>" name="package_main_product">
											<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
										</div> 
									</div>
									<div class="clearfix"></div>
									<div class="col-md-12 text-right">
										<input type="submit" value="Save" class="btn btn-success" />
										<a onclick="$('#edit<?=$main_product->per_package_id?>').slideUp();" class="btn btn-warning">cancel</a>
									</div>
								</div>
								<?=form_close();?> 
							</td>
						</tr>
						<?php }?>
					</tbody>
				</table> 
			</div>
		</div>
		<!-- /. ROW  -->
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>
<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/parent_category.js"></script>
	
	