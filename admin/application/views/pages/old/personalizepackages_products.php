<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Personalize Package Product List
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Enter New Package Product</a>
				</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div id="addNewSubCat" style="display:none;">
				<?=form_open_multipart('personalizepackages/insert_sub_product');?>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							<img id="productImagePreview" src="" style="width:100%;" alt="">
						</div>
						<div class="panel-footer back-footer-blue">
							<input type="file" name="package_image" id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%"> 
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter Sub Product Name</label>
							<input class="form-control" name="personalizedpackage_product_name">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Select Main Product</label>
							<?=form_dropdown("per_package_id", $ddl_main_products, set_value('per_package_id', ''), "tabindex='' id=''  class='form-control'")?>	
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Enter Sub Product Price</label>
							<input class="form-control" name="personalizedpackage_product_price">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Enter Sub Product Quantity</label>
							<input class="form-control" name="personalizedpackage_product_quantity">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12 text-right">
						<input type="submit" value="Save" class="btn btn-success" />
						<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">cancel</a>
					</div>
				</div>
				<?=form_close();?> 
				<div class="clearfix"></div>
				<hr />
			</div>
			<div class="col-md-12 text-right"> 
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Image</th>
							<th>Product Name</th> 
							<th>Package Name</th> 
							<th>Price</th> 
							<th>Quantity</th> 
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						<?php $i=0; foreach($sub_products as $sub_product){ $i++;?>
						<tr>
							<td><?=$i?></td>
							<td class="text-center"><img src="<?=IMAGEACCESSPATH.$sub_product->per_product_img?>" style="height:60px;" /></td>
							<td><?=$sub_product->product_name?></td> 
							<td><?=$sub_product->package_main_product?></td> 
							<td><?=$sub_product->product_price?></td> 
							<td><?=$sub_product->quantity?></td> 
							<td class="text-center"><a onclick="$('#edit<?=$sub_product->sub_product_id?>').show();" class="btn btn-warning">Edit</a></td>
							<td class="text-center"><a onclick="$('#delete<?=$sub_product->sub_product_id?>').show();" class="btn btn-danger">Delete</a></td>
						</tr>
						<tr style="display:none; text-align:center;" id="delete<?=$sub_product->sub_product_id?>">
							<td colspan="8">
								<br /><h4>Are your sure you want to delete this product : <?=$sub_product->product_name?></h4><br /><br />
								<a href="<?=site_url();?>Personalizepackages/delete_sub_product/<?=$sub_product->sub_product_id?>" class="btn btn-danger">Yes !</a>
								<a onclick="$('#delete<?=$sub_product->sub_product_id?>').hide();" class="btn btn-info">cancel</a>
								<br /><br />
							</td>
						</tr>
						<tr style="display:none" id="edit<?=$sub_product->sub_product_id?>">
							<td colspan="8">
								<?=form_open_multipart('personalizepackages/update_sub_product');?>
									<div class="col-md-3 col-sm-3 col-xs-6">
										<div class="panel panel-primary text-center no-boder bg-color-blue">
											<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
												<img id="productImagePreview<?=$sub_product->sub_product_id?>" src="<?=IMAGEACCESSPATH.$sub_product->per_product_img?>" style="width:100%;" alt="">
											</div>
											<div class="panel-footer back-footer-blue">
												<input type="file" name="package_image" id="productImg<?=$sub_product->sub_product_id?>" onchange="previewImage(productImg<?=$sub_product->sub_product_id?>,'productImagePreview<?=$sub_product->sub_product_id?>');" class="" style="width:100%"> 
											</div>
										</div>
									</div>
									<div class="col-md-9">
										<div class="col-md-12">
											<div class="form-group">
												<label>Change Sub Product Name</label>
												<input class="form-control hidden" value="<?=$sub_product->sub_product_id?>" name="sub_product_id">
												<input class="form-control hidden" value="<?=$sub_product->per_product_img?>" name="sub_product_image">
												<input class="form-control" value="<?=$sub_product->product_name?>" name="sub_product_name">
												<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
											</div> 
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label>Change Main Product</label>
												<?=form_dropdown("per_package_id", $ddl_main_products, set_value('per_package_id', $sub_product->per_package_id), "tabindex='' id=''  class='form-control'")?>	
												<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
											</div> 
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label>Change Sub Product Price</label>
												<input class="form-control" name="product_price" value="<?=$sub_product->product_price?>" />
												<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
											</div> 
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label>Change Sub Product Quantity</label>
												<input class="form-control" name="amount"value="<?=$sub_product->quantity?>" />
												<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
											</div> 
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 text-right">
											<input type="submit" value="Save" class="btn btn-success" />
											<a onclick="$('#edit<?=$sub_product->sub_product_id?>').hide();" class="btn btn-warning">cancel</a>
										</div>
									</div>
								<?=form_close();?> 
							</td>
						</tr>
						
						
						<?php }?>
					</tbody>
				</table> 
			</div>
		</div>
		<!-- /. ROW  -->
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>
<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/parent_category.js"></script>
	
	