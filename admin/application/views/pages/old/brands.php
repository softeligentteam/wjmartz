<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>
					Brands
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Add New Brand</a>
				</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div id="addNewSubCat" style="display:none;">
				<?=form_open_multipart('brand_specials/insert_brands');?>
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="panel panel-primary text-center no-boder bg-color-blue">
							<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
								<img id="productImagePreview" src="" style="width:100%;" alt="">
							</div>
							<div class="panel-footer back-footer-blue">
								<input type="file" required name="brand_img" id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%"> 
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="form-group">
							<label>Enter Brand Name</label>
							<input class="form-control" required name="brand" />
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p> 
							<div class="col-md-4">  
								<div class="form-group"> 
									<?=form_dropdown("parent_cat", $parent_cat_list, set_value('parent_cat', ''), "tabindex='5' id='parent_cat' required class='form-control'")?>		
								</div>  
							</div>
							
							<!--label>&lt; Select Brand Image</label><br /><br /-->
							
							<input type="submit" value="Save" class="btn btn-success" />
							<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">cancel</a>
						</div> 
					</div>
				<?=form_close();?> 
				<div class="clearfix"></div>
				<hr />
			</div>
			<div class="col-md-12 text-right"> 
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Image</th>
							<th>Brand Name</th>
							<th>Brand Status</th>
							<th colspan="3"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						<?php  foreach($brands as $brand){ ?>
						<tr>
							<td><?=$index?></td>
							<td class="text-center" ><img src="<?=IMAGEACCESSPATH.$brand->brand_img_url?>" style="height:75px"/></td>
							<td><?=$brand->brand?></td>
							<?php if($brand->brand_status==1){?>
							<td class="text-success"><b>Active</b></td>
							<?php }else{?>
							<td class="text-danger"><b>Inactive</b></td>
							<?php } ?>
							<td class="text-center"><a onclick="$('#edit<?=$brand->brand_id?>').show();" class="btn btn-primary">Edit</a></td>
							<td class="text-center"><a onclick="$('#delete<?=$brand->brand_id?>').show();" class="btn btn-danger">Delete</a></td>
							<td class="text-center">
								<?php if($brand->brand_status == 0){?>
								<?=form_open('brand_specials/active_brand')?>
								<button type="submit" class="btn btn-success" name="active"value="<?=$brand->brand_id?>">Active</button>
								<?=form_close();?>
								<?php } else{ ?>
								<?=form_open('brand_specials/inactive_brand')?>
								<button type="submit" class="btn btn-warning" name="inactive"value="<?=$brand->brand_id?>">Inactive </button>
								<?=form_close();?>
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td style="display:none; text-align:center;" colspan="7" id="delete<?=$brand->brand_id?>" >
								<br />
								<h4>Are you sure you want to delete brand : <?=$brand->brand?></h4>
								<br /><br />
								<a class="btn btn-danger" href="<?=site_url();?>Brand_specials/delete_brands/<?=$brand->brand_id?>">Yes ! </a>
								<a class="btn btn-info" onclick="$('#delete<?=$brand->brand_id?>').hide();">cancel</a>
								<br /><br />
							</td>
						</tr>
						<tr>
							<td style="display:none;" colspan="6" id="edit<?=$brand->brand_id?>">
								<?=form_open_multipart('brand_specials/update_brands');?>
									<div class="col-md-3 col-sm-3 col-xs-6">
										<div class="panel panel-primary text-center no-boder bg-color-blue">
											<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
												<img id="productImagePreview<?=$brand->brand_id?>" src="<?=IMAGEACCESSPATH.$brand->brand_img_url?>" style="width:100%;" alt="">
											</div>
											<div class="panel-footer back-footer-blue">
												<input type="file" name="brand_img" id="productImg<?=$brand->brand_id?>" onchange="previewImage(productImg<?=$brand->brand_id?>,'productImagePreview<?=$brand->brand_id?>');" class="" style="width:100%"> 
											</div>
										</div>
									</div>
									<div class="col-md-9">
										<div class="form-group">
											<label>Enter Brand Name</label>
											<input class="form-control hidden" name="id" value="<?=$brand->brand_id?>" />
											<input class="form-control hidden" name="image" value="<?=$brand->brand_img_url?>" />
											<input class="form-control" required name="brand_name" value="<?=$brand->brand?>" />
											<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
											
											<br /><br />
											<input type="submit" value="Save" class="btn btn-success" />
											<a onclick="$('#edit<?=$brand->brand_id?>').hide();" class="btn btn-warning">cancel</a>
										</div> 
									</div>
								<?=form_close();?> 
							</td>
						</tr>
						<?php $index++;
						}?>
					</tbody>
				</table> 
				<?php echo $this->pagination->create_links();?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>

<div class="insert_success" id="confirm_delete" style="display:none">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 id="dlt_msg" class="text-center">Are you sure you want to delete</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a href="" id="deleteMAinCatHref" id="" class="btn btn-primary">Yes</a>
					<a onclick="$('#confirm_delete').hide()" id="" class="btn btn-primary">No</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/brand.js"></script>
	
	