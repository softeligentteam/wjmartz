<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h5 style="padding-bottom:0;">
					All Pincode Wise Price List
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info pull-right">Enter New Pincode</a>
				</h5>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr style="margin-top:0;" />
		<div class="row">
			<div id="addNewSubCat" style="display:none;">
				<?=form_open('pincode/add_pincodewise_delivery_charges','onsubmit="return onSubmitpin();"');?>
					<div class="col-md-4">
						<div class="form-group">
							<label>Enter New Pincode</label>
							<input type ="number" class="form-control" name="pincode" id="pin" onblur="check_pincode();" required />
							<span class="errorMessage" id="noofmemberserror" style="color:red;></span>
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Enter Respective Service Charge</label>
							<input type ="number" class="form-control" name="charge" required />
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Enter Respective Delivery Day</label>
							<input type ="number" class="form-control" name="day" required />
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-12">
						<?php echo form_radio('payment', '1', false,array('id'=>'cod')); ?></td><td><?php echo form_label('Cash On Delivery', 'codlabel',array('id'=>'codlabel'));?>
						<?php echo form_radio('payment', '0', false,array('id'=>'online')); ?></td><td><?php echo form_label('On-line Payment', 'onlinelabel',array('id'=>'onlinelabel'));?>&nbsp;&nbsp;&nbsp;				
					</div>
					<div class="col-md-12">
						<input type="submit" value="Save" class="btn btn-success" />
						<a onclick="location.reload();" class="btn btn-warning">cancel</a>
					</div>
					<div class="clearfix"></div>
					<hr />
				<?=form_close();?>
			</div>
			<div class="col-md-12 text-right">
				<div id="updateNewpincode" style="display:none;">
					<?=form_open('pincode/edit_pincode');?>
						<div class="col-md-3">
							<div class="form-group">
								<label>Pincode ID</label>
								<input class="form-control" id="pin_id" name="pin_id" readonly />
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label> Pincode</label>
								<input type ="number" class="form-control" id="pincode" name="pin" value="" readonly required />
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label> Respective Service Charge</label>
								<input type ="number" class="form-control" id="del_charge" name="del_charge"  required />
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label> Respective Delivery Day</label>
								<input type ="number" class="form-control" id="del_day" name="del_day"  required />
								<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
							</div> 
						</div>
						<div class="col-md-12">
						<?php echo form_radio('payment1', '1', false,array('id'=>'cod1')); ?></td><td><?php echo form_label('Cash On Delivery', 'codlabel1',array('id'=>'codlabel1'));?>
						<?php echo form_radio('payment1', '0', false,array('id'=>'online1')); ?></td><td><?php echo form_label('On-line Payment', 'onlinelabel1',array('id'=>'onlinelabel1'));?>&nbsp;&nbsp;&nbsp;				
					</div>
						<div class="col-md-12">
							<input type="submit" value="Save" class="btn btn-success" />
							<a onclick="location.reload();" class="btn btn-warning">cancel</a>
						</div>
						<div class="clearfix"></div>
						<hr />
					<?=form_close();?>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Pincode</th>
							<th>Delivery&nbsp;Charges</th>
							<th>Delivery Day</th>
							<th>COD Applicable</th>
							<th><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						<?php  
                           foreach ($pincode_list as $pincode)  
                          {  
                        ?>	
						<tr>
							<td><?=$index?></td>
							<td><?=$pincode->pincode?></td>
							<td><?=$pincode->delivery_charges?></td>
							<td><?=$pincode->delivery_day?></td>
							<td>
								<?php 
									if($pincode->is_cod == 0){
										echo'<span class="text-danger">NO</span>';
									}else{
										echo'<span class="text-success">YES</span>';
									}
								?>
							</td>
							<td class="text-center"><a onclick="edit_this('<?=$pincode->pincode_id?>','<?=$pincode->pincode?>','<?=$pincode->delivery_charges?>','<?=$pincode->delivery_day?>','<?=$pincode->is_cod?>');" id="myBtn" class="btn btn-primary">Edit</a>
							<?php if($pincode->status == 1) {
							?>
								<a onclick="$('#inactivePincode<?=$pincode->pincode_id?>').slideDown();" id="myBtn" class="btn btn-warning">Inactive</a>
							<?php } else {?>
								
								<a onclick="$('#activePincode<?=$pincode->pincode_id?>').slideDown();" id="myBtn" class="btn btn-success">Active</a>
							<?php } ?>
							</td>
							
							<!--td class="text-center"><a onclick="delete_this('< ?=$pincode->pincode_id?>', ' < ?=$pincode->pincode?>');" class="btn btn-danger">Delete</a></td-->
						</tr>
						<div class="insert_success" id="activePincode<?=$pincode->pincode_id?>" style="display:none;">
							<div class="row">
								<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
									<div class="row">
										<div class="col-sm-12 text-center"> 
											<h4>Active Pincode</h4>
											<center>
											<img src="" class='img-responsive' style='width:150px;' />
											</center>
											<p>
												are you sure you want to Active this Pincode?
											</p>
											<br />
										</div>
										<div class="col-sm-12 text-center">
											
											<a title="Delete" href="<?php echo site_url('Pincode/activePincode/').$pincode->pincode_id; ?>" class="btn btn-success">Active</a>
											
											<a onclick="$('#activePincode<?=$pincode->pincode_id?>').slideUp();" id="" class="btn btn-primary">Not Sure</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="insert_success" id="inactivePincode<?=$pincode->pincode_id?>" style="display:none;">
							<div class="row">
								<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
									<div class="row">
										<div class="col-sm-12 text-center"> 
											<h4>Inactivate Pincode</h4>
											<center>
											<img src="" class='img-responsive' style='width:150px;' />
											</center>
											<p>
												are you sure you want to Inactivate this Pincode?
											</p>
											<br />
										</div>
										<div class="col-sm-12 text-center">
											
											<a title="Delete"class="btn btn-danger" href="<?php echo site_url('Pincode/inactivePincode/').$pincode->pincode_id; ?>" class="btn btn-danger">Inactive</a>
											
											<a onclick="$('#inactivePincode<?=$pincode->pincode_id?>').slideUp();" id="" class="btn btn-primary">Not Sure</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					   <?php $index++;
						 
						  } ?>	
					</tbody>
				</table> 
				<?php echo $this->pagination->create_links();?>
			</div>
		</div>
		<!-- /. ROW  -->
	</div>
	<!-- /. PAGE INNER  -->
</div>

<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script> var site_url = "<?php print site_url(); ?>"; </script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/add/pincode.js"></script>

	
	