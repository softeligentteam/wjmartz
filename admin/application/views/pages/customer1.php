<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12"> 
				<div class="col-md-4"> 
					<h5 style="padding-bottom:0;">Customer Details</h3>
				</div>
				<div class="col-md-8">  
					<?=form_open('customer/search_customer_details');?>
						<div class="col-md-8">  
							<div class="form-group"> 
								<input type="text" class="form-control" id = "customer_details" name = "customer_details" placeholder="Enter mobile number and Name for search" /> 		
							</div>  
							<button type="submit" class="btn btn-info" style="position: absolute; right: 0; top: 0;">
								<i class="fa fa-search"></i>
							</button>
						</div>  
					<?=form_close();?>
					<div class="col-md-4 py-0">
						 <form method="post" action="<?=site_url(); ?>/excel_export/export_all_customer_details">
							<input type="submit" name="export" class="btn btn-info pull-right" value="Generate Excel Sheet" />
						</form>
					</div>
				</div>
			</div>
		</div>
		<hr style="margin-top: 0;" />
		<div class="row">
			<div class="col-md-12"> 
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Cust.&nbsp;ID</th> 
							<th>Customer&nbsp;Name</th> 
							<th>Contact</th> 
							<th>Action</th> 
							<!--<th>Email</th> 
							<th>Address</th> 
							<th>Date&nbsp;Of&nbsp;Birth</th> -->
						</tr>
					</thead>
					<tbody>
						
						<?php  
						if(count($customer_list)!==0)
						{
							$index=1;
                           foreach ($customer_list as $customer)  
                          {  
                        ?>	
						<tr>
							<td><?=$index?></td>
							<td><?=$customer->cust_id?></td>
							<td><?=$customer->first_name .' '.$customer->last_name?></td>
							<td><?=$customer->mobile?></td>
							<td data-th="Action">
                                <div><a href="<?=site_url();?>customer/view_customer_details/<?=$customer->cust_id?>" "title="View Member"><i class="fa fa-eye istyle" aria-hidden="true"></i></a></div>
							</td>	
							<!--<td><?=$customer->email?></td>
							<td><?=$customer->address ." ".$customer->area_name ." ".$customer->city ."-". $customer->pincode?></td>
							<td><?=$customer->dob?></td>-->
						</tr>
					   <?php $index++;
						  }						  
						}else { ?>
							<tr class="warning no-result">
								<td colspan="7"><i class="fa fa-warning"/> No records found...</td>
							</tr>
						<?php } ?> 	
					</tbody>
				</table>
				<?php echo $this->pagination->create_links();?>
				<!--div class="col-sm-12" style="padding-left:0;"> 
				  <ul class="pagination">
					<li><a href="#">&lt;</a></li>
					<li><a href="#">1</a></li>
					<li class="active"><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">&gt;</a></li>
				  </ul> 
				</div-->
			</div>
		</div>
		<!-- /. ROW  -->
	</div>
	<!-- /. PAGE INNER  -->
</div>
	