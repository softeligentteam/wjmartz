<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
		$this->load->model('Slidermodel');
    }
    
    /* public  function view_slider(){ 
		//$header['metadata'] = $this->load->view('metadata/home.html', TRUE, NULL);
		$data['user_silder']=$this->Slidermodel->get_slider();
		$this->load->view('layouts/header');
		$this->load->view('pages/view_silder',$data);
		$this->load->view('layouts/footer');
    }
	public function update_slider()
	{
		if($this->input->post()){
			$slider = $this->input->post('submit');
			//var_dump($slider);
			$status = 0;
			$slider_image = null;
			if($slider === "user"){
				if($this->upload_slider_image($slider)){
					//var_dump($slider);
					$slider_image = $this->Slidermodel->get_user_slider_image_path();
					$status = $this->Slidermodel->update_user_slider();
					
					//echo('sucess');
				}
			}
			else if($slider === 'doctor'){
				if($this->upload_slider_image($slider)){
					$slider_image = $this->Homemodel->get_doctor_slider_image_path();
					$status = $this->Homemodel->update_doctor_slider();
				}
			}
			else{
				//echo('error');
				redirect('slider/view_slider');
			}
			$this->set_slider_update_message($status, $slider, $slider_image);
		}
		redirect('slider/view_slider');
	}
	
	public function delete_slider_image($slider_path)
	{
		if($slider_path !== NULL){
			if(!empty($slider_path[0]->image_path)){
				if(file_exists(IMAGEBASEPATH.$slider_path[0]->image_path)){
					unlink(IMAGEBASEPATH.$slider_path[0]->image_path);
				}
			}
		}
	}
    
    public function set_slider_update_message($status, $slider, $slider_image)
	{
		if($status !== 0){
			$this->delete_slider_image($slider_image);
			$this->session->set_flashdata(array (
				"css_class"=> 'text-success',
				"message"=> 'Slider image is uploaded sucessfully.'
			));	
		}else{
			$this->session->set_flashdata(array (
				"css_class"=> 'text-alert',
				"message"=> 'Slider image is not uploaded successfully. Try again.'
			));	
		}
	}
	
	function upload_slider_image($slider) {
		if (empty ( $_FILES ['slider_img'] ['name'] )) {
			echo "<script>console.log('No file found.');</script>";
			return FALSE;
		} else {
			$path = null;
			if($slider === "user")
				$path = IMAGEBASEPATH . APPSLIDERUPLOAD;
			else
				$path = DOCTORIMAGEUPLOAD;

			$config ['upload_path'] = $path;
			$config ['allowed_types'] = 'png|jpg|jpeg';
			$config['min_width'] = '900'; $config['min_height'] = '700';
			$config['max_width'] = '900'; $config['max_height'] = '700';
			$config['max_size'] = 300;
			// $config['min_width'] = '360'; $config['min_height'] = '480';
			// $config['max_width'] = '360'; $config['max_height'] = '480';
			//$config ['encrypt_name'] = TRUE;
			$config ['override'] = FALSE;
			echo "<script>console.log('path is ".$path."');</script>";
			$this->load->library ( 'upload' );
			$this->upload->initialize($config);
			if (! is_dir ( $config ['upload_path'] )) {
				mkdir ( $config ['upload_path'], 0777, TRUE );
			}
			
			if ($this->upload->do_upload ('slider_img')) {
				$upload_data = $this->upload->data();
				$_POST ['slider_img'] = APPSLIDERUPLOAD . $upload_data ['file_name'];
				echo "<script>console.log('Success.');</script>";
				return TRUE;
			} else {
				//$this->form_validation->set_message ( 'upload_slider_photo', $this->upload->display_errors () );
				echo "<script>console.log('Errors: ".$this->upload->display_errors ()."');</script>";
				return FALSE;
			}
		}
	} */
	/***** Common method for Andriod and Website Start *****/
	public function delete_slider_image($slider_path)
	{
		if($slider_path !== NULL){
			if(!empty($slider_path[0]->slider_image)){
				if(file_exists(IMAGEBASEPATH.$slider_path[0]->slider_image)){
					unlink(IMAGEBASEPATH.$slider_path[0]->slider_image);
				}
			}
		}
	}
	/***** Common method for Andriod and Website Start *****/
	
	/****** Slider Image add and update for App Start ******/
	public function slider_for_app() {
		$data['android_silder']=$this->Slidermodel->get_slider_for_andriod();
		$this->load->view('layouts/header');
		$this->load->view('pages/view_andriod_silder',$data);
		$this->load->view('layouts/footer');
	}
	public function update_app_slider_img()
	{
		if($this->validate_app_slider_img())
		{
			$slider_image = $this->Slidermodel->get_slider_image_path();
			$slider_data = $this->Slidermodel->update_app_slider();
			if($slider_data!==NULL)
			{
				$this->delete_slider_image($slider_image);
				$this->session->set_flashdata(array (
				"css_class"=> 'text-success',
				"message"=> 'Slider image is uploaded sucessfully.'
			));	
				redirect('Slider/slider_for_app');
			}
			else
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-alert',
				"message"=> 'Slider image is not uploaded successfully. Try again.'
			));	
				redirect('Slider/slider_for_app');
			}
		}
		else
		{
			redirect('Slider/slider_for_app');
		}
	}
	public function validate_app_slider_img()
	{
    	$this->form_validation->set_error_delimiters ('<div class="error_msg">', '</div>');
    	$this->form_validation->set_rules ('slider_img', 'Android Slider Image', 'callback_upload_app_slider_image');
    	return $this->form_validation->run();
	}
	public function upload_app_slider_image() 
	{
		if (empty ( $_FILES ['slider_img'] ['name'])) {
    		$this->form_validation->set_message ( 'upload_app_slider_image', 'Please select Slider Image' );
    		return FALSE;
    	} else {
    		$config ['upload_path'] = IMAGEBASEPATH . APPSLIDERUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
			$config['max_size'] = 300;
			$config['max_width'] = 900;
			$config['max_height'] = 700;
    		$config ['encrypt_name'] = TRUE;
    		$config ['override'] = FALSE;
    			
    		$this->load->library ( 'upload' );
    		$this->upload->initialize ( $config );
    		if (! is_dir ( $config ['upload_path'] )) {
    			mkdir ( $config ['upload_path'], 0777, TRUE );
    		}
    			
    		if ($this->upload->do_upload( 'slider_img')) {
    			$upload_data = $this->upload->data();
    			$_POST ['slider_img'] = APPSLIDERUPLOADPATH . $upload_data ['file_name'];
    			return TRUE;
    		} else {
    			$this->form_validation->set_message ( 'upload_app_slider_image', $this->upload->display_errors ());
    			return FALSE;
    		}
    	}
	}
	public function inactiveSliderApp() 
	{
		$slider_img_id = $this->uri->segment(3,0);
		$slider_data = $this->Slidermodel->inactiveSlider($slider_img_id);
		if($slider_data)
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-success',
				"message"=> 'Slider image is Inactive sucessfully.'
			));	
				redirect('Slider/slider_for_app');
			}
			else
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-alert',
				"message"=> 'Fail to Inactive Slider image. Try again.'
			));	
				redirect('Slider/slider_for_app');
			}
	}
	public function activeSliderApp() 
	{
		$slider_img_id = $this->uri->segment(3,0);
		$slider_data = $this->Slidermodel->activeSlider($slider_img_id);
		if($slider_data)
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-success',
				"message"=> 'Slider image is Active sucessfully.'
			));	
				redirect('Slider/slider_for_app');
			}
			else
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-alert',
				"message"=> 'Fail to Active Slider image. Try again.'
			));	
				redirect('Slider/slider_for_app');
			}
	}
	/****** Slider Image add and update for App End ******/
	
	
	
	/****** Slider Image add and update for website Start******/
	public function slider_for_website() {
		//$header['metadata'] = $this->load->view('metadata/home.html', TRUE, NULL);
		$data['website_silder']=$this->Slidermodel->get_slider_for_website();
		$this->load->view('layouts/header');
		$this->load->view('pages/view_website_silder',$data);
		$this->load->view('layouts/footer');
	}
	public function update_website_slider_img()
	{
		if($this->validate_website_slider_img())
		{
			$slider_image = $this->Slidermodel->get_slider_image_path();
			$slider_data = $this->Slidermodel->update_website_slider();
			if($slider_data!==NULL)
			{
				$this->delete_slider_image($slider_image);
				$this->session->set_flashdata(array (
				"css_class"=> 'text-success',
				"message"=> 'Slider image is uploaded sucessfully.'
			));	
				redirect('Slider/slider_for_website');
			}
			else
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-alert',
				"message"=> 'Slider image is not uploaded successfully. Try again.'
			));	
				redirect('Slider/slider_for_website');
			}
		}
		else
		{
			redirect('Slider/slider_for_website');
		}
	}
	public function validate_website_slider_img()
	{
    	$this->form_validation->set_error_delimiters ('<div class="error_msg">', '</div>');
    	$this->form_validation->set_rules ('slider_img', 'Android Slider Image', 'callback_upload_website_slider_image');
    	return $this->form_validation->run();
	}
	public function upload_website_slider_image() 
	{
		if (empty ( $_FILES ['slider_img'] ['name'])) {
    		$this->form_validation->set_message ( 'upload_website_slider_image', 'Please select Slider Image' );
    		return FALSE;
    	} else {
    		$config ['upload_path'] = IMAGEBASEPATH . WEBSLIDERUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
			$config['max_size'] = 300;
			$config['max_width'] = 1680;
			$config['max_height'] = 650;
    		$config ['encrypt_name'] = TRUE;
    		$config ['override'] = FALSE;
    			
    		$this->load->library ( 'upload' );
    		$this->upload->initialize ( $config );
    		if (! is_dir ( $config ['upload_path'] )) {
    			mkdir ( $config ['upload_path'], 0777, TRUE );
    		}
    			
    		if ($this->upload->do_upload( 'slider_img')) {
    			$upload_data = $this->upload->data();
    			$_POST ['slider_img'] = WEBSLIDERUPLOADPATH . $upload_data ['file_name'];
    			return TRUE;
    		} else {
    			$this->form_validation->set_message ( 'upload_website_slider_image', $this->upload->display_errors ());
    			return FALSE;
    		}
    	}
	}
	public function inactiveSliderWebsite() 
	{
		$slider_img_id = $this->uri->segment(3,0);
		$slider_data = $this->Slidermodel->inactiveSlider($slider_img_id);
		if($slider_data)
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-success',
				"message"=> 'Slider image is Inactive sucessfully.'
			));	
				redirect('Slider/slider_for_website');
			}
			else
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-alert',
				"message"=> 'Fail to Inactive Slider image. Try again.'
			));	
				redirect('Slider/slider_for_website');
			}
	}
	public function activeSliderWebsite() 
	{
		$slider_img_id = $this->uri->segment(3,0);
		$slider_data = $this->Slidermodel->activeSlider($slider_img_id);
		if($slider_data)
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-success',
				"message"=> 'Slider image is Active sucessfully.'
			));	
				redirect('Slider/slider_for_website');
			}
			else
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-alert',
				"message"=> 'Fail to Active Slider image. Try again.'
			));	
				redirect('Slider/slider_for_website');
			}
	}
	/****** Slider Image add and update for website End******/
}