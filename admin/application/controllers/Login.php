<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	 public function __construct() 
	 {

            parent::__construct();
            $this->load->model('Loginmodel');
     }
	public function index()
	{
		$this->load->view('login');
	}

	
        /**************** Admin Authentication ****************/
	public function authenticate()
	{
			$user_data=$this->Loginmodel->authenticate_user();
			if($user_data!==NULL)
			{
				$this->set_session($user_data);
				redirect('home/index');
			}
			else
			{
				$this->session->set_flashdata(array('css_class' => 'text-danger', 'message' => 'Email Address or Password is wrong...'));
				redirect('login');
			}
		
	}
	
	/******************** Logout User *********************/
	public function logout()
	{
		$this->unset_session();
		redirect('login');
	}
	
	/****************** Session Section **********************/
        public function set_session($user_data)
	{
		$this->session->set_userdata(
			array(
				'id' => $user_data->id,
				'user_name' => $user_data->user_name,
				'us_logged_in' => TRUE
			)
		);
	}
	
	public function unset_session()
	{
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('user_name');
		$this->session->unset_userdata('us_logged_in');
        $this->session->sess_destroy();
	}
}
