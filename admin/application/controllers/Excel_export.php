<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_export extends CI_Controller {
 
	public function __construct() 
	{

            parent::__construct();
            $this->load->model('Excel_export_model');
			$this->load->model('Ddlmodel');
			
    }

	public function export_all_customer_details()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	  $table_columns = array("#","Cust. ID","Customer Name", "Contact","Email", "Address", "Date Of Birth");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $customer_data = $this->Excel_export_model->get_all_customer_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($customer_data as $row)
	  {
		$address =  $row->address.','.$row->city.','.$row->area_name.','.$row->pincode.','.	$row->state;
		$cust_name = $row->first_name.' '.$row->last_name;
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->cust_id);
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $cust_name);
		$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->mobile);
		$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->email);
		$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $address);
		$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->dob);
		$excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="CustomerData.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	
	public function export_woman_ware_product()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	  $table_columns = array("#","Product No.","Product Name", "Product Price", "Wholesale Price");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $product_ware_data = $this->Excel_export_model->get_woman_ware_product_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($product_ware_data as $row)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->prod_id);
	   //$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->prod_image_urls);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->prod_name);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->prod_price);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->pwholesale_price);
	   $excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="WomanWareData.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	public function export_woman_ware_product_inactive()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	  $table_columns = array("#","Product No.","Product Name", "Product Price", "Wholesale Price");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $product_ware_inactive_data = $this->Excel_export_model->get_woman_ware_product_inactive_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($product_ware_inactive_data as $row)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->prod_id);
	   //$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->prod_image_urls);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->prod_name);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->prod_price);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->pwholesale_price);
	   $excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="WomanWareInactiveData.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	
	public function export_inprocess_order()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	   $table_columns = array("#","	Order ID","Customer Name","Email","Address","Order Date", "Order Amount");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $inprocess_order_data = $this->Excel_export_model->get_inprocess_order_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($inprocess_order_data as $row)
	  { 
	   $address =  $row->address.','.$row->city.','.$row->area_name.','.$row->pincode.','.$row->state;
	   $cust_name = $row->first_name.' '.$row->last_name;
	   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->order_id);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $cust_name);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->email);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row,$address);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->order_date);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->amount);
	   $excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="InprocessOrderData.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	public function export_ready_to_dispatch_order()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	  $table_columns = array("#","Order ID.","Customer Name","	Address");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $dispatch_order_data = $this->Excel_export_model->get_ready_to_dispatch_order_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($dispatch_order_data as $row)
	  {
		$address =  $row->address.','.$row->area_name.','.$row->pincode;
		$cust_name = $row->first_name.' '.$row->last_name;  
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->order_id);
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $cust_name);
		$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $address);
		$excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="ReadyToDispatchOrderData.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	public function export_delivered_order()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	  $table_columns = array("#","Order ID.","Customer Name","Order Date", "Order Amount");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $delivered_order_data = $this->Excel_export_model->get_delivered_order_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($delivered_order_data as $row)
	  {$cust_name = $row->first_name.' '.$row->last_name;  
	   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->order_id);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $cust_name);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->order_date);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->amount);
	   $excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="DeliveredOrderData.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	public function export_cancelled_order()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	  $table_columns = array("#","Order ID.","Customer Name","Cancelled by", "Modified by", "Order Date","Order Amount");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $cancelled_order_data = $this->Excel_export_model->get_cancelled_order_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($cancelled_order_data as $row)
	  {
		  if($row->modified_by == '0'){
			  $Cancelled_by='Customer';
			}else
			{
				$Cancelled_by='Admin';
			}
		$cust_name = $row->first_name.' '.$row->last_name;  	
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->order_id);
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $cust_name);
		$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $Cancelled_by);
		$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->mobile);
		$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->order_date);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->amount);
	   $excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="CancelledOrderData.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	public function export_failed_order()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	  $table_columns = array("#","Ref Order ID.","Customer Name","Order Date", "Order Amount");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $failed_order_data = $this->Excel_export_model->get_failed_order_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($failed_order_data as $row)
	  {
		$cust_name = $row->first_name.' '.$row->last_name;    
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->order_id);
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $cust_name);
		$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->order_date);
		$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->amount);
		$excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="failedOrderData.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	public function export_payment_sucessful_transaction()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	   $table_columns = array("#","Order ID.","Transaction ID","Customer Name","Tansaction Date", "Transaction Status");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $payment_sucessful_transaction_data = $this->Excel_export_model->get_payment_sucessful_transaction_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($payment_sucessful_transaction_data as $row)
	  {
		$cust_name = $row->first_name.' '.$row->last_name;  
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->order_id);
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->transaction_id);
		$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $cust_name);
		$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->payment_datetime);
		$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->payment_status);
		$excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="PaymentSucessfulDate.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	public function export_payment_failed_transaction()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	  $table_columns = array("#","Ref Order ID","Transaction ID","Customer Name","Tansaction Date", "Transaction Status","Failure Reason");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $payment_failed_transaction_data = $this->Excel_export_model->get_payment_failed_transaction_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($payment_failed_transaction_data as $row)
	  {
		$cust_name = $row->first_name.' '.$row->last_name;    
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->ref_order_id);
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->transaction_id);
		$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $cust_name);
		$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->payment_datetime);
		$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->payment_status);
		$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->trans_reason);
		$excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="PaymentFailedDate.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	
	public function inactive_child_categories()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	   $table_columns = array("#","Child Category Name","Status");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $child_cat_list_inactive_data = $this->Excel_export_model->get_child_cat_list_inactive_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($child_cat_list_inactive_data as $row)
	  {
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->child_cat_name);
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, 'Inactive');
		$excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="InactiveChildCategoriesData.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	public function export_woman_footware_product()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	  $table_columns = array("#","Product No.","Product Name", "Product Price", "Wholesale Price");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $product_ware_data = $this->Excel_export_model->get_woman_footware_product_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($product_ware_data as $row)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->prod_id);
	   //$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->prod_image_urls);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->prod_name);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->prod_price);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->pwholesale_price);
	   $excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="WomanFootwareData.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	public function export_woman_footware_product_inactive()
	{
	  $object = new PHPExcel();
	  $object->setActiveSheetIndex(0);
	  $table_columns = array("#","Product No.","Product Name", "Product Price", "Wholesale Price");

	  $column = 0;

	  foreach($table_columns as $field)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
	   $column++;
	  }

	  $product_ware_inactive_data = $this->Excel_export_model->get_woman_footware_product_inactive_data();

	  $excel_row = 2;
	  $index=1;
	  foreach($product_ware_inactive_data as $row)
	  {
	   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $index++);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->prod_id);
	   //$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->prod_image_urls);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->prod_name);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->prod_price);
	   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->pwholesale_price);
	   $excel_row++;
	  }

	  $object_writer = PHPExcel_IOFactory::createWriter($object, 'HTML');
	  header('Content-Type: application/vnd.ms-excel');
	  header('Content-Disposition: attachment;filename="WomanfootwareInactiveData.xls"');
	  $object_writer->save('php://output');
	  exit;
	}
	
}