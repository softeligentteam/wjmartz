<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pdf_export extends CI_Controller{
  public function __construct()
  {
    parent::__construct();
	$this->load->model("Ordermodel");
    //Codeigniter : Write Less Do More
  }
  function index()
  {
	$order_id = $this->uri->segment(3,0);
	$data['order']=$this->Ordermodel->get_orderidwise_order_details($order_id);
	$data['order_product']=$this->Ordermodel->get_orderwise_products($order_id);
    $html = $this->load->view('table_report', $data, true);
    $filename = 'report_'.time();
    $this->pdfgenerator->generate($html, $filename, true, 'A4', 'portrait');
  }
  function get_inprocess_data()
  {
	$order_id = $this->uri->segment(3,0);
	$data['order']=$this->Ordermodel->get_orderidwise_order_details($order_id);
	$data['order_product']=$this->Ordermodel->get_orderwise_products($order_id);
    $html = $this->load->view('table_report_inprocess', $data, true);
    $filename = 'report_'.time();
    $this->pdfgenerator->generate($html, $filename, true, 'A4', 'portrait');
  }
}