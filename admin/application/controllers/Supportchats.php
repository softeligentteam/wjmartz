<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supportchats extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Support_Services_model');
	}	
	 
	public function view_supportchats()
	{
		$per_page =5;
		$url = site_url().'/Supportchats/view_supportchats';
		$number_of_rows = $this->Support_Services_model->get_no_of_rows_chat();
		
		$data['support_chat']=$this->Support_Services_model->get_support_chat($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url,$number_of_rows,$per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/view_supportchats',$data);
		$this->load->view('layouts/footer');
	}
}
