<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supportservices extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Support_Services_model');
	}	
	 
	public function view_supportservices()
	{
		$per_page =5;
		$url = site_url().'/supportservices/view_supportservices';
		$number_of_rows = $this->Support_Services_model->get_no_of_rows_services();
		
		$data['services_list']=$this->Support_Services_model->get_all_support_services($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url,$number_of_rows,$per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/view_supportservices',$data);
		$this->load->view('layouts/footer');
	}
	
	public function add_support_services()
	{
		if($this->input->post())
		{
			$this->Support_Services_model->insert_support_services();
			
			$message = 'Support Services is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('supportservices/view_supportservices');
		}else{
			$message = 'Support Services  insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('supportservices/view_supportservices');
		}
	}
	
	
	public function add_supportservices()
	{
		$this->load->view('layouts/header');
		$this->load->view('pages/add_supportservices');
		$this->load->view('layouts/footer');
	}
	
	public function delete_support_service()
	{
		$service_id=$this->uri->segment(3,0);
		if($this->Support_Services_model->del_support_service($service_id))
		{	
			
			$message = 'Support Service is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('supportservices/view_supportservices');
		}else{
			$message = 'Support Service  is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('supportservices/view_supportservices');
		}
	}
	
	public function edit_service()
	{
		if($this->input->post())
		{	
		if($this->Support_Services_model->update_service())
		{	
			
			$message = 'Support Service is updated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('supportservices/view_supportservices');
		}else{
			$message = 'Support Service is not updated sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('supportservices/view_supportservices');
		}
		}	
	}
	
}
