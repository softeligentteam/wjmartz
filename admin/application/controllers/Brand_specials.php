<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand_specials extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->output->set_header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
		  if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Brandmodel');
		$this->load->model('Ddlmodel');
	}	
	 
	public function brands()
	{
		$per_page = 15;
		$url = site_url().'/brand_specials/brands';
		$number_of_rows = $this->Brandmodel->get_no_of_rows();
		
		$data['parent_cat_list']=$this->Ddlmodel->get_parent_category_list();
		$data['brands'] = $this->Brandmodel->get_brand_list($per_page);
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url,$number_of_rows,$per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/brands', $data);
		$this->load->view('layouts/footer');
	}
	
	public function insert_brands()
	{
		if($this->input->post())
		{
			if($this->upload_brand_img())
			{
				$this->Brandmodel->insert_brands();
				
				$message = 'Brand is inserted sucessfully';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('brand_specials/brands');
			}
			else
			{
				$message = 'Fail To uplaod Brand Img';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('brand_specials/brands');

			}
		}else{
			$message = 'Brand  insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('brand_specials/brands');
		}
			
	}
	function upload_brand_img() {
			if (empty ( $_FILES ['brand_img'] ['name'])) {
    		return FALSE;
    	} else {
    		$config ['upload_path'] = IMAGEBASEPATH . BRANDUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
			//$config['max_size'] = 1024 * 8;
			//$config['max_width'] = 512;
			//config['max_height'] = 512;
			$config ['encrypt_name'] = TRUE;
    		$config ['override'] = FALSE;
    			
    		$this->load->library ( 'upload' );
    		$this->upload->initialize ( $config );
    		if (! is_dir ( $config ['upload_path'] )) {
    			mkdir ( $config ['upload_path'], 0777, TRUE );
    		}
    			
    		if ($this->upload->do_upload( 'brand_img')) {
    			$upload_data = $this->upload->data();
    			$_POST ['brand_img'] = BRANDUPLOADPATH . $upload_data ['file_name'];
    			return TRUE;
    		} else {
    			return FALSE;
    		}
    	}
    }
	public function delete_brands()
	{
		$brand_id=$this->uri->segment(3,0);
		if($this->Brandmodel->del_brand($brand_id))
		{	
			
			$message = 'Brand is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('brand_specials/brands');
		}else{
			$message = 'Brand is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('brand_specials/brands');
		}
	}
	
	public function update_brands()
	{
		if($this->input->post())
		{	$brand_image = $this->Brandmodel->get_brand_image_path();
			if($this->update_brand_image())
			{
				$this->Brandmodel->update_brands();
				$this->delete_brand_images($brand_image);
				
				$message = 'Brand updated sucessfully';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));	
				
				redirect('brand_specials/brands');
			}
			else
			{
				$message = 'Fail To update Brand Image';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));	
				
				redirect('brand_specials/brands');
			}
		}else{
			$message = 'Brand is not updated sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('brand_specials/brands');
		}		 
	}
	function update_brand_image() {
    	
			
			if (empty ( $_FILES ['brand_img'] ['name'])) {
    		return FALSE;
    	} else {
    		$config ['upload_path'] = IMAGEBASEPATH . BRANDUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
			//$config['max_size'] = 1024 * 8;
			//$config['max_width'] = 512;
			//config['max_height'] = 512;
			$config ['encrypt_name'] = TRUE;
    		$config ['override'] = FALSE;
    			
    		$this->load->library ( 'upload' );
    		$this->upload->initialize ( $config );
    		if (! is_dir ( $config ['upload_path'] )) {
    			mkdir ( $config ['upload_path'], 0777, TRUE );
    		}
    			
    		if ($this->upload->do_upload( 'brand_img')) {
    			$upload_data = $this->upload->data();
    			$_POST ['brand_img'] = BRANDUPLOADPATH . $upload_data ['file_name'];
    			return TRUE;
    		} else {
    			return FALSE;
    		}
    	}
    }
	
	public function inactive_brand()
	{
		if($this->input->post())
		{
			$this->Brandmodel->inactivate_brands();
			
			$message = 'Brand is Inactivated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('brand_specials/brands');
		}else{
			$message = 'Brand  Inactivation failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('brand_specials/brands');
		}
	}
	
	public function active_brand()
	{
		if($this->input->post())
		{
			$this->Brandmodel->activate_brands();
			
			$message = 'Brand is Activated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('brand_specials/brands');
		}else{
			$message = 'Brand  Activation failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('brand_specials/brands');
		}
	}
	//Pooja Jadhav
	public function search_brand()
	{
		$brand_name=$this->input->post('brand_name');
		if(isset($brand_name) && !empty($brand_name))
		{
			$data['brands']=$this->Brandmodel->get_search_brand_details($brand_name);
			$this->load->view('layouts/header');
			$this->load->view('pages/brands',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('brand_specials/brands');
		}	
	}
//KT[18-04-18]
	private function delete_brand_images($brand_image)
	{
			if($brand_image !== NULL){
			if(!empty($brand_image[0]->brand_img_url)){
				if(file_exists(IMAGEBASEPATH.$brand_image[0]->brand_img_url)){
					unlink(IMAGEBASEPATH.$brand_image[0]->brand_img_url);
				}
			}
		}
		
	}
	
}
