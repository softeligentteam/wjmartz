<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pincode extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Pincodemodel');
		$this->load->model('Ddlmodel');
	}	
	 
	public function pincode_wise_price_list()
	{
		$per_page = 15;
		$url = site_url().'/pincode/pincode_wise_price_list';
		$number_of_rows = $this->Pincodemodel->get_no_of_rows();
		
		$data['pincode_list']=$this->Pincodemodel->get_all_pincodes_with_charges($per_page);
		$data['area_list']=$this->Ddlmodel->get_area_list();
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/pincode_wise_price',$data);
		$this->load->view('layouts/footer');
	}
	
	public function active_pincode_wise_price_list()
	{
		$per_page = 15;
		$url = site_url().'/pincode/active_pincode_wise_price_list';
		$number_of_rows = $this->Pincodemodel->get_active_no_of_rows();
		
		$data['pincode_list']=$this->Pincodemodel->get_active_pincodes_with_charges($per_page);
		$data['area_list']=$this->Ddlmodel->get_area_list();
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/active_pincode_wise_price',$data);
		$this->load->view('layouts/footer');
	}
	public function inactive_pincode_wise_price_list()
	{
		$per_page = 15;
		$url = site_url().'/pincode/inactive_pincode_wise_price_list';
		$number_of_rows = $this->Pincodemodel->get_inactive_no_of_rows();
		
		$data['pincode_list']=$this->Pincodemodel->get_inactive_pincodes_with_charges($per_page);
		$data['area_list']=$this->Ddlmodel->get_area_list();
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/inactive_pincode_wise_price',$data);
		$this->load->view('layouts/footer');
	}
	
	
	public function add_pincodewise_delivery_charges()
	{
		if($this->input->post())
		{
			$this->Pincodemodel->insert_delivery_charges();
			
			$message = 'Delivery Charge And Day is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('pincode/pincode_wise_price_list');
		}else{
			$message = 'Delivery Charge And Day insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('pincode/pincode_wise_price_list');
		}
	}
	
	public function delete_pincode()
	{
		$pincode_id=$this->uri->segment(3,0);
		if($this->Pincodemodel->del_pincode($pincode_id))
		{	
			
			$message = 'Pincode is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('pincode/pincode_wise_price_list');
		}else{
			$message = 'Pincodeis not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('pincode/pincode_wise_price_list');
		}
	}

	public function edit_pincode()
	{
		if($this->input->post())
		{	
		if($this->Pincodemodel->update_pincode())
		{	
			
			$message = 'Pincode is updated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('pincode/pincode_wise_price_list');
		}else{
			$message = 'Pincode is not updated sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('pincode/pincode_wise_price_list');
		}
		}	
	}
	
	/*----- KT[date:-18/04/18]-----*/
	public function check_pincode()
	{
		$pincode=$this->input->post('pin');
		//echo $pincode;
		if($this->input->post())
		{	
			$data=$this->Pincodemodel->get_pincode($pincode);
		}
		else
		{
			$data=NULL;
		}
		echo json_encode($data);
	}
	
	public function inactivePincode() 
	{
		$Pincode_id = $this->uri->segment(3,0);
		$slider_data = $this->Pincodemodel->inactivePincode($Pincode_id);
		if($slider_data)
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-success',
				"message"=> 'Pincode is Inactive sucessfully.'
			));	
				redirect('/pincode/pincode_wise_price_list');
			}
			else
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-alert',
				"message"=> 'Fail to Inactive Pincode. Try again.'
			));	
				redirect('/pincode/pincode_wise_price_list');
			}
	}
	public function activePincode() 
	{
		$Pincode_id = $this->uri->segment(3,0);
		$slider_data = $this->Pincodemodel->activePincode($Pincode_id);
		if($slider_data)
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-success',
				"message"=> 'Pincode is Active sucessfully.'
			));	
				redirect('/pincode/pincode_wise_price_list');
			}
			else
			{
				$this->session->set_flashdata(array (
				"css_class"=> 'text-alert',
				"message"=> 'Fail to Active Pincode. Try again.'
			));	
				redirect('/pincode/pincode_wise_price_list');
			}
	}
	
}
