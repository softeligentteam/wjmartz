<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Achild_categories extends CI_Controller 
{
	public function get_child_category_list()
	{
		$this->load->model('Achildcategorymodel');
		$parent_cat_id=$this->uri->segment(3,0);
		$data['child_cat_list']=$this->Achildcategorymodel->get_parent_categorywise_child_categories($parent_cat_id);
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
		
	}
}