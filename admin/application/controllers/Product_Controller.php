<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_Controller extends CI_Controller {
	
	public function __construct() {

        parent::__construct();
		$this->load->model('Ddlmodel');
    }
	
	public function view_form_women_ware()
	{
		$this->load->view('layouts/header');
		$this->load->view('pages/add_women_ware',$this->bind_dropdownlist_for_women_ware());
		$this->load->view('layouts/footer');
	}
	
	 protected function bind_dropdownlist_for_women_ware()
     {
		$data['brand_list'] = $this->Ddlmodel->get_brand_list();
		$data['size_list'] = $this->Ddlmodel->get_psize_list();
		$data['color_list'] = $this->Ddlmodel->get_pcolor_list();
		$data['pc_catlist'] = $this->Ddlmodel->get_child_category_list_for_women_ware();
		$data['pp_catlist']= $this->Ddlmodel->get_parent_category_list_for_women_ware();
		$data['ftype_list'] = array(
			''=>'Select Fabric Type',
			'Cotton'=>'Cotton',
			'Linen'=>'Linen',
			'Synthetic'=>'Synthetic',
			'Woolen'=>'Woolen',
			'Chiffon'=>'Chiffon',
			'Lace'=>'Lace',
			'Polyster'=>'Polyster',
			'Silk'=>'Silk',
			'Denim'=>'Denim',
			'Khadi cotton'=>'Khadi cotton',
			'Georgette'=>'Georgette',
			'Spandex'=>'Spandex'
		);
		return $data;
	 }
	 
	 public function view_form_women_footware()
	{
		$this->load->view('layouts/header');
		$this->load->view('pages/add_women_footware',$this->bind_dropdownlist_for_women_footware());
		$this->load->view('layouts/footer');
	}
	
	 protected function bind_dropdownlist_for_women_footware()
     {
		$data['brand_list'] = $this->Ddlmodel->get_brand_list_footware();
		$data['size_list'] = $this->Ddlmodel->get_psize_list_footware();
		$data['color_list'] = $this->Ddlmodel->get_pcolor_list();
		$data['pc_catlist'] = $this->Ddlmodel->get_child_category_list_for_women_footware();
		$data['product_for'] = $this->Ddlmodel->get_product_for_list_footware();
		$data['ftype_list'] = array(
			''=>'Select  Type',
			'Chappal'=>'Chappal',
			'Sandal'=>'Sandal',
			'Heel'=>'Heel',
			'Shoe'=>'Shoe'
		);
		return $data;
	 }
	 
	 public function view_women_wares($data)
	{
		$this->load->view('layouts/header');
		$this->load->view('pages/view_women_ware',$data);
		$this->load->view('layouts/footer');
	}
	
	public function view_women_wares_inactive($data)
	{
		$this->load->view('layouts/header');
		$this->load->view('pages/view_women_ware_inactive',$data);
		$this->load->view('layouts/footer');
	}
	
	public function view_women_footwares_inactive($data)
	{
		$this->load->view('layouts/header');
		$this->load->view('pages/view_women_footware_inactive',$data);
		$this->load->view('layouts/footer');
	}
	
	public function view_women_footwares($data)
	{
		$this->load->view('layouts/header');
		$this->load->view('pages/view_women_footware',$data);
		$this->load->view('layouts/footer');
	}
	
	public function view_update_women_ware($data)
	{
		$this->load->view('layouts/header');
		$this->load->view('pages/update_women_ware',$data);
		$this->load->view('layouts/footer');
	}
	
	public function view_update_women_footware($data)
	{
		$this->load->view('layouts/header');
		$this->load->view('pages/update_women_footware',$data);
		$this->load->view('layouts/footer');
	}
	
	
	
}