<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aproduct extends CI_Controller 
{
	 public function __construct() 
	 {
        parent::__construct();
		$this->load->model('Aproductmodel');
		
	 }	
	 
	 public function general_product_list()
	{
		$child_cat_id=$this->uri->segment(3,0);
		$count=$this->Aproductmodel->get_no_of_rows($child_cat_id);
		$per_page=6;
		
		if($count>0)
		{
			$data['products']= $this->Aproductmodel->get_genral_products($per_page,$child_cat_id);
			$data['count']=$this->Aproductmodel->get_no_of_rows($child_cat_id);
			$data['success']=1;
			$data['imageaccesspath']=IMAGEACCESSPATH;
		}else{
			$data['success']=0;
			$data['products']='NO records found...';
		}	
		echo json_encode($data);
	}
	
	public function brand_list()
	{
		$data['brand_list']=$this->Aproductmodel->get_all_brands();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function brand_wise_product_list()
	{
		$brand_id=$this->uri->segment(3,0);
		$count=$this->Aproductmodel->get_no_of_rows_brands($brand_id);
		$per_page=6;
		
		if($count>0)
		{
			$data['products']= $this->Aproductmodel->get_brand_products($per_page,$brand_id);
			$data['count']=$this->Aproductmodel->get_no_of_rows_brands($brand_id);
			$data['success']=1;
			$data['imageaccesspath']=IMAGEACCESSPATH;
		}else{
			$data['success']=0;
			$data['products']='NO records found...';
		}	
		echo json_encode($data);
	}
	
	public function pune_special_brand_list()
	{
		$data['pune_special_list']=$this->Aproductmodel->get_all_pune_special();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
}