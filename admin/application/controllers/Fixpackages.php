<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fixpackages extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		if(NULL == $this->session->userdata('id')){
            redirect('login');
        }
		$this->load->model('Fixpackagesmodel');
		$this->load->model('Ddlmodel');
	}	
	 
	public function view_fixpackages()
	{
		$data['packages']=$this->Fixpackagesmodel->get_fixpackages_list();
		$this->load->view('layouts/header');
		$this->load->view('pages/fixpackages', $data);
		$this->load->view('layouts/footer');
	}
	
	public function fixpackage_products()
	{
		$data['package_list']=$this->Ddlmodel->get_fixpackages_list();
		$data['package_products']=$this->Fixpackagesmodel->get_fixpackages_product_list();
		$this->load->view('layouts/header');
		$this->load->view('pages/fixpackageproducts', $data);
		$this->load->view('layouts/footer');
	}
	
	public function insert_package(){
		if($this->input->post())
		{
			$this->Fixpackagesmodel->insert_package();
			
			$message = 'Package is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('fixpackages/view_fixpackages/');
		}else{
			$message = 'Product  insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('fixpackages/view_fixpackages/');
		}
	}
	
	public function insert_fixpackage_product(){
		if($this->input->post())
		{
			$this->Fixpackagesmodel->insert_fixpackage_product();
			
			$message = 'Package is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('fixpackages/fixpackage_products/');
		}else{
			$message = 'Package insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('fixpackages/fixpackage_products/');
		}
	}
	
	public function update_fixpackage(){
		if($this->input->post())
		{
			$this->Fixpackagesmodel->update_fixpackage();
			
			$message = 'Package updated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('fixpackages/view_fixpackages/');
		}else{
			$message = 'Package updation failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('fixpackages/view_fixpackages/');
		}
	}
	
	public function update_fixpackage_product(){
		if($this->input->post())
		{
			$this->Fixpackagesmodel->update_fixpackage_product();
			
			$message = 'Package product is updated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('fixpackages/fixpackage_products/');
		}else{
			$message = 'Package product updation failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('fixpackages/fixpackage_products/');
		}
	}
	
	public function delete_fixpackage_product()
	{
		$package_product_id=$this->uri->segment(3,0);
		if($this->Fixpackagesmodel->delete_fixpackage_product($package_product_id))
		{	
			$message = 'Package product is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('fixpackages/fixpackage_products');
		}else{
			$message = 'Package product is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('fixpackages/fixpackage_products');
		}			
	}
	
	public function delete_fixpackage()
	{
		$package_id=$this->uri->segment(3,0);
		if($this->Fixpackagesmodel->delete_fixpackage($package_id))
		{	
			$message = 'Package is deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));	
			
			redirect('fixpackages/view_fixpackages');
		}else{
			$message = 'Package product is not deleted sucessfully';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('fixpackages/view_fixpackages');
		}			
	}
	
}
