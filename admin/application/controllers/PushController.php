<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PushController extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();

		$this->load->model('Pushmodel');
		
    	}	
		
	 // this function will redirect to book service page
	 function index()
	 {
		$this->subscribe();
	 }

	 // this function to load service book page
	 function subscribe()
	 {
		 $this->load->view('pages/site_subscriber');
	 }

	public function register_device()
	{
		$status = $this->Pushmodel->insert_pushdata();	
		echo json_encode($status);
	}
	
	  
	    
    public function get_all_device_details()
    {
    	$data['device_details'] = $this->Pushmodel->get_all_device_details();
    	echo json_encode($data['device_details']);
    }
    
    // send notification to registered devices from admin panel
      public function send_notification()
	{
	    $message = $this->input->post('message');
	    $device_details = $this->Pushmodel->get_all_device_details();
	    $data['devices'] = array(); 
		foreach($device_details as $dids)
		{
		    $data['devices'] = $dids['device_id'];
		      $fields = array(
	             'registration_id' => $data['devices'],
	             'data' => $message
        	     );
        	   $result = $this->sendPushNotification($fields);
		}
	   echo json_encode($result);
       }
       
        private function sendPushNotification($fields) {
         
      
        //firebase server url to send the curl request
        $url = 'https://fcm.googleapis.com/fcm/send';
 
        //building headers for the request
        $headers = array(
            'Authorization: key=' . FIREBASE_API_KEY,
            'Content-Type: application/json'
        );
 
        //Initializing curl to open a connection
        $ch = curl_init();
 
        //Setting the curl url
        curl_setopt($ch, CURLOPT_URL, $url);
        
        //setting the method as post
        curl_setopt($ch, CURLOPT_POST, true);
 
        //adding headers 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        //disabling ssl support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        //adding the fields in json format 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        //finally executing the curl request 
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        //Now close the connection
        curl_close($ch);
 
        //and return the result 
        return $result;
    }
}