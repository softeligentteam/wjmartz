<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Email_manager {

	public function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->library('email');
		$this->initialize_email();
	}
	
	/************ INITIALIZE EMAIL CREDENTIALS **************/
	public function initialize_email(){
		$this->CI->email->initialize(array(
    			'protocol' => 'smtp',
    			'smtp_host' => 'webmail.wjmartz.com',
    			'smtp_user' => 'support@wjmartz.com',
    			'smtp_pass' => 'Support@123',
    			'smtp_port' => 587,
    			'smtp_timeout' => '5',
    			'mailtype'  => 'html',
    			'charset'   => 'utf-8',
    			'crlf' => "\r\n",
    			'newline' => "\r\n"
			));
	}
	
	public function send_order_delivery_email($email,$message)
	{
		$this->CI->email->from('support@wjmartz.com', 'Wjmartz');
		$this->CI->email->to($email);
		$this->CI->email->subject('Order Delivery Successfully');
		$this->CI->email->message($message);
		$this->CI->email->send();
			
	}
	
	public function send_order_cancel_email($email,$message)
	{
		$this->CI->email->from('support@wjmartz.com', 'Wjmartz');
		$this->CI->email->to($email);
		$this->CI->email->subject('Order Cancelled Successfully');
		$this->CI->email->message($message);
		$this->CI->email->send();
			
	}
	
	public function send_order_ready_email($email,$message)
	{
		$this->CI->email->from('support@wjmartz.com', 'Wjmartz');
		$this->CI->email->to($email);
		$this->CI->email->subject('Order Ready To Dispatch');
		$this->CI->email->message($message);
		$this->CI->email->send();
			
	}
}	