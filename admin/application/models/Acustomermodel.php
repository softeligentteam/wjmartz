<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acustomermodel extends CI_Model
{
	private function bind_customer_details()
	{
		$firstname=$this->input->post('firstname');
		$lastname=$this->input->post('lastname');
		$mobile=$this->input->post('mobile');
		$email=$this->input->post('email');
	
		$customer_data=array(
		'first_name'=>$firstname,
		'last_name'=>$lastname,
		'email'=>$email,
		'mobile'=>$mobile,
		'created_on'=>date('Y-m-d H:i:s')
		);
		return $customer_data;
	}
	
	private function bind_customer_address($temp_cust_id)
	{
		$address=$this->input->post('address');
		$pincode=$this->input->post('pincode_id');
		
		$customer_address_data=array(
		'cust_id'=>$temp_cust_id,
		'address'=>$address,
		'pincode'=>$pincode
		);
		
		return $customer_address_data;
	}
	
	
	public function insert_customer()
	{
		$this->db->trans_begin();
        $this->db->insert('customer', $this->bind_customer_details());
        $temp_cust_id = $this->db->insert_id();
		$this->db->insert('customer_address', $this->bind_customer_address($temp_cust_id));
		$address_id = $this->db->insert_id();
		$this->db->set('address_id', $address_id);
		$this->db->where('cust_id',$temp_cust_id);
		$this->db->update('customer');
		 if($this->db->trans_status()===FALSE){
            $this->db->trans_rollback();
            return FALSE;
        }
        else {
            $this->db->trans_commit();
            return TRUE;
        }
	}
	
	public function get_customer_data($mobile)
	{
		$this->db->select('cust_id,first_name,last_name');
		$this->db->where('mobile',$mobile);
		return $this->db->get('customer')->result();
	}
	
	public function get_cutomer_data_to_update($cust_id)
	{
		$this->db->select('first_name,last_name,email,mobile,address,pi.pincode,dob,user_photo');
		$this->db->join('customer_address ca','ca.address_id=c.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->where('c.cust_id',$cust_id);
		return $this->db->get('customer c')->result();
	}
	
	public function update_customer_data()
	{
		$cust_id=$this->input->post('cust_id');
		$firstname=$this->input->post('firstname');
		$lastname=$this->input->post('lastname');
		$mobile=$this->input->post('mobile');
		$email=$this->input->post('email');
		$dob=$this->input->post('dob');
		$photo=$this->upload_user_photo();
		$user_photo=str_replace(IMAGEBASEPATH
				, '', $photo);
		$address=$this->input->post('address');
		$pincode=$this->input->post('pincode_id');
		
		$customer_data=array(
		'first_name'=>$firstname,
		'last_name'=>$lastname,
		'email'=>$email,
		'mobile'=>$mobile,
		'dob'=>$dob,
		'user_photo'=>$user_photo,
		'modified_on'=>date('Y-m-d H:i:s')
		);
		
		$customer_address_data=array(
		'address'=>$address,
		'pincode'=>$pincode
		);
		
		$this->db->where('cust_id',$cust_id);
		$this->db->update('customer',$customer_data);
		$this->db->update('customer_address',$customer_address_data);
		return $this->db->affected_rows();
	}
	
	private function upload_user_photo()
	{
		/******* Uplaod Image ********/
			  $img = $this->input->post('user_photo');
			  $path = IMAGEBASEPATH . USERUPLOADPATH;
			  if(!is_dir($path)) //create the folder if it's not already exists
				{
				 mkdir($path,0755,TRUE);
			   $UPLOAD_DIR = $path ;
			   echo "make directory:".$UPLOAD_DIR;
				} 
			  //define('UPLOAD_DIR', 'images/customers/');
			  $UPLOAD_DIR = $path; 
			  $img = str_replace('data:image/png;base64,', '', $img);
			  $img = str_replace(' ', '+', $img);
			  $data = base64_decode($img);
			  $file = $UPLOAD_DIR . uniqid() . '.png';
			  $success = file_put_contents($file, $data);
			  return $success ? $file : NULL;
	}
	
	public function get_support_services_list()
	{
		$this->db->select('service_id,support_service,service_description')->order_by('service_id');
		$filtered_list['select_Support_service']='Select Support Service';
		$filtered_list['list']=$this->db->get('support_services')->result();
		return $filtered_list;
	}
	
	public function insert_support_chat()
	{
		$service_id=$this->input->post('service_id');
		$cust_id=$this->input->post('cust_id');
		$message=$this->input->post('message');
		
		$data=array(
		'service_id'=>$service_id,
		'cust_id'=>$cust_id,
		'message'=>$message
		);
		
		return $this->db->insert('support_chat',$data);
	}
	

}