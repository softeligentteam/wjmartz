<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fixpackagesmodel extends CI_Model
{
	public function get_fixpackages_list()
	{
		$this->db->select('package_id,package_name,package_price,package_img');
		$this->db->where_not_in('package_id',0);
		return $this->db->get('package')->result();
	}
	
	public function insert_package()
	{
		$package_name=$this->input->post('package_name');
		$package_price=$this->input->post('package_price');
		$package_img=$this->upload_package_photo();
		
		
		$data=array(
		'package_name'=>$package_name,
		'package_price'=>$package_price,
		'package_img'=>$package_img,
		'created_on'=>date('Y-m-d H:i:s'),
		'created_by'=>0,
		);
		
		return $this->db->insert('package',$data);
	}
	
	public function insert_fixpackage_product()
	{
		$package_product_name=$this->input->post('fixpackage_product_name');
		$package_product_img=$this->upload_package_photo();
		$package_product_price=$this->input->post('fixpackage_product_price');
		$fixpackage_product_quantity=$this->input->post('fixpackage_product_quantity');
		$package_id=$this->input->post('package_list');
		
		
		$data=array(
		'package_product'=>$package_product_name,
		'package_product_img'=>$package_product_img,
		'prod_price'=>$package_product_price,
		'prod_quantity'=>$fixpackage_product_quantity,
		'package_id'=>$package_id,
		'in_stock'=>1
		);
		
		return $this->db->insert('package_products',$data);
	}
	
	public function update_fixpackage()
	{
		$fixpackage_name2=$this->input->post('fixpackage_name2');
		$fixpackage_old_image=$this->input->post('fixpackage_old_img');
		$fixpackage_image2=$this->update_package_photo($fixpackage_old_image);
		$fixpackage_price2=$this->input->post('fixpackage_price2');
		$fixpackage_id2=$this->input->post('fixpackage_id2');
		$no_img = PCAKAGEIMAGEUPLOADPATH."/no_image.jpg";
		
		
		
		$data=array(
			'package_name'=>$fixpackage_name2,
			'package_img'=>$fixpackage_image2,
			'package_price'=>$fixpackage_price2
		);
		$this->db->where('package_id',$fixpackage_id2);
		return $this->db->update('package',$data);
	}
	
	public function update_fixpackage_product()
	{
		$id=$this->input->post('id');
		$url=$this->input->post('url');
		
		$name=$this->input->post('name');
		$package=$this->input->post('edited_package_list');
		$price=$this->input->post('price');
		$quantity=$this->input->post('quantity');
		$edited_fixpackage_product_img=$this->update_package_product_photo($url);
		
		$data=array(
			'package_product'=>$name,
			'package_id'=>$package,
			'prod_price'=>$price,
			'prod_quantity'=>$quantity,
			'package_product_img'=>$edited_fixpackage_product_img
		);
		$this->db->where('package_product_id',$id);
		return $this->db->update('package_products',$data);
	}
	
	function update_package_product_photo($url) {
    	$file_url = "";
		if ($_FILES["package_image"]["error"] == 0) {

			$file_element_name = 'package_image';

			$config['upload_path'] = PCAKAGEPRODUCTUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp'; 
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload($file_element_name)) {
				$file_url = PCAKAGEPRODUCTUPLOADPATH."/no_image.jpg"; 
			} else {
				$data = $this->upload->data();
				$file_url = PCAKAGEPRODUCTUPLOADPATH.$data['file_name']; 
			}

			@unlink($_FILES[$file_element_name]);

		} else {
			 
			$file_url = $url;
		}
		
		return $file_url;
    }
	
	
	
	function update_package_photo($fixpackage_old_image) {
    	$file_url = "";
		if ($_FILES["package_image"]["error"] == 0) {

			$file_element_name = 'package_image';

			$config['upload_path'] = PCAKAGEIMAGEUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp'; 
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload($file_element_name)) {
				$file_url = PCAKAGEIMAGEUPLOADPATH."/no_image.jpg"; 
			} else {
				$data = $this->upload->data();
				$file_url =PCAKAGEIMAGEUPLOADPATH.$data['file_name']; 
			}

			@unlink($_FILES[$file_element_name]);

		} else {
			 
			$file_url = $fixpackage_old_image;
		}
		
		return $file_url;
    }
	
	
	function upload_package_photo() {
    	$file_url = "";
		if ($_FILES["package_image"]["error"] == 0) {

			$file_element_name = 'package_image';

			$config['upload_path'] = PCAKAGEIMAGEUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
			//$config['max_size'] = 1024 * 8;
			// $config['max_width'] = 512;
			// $config['max_height'] = 512;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload($file_element_name)) {
				$file_url = PCAKAGEIMAGEUPLOADPATH."/no_image.jpg";
			   // $msg = $this->upload->display_errors('', '');
				// echo "<script>console.log($msg);</script>";
			} else {
				$data = $this->upload->data();
				$file_url = PCAKAGEIMAGEUPLOADPATH.$data['file_name'];
				// echo "<script>console.log('File uploaded successfully.');</script>";
			}

			@unlink($_FILES[$file_element_name]);

		} else {
			 
			$file_url = PCAKAGEIMAGEUPLOADPATH."/no_image.jpg";
		}
		
		return $file_url;
    }
	
	
	
	
	
	
	function upload_package_product_photo() {
    	$file_url = "";
		if ($_FILES["package_image"]["error"] == 0) {

			$file_element_name = 'package_image';

			$config['upload_path'] = PCAKAGEPRODUCTUPLOADPATH;
			$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
			//$config['max_size'] = 1024 * 8;
			// $config['max_width'] = 512;
			// $config['max_height'] = 512;
			$config['encrypt_name'] = TRUE;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload($file_element_name)) {
				$file_url = PCAKAGEPRODUCTUPLOADPATH."/no_image.jpg";
			   // $msg = $this->upload->display_errors('', '');
				// echo "<script>console.log($msg);</script>";
			} else {
				$data = $this->upload->data();
				$file_url = PCAKAGEPRODUCTUPLOADPATH.$data['file_name'];
				// echo "<script>console.log('File uploaded successfully.');</script>";
			}

			@unlink($_FILES[$file_element_name]);

		} else {
			 
			$file_url = PCAKAGEPRODUCTUPLOADPATH."/no_image.jpg";
		}
		
		return $file_url;
    }
	
	public function delete_fixpackage_product($package_product_id)
	{
		$this->db->where('package_product_id',$package_product_id);
		return $this->db->delete('package_products');
    }
	
	public function delete_fixpackage($package_id)
	{
		$this->db->where('package_id',$package_id);
		return $this->db->delete('package');
    }
	
	public function get_product_list($child_cat_id, $per_page)
	{	
		$this->db->select('prod_id,prod_name,prod_price,prod_img_url');
		$this->db->where_not_in('prod_id',0);
		$this->db->where('child_cat_id',$child_cat_id);
		return $this->db->get('products',$per_page, $this->uri->segment(3))->result();
	}
	
	public function get_fixpackages_product_list(){
		$this->db->select('package_product_id,package_product,package_product_img,prod_price,prod_quantity,package_name,p.package_id');
		$this->db->where_not_in('package_product_id',0);
		$this->db->join('package p','p.package_id = pp.package_id');
		$this->db->where_not_in('package_product_id',0);
		
		return $this->db->get('package_products pp')->result();
	}

	public function get_no_of_rows($child_cat_id)
	{
		$this->db->where('child_cat_id',$child_cat_id);
		return $this->db->get('products')->num_rows();
	}
	
	public function del_product($prod_id)
	{
		$this->db->where('prod_id',$prod_id);
		return $this->db->delete('products');
	}
}