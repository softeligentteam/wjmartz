<?php
class Excel_export_model extends CI_Model
{
	public function get_all_customer_data()
	{
		$this->db->select('c.cust_id,first_name,last_name,mobile,email,dob,address,pi.pincode,ca.area as area_name,ci.city,s.state')->order_by('c.cust_id','DESC');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('customer_address ca','ca.cust_id=c.cust_id');
		$this->db->where('ca.address_type',1);
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		return $this->db->get('customer c')->result();
	}
	public function get_woman_ware_product_data()
	{
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url')->order_by('prod_id','DESC');
		$this->db->where_not_in('fk_pcat_id',1);
		$this->db->where('status',1);
		return $this->db->get('products')->result();
	}
	public function get_woman_ware_product_inactive_data()
	{
		
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url')->order_by('prod_id','DESC');
		$this->db->where_not_in('fk_pcat_id',1);
		$this->db->where('status',0);
		return $this->db->get('products')->result();
	}
	public function get_inprocess_order_data()
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name,city,state')->order_by('to.order_id','DESC');
		$this->db->from('city ci');
		$this->db->from('state s');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->where('ord_status_id',1);
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		return $this->db->get('order to')->result();
	}
	public function get_ready_to_dispatch_order_data()
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,to.area as area_name')->order_by('to.order_id','DESC');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->where('ord_status_id',2);
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		return $this->db->get('order to')->result();
	}
	public function get_delivered_order_data()
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode');
		$this->db->join('customer c','c.cust_id=to.cust_id')->order_by('to.order_id','DESC');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->where('ord_status_id',3);
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		return $this->db->get('order to')->result();
	}
	public function get_cancelled_order_data()
	{
		$this->db->select('to.order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode,modified_by,modified_by_cust')->order_by('to.order_id','DESC');
		$this->db->join('customer c','c.cust_id=to.cust_id');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->where('ord_status_id',4);
		$this->db->join('payment_details tp','tp.order_id=to.order_id');
		return $this->db->get('order to')->result();
	}
	public function get_failed_order_data()
	{
		$this->db->select('to.ref_order_id as order_id,to.first_name,to.last_name,mobile,email,order_date,amount,to.address,to.pincode');
		$this->db->join('customer c','c.cust_id=to.cust_id')->order_by('to.ref_order_id','DESC');
		$this->db->join('customer_address ca','ca.address_id=to.address_id');
		$this->db->join('pincode pi','pi.pincode_id=ca.pincode');
		$this->db->where('ord_status_id',5);
		$this->db->join('temp_payment_details tp','tp.ref_order_id=to.ref_order_id');
		return $this->db->get('temp_order to')->result();
	}
	public function get_payment_sucessful_transaction_data()
	{
		$this->db->select('o.order_id,transaction_id,payment_datetime,o.first_name,o.last_name,payment_status');
		$this->db->join('customer c','c.cust_id=o.cust_id')->order_by('o.order_id','DESC');
		$this->db->join('payment_details p','p.order_id=o.order_id');
		return $this->db->get('order o')->result();
	}
	public function get_payment_failed_transaction_data()
	{
		$this->db->select('o.ref_order_id,transaction_id,payment_datetime,o.first_name,o.last_name,payment_status,trans_reason');
		$this->db->join('customer c','c.cust_id=o.cust_id')->order_by('o.ref_order_id','DESC');
		$this->db->join('temp_payment_details p','p.ref_order_id=o.ref_order_id');
		return $this->db->get('temp_order o')->result();
	}
	public function get_child_cat_list_inactive_data()
	{	
		$this->db->select('child_cat_id,child_cat_name,child_cat_img_url,parent_cat_id')->order_by('child_cat_id','DESC');
		$this->db->where('status',0);
		return $this->db->get('child_category')->result();
	}
	public function get_woman_footware_product_data()
	{
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url')->order_by('prod_id','DESC');
		$this->db->where('fk_pcat_id',1);
		$this->db->where('status',1);
		return $this->db->get('products')->result();
	}
	public function get_woman_footware_product_inactive_data()
	{
		
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url')->order_by('prod_id','DESC');
		$this->db->where('fk_pcat_id',1);
		$this->db->where('status',0);
		return $this->db->get('products')->result();
	}
}

