<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Slidermodel extends CI_Model {
	
	/* //Update User slider Image
	public function update_user_slider()
	{
		$data = array(
			'image_path'=> $this->input->post('slider_img'),
			'modified_by'=> $this->session->userdata('id'),
			'modified_on'=>date('Y-m-d H:i:s')
		);
		$this->db->where('slider_id', $this->input->post('sid'));
		return $this->db->update('slider', $data);
	}

	//Get User Slider Image path to delete on update new one.
	public function get_user_slider_image_path()
	{
		$this->db->select('image_path')->where('slider_id', $this->input->post('sid'));
		return $this->db->get('slider')->result();
	}
	
	public function get_slider()
	{
		$this->db->select('slider_id, image_path');
		return $this->db->get('slider')->result();	
	}  */
	
	public function get_slider_image_path()
	{
		$this->db->select('slider_image')->where('slider_img_id', $this->input->post('slider_img_id'));
		return $this->db->get('sliders')->result();
	}
	
	/****** Slider Image add and update for App Start ******/
	public function get_slider_for_andriod()
	{
		$this->db->select('slider_img_id, slider_image,status');
		$this->db->where('slider_type','Android');
		//$this->db->where('status',1);
		return $this->db->get('sliders')->result();	
	}
	public function update_app_slider()
	{
		$data = array(
			'slider_image'=> $this->input->post('slider_img'),
			'muser_id'=> $this->session->userdata('id'),
			'modified_on'=>date('Y-m-d H:i:s')
		);
		$this->db->where('slider_img_id', $this->input->post('slider_img_id'));
		$this->db->update('sliders', $data);
		//return $this->db->last_query();
		return $this->db->affected_rows();
	}
	/****** Slider Image add and update for App End ******/
	
	/****** Slider Image add and update for Website Start ******/
	public function get_slider_for_website()
	{
		$this->db->select('slider_img_id, slider_image,status');
		$this->db->where('slider_type','Web');
		//$this->db->where('status',1);
		return $this->db->get('sliders')->result();	
	}
	public function update_website_slider()
	{
		$data = array(
			'slider_image'=> $this->input->post('slider_img'),
			'muser_id'=> $this->session->userdata('id'),
			'modified_on'=>date('Y-m-d H:i:s')
		);
		$this->db->where('slider_img_id', $this->input->post('slider_img_id'));
		$this->db->update('sliders', $data);
		//return $this->db->last_query();
		return $this->db->affected_rows();
	}
	/****** Slider Image add and update for Website End ******/
	
	/****** inactive slider ******/
	public function inactiveSlider($slider_img_id)
	{
        $cmd = "UPDATE sliders SET status = 0 WHERE slider_img_id = $slider_img_id;";
		return $this->db->query($cmd);
	}
	/****** active slider ******/
	public function activeSlider($slider_img_id)
	{
        $cmd = "UPDATE sliders SET status = 1 WHERE slider_img_id = $slider_img_id;";
		return $this->db->query($cmd);
	}
}