<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productmodel extends CI_Model
{
	public function fetch_product_list_women_ware($limit, $start)
	{ 
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url');
		$this->db->where_not_in('fk_pcat_id',1);
		$this->db->where('status',1);
		$this->db->limit($limit, $start);
		return $this->db->get('products')->result();
	}
	
	/************* Get total count of products *****************/
	public function get_record_count_women_ware()
	{
		$this->db->where_not_in('fk_pcat_id',1);
		$this->db->where('status',1);
		return $this->db->count_all('products');
	}
	
	public function get_record_count_women_ware_inactive()
	{
		$this->db->where_not_in('fk_pcat_id',1);
		$this->db->where('status',0);
		return $this->db->count_all('products');
	}
	
	public function fetch_product_list_women_ware_inactive($limit, $start)
	{ 
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url');
		$this->db->where_not_in('fk_pcat_id',1);
		$this->db->where('status',0);
		$this->db->limit($limit, $start);
		return $this->db->get('products')->result();
	}
	public function fetch_product_list_women_footware_inactive($limit, $start)
	{ 
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url');
		$this->db->where('fk_pcat_id',1);
		$this->db->where('status',0);
		$this->db->limit($limit, $start);
		return $this->db->get('products')->result();
	}
	public function fetch_product_list_women_footware($per_page)
	{ 
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url');
		$this->db->where('fk_pcat_id',1);
		$this->db->where('status',1);
		//$this->db->limit($limit, $start);
		return $this->db->get('products',$per_page,$this->uri->segment(3))->result();
	}
	
	/************* Get total count of products *****************/
	public function get_record_count_women_footware()
	{
		$this->db->where('fk_pcat_id',1);	
		$this->db->where('status',1);
		return $this->db->get('products')->num_rows();
	}
	
	public function get_record_count_women_footware_inactive()
	{
		$this->db->where('fk_pcat_id',1);
		$this->db->where('status',0);
		return $this->db->count_all('products');
	}
	
	public function del_product($prod_id)
	{
		$data=array('status'=>0);
		
		$this->db->where('prod_id',$prod_id);
		$this->db->update('products',$data);
		return $this->db->affected_rows();
	}
	
	
	
	
	public function add_women_ware_products()
	{
		$image_url_list[0] = $_POST['prod_img2'];
		for($index = 3; $index <= 6; $index++)
		{
			if(!empty($_FILES['prod_img'.$index]['name']))
			{
				$image_url_list[$index-2] = $_POST['prod_img'.$index];
			}
		 }
		
		/**************** Calculate Discount ********************/
		$prod_mrp = $this->input->post('pmrp');
		$prod_price = $this->input->post('pprice');
		$size_data = $this->input->post('psize[]');
		$quantity = $this->input->post('sizeQty[]');
		$qty = array();
		foreach ($quantity as $row) {
			if ($row != null || $row != "")
			$qty[] = $row;
		}
		$qty_data=$qty;
		
		$discount = (($prod_mrp - $prod_price)*100)/$prod_mrp;
		$applicable_sizes = implode(",", $this->input->post('psize[]'));
		$image_urls = implode(",", $image_url_list);
		
		$gst = $this->input->post('gst');
		$cal_sgst_cgst = ($gst/2);
		if($cal_sgst_cgst > 0)
		{
			$final_gst = $cal_sgst_cgst;
		}
		else
		{
			$final_gst = 0;
		}
		if (isset($_POST['chkbox'])) {
                 $rf_chk=1;
               } else {
                 $rf_chk=0;
             }
		$product = array(
			'fk_pbrand_id' => $this->input->post('brand_id'),
			'fk_pcat_id' => $this->input->post('ppcat_id'),
			'fk_pchild_id' => $this->input->post('pccat_id'),
			'prod_name' => $this->input->post('pname'),
			'fk_pcolor_id' => $this->input->post('pcolor'),
			'fabric_type' => $this->input->post('ftype_id'),
			'fk_psize_ids' => $applicable_sizes,
			'prod_mrp' => $prod_mrp,
			'prod_price' => $prod_price,
			'pwholesale_price' => $this->input->post('pwprice'),
			'prod_fit_details' => $this->input->post('pfitdetails'),
			'prod_fabric_details' => $this->input->post('pfabricdetails'),
			'prod_description' => $this->input->post('pdesc'),
			'delivery_n_return'=> $this->input->post('pdel'),
			'prod_image_url' => $this->input->post('prod_img1'),
			'prod_image_urls' => $image_urls,
			'discount' => $discount,
			'sgst' => $final_gst,
			'cgst' => $final_gst,
			'refund_status'=>$rf_chk,
			'created_by' => $this->session->userdata('id'),
			'status'=>1
		);
		
		$this->db->trans_begin();
		$status['product'] = $this->db->insert('products', $product);
		$prod_id=$this->db->insert_id();
		$value = array();
		for($i=0;$i<count($size_data);$i++)
		{
			$value[] = array(
							'product_id'=>$prod_id,
							'size_id'=>$size_data[$i],
							'quantity'=>$qty_data[$i],
						);
		}
		$status['quantity'] = $this->db->insert_batch('product_slot_quantity',$value);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $status;
	}
	
	public function add_women_footware_products()
	{
		$image_url_list[0] = $_POST['prod_img2'];
		for($index = 3; $index <= 6; $index++)
		{
			if(!empty($_FILES['prod_img'.$index]['name']))
			{
				$image_url_list[$index-2] = $_POST['prod_img'.$index];
			}
		 }
		
		
		/**************** Calculate Discount ********************/
		$prod_mrp = $this->input->post('pmrp');
		$prod_price = $this->input->post('pprice');
		
		$discount = (($prod_mrp - $prod_price)*100)/$prod_mrp;
		
		$applicable_sizes = implode(",", $this->input->post('psize[]'));
		$image_urls = implode(",", $image_url_list);
		
		$size_data = $this->input->post('psize[]');
		$quantity = $this->input->post('sizeQty[]');
		$qty = array();
		foreach ($quantity as $row) {
			if ($row != null || $row != "")
			$qty[] = $row;
		}
		$qty_data=$qty;
		
		$gst = $this->input->post('gst');
		$cal_sgst_cgst = ($gst/2);
		if($cal_sgst_cgst > 0)
		{
			$final_gst = $cal_sgst_cgst;
		}
		else
		{
			$final_gst = 0;
		}
		if (isset($_POST['chkbox'])) {
                 $rf_chk=1;
               } else {
                 $rf_chk=0;
             }
		$product = array(
			'fk_pbrand_id' => $this->input->post('brand_id'),
			'fk_pcat_id' =>1,
			'fk_pchild_id' => $this->input->post('pccat_id'),
			'prod_name' => $this->input->post('pname'),
			'fk_pcolor_id' => $this->input->post('pcolor'),
			'fabric_type' => $this->input->post('ftype_id'),
			'fk_psize_ids' => $applicable_sizes,
			'prod_mrp' => $prod_mrp,
			'prod_price' => $prod_price,
			'pwholesale_price' => $this->input->post('pwprice'),
			'prod_fit_details' => $this->input->post('pfitdetails'),
			'fk_ptype_id' =>  $this->input->post('ptype'),
			'prod_fabric_details' => null,
			'prod_description' => $this->input->post('pdesc'),
			'delivery_n_return'=> $this->input->post('pdel'),
			'prod_image_url' => $this->input->post('prod_img1'),
			'prod_image_urls' => $image_urls,
			'discount' => $discount,
			'sgst' => $final_gst,
			'cgst' => $final_gst,
			'refund_status'=>$rf_chk,
			'created_by' => $this->session->userdata('id'),
			'status'=>1
		);
		
		$this->db->trans_begin();
		$status['product'] = $this->db->insert('products', $product);
		$prod_id=$this->db->insert_id();
		$value = array();
		for($i=0;$i<count($size_data);$i++)
		{
			$value[] = array(
							'product_id'=>$prod_id,
							'size_id'=>$size_data[$i],
							'quantity'=>$this->input->post('sizeQty'.$size_data[$i]),
						);
		}
		$status['quantity'] = $this->db->insert_batch('product_slot_quantity',$value);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $status;
	}
	
	
	
	/************ Update Product ***************/
	public function update_product_women_ware()
	{
		/***************** Calculate Discount ********************/
		$prod_mrp = $this->input->post('pmrp');
		$prod_price = $this->input->post('pprice');
		
		$discount = (($prod_mrp - $prod_price)*100)/$prod_mrp;
		$gst = $this->input->post('gst');
		$cal_sgst_cgst = ($gst/2);
		if($cal_sgst_cgst > 0)
		{
			$final_gst = $cal_sgst_cgst;
		}
		else
		{
			$final_gst = 0;
		}
		
		$data = array(
			'prod_mrp' => $prod_mrp,
			'prod_price' => $prod_price,
			'pwholesale_price' => $this->input->post('pwprice'),
			'prod_fit_details' => $this->input->post('pfitdetails'),
			'prod_fabric_details' => $this->input->post('pfabricdetails'),
			'prod_description' => $this->input->post('pdesc'),
			'delivery_n_return'=> $this->input->post('pdel'),
			'in_stock' => $this->input->post('in_stock'),
			'discount' => $discount,
			'sgst' => $final_gst,
			'cgst' => $final_gst,
		);
		$slot_id = $this->input->post('pslotid[]');
		$qty_data = $this->input->post('sizeQty[]');
		
		//$data1 = array('quantity'=>$this->input->post('pquantity'));
		$this->db->trans_begin();
		$status['product'] = $this->db->where('prod_id', $this->input->post('pid'))->update('products', $data);
		var_dump(count($slot_id));
		$value = array();
		for($i=0;$i<count($slot_id);$i++)
		{
			$value[] = array(
							'slot_id'=>$slot_id[$i],
							'quantity'=>$qty_data[$i],
						);
		}
		//print_r($value);
		if(count($value)>0)
		{
			$status['quantity'] = $this->db->where('product_id', $this->input->post('pid'))->update_batch('product_slot_quantity', $value,'slot_id');
		}
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $status;
	}
	
	/***************** Update Product Images ******************/
	public function update_product_images()
	{
		$image_url_list[0] = $_POST['prod_img2'];
		for($index = 3; $index <= 6; $index++)
		{
			if(!empty($_FILES['prod_img'.$index]['name']))
			{
				$image_url_list[$index-2] = $_POST['prod_img'.$index];
			}
		}
		
		$image_urls = implode(",", $image_url_list);//array('image_url_list'=>json_encode(implode(",", $image_url_list)));
		
		$product = array(
			'prod_image_url' => $this->input->post('prod_img1'),
			'prod_image_urls' => $image_urls,
			'modified_by' => $this->session->userdata('user_id')
		);
		
		$this->db->trans_start();
		$pimage_list = $this->db->select('prod_image_url, prod_image_urls')->where('prod_id', $this->input->post('pid'))->get('products')->row();
		$status = $this->db->where('prod_id', $this->input->post('pid'))->update('products', $product);
		$this->db->trans_complete();
		if($status==TRUE)
			return $pimage_list;
		else
			NULL;
	}
	
	public function get_product2update($pid)
	{
		$this->db->select('prod_id as pid, prod_name as pname, prod_mrp as pmrp, prod_price as pprice, pwholesale_price as pwprice, b.brand as bname, pcc.child_cat_name as pccat, pc.color_name as pcolor, fk_psize_ids as psizes, prod_fit_details as pfitdetails, prod_fabric_details as pfabricdetails, prod_description as pdesc, in_stock,ps.quantity,sgst,cgst,delivery_n_return');
		$this->db->join('brands b', 'p.fk_pbrand_id=b.brand_id');
		$this->db->join('child_category pcc', 'p.fk_pchild_id=pcc.child_cat_id');
		$this->db->join('product_colors pc', 'p.fk_pcolor_id=pc.color_id');
		$this->db->join('product_slot_quantity ps', 'ps.product_id=p.prod_id');
		//$this->db->join('product_types pt', 'p.fk_ptype_id=pt.ptype_id');
		$prod_data = $this->db->where('prod_id='.$pid)->get('products p')->row();
		if($prod_data!==NULL)
			$prod_data->psizes = explode(',',$prod_data->psizes);//$product_sizes;//implode(", ", $sizes);
		return $prod_data;
	}
	
	public function update_product_women_footware()
	{
		/***************** Calculate Discount ********************/
		$prod_mrp = $this->input->post('pmrp');
		$prod_price = $this->input->post('pprice');
		
		$discount = (($prod_mrp - $prod_price)*100)/$prod_mrp;
		$gst = $this->input->post('gst');
		$cal_sgst_cgst = ($gst/2);
		if($cal_sgst_cgst > 0)
		{
			$final_gst = $cal_sgst_cgst;
		}
		else
		{
			$final_gst = 0;
		}
		$data = array(
			'prod_mrp' => $prod_mrp,
			'prod_price' => $prod_price,
			'pwholesale_price' => $this->input->post('pwprice'),
			'prod_fabric_details' => $this->input->post('pfabricdetails'),
			'prod_fit_details' => $this->input->post('pfitdetails'),
			'fk_ptype_id' =>  $this->input->post('ptype'),
			'prod_description' => $this->input->post('pdesc'),
			'delivery_n_return'=> $this->input->post('pdel'),
			'in_stock' => $this->input->post('in_stock'),
			'discount' => $discount,
			'sgst' => $final_gst,
			'cgst' => $final_gst,
		);
		$slot_id = $this->input->post('pslotid[]');
		$qty_data = $this->input->post('sizeQty[]');
		$this->db->trans_begin();
		$status['product'] = $this->db->where('prod_id', $this->input->post('pid'))->update('products', $data);
		$value = array();
		for($i=0;$i<count($slot_id);$i++)
		{
			$value[] = array(
							'slot_id'=>$slot_id[$i],
							'quantity'=>$qty_data[$i],
						);
		}
		if(count($value)>0)
		{
			$status['quantity'] = $this->db->where('product_id', $this->input->post('pid'))->update_batch('product_slot_quantity', $value,'slot_id');
		}
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $status;
	}
	

	
	public function get_product2update_footware($pid)
	{
		$this->db->select('prod_id as pid, prod_name as pname, prod_mrp as pmrp, prod_price as pprice, pwholesale_price as pwprice, b.brand as bname, pcc.child_cat_name as pccat, pc.color_name as pcolor, fk_psize_ids as psizes,prod_description as pdesc, in_stock,ps.quantity,prod_fit_details as pfitdetails,sgst,cgst,ps.quantity,delivery_n_return');
		$this->db->join('brands b', 'p.fk_pbrand_id=b.brand_id');
		$this->db->join('child_category pcc', 'p.fk_pchild_id=pcc.child_cat_id');
		$this->db->join('product_colors pc', 'p.fk_pcolor_id=pc.color_id');
		$this->db->join('product_slot_quantity ps', 'ps.product_id=p.prod_id');
		//$this->db->join('product_types pt', 'p.fk_ptype_id=pt.ptype_id');
		$prod_data = $this->db->where('prod_id='.$pid)->get('products p')->row();
		if($prod_data!==NULL)
			$prod_data->psizes = explode(',',$prod_data->psizes);//$product_sizes;//implode(", ", $sizes);
		return $prod_data;
	}
	
	public function activate_products($id)
	{
		$data=array('status'=>1);
		$this->db->where('prod_id',$id);
		return $this->db->update('products', $data);
	}
	public function get_psize_list()
	{
		$this->db->select('size_id, size')->order_by('size');
		return $this->db->get('product_sizes')->result();
	}
	 //writen by Pooja Jadhav
	public function get_psize_qty_list($product_id)
	{	
		$this->db->select('slot_id,psq.size_id,psq.product_id,ps.size,quantity')->order_by('ps.size');
		$this->db->join('product_sizes ps', 'ps.size_id=psq.size_id');
		$this->db->join('products p', 'p.prod_id=psq.product_id');
		$this->db->where('psq.product_id',$product_id);
		return $this->db->get('product_slot_quantity psq')->result();
	}
	//writen by Pooja Jadhav
	public function get_sizeid_list($product_id)
	{
		$this->db->select('size_id');
		$this->db->where('product_id',$product_id);
		return $this->db->get('product_slot_quantity')->result_array('size_id');
	}
	 //writen by Pooja Jadhav	
	public function get_psize_not_in_qty_slot($size_id)
	{
			$this->db->select('ps.size_id, size')->order_by('size');
			$this->db->where('parent_cat_id',2);
			$this->db->where_not_in('ps.size_id',$size_id);
			return $this->db->get('product_sizes ps')->result();
	}
	
	//Pooja Jadhav
	public function add_more_size_when_update($product_id)
	{
		$applicable_sizes = implode(",", $this->input->post('psize[]'));
		
		$this->db->select('fk_psize_ids');
		$this->db->where('prod_id',$product_id);
		$priveous_str_size= $this->db->get('products')->row('fk_psize_ids');
		$sizeid = $priveous_str_size . ',' . $applicable_sizes;
		$product = array(
			'fk_psize_ids' => $sizeid,
		);
		$size_data = $this->input->post('psize[]');
		$quantity = $this->input->post('sizeQtyup[]');
		$qty = array();
		foreach ($quantity as $row) {
			if ($row != null || $row != "")
			$qty[] = $row;
		}
		$qty_data=$qty;
		$this->db->trans_begin();
		$status['product'] = $this->db->where('prod_id', $product_id)->update('products', $product);
		$value = array();
		for($i=0;$i<count($size_data);$i++)
		{
			$value[] = array(
							'product_id'=>$product_id,
							'size_id'=>$size_data[$i],
							'quantity'=>$qty_data[$i],
						);
		}
		$status['quantity'] = $this->db->insert_batch('product_slot_quantity',$value);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $status;
	}
	
	
	 //writen by Pooja Jadhav
	public function get_psize_qty_list_footware($product_id)
	{	
		$this->db->select('slot_id,psq.size_id,psq.product_id,ps.size,quantity')->order_by('ps.size');
		$this->db->join('product_sizes ps', 'ps.size_id=psq.size_id');
		$this->db->join('products p', 'p.prod_id=psq.product_id');
		$this->db->where('psq.product_id',$product_id);
		return $this->db->get('product_slot_quantity psq')->result();
	}
	//writen by Pooja Jadhav
	public function get_sizeid_list_footware($product_id)
	{
		$this->db->select('size_id');
		$this->db->where('product_id',$product_id);
		return $this->db->get('product_slot_quantity')->result_array('size_id');
	}
	 //writen by Pooja Jadhav	
	public function get_psize_not_in_qty_slot_footware($size_id)
	{
			$this->db->select('ps.size_id, size')->order_by('size');
			$this->db->where('parent_cat_id',1);
			$this->db->where_not_in('ps.size_id',$size_id);
			return $this->db->get('product_sizes ps')->result();
	}
	//Pooja Jadhav
	public function add_more_size_when_update_footware($product_id)
	{
		$applicable_sizes = implode(",", $this->input->post('psize[]'));
		
		$this->db->select('fk_psize_ids');
		$this->db->where('prod_id',$product_id);
		$priveous_str_size= $this->db->get('products')->row('fk_psize_ids');
		$sizeid = $priveous_str_size . ',' . $applicable_sizes;
		$product = array(
			'fk_psize_ids' => $sizeid,
		);
		$size_data = $this->input->post('psize[]');
		$quantity = $this->input->post('sizeQtyup[]');
		$qty = array();
		foreach ($quantity as $row) {
			if ($row != null || $row != "")
			$qty[] = $row;
		}
		$qty_data=$qty;
		$this->db->trans_begin();
		$status['product'] = $this->db->where('prod_id', $product_id)->update('products', $product);
		$value = array();
		for($i=0;$i<count($size_data);$i++)
		{
			$value[] = array(
							'product_id'=>$product_id,
							'size_id'=>$size_data[$i],
							'quantity'=>$qty_data[$i],
						);
		}
		$status['quantity'] = $this->db->insert_batch('product_slot_quantity',$value);
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
		}
		else
		{
			$this->db->trans_commit();
		}
		return $status;
	}
	
	
	
	
	//Pooja Jadhav
	public function add_Today_deals()
	{
		$product_id=$this->input->post('product_list');
		//$deal_img=$this->input->post('deal_img');
		$this->update_deals_status_when_add($product_id);
		$deal_price=$this->input->post('deal_price');
		$this->db->select('prod_price');
		$this->db->where('p.prod_id',$product_id);
		$prod_price = $this->db->get('products p')->row('prod_price');
		if($deal_price < $prod_price)
		{
			$discount = (($prod_price - $deal_price)*100)/$prod_price;
		}
		else
		{
			$discount=0;
		}
		$data=array(
		'deal_product_id'=>$product_id,
		'deal_price'=>$deal_price,
		'discount'=>$discount,
		'deal_image_url'=>$this->input->post('deal_img')
		);
		return $this->db->insert('deals_of_day_product',$data);
	}
	//Pooja Jadhav
	public function update_deals_status_when_add($product_id)
	{
		$this->db->set('deal_status',1); 
		$this->db->where('prod_id', $product_id);
		return $this->db->update('products');
	}
	//Pooja Jadhav
	
	//Pooja Jadhav
	public function get_deals_of_the_day()
	{
		$this->db->select('dodp.deal_product_id,dodp.deal_id,dodp.deal_image_url,p.prod_name,dodp.deal_price');
		$this->db->join('products p', 'p.prod_id=dodp.deal_product_id');
		return $this->db->get('deals_of_day_product dodp')->result();
	}
	//Pooja Jadhav
	public function delete_deals_of_the_day($deal_id)
	{
		$this->db->where('deal_id', $deal_id);
      	return $this->db->delete('deals_of_day_product'); 
	}
	//Pooja Jadhav
	public function update_deals_status($prod_id)
	{
		$this->db->set('p.deal_status',0); 
		$this->db->where('p.prod_id', $prod_id);
		return $this->db->update('products p');
	}
	//Pooja Jadhav
	public function get_ware_product_search_details($product_name)
	{
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url');
		$this->db->from('products');
		$this->db->where_not_in('fk_pcat_id',1);
		$this->db->where('status',1);
		$this->db->like('prod_name',$product_name);
		$search = $this->db->get();
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//Pooja Jadhav
	public function get_ware_product_inactive_search_details($product_name)
	{
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url');
		$this->db->from('products');
		$this->db->where_not_in('fk_pcat_id',1);
		$this->db->where('status',0);
		$this->db->like('prod_name',$product_name);
		$search = $this->db->get();
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//Pooja Jadhav
	public function get_footware_product_search_details($product_name)
	{
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url');
		$this->db->from('products');
		$this->db->where('fk_pcat_id',1);
		$this->db->where('status',1);
		$this->db->like('prod_name',$product_name);
		$search = $this->db->get();
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//Pooja Jadhav
	public function get_footware_product_inactive_search_details($product_name)
	{
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url');
		$this->db->from('products');
		$this->db->where('fk_pcat_id',1);
		$this->db->where('status',0);
		$this->db->like('prod_name',$product_name);
		$search = $this->db->get();
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	
	//Pooja Jadhav
	public function fetch_product_list_women_ware_sizechart($per_page)
	{ 
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url,size_chart_image_url')->order_by('prod_id','DESC');
		$this->db->where_not_in('fk_pcat_id',1);
		$this->db->where('status',1);
		//$this->db->limit($limit, $start);
		return $this->db->get('products',$per_page,$this->uri->segment(3))->result();
	}
	//Pooja Jadhav
	public function fetch_product_list_women_footware_sizechart($per_page)
	{ 
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url,size_chart_image_url')->order_by('prod_id','DESC');
		$this->db->where('fk_pcat_id',1);
		$this->db->where('status',1);
		//$this->db->limit($limit, $start);
		return $this->db->get('products')->result();
	}
	//Pooja Jadhav
	public function update_size_chart()
	{
		$prod_id=$this->input->post('prod_id');
		$data=array(
		'size_chart_image_url'=>$this->input->post('size_chart_img'),
		);
		$this->db->where('prod_id',$prod_id);
		return $this->db->update('products',$data);
	}
	//Pooja Jadhav
	public function get_women_ware_sizechart_search_details($product_name)
	{
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url');
		$this->db->from('products');
		$this->db->where_not_in('fk_pcat_id',1);
		$this->db->where('status',1);
		$this->db->like('prod_name',$product_name);
		$search = $this->db->get();
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//Pooja Jadhav
	public function get_women_footware_sizechart_search_details($product_name)
	{
		$this->db->select('prod_id, prod_name,prod_price, pwholesale_price, prod_image_url as image_url');
		$this->db->from('products');
		$this->db->where('fk_pcat_id',1);
		$this->db->where('status',1);
		$this->db->like('prod_name',$product_name);
		$search = $this->db->get();
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//KT[18-04-18]
	public function get_size_chart_image_path()
	{
		$this->db->select('size_chart_image_url')->where('prod_id', $this->input->post('prod_id'));
		return $this->db->get('products')->result();
	}
	
}