<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paymentmodel extends CI_Model
{
	public function get_successful_transaction($per_page)
	{
		$this->db->select('o.order_id,transaction_id,payment_datetime,o.first_name,o.last_name,payment_status');
		$this->db->join('customer c','c.cust_id=o.cust_id')->order_by('o.order_id','DESC');
		$this->db->join('payment_details p','p.order_id=o.order_id');
		return $this->db->get('order o',$per_page,$this->uri->segment(3))->result();
	}
	
	public function get_failure_transaction($per_page)
	{
		$this->db->select('o.ref_order_id,transaction_id,payment_datetime,o.first_name,o.last_name,payment_status,trans_reason')->order_by('o.ref_order_id','DESC');
		$this->db->join('customer c','c.cust_id=o.cust_id');
		$this->db->join('temp_payment_details p','p.ref_order_id=o.ref_order_id');
		return $this->db->get('temp_order o',$per_page,$this->uri->segment(3))->result();
	}
	
	public function get_no_of_rows()
	{
		return $this->db->get('order')->num_rows();
	}
	
	public function get_no_of_rows1()
	{
		return $this->db->get('temp_order')->num_rows();
	}
	//Pooja Jadhav
	public function get_search_success_transaction_details($payment)
	{
		$this->db->select('o.order_id,transaction_id,payment_datetime,o.first_name,o.last_name,payment_status');
		$this->db->join('customer c','c.cust_id=o.cust_id')->order_by('o.order_id','DESC');
		$this->db->join('payment_details p','p.order_id=o.order_id');
		foreach($payment as $payment1)
		{
			$this->db->like('payment_datetime',$payment1);
			$this->db->or_like('o.first_name',$payment1);
			$this->db->or_like('o.last_name',$payment1);
		}
		$search = $this->db->get('order o');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//Pooja Jadhav
	public function get_search_fail_transaction_details($payment)
	{
		$this->db->select('o.ref_order_id,transaction_id,payment_datetime,o.first_name,o.last_name,payment_status,trans_reason')->order_by('o.ref_order_id','DESC');
		$this->db->join('customer c','c.cust_id=o.cust_id');
		$this->db->join('temp_payment_details p','p.ref_order_id=o.ref_order_id');
		foreach($payment as $payment1)
		{
			$this->db->like('payment_datetime',$payment1);
			$this->db->or_like('o.first_name',$payment1);
			$this->db->or_like('o.last_name',$payment1);
		}
		$search = $this->db->get('temp_order o');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
}