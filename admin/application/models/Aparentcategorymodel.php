<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aparentcategorymodel extends CI_Model
{
	public function get_parent_categories()
	{
		$this->db->select('parent_cat_id,parent_cat_name,parent_cat_img_url');
		$this->db->limit('4');
		return $this->db->get('parent_category')->result();
	}
	
	public function get_all_parent_categories()
	{
		$this->db->select('parent_cat_id,parent_cat_name,parent_cat_img_url');
		return $this->db->get('parent_category')->result();
	}
	
	
}