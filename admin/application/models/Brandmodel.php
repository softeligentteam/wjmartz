<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brandmodel extends CI_Model
{
	//PJ[20-04-2018]
	public function get_brand_list($per_page)
	{	
		$this->db->select('brand_id,brand,brand_img_url,brand_status');
		$this->db->where_not_in('brand_id',0);
		$this->db->where('brand_status',1);
		return $this->db->get('brands',$per_page,$this->uri->segment(3))->result();
	}

	public function get_no_of_rows()
	{
		return $this->db->get('brands')->num_rows();
	}
	
	public function del_product($prod_id)
	{
		$this->db->where('prod_id',$prod_id);
		return $this->db->delete('products');
	}
	
	public function insert_brands()
	{
		
		//$brand=$this->input->post('brand');
		//$brand_image=$this->upload_product_photo();
		
		
		$data=array(
		'brand'=>$this->input->post('brand'),
		'brand_img_url'=>$this->input->post('brand_img'),
		'parent_cat_id'=>$this->input->post('parent_cat')
		);
		
		return $this->db->insert('brands',$data);
	}
	
	public function update_brands()
	{
		$id=$this->input->post('id');
		//$image=$this->input->post('image');
		
		
		//$brand=$this->input->post('brand_name');
		//$brand_img_url=$this->update_image($image);
		
		
		$data=array(
			'brand'=>$this->input->post('brand_name'),
			'brand_img_url'=>$this->input->post('brand_img'),
		);
		
		
		$this->db->where('brand_id',$id);
		return $this->db->update('brands', $data);
	}
	//PJ[20-04-2018]
	public function del_brand($brand_id)
	{
		//$this->db->where('brand_id',$brand_id);
		//return $this->db->delete('brands');
        $cmd = "UPDATE brands SET brand_status = 0 WHERE brand_id = $brand_id;";
		return $this->db->query($cmd);
	}
	
	
	public function inactivate_brands()
	{
		$id=$this->input->post('inactive');
		$data=array('brand_status'=>0);
		$this->db->where('brand_id',$id);
		return $this->db->update('brands', $data);
	}
	
	public function activate_brands()
	{
		$id=$this->input->post('active');
		$data=array('brand_status'=>1);
		$this->db->where('brand_id',$id);
		return $this->db->update('brands', $data);
	}
	//Pooja Jadhav
	public function get_search_brand_details($brand_name)
	{
		$this->db->select('brand_id,brand,brand_img_url,brand_status');
		$this->db->where_not_in('brand_id',0);
		$this->db->like('brand',$brand_name);
		$search = $this->db->get('brands');
		if($search->num_rows()>0)
		{
			return $search->result();
		}
		else
		{
			return $search->result();
		}
	}
	//KT[18-04-18]
	public function get_brand_image_path()
	{
		$this->db->select('brand_img_url')->where('brand_id', $this->input->post('id'));
		return $this->db->get('brands')->result();
	}
}