<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timeslotsmodel extends CI_Model
{
	public function get_timeslots()
	{	
		$this->db->select('time_slot_id,time_slot,shift_status');
		$this->db->where_not_in('time_slot_id',0);
		$this->db->where('status',1);
		return $this->db->get('time_slots ts')->result();
	}
	
	public function insert_timeslots()
	{		
		$time_slot=$this->input->post('time_slot');
		$shift_status=$this->input->post('shift_status');
				
		$data=array(
		'time_slot'=>$time_slot,
		'shift_status'=>$shift_status,
		'status'=>1
		);
		
		return $this->db->insert('time_slots',$data);
	}
	
	public function get_no_of_rows()
	{
		return $this->db->get('time_slots')->num_rows();
	}
	
	
	public function del_time_slot($time_slot_id)
	{
		$data=array('status'=>0);
		$this->db->where('time_slot_id',$time_slot_id);
		return $this->db->update('time_slots',$data);
		//return $this->db->affected_rows();
	}
	
	public function update_time_slot()
	{
		$time_slot_id=$this->input->post('time_id');
		$time_slot=$this->input->post('time');
		$shift_status=$this->input->post('shift');
				
		$data=array(
		'time_slot'=>$time_slot,
		'shift_status'=>$shift_status
		);
		
		$this->db->where('time_slot_id',$time_slot_id);
		$this->db->update('time_slots',$data);
		return $this->db->affected_rows();
	}
	
}