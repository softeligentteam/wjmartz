 <?php
 
 class Ajax_pagination_model extends CI_model
 {
	 function count_all()
	 {
		 $query = $this->db->get('main_category');
		 return $query->num_rows();
	 }
	 
	 function fetch_details($limit, $start)
	 {
		$output = '';
		$this->db->select('main_cat_id,main_cat_name');
		$query = $this->db->get('main_category');
		$output .='
			<table class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Main Category Name</th>
						<th colspan="2"><center>Action</center></th>
					</tr>
				</thead>
				<tbody>
				
		';
		foreach($query->result() as $row)
		{
			$output .= '
				
				<tr>
					<td>'.$row->main_cat_id.'</td>
					<td>'.$row->main_cat_name.'</td>
					<td class="text-center"><a onclick="edit_cat();" id="myBtn" class="btn btn-primary">Edit</a></td>
					<td class="text-center"><a onclick="delete_cat('.$row->main_cat_name.');" class="btn btn-danger">Delete</a></td>
				</tr>
				
			';
		}
		$output .= '</tbody></table>';
		return $output;
	 }
 }