<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Childcategorymodel extends CI_Model
{
	public function insert_child_categories()
	{
		$child_cat_name=$this->input->post('child_category');
		//$child_cat_img=$this->upload_child_cat_photo();
		$parent_cat_id=$this->input->post('parent_cats');
		
		$data=array(
		'child_cat_name'=>$child_cat_name,
		'child_cat_img_url'=>$this->input->post('child_cat_img'),
		'parent_cat_id'=>$parent_cat_id
		);
		
		return $this->db->insert('child_category',$data);
	}
	
	
	public function update_child_categories()
	{
		$child_cat_id=$this->input->post('child_cat_id');

		$child_cat_name=$this->input->post('edit_child_category_name');
		$child_cat_img=$this->input->post('child_cat_img');
		$parent_cat_id=$this->input->post('edit_parent_cat');
		
		$data=array(
		'child_cat_name'=>$child_cat_name,
		'child_cat_img_url'=>$child_cat_img,
		'parent_cat_id'=>$parent_cat_id
		);
		
		$this->db->where('child_cat_id',$child_cat_id);
		return $this->db->update('child_category',$data);
	}
	
	
	public function get_child_cat_list($parent_cat_id)
	{	
		$this->db->select('child_cat_id,child_cat_name,child_cat_img_url,parent_cat_id');
		$this->db->where('status',1);
		$this->db->where_not_in('child_cat_id',0);
		if($parent_cat_id != 0){
			$this->db->where('parent_cat_id',$parent_cat_id);
		}
		return $this->db->get('child_category')->result();
	}
	
	public function get_child_cat_list_inactive_no_of_rows()
	{
		$this->db->where('status',0);
		return $this->db->get('child_category')->num_rows();
	}
	public function get_child_cat_list_inactive($parent_cat_id,$per_page)
	{	
		$this->db->select('child_cat_id,child_cat_name,child_cat_img_url,parent_cat_id');
		$this->db->where('status',0);
		$this->db->where_not_in('child_cat_id',0);
		if($parent_cat_id != 0){
			$this->db->where('parent_cat_id',$parent_cat_id);
		}
		return $this->db->get('child_category',$per_page, $this->uri->segment(3))->result();
	}
	
	public function get_child_categories()
	{	
		$this->db->select('child_cat_id,child_cat_name,child_cat_img_url');
		$this->db->where('status',1);
		$this->db->where_not_in('child_cat_id',0); 
		return $this->db->get('child_category')->result();
	}
	
	public function del_child_category($child_cat_id)
	{
		$data=array('status'=>0);
		$this->db->trans_start();
		$status['category'] = $this->db->where('child_cat_id', $child_cat_id)->update('child_category', $data);
		$status['product'] = $this->db->where('fk_pchild_id', $child_cat_id)->update('products', $data);
		$this->db->trans_complete();
		return $status;
		
	}
	
	public function get_products($child_cat_id){
		$this->db->select('prod_id,prod_name,prod_price,quantity,prod_description');
		$this->db->where_not_in('prod_id',0);
		$this->db->where('child_cat_id',$child_cat_id);
		return $this->db->get('products')->result();
	}
	
	public function activated_child_category($child_cat_id)
	{
		$data=array('status'=>1);
		$this->db->where('child_cat_id', $child_cat_id);
		$this->db->update('child_category', $data);
		return $this->db->affected_rows();
	}
	//KT[18-04-18]
	public function get_child_cat_image_path()
	{
		$this->db->select('child_cat_img_url')->where('child_cat_id', $this->input->post('child_cat_id'));
		return $this->db->get('child_category')->result();
	}
	
}	
	