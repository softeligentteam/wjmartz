<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pushmodel extends CI_Model{

	public function insert_pushdata()
	{
		$userdata = array(
		'email' => $this->input->post('email'),
		'fcm_device_id' => $this->input->post('token')
		);
		//return $userdata;
		$status = $this->db->insert('pn_test', $userdata);
		
		if($status == true)
		{
			return 1;
		}
		else{return 0;}
	}
	
	public function get_all_device_details()
	{
	      	$this->db->select('email, fcm_device_id');
		$device_details = $this->db->get('pn_test')->result();
		$device_id = array();
		$data['error'] = false; 
		$data['devices'] = array(); 
		foreach($device_details as $dids)
		{
		    $data['devices']['id'] = $dids->email;
		    $data['devices']['device_id'] = $dids->fcm_device_id;
		}
		return $data;
	}
	public function get_all_device_data()
	{
	
	      	$this->db->select('fcm_device_id');
		$rows=$this->db->get('pn_test')->result();
	        $tokens= array();
	        if(count($rows)>0)
	        {
	            foreach($rows as $row)
	            {
	                $tokens[] = $row->fcm_device_id;
	            }
	        }
			return $tokens;
	}
}