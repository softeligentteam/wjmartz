var url = "http://demo.classginie.com/wjmartz/index.php/";
var baseurl = "http://demo.classginie.com/wjmartz/";

function insert_success_close(){
	$('.insert_success').fadeOut();
}

function filter(){ 
	debugger;
	var parent_cat_id= $('#parent_cat option:selected').val();  
	alert(parent_cat_id);
	var selected= $('#parent_cat option:selected').html();
	$('.filtered').html(selected);
	$.ajax({
		type: "POST",
		url:  url+"ajax/get_child_categories",
		data: {"parent_cat_id" : parent_cat_id},
		dataType: "html",
		beforeSend: function() {
		$('#displayChildcatList').show();
		$('#appendChildcatList').html('<span style="color:red">Please Wait.......</span>');
		},
		success: function(response) {
			$('#appendChildcatList').html('');
			var obj = jQuery.parseJSON(response);
			$(function() {
				$.each(obj, function(i, item){
					var j = i+1;
					$('#appendChildcatList').append('<tr><td class="text-center">'+j+'</td><td class="text-center"><img src="'+baseurl+item['child_cat_img_url']+'" style="width:50px;" /></td><td class="text-center">'+item['child_cat_name']+'</td><td class="text-center"><a onclick="" id="myBtn" class="btn btn-primary">Edit</a></td><td class="text-center"><a onclick="delete_this(\''+item['child_cat_id']+'\',\''+item['child_cat_name']+'\');" class="btn btn-danger">Delete</a></td></tr>');
				});
			});
		},
		error: function(error) { 
			$('#displayChildcatList').html('<tr><td colspan="4"><h4 style="color:red;"><center>Some Error Occoured, Please Try Again.</center></h3></td></tr>');
		}
	});
}

function delete_this(id,name)
{
		$('#confirm_delete').show();
		$('#dlt_msg').html('');
		$('#dlt_msg').append('Are you sure you want to delete '+name+' Category ?');
		$('#deleteMAinCatHref').prop('href',url+'child_categories/delete_child_category/'+id);
}