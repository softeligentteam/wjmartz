<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aparentcategorymodel extends CI_Model
{
	public function get_parent_categories()
	{
		$this->db->select('parent_cat_id,parent_cat_name,parent_cat_img_url');
		$this->db->limit('5');
		return $this->db->get('parent_category')->result();
	}
	
	public function get_all_parent_categories()
	{
		$this->db->select('parent_cat_id,parent_cat_name,parent_cat_img_url');
                $this->db->where('status',1);
		return $this->db->get('parent_category')->result();
	}
	

	public function get_sectionwise_parent_categories()
	{
		$this->db->select('screen_id,hs.parent_cat_id,parent_cat_name,parent_cat_img_url')->order_by('screen_id','asc');
		$this->db->join('parent_category pc','pc.parent_cat_id=hs.parent_cat_id');
		$this->db->where('hs.status',1);
		return $this->db->get('home_screen_section hs')->result();
	}
	
}