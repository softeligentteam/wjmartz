<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Amaincategorymodel extends CI_Model
{
	public function get_search_result()
	{
		$keyword=$this->input->post('search_keyword');
		
		$this->db->select('prod_id,prod_name,fk_pchild_id as pccat_id,fk_pcolor_id,deal_status');
		$this->db->like('prod_name',$keyword,'after');
		$this->db->where('in_stock',1);
		$this->db->where('status !=', 0);
		$query1=$this->db->get('products')->result();
		
		return $query1;
	}
	
	public function get_product_details($prod_id,$per_page)
	{
		
			$this->db->select('prod_id,prod_name,fk_pcolor_id,prod_price,prod_image_url,fk_pchild_id as pccat_id,fk_psize_ids');
			$this->db->where('prod_id',$prod_id);
			return $this->db->get('products',$per_page,$this->uri->segment(4,0))->result();
		
	}
	
	public function get_no_of_rows()
	{
		return $this->db->get('products')->num_rows();
	}
	
	public function get_search_result_for_category()
	{
		$keyword=$this->input->post('search_keyword');
		
		$this->db->select('child_cat_id,child_cat_name');
		$this->db->like('child_cat_name',$keyword,'after');
		$query1=$this->db->get('child_category')->result();
		
		return $query1;
	}
}