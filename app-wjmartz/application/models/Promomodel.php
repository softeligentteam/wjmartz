<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promomodel extends CI_Model
{
	public function get_promocode($coupon_code)
	{
		//$coupon_code='example123';
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	}

	public function get_promocodetype($coupon_code)
	{
		$this->db->select('promo_type_id');
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	}

	public function get_order_count()
	{
	  $cust_id=$this->input->post('cust_id');
	  $this->db->where('cust_id',$cust_id);
	  return $this->db->get('order')->num_rows();
	}

	public function get_deal_status_array()
	{
		$productdetails = $this->input->post('product_details_coupon');
		$product_details_coupon = json_decode($productdetails, TRUE);
		$deal_status=array();
		 if(is_array($product_details_coupon))
		{
		foreach($product_details_coupon['product_details_coupon'] as $coupon)
			{
				$deal_status[]=$coupon['deal_status'];
			}
		}
		return $deal_status;
	}
	
	 //KT[date 7may 2018]
	public function get_child_cat_list_cart()
	{
		$productdetails = $this->input->post('product_details_coupon');
		$product_details_coupon = json_decode($productdetails, TRUE);
		$cartchildarray=array();
		foreach($product_details_coupon['product_details_coupon'] as $coupon)
		{
			$cartchildarray[]=$coupon['product_cat_id']; 
		}
		return $cartchildarray;
	}
	
	//KT[date 8may 2018]
	public function get_childcat_promo($coupon_code)
	{
		$this->db->select('child_cat_id');
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	} 
	 //KT[date 8may 2018]
	public function get_discount_promo($coupon_code)
	{
		$this->db->select('discount');
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	} 

	
	public function check_coupon_code($coupon_code)
	{
		$flag=FALSE;
		$child_cat=$this->get_childcat_promo($coupon_code);
		$child_cat_cart=$this->get_child_cat_list_cart();
		$size_new=sizeof($child_cat_cart);
		$discount_amount=$this->get_discount_promo($coupon_code);
		
		$ptype=$this->get_promocodetype($coupon_code);
		$order_count=$this->get_order_count();
		$deal_status=$this->get_deal_status_array();
		$size=sizeof($deal_status);
		$call_user=$this->get_promocode($coupon_code);
		$user_count=$call_user->applicable_promo;
		$child_cat_id=$call_user->child_cat_id;
		$cart_amount=$this->input->post('total_amount');
		
		if($ptype->promo_type_id == "3")
		{
			
			for($i=0;$i<$size;$i++)
		    {
			
		 	if($deal_status[$i]=="1")
			{
				$flag=FALSE;
				break;
			}
			else{
				$flag=FALSE;	
			}
			
		    }
		}	
		if($ptype->promo_type_id=="4")
		{
				for($i=0;$i<$size;$i++)
		    {
			if($order_count!=0 )	
			{
				$flag=TRUE;
				break;
			}
			else{
				$flag=FALSE;	
			}
		    }
		}
		
		if($ptype->promo_type_id=="5")
		{
		  for($i=0;$i<$size;$i++)
		    {
			if( $cart_amount <= "1000" )	
			{
				$flag=TRUE;
				break;
			}
			else{
				$flag=FALSE;	
			}
		    }
		}
		
		if($ptype->promo_type_id=="6")
		{
			$productdetails = $this->input->post('product_details_coupon');
			$product_details_coupon = json_decode($productdetails, TRUE);
			$total_amount=0;
			for($i=0;$i<$size_new;$i++)
			{
				if($child_cat_cart[$i] == $child_cat->child_cat_id)
				{
					foreach($product_details_coupon['product_details_coupon'] as $coupon)
					{
						$total_amount=(int)$total_amount + (int)$coupon['product_price'];	
					}
					$abc=(int)$cart_amount  - (int)$total_amount;
					$disc_amount= (int)$total_amount * (int)$discount_amount->discount /100;
					$amount= (int)$total_amount - (int)$disc_amount;
					$amt=(int)$abc + (int)$amount;
					$this->session->set_userdata('cartamt',$amt);
					
					$flag=FALSE;
					break;
				}
				else{
						$flag=TRUE;	
				}
			}	
				
		}	
		
		
		return $flag;
		//var_dump($flag);
	}
	
	

	public function get_data_for_cart($coupon_code)
	{
		$this->db->select('promo_id,promo_type_id,discount,promocode,child_cat_id');
		$this->db->where('promocode',$coupon_code);
		return $this->db->get('promocode')->row();
	}

	public function get_data_for_promo_offers()
	{
		$this->db->select('promo_id,promo_type_id,discount,promo_image_url,description,promocode');
		$this->db->where('status',1);
		return $this->db->get('promocode')->result();
	}
	
	
	
}