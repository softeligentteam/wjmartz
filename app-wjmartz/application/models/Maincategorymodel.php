<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maincategorymodel extends CI_Model
{
	public function get_all_main_categories()
	{
		$this->db->select('main_cat_id,main_cat_name');
		return $this->db->get('main_category')->result();
	}
	
	
}