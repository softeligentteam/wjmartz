<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Get_pagination 
{
	public function generate_pagination($url, $number_of_rows, $per_page)
	{
		$config['total_rows'] = $number_of_rows;
		$config['base_url'] = $url;
		$config['per_page'] = $per_page;
		
		$config['full_tag_open'] = '<div class="col-sm-8"><nav aria-label="Page navigation example"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav></div>';
		
		$config['first_tag_open'] = '<li class="page-item">';
		$config['last_tag_open'] = '<li class="page-item">'; 
		
		$config['next_tag_open'] = '<li class="page-item">';
		$config['prev_tag_open'] = '<li>';
		
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		
		$config['first_tag_close'] = '</li>';
		$config['last_tag_close'] = '</li>'; 
		
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="page-item" style="background-color:gray;"><a>';
		$config['cur_tag_close'] = '</a></li>';
		
		return $config;
	}
}