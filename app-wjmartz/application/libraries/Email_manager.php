<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Email_manager {

	public function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->library('email');
		$this->initialize_email();
	}
	
	/************ INITIALIZE EMAIL CREDENTIALS **************/
	public function initialize_email(){
		$this->CI->email->initialize(array(
    			'protocol' => 'smtp',
    			'smtp_host' => 'webmail.wjmartz.com',
    			'smtp_user' => 'support@wjmartz.com',
    			'smtp_pass' => 'Support@123',
    			'smtp_port' => 587,
    			'smtp_timeout' => '5',
    			'mailtype'  => 'html',
    			'charset'   => 'utf-8',
    			'crlf' => "\r\n",
    			'newline' => "\r\n"
			));
	}
	
	public function send_order_place_email($email,$message)
	{
		$this->CI->email->from('support@wjmartz.com', 'Wjmartz');
		$this->CI->email->to($email);
		$this->CI->email->subject('Order Placed Successfully');
		$this->CI->email->message($message);
		$this->CI->email->send();
			
	}
	
	public function send_order_place_fail_email($email,$message)
	{
		$this->CI->email->from('support@wjmartz.com', 'Wjmartz');
		$this->CI->email->to($email);
		$this->CI->email->subject('Order Placed Failed');
		$this->CI->email->message($message);
		$this->CI->email->send();
			
	}
	
	public function send_welcome_email($email,$message)
	{
		$this->CI->email->from('support@wjmartz.com', 'Wjmartz');
		$this->CI->email->to($email);
		$this->CI->email->subject('Customer Registration Successful');
		$this->CI->email->message($message);
		$this->CI->email->send();
			
	}
	
	public function send_order_cancel_email($email,$message)
	{
		$this->CI->email->from('support@wjmartz.com', 'Wjmartz');
		$this->CI->email->to($email);
		$this->CI->email->subject('Order Cancelled Successfully');
		$this->CI->email->message($message);
		$this->CI->email->send();
			
	}
	
	public function send_refund_order_email($email,$message)
	{
		$this->CI->email->from('support@wjmartz.com', 'Wjmartz');
		$this->CI->email->to($email);
		$this->CI->email->subject('Return Exchange Order');
		$this->CI->email->message($message);
		$this->CI->email->send();
			
	}
}	