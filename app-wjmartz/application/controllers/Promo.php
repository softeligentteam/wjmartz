<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();

		$this->load->model('Promomodel');
		
	}	
	
	public function apply_coupon_code()
	{
		$coupon_code=$this->input->post('coupon_code');
		if($this->input->post())
		{	
		$result=$this->Promomodel->get_promocode($coupon_code);
		if($result!=NULL)
		{
			if($this->Promomodel->check_coupon_code($coupon_code)===FALSE)
			{	
				$data['status']='1';
				$data['coupon_data']=$this->Promomodel->get_data_for_cart($coupon_code);
				if($result->promo_type_id ==6)
				{	
				 $data['coupon_amount']=$this->session->userdata('cartamt');
				}
				else{
				 $data['coupon_amount']=null;
				}	
				$data['message']=PROMOSUCCESSMESSAGE;
				
				
			}else
			{
			
				$data['status']='0';
				 $data['coupon_amount']=null;
				if($result->promo_type_id==5)
			      {	
				$data['message']= PROMODEALIVERYMESSAGE;
			      }elseif($result->promo_type_id==6)
			      {
			       $data['message']=PROMOSUBCATMESSAGE;
			      }
			      else{
			        $data['message']=PROMODEALMESSAGE;
			      }
			}			
		}else
		{
			$data['status']='0';
			$data['message']=PROMODOESNOTEXISTMESSAGE;
			 $data['coupon_amount']=null;
		}
			
		}else
		{
			$data['status']='0';
			$data['message']=NULL;
			 $data['coupon_amount']=null;
		}
		echo json_encode($data);
		
	}
	public function get_offers()
{
	$data['offers_data']=$this->Promomodel->get_data_for_promo_offers();
	$data['imageaccesspath']=IMAGEACCESSPATH;
	echo json_encode($data);
}
}	