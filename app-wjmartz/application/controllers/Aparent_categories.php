<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aparent_categories extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('Aparentcategorymodel');
		
	}

	/****************** For Home *****************/
	public function get_parent_categories_home()
	{
		$data['parent_categories']=$this->Aparentcategorymodel->get_parent_categories();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function get_all_parent_categories()
	{
		$data['parent_categories']=$this->Aparentcategorymodel->get_all_parent_categories();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}

	public function get_section_parent_categories()
	{
		$data['sec_parent_cat']=$this->Aparentcategorymodel->get_sectionwise_parent_categories();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	
}	 