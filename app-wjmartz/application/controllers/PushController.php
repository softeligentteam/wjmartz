<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PushController extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();

		$this->load->model('Pushmodel');

		
    	}	
		
	 // this function will redirect to book service page
	 function index()
	 {
		$this->subscribe();
	 }

	 // this function to load service book page
	 function subscribe()
	 {
		 $this->load->view('pages/site_subscriber');
	 }

	public function register_device()
	{
		$status = $this->Pushmodel->insert_pushdata();	
		echo json_encode($status);
	}
	
	  
	    
    public function get_all_device_details()
    {
    	$data['device_details'] = $this->Pushmodel->get_all_device_details();
    	echo json_encode($data['device_details']);
    }
    
    // send notification to registered devices from admin panel
   /*   public function send_notification()
	{
	    $title = $this->input->post('title');		
	    $message = $this->input->post('message');
	    $device_details = $this->Pushmodel->get_all_device_details();
	    $data['devices'] = array(); 
	    $result =0;
		foreach($device_details as $dids)
		{
		    $data['devices'] = $dids->;
		      $fields = array(
	             'registration_id' => $data['devices'],
	             'data' => $message,
	             'title' => $title
        	     );
        	   $result = $this->sendPushNotification($fields);
		}
	   echo json_encode($result);
       }*/
        public function getPush($title, $message, $image) {
        $res = array();
        $res['data']['title'] = $title;
        $res['data']['message'] = $message;
        $res['data']['image'] = $image;
        return $res;
      }
        public function send_notification()
	{
	   //importing required files 
		
		$data['devices'] = $this->Pushmodel->get_all_device_details();
		$title = $this->input->post('title');
		$message = $this->input->post('message');
		$this->load->library('Firebase');
		
		$response = array(); 
		 
		 
		 //creating a new push
		 $push = null; 
		 
		
		 //if the push don't have an image give null in place of image
		 // $this->load->library('Push');
		 // $push = new Push();
		
		 //getting the push from push object
		 $mPushNotification = $this->getPush($title, $message, null); 
		 
		
		 
		 //creating firebase class object 
		 $firebase = new Firebase(); 
		 
		// foreach($device_details as $dids)
		//{
		    $data['devices'] = 'dG1y-UjEusU:APA91bHS-BXX1UbM7ynQamrbrusJohzSn9jMy85l0v15uhSrgzcwyDfwbRy-yf0hUrZSRTlbVPojr86OOb-GXappuor3-wIRPh0ZXujEQsonLsDLWGa1M4e3TQiU0zZAsz4wUjntZ3FH';
		     
		 echo json_encode($firebase->send($data['devices'], $mPushNotification));
		 //}
		 //sending push notification and displaying result 
		 
		 
	
		 
	
	   //echo json_encode($response);
       }
       
        public function sendPushNotification($fields) {
         
      
        //firebase server url to send the curl request
        $url = 'https://fcm.googleapis.com/fcm/send';
 
        //building headers for the request
        $headers = array(
            'Authorization: key=' . FIREBASE_API_KEY,
            'Content-Type: application/json'
        );
 
        //Initializing curl to open a connection
        $ch = curl_init();
 
        //Setting the curl url
        curl_setopt($ch, CURLOPT_URL, $url);
        
        //setting the method as post
        curl_setopt($ch, CURLOPT_POST, true);
 
        //adding headers 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        //disabling ssl support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        //adding the fields in json format 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        //finally executing the curl request 
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        //Now close the connection
        curl_close($ch);
 
        //and return the result 
        return $result;
    }
    
    
   //getting all tokens to send push to all devices
	public function sendSinglePush()
	{
		$response = array(); 
		$message1=$this->input->post('message');
		$message=json_encode($message1);
		$title1=$this->input->post('title');
		$title=json_encode($title1);
		$email=$this->input->post('email');
		$image1=$this->input->post('image');
		$image=json_encode($image1);
		
		//$token=$this->getTokenByEmail($email);
		//var_dump($token);
		//var_dump($email);
		if($this->input->post())
		{	
			//hecking the required params 
			/*if(isset($_POST['title']) and isset($_POST['message']) and isset($_POST['email'])){*/

				
				//first check if the push has an image with it
				/*$this->load->library('Push');
				if(isset($_POST['image'])){
					$push = new Push(
							$_POST['title'],
							$_POST['message'],
							$_POST['image']
						);
				}else{
					//if the push don't have an image give null in place of image
					$push = new Push(
							$_POST['title'],
							$_POST['message'],
							null
						);
				}

				//getting the push from push object
				$this->load->library('push');
				$mPushNotification = $this->push->getPush(); 
				var_dump($mPushNotification);*/

				//getting the token from database object 
				$devicetoken1 =$this->getTokenByEmail($email);
				$devicetoken=implode('',$devicetoken1);
				var_dump($devicetoken);

				//creating firebase class object 
				//$firebase = new Firebase(); 

				//sending push notification and displaying result
				$this->load->library('firebase');
				echo  $this->firebase->send($devicetoken,$message,$title,$image);
			/*}else{
				$response['error']=true;
				$response['message']='Parameters missing';*/
			
		}else{
			$response['error']=true;
			$response['message']='Invalid request';
		}

		echo json_encode($response);
	}   
}