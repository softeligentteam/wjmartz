<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aproduct extends CI_Controller 
{
	 public function __construct() 
	 {
        parent::__construct();
		$this->load->model('Aproductmodel');
		$this->load->model('Ddlmodel');
	 }	
	 
	 public function general_product_list()
	{
		$child_cat_id=$this->uri->segment(3,0);
		$count=$this->Aproductmodel->get_no_of_rows($child_cat_id);
		$per_page=6;
		
		if($count>0)
		{
			$data['products']= $this->Aproductmodel->get_genral_products($per_page,$child_cat_id);
			$data['count']=$this->Aproductmodel->get_no_of_rows($child_cat_id);
			$data['success']=1;
			$data['product_type']=2;
			$data['imageaccesspath']=IMAGEACCESSPATH;
		}else{
			$data['success']=0;
			$data['products']='NO records found...';
		}	
		echo json_encode($data);
	}
	
	public function brand_list()
	{
		$data['brand_list']=$this->Aproductmodel->get_all_brands();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function brand_wise_product_list()
	{
		$brand_id=$this->uri->segment(3,0);
		$count=$this->Aproductmodel->get_no_of_rows_brands($brand_id);
		$per_page=6;
		
		if($count>0)
		{
			$data['products']= $this->Aproductmodel->get_brand_products($per_page,$brand_id);
			$data['count']=$this->Aproductmodel->get_no_of_rows_brands($brand_id);
			$data['success']=1;
			$data['product_type']=1;
			$data['imageaccesspath']=IMAGEACCESSPATH;
		}else{
			$data['success']=0;
			$data['products']='NO records found...';
		}	
		echo json_encode($data);
	}
	
	public function pune_special_brand_list()
	{
		$data['pune_special_list']=$this->Aproductmodel->get_all_pune_special();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function pune_special_wise_product_list()
	{
		$brand_id=$this->uri->segment(3,0);
		$count=$this->Aproductmodel->get_no_of_rows_pune_special($brand_id);
		$per_page=6;
		
		if($count>0)
		{
			$data['products']= $this->Aproductmodel->get_pune_special_products($per_page,$brand_id);
			$data['count']=$this->Aproductmodel->get_no_of_rows_pune_special($brand_id);
			$data['success']=1;
			$data['product_type']=1;
			$data['imageaccesspath']=IMAGEACCESSPATH;
		}else{
			$data['success']=0;
			$data['products']='NO records found...';
		}	
		echo json_encode($data);
	}
	
	public function product_list()
	{
		$child_cat_id = $this->uri->segment(3,0);//Product's Child Category Id - Tshirt
		//$ptype_id = 1;//Product's Type Id - Tshirt
		
		
		$count=$this->Aproductmodel->get_no_of_rows1($child_cat_id);
		$per_page=6;
		
		if($count>0)
		{
			$data['products'] = $this->Aproductmodel->get_catandtypewise_products($child_cat_id,$per_page);
			$data['psize_list'] = $this->Aproductmodel->get_psize_list();
			$data['pccat_id']=$child_cat_id;
			$data['count']=$this->Aproductmodel->get_no_of_rows1($child_cat_id);
			$data['success']=1;
			$data['imageaccesspath']=IMAGEACCESSPATH;
		}else{
			$data['success']=0;
			$data['products']='NO records found...';
		}	
		echo json_encode($data);
	}
	
	public function product_details()
	{
		$pccat_id =  $this->uri->segment(3,0);	
		$pid =  $this->uri->segment(4,0);
		$pcolor_id =  $this->uri->segment(5,0);
		$status= $this->uri->segment(6,0);
		if($status==0)
		{
		    $product['product_details'] =$this->Aproductmodel->get_product2view_details($pccat_id, $pid);
		    $product['rec_products'] = $this->Aproductmodel->get_recommended_products($pccat_id,$pcolor_id);
		}else{
		     $product['product_details'] =$this->Aproductmodel->get_product2view_details_for_deal($pccat_id, $pid);
		     $product['rec_products'] = $this->Aproductmodel->get_recommended_products_for_deal($pccat_id,$pcolor_id,$pid);
		}
		  $product['psize_list'] = $this->Aproductmodel->get_product_size_list($pid);
		  $product['base_url'] = IMAGEACCESSPATH;
		  echo json_encode($product);
	}
	
	public function product_filteration()
	{
		$data['filteration'] = NULL;
		//$parent_cat_id=$this->uri->segment(3,0);
		if($this->input->post())
		{
			$data['filteration']=$this->Aproductmodel->get_filtered_products();
			$data['psize_list'] = $this->Aproductmodel->get_psize_list();
			$data['base_url'] = IMAGEACCESSPATH;
		}	
		echo json_encode($data);
	}
	
	public function high_price_filtration()
	{
		$pccat_id =  $this->uri->segment(3,0);	
		$count=$this->Aproductmodel->get_no_of_rows2($pccat_id);
		$per_page=6;
		if($count>0)
		{
			$data['products'] = $this->Aproductmodel->get_filter_high_price($pccat_id,$per_page);
			if(count($data)>0)
			{	
				$data['products'] = $this->Aproductmodel->get_filter_high_price($pccat_id, $per_page);
				$data['psize_list'] = $this->Aproductmodel->get_psize_list();
				$data['pccat_id']=$pccat_id;
				$data['count']=$this->Aproductmodel->get_no_of_rows2($pccat_id);
				$data['success']=1;
				$data['imageaccesspath']=IMAGEACCESSPATH;			
			}
		}else{
			$data['success']=0;
			$data['products']='NO records found...';
		}		
		echo json_encode($data);
		
	}
	
	public function low_price_filtration()
	{
		$pccat_id =  $this->uri->segment(3,0);	
		$count=$this->Aproductmodel->get_no_of_rows2($pccat_id);
		$per_page=6;
		if($count>0)
		{
			$data['products'] = $this->Aproductmodel->get_filter_low_price($pccat_id,$per_page);
			if(count($data)>0)
			{	
				$data['products'] = $this->Aproductmodel->get_filter_low_price($pccat_id, $per_page);
				$data['psize_list'] = $this->Aproductmodel->get_psize_list();
				$data['pccat_id']=$pccat_id;
				$data['count']=$this->Aproductmodel->get_no_of_rows2($pccat_id);
				$data['success']=1;
				$data['imageaccesspath']=IMAGEACCESSPATH;			
			}
		}else{
			$data['success']=0;
			$data['products']='NO records found...';
		}		
		echo json_encode($data);
		
	}
	
	public function get_product_sizes()
	{
		$pcat_id=$this->uri->segment(3,0);
		$data['psize_list']= $this->Ddlmodel->get_psize_list_for_filteration($pcat_id);
		echo json_encode($data);
	}
	
	public function get_product_color()
	{
		$data['pcolor_list']= $this->Ddlmodel->get_pcolor_list();
		echo json_encode($data);
	}
	/**** KT[25-04-18] ****/
	public function get_product_type()
	{
		$data['ptype_list']= $this->Ddlmodel->get_ptype_list();
		echo json_encode($data);
	}
	
	public function new_arrival_products()
	{
		$data['new_arrival']=$this->Aproductmodel->get_new_arrival_products();
                $data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function recommended_products()
	{
		$child_cat_id=$this->uri->segment(3,0);
		$data['recommended_products']=$this->Aproductmodel->get_recommended_products_new($child_cat_id);
		$data['psize_list'] = $this->Aproductmodel->get_psize_list();
                $data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	
	public function deals_of_the_day_products()
	{
		$data['deal_day_products']=$this->Aproductmodel->get_deals_of_the_day_product();
                $data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
        
        
	public function deals_of_the_day_products_home()
	{
		$data['deal_day_products']=$this->Aproductmodel->get_deals_of_the_day_product_home();
                $data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
}