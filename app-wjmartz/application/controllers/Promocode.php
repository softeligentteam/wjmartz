<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promocode extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
		$this->output->set_header('Cache-Control: no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
		$this->load->model('Promocodemodel');
		$this->load->model('Ddlmodel');
		
	}
	
	public function view_userwise_promo()
	{
		$per_page = 15;
		$url = site_url().'/promocode/view_userwise_promo';
		$number_of_rows = $this->Promocodemodel->get_userwise_promocode_no_of_rows();
		$data['promo_list'] =$this->Promocodemodel->get_userwise_promo_list($per_page);
		$data['pc_catlist'] = $this->Ddlmodel->get_child_cat_list_promocode();
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/userwise_promo',$data);
		$this->load->view('layouts/footer');
	}
	
	public function add_userwise_promocode()
	{
		if($this->input->post())
		{
			if($this->validate_userwise_promocode_form())
			{
				$this->Promocodemodel->insert_userwise_promo();
				$message = 'Promocode is inserted sucessfully';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('promocode/view_userwise_promo');
			}
			else{
				$message = 'Promocode insertion failed. Please upload proper image';
				$css_class = 'text-alert';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('promocode/view_userwise_promo');
			}
		}
		else{
				$message = 'Promocode insertion failed. Please upload proper image';
				$css_class = 'text-alert';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('promocode/view_userwise_promo');
			}
	}
	//Pooja Jadhav
	public function validate_userwise_promocode_form()
	{
		$this->form_validation->set_error_delimiters('<div class="error_msg">', '</div>');
		$this->form_validation->set_rules('userwise_promo_img', 'Promocode Image', 'callback_validate_and_upload_userwise_promocode');
		return $this->form_validation->run();
	}
	//Pooja Jadhav
	public function validate_and_upload_userwise_promocode()
	{
		if (empty($_FILES['userwise_promo_img'])) 
		{
			$this->form_validation->set_message('userwise_promo_img', 'Please select promocode image');
            return FALSE;
        }
		else
		{
			$config['upload_path'] = SIZEIMAGEUPLOAD;
			$config['allowed_types'] = 'png|jpg|jpeg';
			$config['max_size'] = '300';
			$config['min_width'] = '700'; 
			$config['min_height'] = '400';
		    $config['max_width'] = 700;
		    $config['max_height'] = 400;
			$config['encrypt_name'] = TRUE;
			$config['override'] = FALSE;
			
			$this->load->library('upload');
			$this->upload->initialize($config);
			if (!is_dir($config['upload_path'])) 
			{
				mkdir($config['upload_path'], 0777, TRUE);
				echo "<script>console.log('created.');</script>";
			}
			if($this->upload->do_upload('userwise_promo_img'))
            {
                $upload_data = $this->upload->data();
                $_POST['userwise_promo_img'] = $config['upload_path'].'/'.$upload_data['file_name'];
				echo "<script>console.log('Sucess.');</script>";
                return TRUE;
            }else
            {
                $this->form_validation->set_message($msg_name, $this->upload->display_errors());
                return false;
            }
        }
	}
	
	public function view_datewise_promo()
	{
		$data['promo_list'] =$this->Promocodemodel->get_datewise_promo_list();
		$data['pc_catlist'] = $this->Ddlmodel->get_child_cat_list_promocode();
		$this->load->view('layouts/header');
		$this->load->view('pages/datewise_promo',$data);
		$this->load->view('layouts/footer');
	}
	
	public function add_datewise_promocode()
	{
		if($this->input->post())
		{
			if($this->validate_datewise_promocode_form())
			{
				$this->Promocodemodel->insert_datewise_promo();
				$message = 'Promocode is inserted sucessfully';
				$css_class = 'text-success';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('promocode/view_datewise_promo');
			}else{
				$message = 'Promocode insertion failed. Please upload proper image';
				$css_class = 'text-alert';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('promocode/ view_datewise_promo');
			}
		}else{
				$message = 'Promocode insertion failed. Please upload proper image';
				$css_class = 'text-alert';
				$this->session->set_flashdata(array (
						'css_class'=>$css_class,
						'message'=>$message,
				));		
				redirect('promocode/ view_datewise_promo');
			}	
	}
	//Pooja Jadhav
	public function validate_datewise_promocode_form()
	{
		$this->form_validation->set_error_delimiters('<div class="error_msg">', '</div>');
		$this->form_validation->set_rules('datewise_promo_img', 'Promocode Image', 'callback_validate_and_upload_datewise_promocode');
		return $this->form_validation->run();
	}
	//Pooja Jadhav
	public function validate_and_upload_datewise_promocode()
	{
		if (empty($_FILES['datewise_promo_img'])) 
		{
			$this->form_validation->set_message('datewise_promo_img', 'Please select promocode image');
            return FALSE;
        }
		else
		{
			$config['upload_path'] = SIZEIMAGEUPLOAD;
			$config['allowed_types'] = 'png|jpg|jpeg';
			//$config['max_size'] = '300';
			//$config['min_width'] = '700'; 
			//$config['min_height'] = '400';
		    //$config['max_width'] = 700;
		    //$config['max_height'] = 400;
			
			$config['max_size'] = '300';
			$config['min_width'] = '400'; 
			$config['min_height'] = '550';
			
			$config['encrypt_name'] = TRUE;
			$config['override'] = FALSE;
			
			$this->load->library('upload');
			$this->upload->initialize($config);
			if (!is_dir($config['upload_path'])) 
			{
				mkdir($config['upload_path'], 0777, TRUE);
				echo "<script>console.log('created.');</script>";
			}
			if($this->upload->do_upload('datewise_promo_img'))
            {
                $upload_data = $this->upload->data();
                $_POST['datewise_promo_img'] = $config['upload_path'].'/'.$upload_data['file_name'];
				echo "<script>console.log('Sucess.');</script>";
                return TRUE;
            }else
            {
                $this->form_validation->set_message($msg_name, $this->upload->display_errors());
                return false;
            }
        }
	}
	
	public function view_permant_promo()
	{
		$per_page = 15;
		$url = site_url().'/promocode/view_permant_promo';
		$number_of_rows = $this->Promocodemodel->get_permant_promocode_no_of_rows();
		$data['promo_list'] =$this->Promocodemodel->get_permant_promo_list($per_page);
		//$data['pc_catlist'] = $this->Ddlmodel->get_child_cat_list_promocode();
		$data['index'] = $this->uri->segment(3)+1;
		$this->load->library('get_pagination');
		$config = $this->get_pagination->generate_pagination($url, $number_of_rows, $per_page);
		$this->pagination->initialize($config);
		$this->load->view('layouts/header');
		$this->load->view('pages/permant_promo',$data);
		$this->load->view('layouts/footer');
	}
	
	public function add_permant_promocode()
	{
		if($this->input->post())
		{
			$this->Promocodemodel->insert_permant_promo();
			$message = 'Promocode is inserted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('promocode/view_permant_promo');
		}else{
			$message = 'Promocode insertion failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('promocode/ view_permant_promo');
		}
			
	}
	
	public function inactive_promocode()
	{
		if($this->input->post())
		{
			$this->Promocodemodel->inactivate_promocode();
			
			$message = 'Promocode is Deleted sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('promocode/view_userwise_promo');
		}else{
			$message = 'Promocode Deleted failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
				redirect('promocode/view_userwise_promo');
		}
	}
	
	public function active_promocode()
	{
		if($this->input->post())
		{
			$this->Promocodemodel->activate_promocode();
			
			$message = 'Promocode is Activated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('promocode/view_userwise_promo');
		}else{
			$message = 'Promocode  Activation failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('promocode/view_userwise_promo');
		}
	}

	public function inactive_promocode_date()
	{
		if($this->input->post())
		{
			$this->Promocodemodel->inactivate_promocode();
			
			$message = 'Promocode is Inactivated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('promocode/view_datewise_promo');
		}else{
			$message = 'Promocode Inactivation failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
				redirect('promocode/view_datewise_promo');
		}
	}
	
	public function active_promocode_date()
	{
		if($this->input->post())
		{
			$this->Promocodemodel->activate_promocode();
			
			$message = 'Promocode is Activated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('promocode/view_datewise_promo');
		}else{
			$message = 'Promocode  Activation failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('promocode/view_datewise_promo');
		}
	}

	public function inactive_promocode_permant()
	{
		if($this->input->post())
		{
			$this->Promocodemodel->inactivate_promocode();
			
			$message = 'Promocode is Inactivated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('promocode/view_permant_promo');
		}else{
			$message = 'Promocode Inactivation failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
				redirect('promocode/view_permant_promo');
		}
	}
	
	public function active_promocode_permant()
	{
		if($this->input->post())
		{
			$this->Promocodemodel->activate_promocode();
			
			$message = 'Promocode is Activated sucessfully';
			$css_class = 'text-success';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('promocode/view_permant_promo');
		}else{
			$message = 'Promocode  Activation failed';
			$css_class = 'text-alert';
			$this->session->set_flashdata(array (
					'css_class'=>$css_class,
					'message'=>$message,
			));		
			redirect('promocode/view_permant_promo');
		}
	}
	//Pooja Jadhav
	public function search_userwise_promocode_details()
	{
		$promocode=$this->input->post('promocode');
		if(isset($promocode) && !empty($promocode))
		{
			$data['promo_list']=$this->Promocodemodel->get_search_userwise_promocode_details($promocode);
			$this->load->view('layouts/header');
			$this->load->view('pages/userwise_promo',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('promocode/view_userwise_promo');
		}
	}
	//Pooja Jadhav
	public function search_permant_promocode_details()
	{
		$promocode=$this->input->post('promocode');
		if(isset($promocode) && !empty($promocode))
		{
			$data['promo_list']=$this->Promocodemodel->get_search_permant_promocode_details($promocode);
			$this->load->view('layouts/header');
			$this->load->view('pages/permant_promo',$data);
			$this->load->view('layouts/footer');
		}
		else
		{
			redirect('promocode/view_permant_promo');
		}
	}
}	