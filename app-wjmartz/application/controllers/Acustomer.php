<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acustomer extends CI_Controller 
{
	 public function __construct() 
	 {
        parent::__construct();
		$this->load->model('Acustomermodel');
		$this->load->model('Ddlmodel');
	 }	
	 
	 
	 public function customer_login_with_otp()
	{
		$mobile = $this->input->post('mobile_number');
		$otp = rand(100000, 999999);
		if($mobile!==NULL){
			$this->db->where('mobile',$mobile);
			$result=$this->db->get('customer')->row();
			
			if($result!=NULL){
				//100000;
				//$this->session->set_userdata(array('mobile'=>$mobile,'otp'=>$otp));
				//$this->load->library('sms_gateway');
				//$result1=$this->sms_gateway->genrate_otp($mobile,$otp);
					$data['status']=1;
					echo json_encode($data);
			/*	if($result!=NULL)
				{	
					$data['otp']=$otp;
					$data['status']=1;
					$data['customer_data']=$this->Acustomermodel->get_customer_data($mobile);
					$data['imageaccesspath']=IMAGEACCESSPATH;
					echo json_encode($data);
				}	*/
			}
			else{
				$this->load->library('sms_gateway');
				$this->sms_gateway->genrate_otp($mobile,$otp);
				$data['otp']=$otp;
				$data['status']=0;
				echo json_encode($data);
			}
		}
		else{
			$data['status']=1;
			echo json_encode($data);
		}
		
	}
	
	public function new_customer_registration()
	{
		if($this->input->post())
		{	
			if($this->Acustomermodel->insert_customer())
			{
				$data['customer_data']=$this->Acustomermodel->get_last_cust_id();
				$data['promo']=$this->Acustomermodel->get_promocode();
				$message=$this->load->view('email/welcome',$data,TRUE);
				$this->load->library('email_manager');
				$this->email_manager->send_welcome_email($data['customer_data']->email,$message);
				$this->load->library('sms_gateway');
				$this->sms_gateway->send_sms_for_welcome($data['customer_data']->mobile,$data['customer_data']->first_name);
				//$this->sms_gateway->send_sms_for_promocode($data['customer_data']->mobile,$data['customer_data']->first_name,$data['promo']->discount,$data['promo']->promocode);
				$data['imageaccesspath']=IMAGEACCESSPATH;
				echo json_encode($data);
				//echo 1;
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}	
		
	}
	
	public function customer_data_for_update_profile()
	{
		$cust_id=$this->uri->segment(3,0);
		$data['customer_data']=$this->Acustomermodel->get_cutomer_data_to_update($cust_id);
		$data['address1']=$this->Acustomermodel->get_customer_address1($cust_id);
		$data['address2']=$this->Acustomermodel->get_customer_address2($cust_id);
		$data['address3']=$this->Acustomermodel->get_customer_address3($cust_id);
		$data['state_list']=$this->Ddlmodel->state_list();
		$data['city_list']=$this->Ddlmodel->city_list();
		//$data['area_list']=$this->Ddlmodel->area_list();
		$data['pincode_list']=$this->Ddlmodel->pincode_list();
		$data['imageaccesspath']=IMAGEUSERPATH;
		echo json_encode($data);
	}
	
	public function update_customer_name()
	{
			if($this->Acustomermodel->update_cust_name()>0)
			{
				echo 1;
			}else{
				echo 0;
				
			}	
	}
	
	public function update_customer_email()
	{
			if($this->Acustomermodel->update_email()>0)
			{
				echo 1;
			}else{
				echo 0;
				
			}	
	}
	
	public function update_customer_birthdate()
	{
			if($this->Acustomermodel->update_birthdate()>0)
			{
				echo 1;
			}else{
				echo 0;
				
			}	
	}
	

	public function update_customer_photo()
	{
			if($this->Acustomermodel->update_profile_photo()>0)
			{
				echo 1;
			}else{
				echo 0;
				
			}	
	}
	
	public function update_customer_address1()
	{
			if($this->Acustomermodel->update_address_home()>0)
			{
				echo 1;
			}else{
				echo 0;
				
			}	
	}
	
	public function update_customer_address2()
	{
			if($this->Acustomermodel->update_address_work()>0)
			{
				echo 1;
			}else{
				echo 0;
				
			}	
	}
	
	public function update_customer_address3()
	{
			if($this->Acustomermodel->update_address_other()>0)
			{
				echo 1;
			}else{
				echo 0;
				
			}	
	}
	
	public function insert_address2()
	{
		if($this->input->post())
		{	
			if($this->Acustomermodel->insert_address_work())
			{
				echo 1;
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}	
	}
	
	public function insert_address3()
	{
		if($this->input->post())
		{	
			if($this->Acustomermodel->insert_address_other())
			{
				echo 1;
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}	
	}
	
	public function email_duplication()
	{
		$email = $this->input->post('email');
		if($email!==NULL)
		{
			$this->db->where('email',$email);
			$result=$this->db->get('customer')->row();
			
			if($result!=NULL)
			{
				echo 1;
			}else{
				echo 0;
			}
		}else{
			echo 0;
		}	
	}
	
	public function support_service_list()
	{
		$data['support_service_list']=$this->Acustomermodel->get_support_services_list();
		echo json_encode($data);
	}
	
	public function support_chat()
	{
		if($this->input->post())
		{
			if($this->Acustomermodel->insert_support_chat())
			{
				echo 1;
			}else{
				echo 0;
			}	
		}else{
			echo 0;
		}	
	}
	
	public function address_list()
	{
		$cust_id=$this->uri->segment(3,0);
		$data['customer_address']=$this->Acustomermodel->get_address_list($cust_id);
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	}
	
	public function pincode_list()
	{
		$data['pincode_list']=$this->Acustomermodel->get_pincode_list();
		echo json_encode($data);
	}
	
	public function customer_data_for_drawer()
	{
		$cust_id=$this->uri->segment(3,0);
		$data['customer_data']=$this->Acustomermodel->get_customer_data_for_drawer($cust_id);
        $data['imageaccesspath']=IMAGEUSERPATH;
		echo json_encode($data);
	}

    public function generate_otp_for_update_mobile()
    {
		$mobile = $this->input->post('mobile');
		$otp = rand(100000, 999999);
		if($mobile!==NULL){
			$this->db->where('mobile',$mobile);
			$result=$this->db->get('customer')->row();
			
			if($result==NULL){
				//100000;
				$this->session->set_userdata(array('mobile'=>$mobile,'otp'=>$otp));
				$this->load->library('sms_gateway');
				$result1=$this->sms_gateway->genrate_otp($mobile,$otp);
				
				if($result1!=NULL)
				{	
					$data['otp']=$otp;
					$data['status']=1;
					echo json_encode($data);
				}	
			}
			else{
				$data['otp']=$otp;
				$data['status']=0;
				echo json_encode($data);
			}
		}
		else{
			$data['status']=0;
			echo json_encode($data);
		}
           
    }
	
	public function update_mobile_with_otp()
	{
		$cust_id=$this->input->post('cust_id');
		$mobile = $this->input->post('mobile');
	
		//var_dump($user_otp);
		if($mobile!==NULL ){
			
			
				
				$update_data=array(
				'mobile'=>$mobile
				);
				$this->db->where('cust_id',$cust_id);
				$this->db->update('customer',$update_data);
				if($this->db->affected_rows() > 0 )
				{	
					$data['status']="1";
					echo json_encode($data);
				}
		
			else{
				$data['status']="0";
				echo json_encode($data);
			}
		}
		else{
			$data['status']="0";
			echo json_encode($data);
		}
		
	}
	
	public function address_listing()
	{
		$data['state_list']=$this->Ddlmodel->state_list();
		$data['city_list']=$this->Ddlmodel->city_list();
		$data['area_list']=$this->Ddlmodel->area_list();
		$data['pincode_list']=$this->Ddlmodel->pincode_list();
		echo json_encode($data);
	}

        public function change_password()
    {
        $cust_id=$this->input->post('cust_id');
        $old_password=$this->input->post('old_password');
        $new_password=md5($this->input->post('new_password'));
        $data=array('password'=>$new_password);
      // $this->db->where('password',md5($old_password));
        $this->db->where('cust_id',$cust_id);
        $this->db->update('customer',$data);
        if ($this->db->affected_rows() > 0)
        {
          echo 1;
        }else
         {
           echo 0;
         }
    }
    
    public function authentication()
    {
 
        if($this->input->post())
        {
            $user_data['user']=$this->Acustomermodel->authenticate_user();
            //var_dump($user_data);
            echo json_encode($user_data);
        }else{
            echo 0;
        }
        
    }
    
    public function email_exist()
    {
    	$data['email']=$this->Acustomermodel->check_email();
    	if($this->input->post())
    	{
    		if($data['email']==NULL)
    		{
    	  		echo 0;
    		}else{
    	  		echo 1;
    		}
    	}else{
    		echo 0;
    	}		
    }
    public function forgot_password_otp()
	{
		//$otp = 100000;
		$otp = rand(100000, 999999);
		$mobile = $this->input->post('mobile_number');
		if($mobile!==NULL){
			$this->db->where('mobile',$mobile);
			$result=$this->db->get('customer')->row();
			
			if($result!=NULL){
				$this->load->library('sms_gateway');
				$generate_otp=$this->sms_gateway->generate_otp_forgot_password($mobile,$otp);
				if($generate_otp!=NULL)
				{
				//$this->session->set_userdata(array('mobile'=>$mobile,'otp'=>$otp));
				$data['otp']=$otp;
				$data['status']=1;
				echo json_encode($data);
				}
			}
			else{
				//$this->load->library('sms_gateway');
				//$this->sms_gateway->genrate_otp($mobile,$otp);
				$data['otp']="";
				$data['status']=0;
				echo json_encode($data);
			}
		}
		else{
			$data['status']=0;
			echo json_encode($data);
		}
		
	}

	public function forgot_password()
	{
		
		if($this->Acustomermodel->update_password_for_forgot_password() > 0)
		{
			echo 1;
		}else{
			echo 0;
		}
		
		
	}
        
}	 