<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('login');
	}
	
	
	public function new_arrival_products()
	{
		$data['new_arrival']=$this->Aproductmodel->get_new_arrival_products();
		echo json_encode($data);
	}
	
	public function get_new_arrival_products()
	{
		$this->db->select('prod_id,prod_name,prod_img_url,prod_price,prod_description,quantity');
		$this->db->where_not_in('prod_id',0);
		$this->db->order_by('prod_id', 'DESC');
		$this->db->limit('10');
		return $this->db->get('products')->result();
	}
	
	public function recommended_products()
	{
		$child_cat_id=$this->uri->segment(3,0);
		$data['recommended_products']=$this->Aproductmodel->get_recommended_products($child_cat_id);
		echo json_encode($data);
	}
	
	public function get_recommended_products($child_cat_id)
	{
		$this->db->select('prod_id,prod_name,prod_img_url,prod_price,prod_description,quantity');
		$this->db->where_not_in('prod_id',0);
		$this->db->where('fk_pchild_id', $child_cat_id);
		$this->db->order_by('prod_id', 'RANDOM');
		$this->db->limit('3');
		return $this->db->get('products')->result();
	}
}
