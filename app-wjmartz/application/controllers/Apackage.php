<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apackage extends CI_Controller 
{ 
	 public function predefined_package_list()
	 {
		$this->load->model('Apackagemodel');
		$data['package_list']=$this->Apackagemodel->get_all_predefined_packages();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	 }
	 
	 public function predefined_package_details()
	 {
		$this->load->model('Apackagemodel');
		$package_id=$this->uri->segment(3,0);
		$data['package_details']=$this->Apackagemodel->get_details_for_package($package_id);
		$data['package_list']=$this->Apackagemodel->get_predefined_package($package_id);
		$data['product_type']=2;
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	 }
	 
	 public function customised_package_categories()
	 {
		$this->load->model('Apackagemodel');
		$data['package_category']=$this->Apackagemodel->get_all_customised_package_categories();
		$data['imageaccesspath']=IMAGEACCESSPATH;
		echo json_encode($data);
	 }
	 
	 public function customised_package_products()
	 {
		$this->load->model('Apackagemodel');
		$per_package_id=$this->uri->segment(3,0);
		$count=$this->Apackagemodel->get_no_of_rows($per_package_id);
		$per_page=6;
		
		if($count>0)
		{
			$data['products']=$this->Apackagemodel->get_customised_package_products($per_page,$per_package_id);
			$data['count']=$this->Apackagemodel->get_no_of_rows($per_package_id);
			$data['success']=1;
			$data['product_type']=4;
			$data['imageaccesspath']=IMAGEACCESSPATH;
		}else{
			$data['success']=0;
			$data['products']='NO records found...';
		}	
		echo json_encode($data);
	 }
	 
	 public function package_addon_list()
	 {
		$this->load->model('Apackagemodel');
		$count=$this->Apackagemodel->get_no_of_rows_addons();
		$per_page=6;
		
		if($count>0)
		{
			$data['addons']=$this->Apackagemodel->get_all_package_addons($per_page);
			$data['count']=$this->Apackagemodel->get_no_of_rows_addons();
			$data['success']=1;
			$data['product_type']=4;
			$data['imageaccesspath']=IMAGEACCESSPATH;
		}else{
			$data['success']=0;
			$data['addons']='NO records found...';
		}	
		echo json_encode($data);
	 }

	
}	 
	 
	 