<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('layouts/header');
		$this->load->view('pages/home');
		$this->load->view('layouts/footer');
	}
	public function log_in()
	{
		$this->load->view('login');
	}
	public function under_construction($page)
	{
		$name['name'] = $page; 
		$this->load->view('layouts/header');
		$this->load->view('pages/blank', $name);
		$this->load->view('layouts/footer');
	}
	
	public function send_email_for_contact_us()
	{
		
		$send_welcome=$this->send_welcome_mail();
		$send_enquiry=$this->send_enquiry_mail();
		//var_dump($send_welcome);
		
		if($send_welcome!=NULL && $send_enquiry!=NULL) 
		{
			echo 1;
		}else{
			echo 0;
		}	
		
	}
	
	private function send_welcome_mail()
	{
		$this->load->library('email_manager');
		$this->email_manager->initialize_email();
		$name['name']=$this->input->post('name');
		
		$message= $this->load->view('email/contact_us',$name,TRUE);
		
		$this->email->from('support@wjmartz.com', 'Wjmartz');
		$this->email->to($this->input->post('email'));
		$this->email->subject('Wjmartz Welcome');
		$this->email->message($message);
		return $this->email->send();		
		
	}
	
	private function send_enquiry_mail()
	{
		$this->load->library('email_manager');
		$this->email_manager->initialize_email();
		$remark=$this->input->post('message');
		$name=$this->input->post('name');
		$subject=$this->input->post('subject');
		$email=$this->input->post('email');
		$contact=$this->input->post('mobile');
		$message= "<html> 
						Name: ".$name." <br><br>
						Subject: ".$subject." <br><br>
						Email: ".$email." <br><br>
						Contact No: ".$contact." <br><br>
						Message: <br><br>".$remark." 
						</html>";
		$this->email->from('support@wjmartz.com', 'Wjmartz');
		$this->email->to('support@wjmartz.com');
		$this->email->subject($this->input->post('subject'));
		$this->email->message($message);
		return $this->email->send();		
	}
	
}
