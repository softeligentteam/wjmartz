<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>WJMartz | User_Dashboard</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="<?=base_url();?>assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="<?=base_url();?>assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="<?=base_url();?>assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TITLE ICON-->
	<link rel="shortcut icon" href="<?=base_url();?>assets/img/favicon.png" />
</head>
<body>
    <div id="wrapper">
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-square-o "></i>&nbsp;Welcome Admin</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right"> 
                        <li><a href="<?=site_url();?>/home/log_in">Logout</a></li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li class="text-center user-image-back">
                        <img src="<?=base_url();?>assets/img/logo.png" class="" />
                    </li>
                    <li>
                        <a href="<?=site_url();?>home/index"><i class="fa fa-desktop "></i>Dashboard</a>
                    </li>
					<li>
						<a href="<?=site_url();?>main_categories/view_all/"><i class="fa fa-table "></i>Main Categories</a>
					</li>
					<li>
						<a href="<?=site_url();?>parent_categories/view_all/"><i class="fa fa-table "></i>Parent Categories</a>
					</li>
					<li>
						<a href="<?=site_url();?>child_categories/view_all/"><i class="fa fa-table "></i>Child Categories</a>
					</li>
					<li>
						<a href="<?=site_url();?>products/view_all/"><i class="fa fa-table "></i>Products</a>
					</li>
                </ul>
            </div> 
        </nav>
        <!-- /. NAV SIDE  -->
		