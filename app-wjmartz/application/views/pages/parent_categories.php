<div id="page-wrapper">
	<div id="page-inner">
		<div class="row">
			<div class="col-md-12">
				<h2>Parent Categories</h2>
			</div>
		</div>
		<!-- /. ROW  -->
		<hr />
		<div class="row">
			<div class="col-md-12"> 
				<div class="col-md-6"> 
					<h5 class="filtered"><i class="fa fa-filter" aria-hidden="true"></i> All sub-categories </h5>
				</div>
				<form>
					<div class="col-md-4">  
						<div class="form-group"> 
							<select name="main_cat" tabindex="5" id="main_cat" required="" class="form-control">
								<option value="" selected="selected">Select Main Category...</option>
								<option value="">Demo</option>
								<option value="">Demo</option>
								<option value="">Demo</option>
								<option value="">Demo</option>
							</select> 		
						</div>  
					</div>
					<div class="col-md-2"> 
						<a onclick="" class="btn btn-info" style="width:100%">Filter</a>
					</div>
				</form>
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Parent - Category Name</th>
							<th>Parent - Category image </th>
							<th colspan="2"><center>Action</center></th>
						</tr>
					</thead>
					<tbody>
						
						<tr>
							<?php  if(count($parent_cat_list)>0){
						$i=1;
                           foreach ($parent_cat_list as $parent_cat)  
                          {  
                        ?>	
						<tr>
							<td><?=$i?></td>
							<td><?=$parent_cat->parent_cat_name?></td>
							<td><img src=<?=base_url().$parent_cat->parent_cat_img_url?> style="width:50px;"></img></td>
							<td class="text-center"><a onclick="" id="myBtn" class="btn btn-primary">Edit</a></td>
							<td class="text-center"><a onclick="" class="btn btn-danger">Delete</a></td>
						</tr>
					   <?php $i++;
						 
						}  } ?>	
					</tbody>
				</table> 
				<div class="col-sm-12" style="padding-left:0;">
					<a onclick="$('#addNewSubCat').slideDown();" class="btn btn-info" style="margin:20px 0;">Add New Parent Category</a>
				</div>
			</div>
		</div>
		<!-- /. ROW  -->
		
		
		
		<div  id="addNewSubCat" style="display:none;">
			<div class="row">
			<?=form_open_multipart('parent_categories/add_parent_category');?>
				<div class="col-md-12">
					<h2>Add New Parent-Categories</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-6">
					<h5>Upload Parent-Category Image :</h5>
					
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							<img id="productImagePreview" src="" style="width:100%;" alt="" />
						</div>
						<div class="panel-footer back-footer-blue">
						<input type='file' name="parent_cat_img" id="productImg" onchange="previewImage(productImg,'productImagePreview');" class="" style="width:100%" /> 
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<h5>Parent Categoriy Details :</h5> 
					 
					<div class="col-md-12">
						<div class="form-group">
							<label>Enter New Parent Category Name</label>
							<input class="form-control" name="parent_category">
							<p class="help-block" id="enterSubCatNameError" style="color:red; height:20px;"></p>
						</div> 
					</div>
					<div class="col-md-12">
						<input type="submit" value="Save" class="btn btn-success" />
						<a onclick="$('#addNewSubCat').slideUp();" class="btn btn-warning">Cancle</a>
					</div>
				</div>

			</div>
			<hr />
			<?=form_close();?>
			<!-- /. ROW  --> 
		</div>
		
		<div  id="viewSubCategory" style="display:none;">
			<div class="row">
				<div class="col-md-12">
					<h2>Sub-Category Name</h2>
				</div>
			</div>
			<hr />
			<div class="row">
				<?php for($i=1; $i<13; $i++) {?>
				<div class="col-md-3">
					<div class="panel panel-primary text-center no-boder bg-color-blue">
						<div class="panel-body" id="previewImg" style="min-height:200px; text-align:center;">
							
						</div>
						<div class="panel-footer back-footer-blue">
							<div class="col-md-6">
							Product <?=$i?> Name 
							</div>
							<div class="col-md-6">
								Prize : 123 Rs.
							</div><div class="clearfix"></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="col-sm-12">
					<ul class="pagination">
						<li><a href="#">&lt;</a></li>
						<li><a href="#">1</a></li>
						<li class="active"><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">&gt;</a></li>
					</ul> 
				</div>
			</div>
			<hr />
		</div>
	</div>
	<!-- /. PAGE INNER  -->
</div>
<?php if(NULL !== $this->session->flashdata('message')) { ?>
<div class="insert_success">
	<div class="row">
		<div class="col-md-offset-4 col-md-4 col-md-offset-4 pane">
			<div class="row">
				<div class="col-sm-12">
					<br />
					<h4 class="<?php echo $this->session->flashdata('css_class')?>">					
						<center><?php echo $this->session->flashdata('message')?></center>
					</h4>
					<br />
				</div>
				<div class="col-sm-12 text-center">
					<a onclick="insert_success_close();" id="myBtn" class="btn btn-primary">ok</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script src="<?=base_url();?>assets/js/add/parent_category.js"></script>
	
	